'use strict';

var ElementsUtil = require("../util/element.util");
/*
	This PF will Do Enrolment of user for Auto Pay
	*/

var AutoReUpPf = function() {
	
	//*[@id="lnk_refill_autoreup"]
	//this.autoReUp = element(by.xpath("//a[@id='lnk_refill_autoreup']"));
	this.autoReUpMenu = element(by.id("REFILL"));
	this.autoReUp = element(by.id("nav-Auto-Reup"));
	this.minTextBox=element.all(by.id('refill-acct')).get(1);
	this.continueBtnForAutoPay = element(by.buttonText('CONTINUE'));
	//this.passwordForAutoPay = element.all(by.xpath("//*[@id='wfmloginpinpwd-device']")).get(1);
	this.passwordForAutoPay = element.all(by.id('loginpinpwd-device')).get(1);
	this.continueBtnForPassword = element(by.id('btn_login'));
	this.doneBtn = element(by.id('done'));
	
	/*
	*This function will click on Auto Pay link in home page
	*/
	this.goToPayServiceAndAutoPay = function(){
		ElementsUtil.hoverAndSelect(this.autoReUpMenu, this.autoReUp);
	};
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isPaymentDone = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/confirmation_activation$/);
	};
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isServeyPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/redirectsurvey?/);
	};
	
	/*
	* This function will click on  "Done" button after successful payment
	*/
	this.clickOnDoneBtn = function(){
		ElementsUtil.waitForElement(this.doneBtn);
		this.doneBtn.click();
	};
	
	this.isPayServiceAndAutoPayLoaded = function(){
		ElementsUtil.waitForElement(this.minTextBox);
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /autorefill/.test(url);
		});
	};
	
	/*
	*Parameters : mailId
	*This function will send input to mailId text field and clicks on continue button
	*/
	this.enterPhoneNumberOrMailId=function(phoneNumber){
		console.log("phoneNumber :: "+phoneNumber);
		this.minTextBox.sendKeys(phoneNumber);	
		this.continueBtnForAutoPay.click();
	};
	
	/*
	* This function will return true on success redirection of page to specified url
	*/	
	this.isPhoneNumberOrMailIdLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/autorefill$/);
	};
	
	/*
	*Parameters : password
	*This function will send input to password text field and clicks on continue button
	*/
	this.enterPasswordForAutoPay=function(password){
		this.passwordForAutoPay.sendKeys(password);	
		this.continueBtnForPassword.click();		
	};
};

module.exports = new AutoReUpPf;