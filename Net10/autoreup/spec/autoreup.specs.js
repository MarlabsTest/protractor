'use strict';

var autoReUp = require("../autoreup.po");
var sessionData = require("../../common/sessiondata.do");
var activation = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var dataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil = require("../../util/common.functions.util");
var home = require("../../common/homepage.po");

describe('Enrolling for Auto ReUp service : ', function() {

	/*
	*This scenario will click on Auto Pay link in home page and checks whether
	*page is redirected to expected url
	*/

	it('should click on auto reup',function(done){		
		autoReUp.goToPayServiceAndAutoPay();
		expect(autoReUp.isPayServiceAndAutoPayLoaded()).toBe(true);
		done();
	});
	
	/*
	This scenario will send  mailId  or phone number and click on continue button
	checks whether page is redirected to expected url
	*/
	it('should enter mailId  or phone number and click on continue button',function(done){
		console.log("min of esn",dataUtil.getMinOfESN(sessionData.netten.esn));
		autoReUp.enterPhoneNumberOrMailId(dataUtil.getMinOfESN(sessionData.netten.esn));
		//expect(autoReUp.isPhoneNumberOrMailIdLoaded()).toBe(true);
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(myAccount.isAutoRefillCheckoutPageLoaded()).toBe(true);
		done();
	});
	
	/**
	 * Refresh checkout page and switch back to autorefill
	 * /
	it('should switch autorefill status to yes', function(done) {
		browser.driver.navigate().refresh();
		myAccount.switchCheckOutWithAutoRefill();
		
		expect(myAccount.isAutoRefillStatusYes()).toBe(true);
		done();		
	});
	*/
	
	/*
	 * Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 *
	 */
	it('should load the payment form', function(done) {
		browser.driver.navigate().refresh();
		//myAccount.enterCcDetails(""+generator.GenCC(sessionData.netten.cardType),CommonUtil.getCvv(sessionData.netten.cardType));
		//myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.enterAutoReupCcDetails(""+generator.GenCC(sessionData.netten.cardType),CommonUtil.getCvv(sessionData.netten.cardType));
		myAccount.enterAutoReupBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		//expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(myAccount.isAutoRefillCheckoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	 */	
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the home page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(home.isHomePageLoaded()).toBe(true);
		done();		
	});
});
