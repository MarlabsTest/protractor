'use strict';

/*
This PO will Do Enrolment of user for Auto Pay
*/

var autoReUp = require("../autoreup/autoreup.pf");
var AutoReUpPO = function() {
   	
	/*
	 This function will click on Auto Pay link in home page
	*/
	this.goToPayServiceAndAutoPay = function() {
		return autoReUp.goToPayServiceAndAutoPay();
	};
	
	/*
	*This function will click on "New Payment" tab in checkout page
	*/
	this.clickOnDoneBtn=function(){
		return autoReUp.clickOnDoneBtn();
	}
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isServeyPageLoaded = function() {
		return autoReUp.isServeyPageLoaded();
	};
	 	 
	/*
	This function is to check whether autoreup login page loaded
	*/
	this.isPayServiceAndAutoPayLoaded = function() {
		return autoReUp.isPayServiceAndAutoPayLoaded();
	};	
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isPaymentDone = function() {
		return autoReUp.isPaymentDone();
	};
	  
	/*
	 *Parameters : MailId
	 *This function will send input to mailId text field and clicks on continue button
	*/
	this.enterPhoneNumberOrMailId = function(mailId) {
		return autoReUp.enterPhoneNumberOrMailId(mailId);
	};
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isPhoneNumberOrMailIdLoaded = function() {
		return autoReUp.isPhoneNumberOrMailIdLoaded();
	};
	
	/*
	*Parameters : password
	*This function will send input to password text field and clicks on continue button
	*/
	this.enterPasswordForAutoPay = function(password) {
		return autoReUp.enterPasswordForAutoPay(password);
	};
};

module.exports = new AutoReUpPO;