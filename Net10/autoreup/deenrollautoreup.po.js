'use strict';

	/*
		This PF will De Enroll user from Auto Pay service	
	*/
	
var deEnrollAutoPay = require("../autoreup/deenrollautoreup.pf");
var DeEnrollAutoPayPO = function() {

	/*
		This function will click on "Cancel Auto Pay Enrollment" link .
	*/
	this.clickOnAutoReupBtn = function(){
		deEnrollAutoPay.clickOnAutoReupBtn();	
	};
	
	
	/*
		This function will select reason for De Enrollment of Auto pay from drop down 
	*/	
	this.selectReasonForCancelAutorefil = function(){
		 deEnrollAutoPay.selectReasonForCancelAutorefil();
	 };
	 
	 /*
		This function will check whether the manage enroll page is loaded or not
	*/
	 this.isManageEnrollPageLoaded = function(){
		 return deEnrollAutoPay.isManageEnrollPageLoaded();
	 };
	 
	 /*
		To cancel the autoreup service
	*/
	 this.cancelAutoReUp = function(){
		 deEnrollAutoPay.cancelAutoReUp();
	 };
	 
	 /*
		Check whether the cancel reup popup shown
	*/
	 this.isCancelAutoReUpPopupShown = function(){
		 return deEnrollAutoPay.isCancelAutoReUpPopupShown();
	 };
};

module.exports = new DeEnrollAutoPayPO;