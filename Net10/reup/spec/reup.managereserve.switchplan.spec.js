'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");

var reUpPage = require("../reup.po");

describe('Net10 switch Plan', function() {
	
	it('navigates to ReUp page', function(done) {
		browser.driver.navigate().refresh();
		console.log("should navigate to Refill page");
		reUpPage.goToRefillWithPin();		
		expect(reUpPage.isRefillPagePageLoaded()).toBe(true);		
		done();
	});
	
	
	it('pin', function(done) {
		reUpPage.enterPin(dataUtil.getPIN(sessionData.netten.pinPartNumber));
		expect(reUpPage.goToConfirmOrderPage()).toBe(true);
		done();
	});
	
	it('click on Manage Reserve', function(done) {
		reUpPage.clickOnManageReserve();
		expect(reUpPage.isManageReserveLoaded()).toBe(true);
		done();
	});
	
	it('click on Apply Now', function(done) {
		reUpPage.clickOnApplyNow();
		expect(reUpPage.isPopUpLoaded()).toBe(true);
		done();
	});
	
	it('click on Add Now', function(done) {
		reUpPage.clickOnAddnow();
		expect(reUpPage.isManageReserveLoaded()).toBe(true);
		done();
	});
	
	it('Confirmation', function(done) {
		reUpPage.clickOnDone();
		expect(reUpPage.goToHomePage()).toBe(true);
		done();
	});

});
	