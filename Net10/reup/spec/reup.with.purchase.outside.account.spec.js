'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var shop = require("../../shop/shop.po");
var myAccount = require("../../myaccount/myaccount.po");
var reUpPage = require("../reup.po");
var generator = require('creditcard-generator');
var CommonUtil = require('../../util/common.functions.util');
var CCUtil = require("../../util/creditCard.utils");

describe('Net10 ReUp with purchase outside account', function() {
	
	it('navigates to Refill option', function(done) {
		reUpPage.goToreUpLink();		
		expect(reUpPage.isReUpPagePageLoaded()).toBe(true);		
		done();
	});	
	it('Provide MIN and buy Airtime', function(done) {
		//reUpPage.addAirTime(dataUtil.getMinOfESN(sessionData.netten.esn),dataUtil.getPIN(sessionData.netten.pinPartNumber));
		reUpPage.buyAirTime(dataUtil.getMinOfESN(sessionData.netten.esn));
		expect(shop.isShopServicePlansListed()).toBe(true);
		done();
	});	
	it('choose plan', function(done) {
		var planType		= sessionData.netten.shopPlan;
		var planName		= sessionData.netten.planName;
		var isAutoRefill	= sessionData.netten.autoRefill;
		var phoneType		= sessionData.netten.phoneType
		shop.chooseReupPlanByName(planType,planName,isAutoRefill,phoneType);
		console.log('Back to Spec');
		expect(shop.autorefillPopUpcheck()).toBe(true);
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	it('select plan enrollment', function(done){
		shop.ClickOnEnrollILDPlan();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
	it('Enter payment details for checkout', function(done) {
		//myAccount.enterCcDetails(""+generator.GenCC(sessionData.netten.cardType),CommonUtil.getCvv(sessionData.netten.cardType));
		myAccount.enterReupCcDetails(""+generator.GenCC(sessionData.netten.cardType),CommonUtil.getCvv(sessionData.netten.cardType));
		//myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.enterReupBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.pin,CCUtil.city);
		//myAccount.enterAutoReupBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.pin,CCUtil.city);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();
	});
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the home page', function(done) {
		activation.clickOnSummaryBtn();
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();		
	});	

});
	