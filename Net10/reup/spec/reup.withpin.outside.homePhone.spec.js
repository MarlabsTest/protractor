'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");

var reUpPage = require("../reup.po");

describe('Net10 ReUp with PIN for HOME Phone', function() {
	
	it('navigates to ReUp page', function(done) {
		console.log("should navigate to My Refill page");
		reUpPage.goToreUpLink();		
		expect(reUpPage.isReUpPagePageLoaded()).toBe(true);		
		done();
	});
	
	
	it('enter min and pin', function(done) {
		reUpPage.addAirTime(dataUtil.getMinOfESN(sessionData.netten.esn),dataUtil.getPIN(sessionData.netten.redemptionPin));
		reUpPage.clickOnContinue();
		expect(reUpPage.goToConfirmOrderPage()).toBe(true);
		done();
	});
	
	it('Confirmation', function(done) {
		reUpPage.clickOnDone();
		expect(reUpPage.goToHomePage()).toBe(true);
		done();
	});
	
	

});
	