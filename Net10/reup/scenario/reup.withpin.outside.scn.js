'use strict';

var homePage = require("../../common/homepage.po");
var drive = require('jasmine-data-provider');
var sessionData = require("../../common/sessiondata.do");
var FlowUtil = require('../../util/flow.util');
var refillUtil = require('../../util/refill.util');

describe('NT ReUp Service with PIN', function() {

	var activationData = refillUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		sessionData.netten.type = inputActivationData.Type;
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.simPartNumber = inputActivationData.SIM;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.pinPartNumber = inputActivationData.PIN;
				sessionData.netten.redemptionPin = inputActivationData.redemptionPin;
				done();
			});
			FlowUtil.run('NT_REFILL_WITHPIN_ALL');	
		}).result.data = inputActivationData;
	});
});