'use strict';

var homePage = require("../../common/homepage.po");
var drive = require('jasmine-data-provider');
var sessionData = require("../../common/sessiondata.do");
var FlowUtil = require('../../util/flow.util');
var refillUtil = require('../../util/refill.util');

describe('NT Reup service Plan outside account with purchase', function() {

	var activationData = refillUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {		
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.simPartNumber = inputActivationData.SIM;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.pinPartNumber = inputActivationData.PIN;
				sessionData.netten.phoneType = inputActivationData.Type;
				sessionData.netten.shopPlan = inputActivationData.shopplan;
				sessionData.netten.planName = inputActivationData.PlanName;
				sessionData.netten.autoRefill = inputActivationData.AutoRefill;
				done();
			});
			FlowUtil.run('NT_REFILL_WITH_PURCH_OUTSIDE_ACC');	
		}).result.data = inputActivationData;
	});
});