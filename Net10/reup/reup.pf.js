'use strict';
var ElementsUtil = require("../util/element.util");
var sessionData = require("../common/sessiondata.do");
var dataUtil= require("../util/datautils.util");
var ConstantsUtil = require("../util/constants.util");


var ReUpPf = function() {
	
	this.reUpLink = element.all(by.id('lnk_REFILL'));
	this.phoneNumber= element.all(by.name('deviceinfo')).get(3);
	this.minTextFieldForPurchase = element.all(by.css('input[id="formreup"]')).get(1);
	this.minTextField = element(by.xpath('/html/body/tf-update-lang/div[2]/div[2]/div/div/div[2]/div/div/tf-page-body/div/div[1]/div[2]/div[2]/ng-include/tf-split-vertical-or/div/div[3]/div/tf-split-vertical-or-right/form/div[1]/div[2]/div[1]/div/tf-phone/div/div/input'));
	this.pinTextField = element(by.css('input[id="form_redeem.pin"]'));
	this.addAirTimeButton = element(by.css('button[id="AddAirtime"]'));
	this.buyAirTimeButton = element(by.css('button[id="RenewService"]'));
	this.doneConfirmButton = element(by.id('done_confirmation'));
	this.buyPlanButton = element(by.css('button[id="RenewService"]'));
	this.continueBttn = element.all(by.css("span[translate='BAT_24130']")).get(0);
	this.refillWithPin = element.all(by.css('[ng-click="action()"]')).get(1);
	this.addRefillAirTimeButton = element(by.id('AddAirtime_btn'));
	this.pinBox = element.all(by.id('formPlan.pin')).get(1);
	
	/*This  method will navigates to reup page*/
	this.goToreUpLink = function() {
		return this.reUpLink.click();
	};
	
	//*** method to check whether the reup page loaded or not
	this.isReUpPagePageLoaded = function() {
	ElementsUtil.waitForElement(this.phoneNumber,2000);
		return this.phoneNumber.isPresent();//ElementsUtil.waitForUrlToChangeTo(/collectminpinpromo/);
	};
	
	/*This method will enter min and pin and clicks on add air time button*/
	this.addAirTime = function(min,pin) {
		//ElementsUtil.waitForElement(this.phoneNumber,2000);
		this.phoneNumber.sendKeys(min);
		this.pinTextField.sendKeys(pin);
		this.addAirTimeButton.click();
	};
	// Enter Min and click on Buy Plan for purchase
	this.buyAirTime = function(min) {
		this.minTextFieldForPurchase.sendKeys(min);
		this.buyAirTimeButton.click();
	};
	
	this.clickOnContinue=function(){
		ElementsUtil.waitForElement(this.continueBttn);
		this.continueBttn.click();
	}
	
	 /*This method will check whether the confirm page loaded or not*/
	this.goToConfirmOrderPage = function() {
		return ElementsUtil.waitForUrlToChangeTo(/redemptionconfirmationSuccess$/);
	};
	
	/*This method will click on done button*/
	this.clickOnDone = function() {
		ElementsUtil.waitForElement(this.doneConfirmButton);
		return this.doneConfirmButton.click();
	};
	
	 /*This method will check whether the home page loaded or not*/
	this.goToHomePage = function() {
		return ElementsUtil.waitForUrlToChangeTo(/siteng.net10.com/);
	};
	
	this.enterMin = function(min) {
		//ElementsUtil.waitForElement(this.minTextFieldForPurchase,2000);
		this.minTextFieldForPurchase.sendKeys(dataUtil.getMinOfESN(sessionData.simplemobile.esn));
		return this.buyPlanButton.click();
	};
	this.goToRefillWithPin = function(){
		ElementsUtil.waitForElement(this.refillWithPin);
		this.refillWithPin.click();
	}
	this.isRefillPagePageLoaded = function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /dashboard/.test(url);
		});
	};
	
	this.enterPin = function(pin) {
		this.pinBox.sendKeys(pin);
		this.addRefillAirTimeButton.click();
	};
	
	};

module.exports = new ReUpPf;