'use strict';
var ElementUtil = require("../util/element.util");

var Menu = function() {

	this.shopLink = element(by.id('SHOP'));
	
	this.selectPlanOpt = element.all(by.id('lnk_shop_airtime_serviceplans')).get(0);
	this.activateLink = element.all(by.id('lnk_tfhome_activate')).get(0);
	this.refilllink = element(by.id('REFILL'));
	this.homeLink = element.all(by.id('lnk_home')).get(0);
	this.myAccount = element.all(by.id('lnk_tfhome_myaccount')).get(2);//changed 
	
	this.isHomePageLoaded = function() {
		//$$//ElementUtil.waitForElement(this.shopLink);
		return this.shopLink.isDisplayed();
	};
	
	this.goToShop = function() {
		ElementUtil.waitForElement(this.shopLink);
		return this.shopLink.click();
	};
	this.goToShopAndSelectPlan = function() {
		ElementUtil.waitForElement(this.shopLink);
		ElementUtil.hoverAndSelect(this.shopLink, this.selectPlanOpt);
	};

	this.goToActivate = function() {
		//$$//ElementUtil.waitForElement(this.activateLink);
		this.activateLink.click();
	};
	
	this.goToRefill = function() {	
		ElementUtil.waitForElement(this.refilllink); 
		return this.refilllink.click();
	};
	
	//reactivation scenario -aswathy
	this.goToMyAccount = function() {
		ElementUtil.waitForElement(this.myAccount);
		//browser.wait(expectedConditions.visibilityOf(this.myAccount),40000);
		this.myAccount.click();
	};
	this.goToHomePage = function() {
		//ElementUtil.waitForElement(this.homeLink);
		//browser.wait(expectedConditions.visibilityOf(this.myAccount),40000);
		this.homeLink.click();
	};
	//reactivation ends
};

module.exports = new Menu;
