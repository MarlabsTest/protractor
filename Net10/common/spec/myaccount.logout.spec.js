'use strict';

var myAccount = require("../../myaccount/myaccount.po");
var login = require("../../login/login.po");

//Load HomePage Scenario
describe('Net10 SignOut', function() {

	beforeEach(function () {

	});
	
	it('Wait for LDAP Updatation',function(done){
		console.log("LDAP WAITING...............");
		browser.sleep(40000);
		console.log("DONE!!");
		done();
	});
	
	it('should logout Net10 my account page', function(done) {
		console.log("should logout Net10 my account page");
		myAccount.signOut();	
		expect(login.isSignOut()).toBe(true);
		done();
	});
	
});
	