'use strict';

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('../Net10/properties/Protractor.properties');
var homePage = require("../../common/homepage.po");
var Constants = require("../../util/constants.util");

/*
 * This Spec is to Load a Net10 Home page.
 */

describe('Net10 HomePage', function() {

	it('should load the Net10 home page', function(done) {
		//homePage.load(Constants.ENV[browser.params.environment]);
		homePage.load(properties.get('weburl.'+browser.params.environment));
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();
	});

});
