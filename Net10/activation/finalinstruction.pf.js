//this represents a modal for final instruction page

'use strict';

var ElementsUtil = require("../util/element.util");

var FinalInstruction = function() {

	this.finalInstructionContinueBtn =  element(by.css("button[id='btn_activatepin']"));
	//element.all(by.css('[ng-click="action()"]')).get(1); btn_continuesetupphone
	this.finalInstructionContinueBtnPurchase =  element(by.css("button[id='btn_activatenopin']"));
	//this.actualFICPurchaseBtn = this.finalInstructionContinueBtnPurchase.get(3);
	this.finalInstructionContinueBtnPortInPurchase =  element(by.css("button[id='btn_summaryviewpurchasecontinue']"));
	this.finalInstructionContinueBtnPortInPin =  element(by.css("button[id='btn_summaryviewpincontinue']"));
	
	
	//** this method is to check whether the final instruction page is loaded or not
	this.finalInstructionPageLoaded = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtn),40000);
		ElementsUtil.waitForElement(this.finalInstructionContinueBtn);
		return this.finalInstructionContinueBtn.isPresent();
	};
	
	//** this method is to check whether the final instruction page is loaded or not
	this.finalInstructionPageLoadedPortInPurchase = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtn),40000);
//		ElementsUtil.waitForElement(this.finalInstructionContinueBtnPortInPurchase);
		return this.finalInstructionContinueBtnPortInPurchase.isPresent();
	};
	
	this.finalInstructionPageLoadedPortInPin = function(){
		ElementsUtil.waitForElement(this.finalInstructionContinueBtnPortInPin);
		return this.finalInstructionContinueBtnPortInPin.isPresent();
	};
  
	//** this method is to proceed from the final instruction page
	this.finalInstructionProceed = function(){
		this.finalInstructionContinueBtn.click();
	};
	
	//** this method is to proceed from the final instruction page
	this.finalInstructionProceedPortInPurchase = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtn),40000);
		//$$//ElementsUtil.waitForElement(this.finalInstructionContinueBtnPortInPurchase);
		this.finalInstructionContinueBtnPortInPurchase.click();
	};
	
	this.finalInstructionProceedPortInPin = function(){
		ElementsUtil.waitForElement(this.finalInstructionContinueBtnPortInPin);
		this.finalInstructionContinueBtnPortInPin.click();
	};
	
	this.finalInstructionPageLoadedPurchase = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtnPurchase),40000);
		ElementsUtil.waitForElement(this.finalInstructionContinueBtnPurchase);
		//browser.wait(waitutil.elementHasCome(this.finalInstructionContinueBtnPurchase, 3), 5000);
		return this.finalInstructionContinueBtnPurchase.isPresent();
	};
	
	this.finalInstructionProceedPurchase = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtnPurchase),40000);
		//$$//ElementsUtil.waitForElement(this.finalInstructionContinueBtnPurchase);
		//browser.wait(waitutil.elementHasCome(this.finalInstructionContinueBtnPurchase, 3), 5000);
		this.finalInstructionContinueBtnPurchase.click();
	};
	
};
module.exports = new FinalInstruction;
