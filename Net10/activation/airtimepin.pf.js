//this represents a modal for airtimepin page

'use strict';
var ElementsUtil = require("../util/element.util");

var AirtimePin = function() {

	this.airtimePinBox = element.all(by.id('pinmin')).get(1);	
	this.airtimePinContinueBtn =  element(by.id('airtimePinActivation_btn'));	
	
	
	//** method to enter airtime pin and proceed from the page
	this.enterAirTimePin = function(pin){
		//$$//ElementsUtil.waitForElement(this.airtimePinBox);
		this.airtimePinBox.clear().sendKeys(pin);
		//ElementsUtil.waitForElement(this.airtimePinContinueBtn);
		this.airtimePinContinueBtn.click();
	};

	

};
module.exports = new AirtimePin;