'use strict';
var expectedConditions = protractor.ExpectedConditions;
//var Waitutil = require("../util/waitutil.pf");
var ElementUtil = require("../util/element.util");

var purchasePlan = function() {
	
	//var waitutil = new Waitutil();
	this.airtimePurchaseContinueBtn =  element(by.id('airtimePurchaseActivation_btn'));
	this.buyServicePlanBtn			= element.all(by.id('btn_paygplancardaddtocart')).get(1);
	this.enrollInAutoRefilBtn		= element.all(by.id('btn_enrollinautorefill')).get(2);
	this.onetimepurchaseBtn			= element.all(by.id('btn_onetimepurchase')).get(1);
	this.continuecheckoutBtn		= element.all(by.id('btn_continuecheckout')).get(1);
	//this.payAsYouGoOption			= element.all(by.css('ng-click='iconClick()')).get(1);
	this.payAsYouGoOption			 = element.all(By.css("[ng-click='iconClick()']")).get(2);
	
	this.servicePlanPageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/airtimeserviceplan$/);
	};
	//remya 
	this.baseServicePlanPageLoaded = function(){
		return browser.getCurrentUrl().then(function(url){
			//console.log('baseSevicePlanPageLoaded url:-',url);
			return /serviceplan$/.test(url);
		});
	};
	this.selectPayAsYouGoPlan = function(){
		ElementUtil.waitForElement(this.payAsYouGoOption);
		this.payAsYouGoOption.click();
	//	ElementUtil.waitForElement(this.buyServicePlanBtn);
		//.buyServicePlanBtn.click();
		/*
		ElementUtil.waitForElement(this.onetimepurchaseBtn);
		this.onetimepurchaseBtn.click();*/
	}	
	this.buyPlanBtnLoaded = function(){
		ElementUtil.waitForElement(this.buyServicePlanBtn);
		return this.buyServicePlanBtn.isPresent();
	}
	this.buyServicePlan = function(){
		ElementUtil.waitForElement(this.buyServicePlanBtn);
		this.buyServicePlanBtn.click();
	};
	this.isPurchasePopUpLoaded = function(){
		ElementUtil.waitForElement(this.onetimepurchaseBtn);
		return this.onetimepurchaseBtn.isPresent();
	};
	this.purchaseOneTimePlan = function(){
		ElementUtil.waitForElement(this.onetimepurchaseBtn);
		this.onetimepurchaseBtn.click();
	};
	this.isChkoutPopUpLoaded = function(){
		ElementUtil.waitForElement(this.continuecheckoutBtn);
		return this.continuecheckoutBtn.isPresent();
	};
	this.clickOnCheckout = function(){
		ElementUtil.waitForElement(this.continuecheckoutBtn);
		this.continuecheckoutBtn.click();
	};
	
	
	//remya
  
	this.airtimePurchase = function(){
		//$$//browser.wait(expectedConditions.visibilityOf(this.airtimePurchaseContinueBtn),10000);
		//browser.wait(waitutil.elementHasCome(this.airtimePurchaseContinueBtn), 5000);
		this.airtimePurchaseContinueBtn.click();
	};
};
module.exports = new purchasePlan;

