//this represnts the modal for zipcode(new number selection) flow

'use strict';
var ElementsUtil = require("../util/element.util");
var zipCode = function() {
		
	this.zipCodeTextBox = element.all(by.name('zip')).get(1);	
	this.zipCodeContinueBtn =  element.all(by.id('btn_continuezipcode')).get(0);
	this.zipCodeHotSpot =  element.all(by.id('collect_zip_code')).get(1);
	this.continueBtnHotSpot = element(by.id("btn_collect_zip"));
	
	

	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.keepMyPhonePageLoaded = function(){
		return browser.getCurrentUrl().then(function(url) {			
			return /keepcollectphone$/.test(url);
		});
	};
	
	this.isZipCodePageLoaded = function(){
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /collectdevicezip$/.test(url);
		});
	};
	
	//** method to enter zipcode
	this.enterZipCode = function(zipCode){		
		this.zipCodeTextBox.clear().sendKeys(zipCode);
		this.zipCodeContinueBtn.click();
	};
	//** method to enter zipcode
	this.enterZipCodeHotSpot = function(zipCode){		
		ElementsUtil.waitForElement(this.zipCodeHotSpot);
		this.zipCodeHotSpot.clear().sendKeys(zipCode);
		this.continueBtnHotSpot.click();
	};

	

};
module.exports = new zipCode;