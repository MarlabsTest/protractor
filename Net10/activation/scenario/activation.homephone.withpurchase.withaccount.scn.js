'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.homephone.util');
var sessionData = require("../../common/sessiondata.do");

describe('NT Home Phone Activation with Purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.cardType = inputActivationData.cardType;
				sessionData.netten.autoRefill = inputActivationData.AutoRefill;
				sessionData.netten.planName = inputActivationData.PlanName;
				sessionData.netten.shopPlan = inputActivationData.shopplan;
				done();
			});
			FlowUtil.run('NT_ACTIVATION_HOMEPHONE_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});