'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.homephone.util');
var sessionData = require("../../common/sessiondata.do");

describe('NT Home Phone Activation with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('NT_ACTIVATION_HOMEPHONE_WITHPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});