'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('NT External Port Activation with Purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.simPartNumber = inputActivationData.SIM;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.cardType = inputActivationData.cardType;
				sessionData.netten.autoRefill = inputActivationData.AutoRefill;
				sessionData.netten.planName = inputActivationData.PlanName;
				sessionData.netten.shopPlan = inputActivationData.shopplan;
				sessionData.netten.phoneType = inputActivationData.phoneType;
				done();
			});
			FlowUtil.run('NT_EXTERNAL_PORT_WITHPURCHASE');
		}).result.data = inputActivationData;
	});
});