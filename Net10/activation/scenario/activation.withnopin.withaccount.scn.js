'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.withnopin.util');
var sessionData = require("../../common/sessiondata.do");

describe('NT Activation GSM with No PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.simPartNumber = inputActivationData.SIM;
				sessionData.netten.zip = inputActivationData.ZipCode;				
				done();
			});
			FlowUtil.run('NT_ACTIVATION_WITHNOPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});