/*
 * **********Scenario is the consolidation of Upgrade  flows in Net10 ******** 
 * 
 * 1.Read the input data for upgrade from CSV
 * 2.Based on the partnumber decide which flow to proceed with.
 * 3.If the partnumber starts with 'PH' ,it will be a  BYOP upgrade.In that we are initially doing a BYOP activation process
 * 4.The third upgrade flow is the normal Net10 phone upgrade.  We will be doing activation with PIN flow.
 * 5.In all the above cases,After activating the device, will get the MIN number.
 * 6.After getting MIN, It's executes an phone upgrade spec with new part numbers and the same MIN.Finally, the older ESN gets upgraded with the new ESN with the same MIN number.
 */
'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var phoneupgradeUtil = require('../../util/phoneupgrade.util');
var sessionData = require("../../common/sessiondata.do");

describe('Upgrade Flow', function() {
	var phoneupgradeData = phoneupgradeUtil.getTestData();
	console.log('phoneupgradeData:', phoneupgradeData);
	console.log('protractor.basePath ', protractor.basePath);
	
	drive(phoneupgradeData, function(inputPhoneUpgradeData) {
		sessionData.netten.esnPartNumber = inputPhoneUpgradeData.FromPartNumber;
		console.log('sessionData.netten.esnPartNumber: ',sessionData.netten.esnPartNumber );
		sessionData.netten.arFlag = inputPhoneUpgradeData.AR;
		console.log('sessionData.netten.arFlag::',sessionData.netten.arFlag);
		sessionData.netten.phoneStatus = inputPhoneUpgradeData.PhoneStatus;
		describe('Drive Spec', function() {
			it('Copying phone upgrade test data to session', function(done) {
				sessionData.netten.simPartNumber = inputPhoneUpgradeData.FromSIM;
				sessionData.netten.zip = inputPhoneUpgradeData.FromZipCode;
				sessionData.netten.pinPartNumber = inputPhoneUpgradeData.FromPIN;
				sessionData.netten.toEsnPartNumber = inputPhoneUpgradeData.ToPartNumber;
				sessionData.netten.toSimPartNumber = inputPhoneUpgradeData.ToSIM;
				sessionData.netten.toPinPartNumber = inputPhoneUpgradeData.ToPIN;
				console.log('Copying upgrade', inputPhoneUpgradeData);
				done();
			
		});
			FlowUtil.run('NT_UPGRADE_FLOWS');
		}).result.data = inputPhoneUpgradeData;
	});
});