'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/byop.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('NT Byop Activation with purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.simPartNumber = inputActivationData.SIM;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.planName = inputActivationData.PlanName;
				sessionData.netten.autoRefill = inputActivationData.AutoRefill;
				sessionData.netten.cardType = inputActivationData.cardType;
				sessionData.netten.shopPlan = inputActivationData.shopplan;
				sessionData.netten.phoneType = inputActivationData.phoneType;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('NT_BYOP_ACTIVATION_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});