/*
 * **********Scenario is the consolidation of Upgrade  flows in TracFone ******** 
 * 
 * 1.Read the input data for activatePin from CSV
 * 2.Based on the deviceType ,decide which flow to proceed with.
 * 3. ESN gets upgraded with the new ESN with the same MIN number.
 */

'use strict';

var sessionData = require("../../common/sessiondata.do");
var CommonUtil =  require('../../util/common.functions.util');
var FlowUtil = require('../../util/flow.util');

describe('net10 activation' , function() {
	if(sessionData.netten.deviceType == "phone"){
		FlowUtil.run('NT_ACTIVATION_WITHPIN_WITHACCOUNT');				
	}else if(sessionData.netten.deviceType == "homePhone"){
		FlowUtil.run('NT_ACTIVATION_HOMEPHONE_WITHPIN_WITHACCOUNT');		
	}else if(sessionData.netten.deviceType == "byopPhone"){
		FlowUtil.run('NT_BYOP_ACTIVATION_WITHPIN_WITHACCOUNT');
	}else{
		FlowUtil.run('NT_ACTIVATION_HOTSPOT_WITHPIN_WITHACCOUNT');
	}
});
