'use strict';

var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");

describe('Set lease status for ESN', function() {

	it('set status', function(done) {
		console.log("----before----setLeaseStatus()------");
		var esnval = DataUtil.getESN(sessionData.netten.esnPartNumber);
		console.log('returns', esnval);
		sessionData.netten.esn = esnval;
		
		
		var simval = DataUtil.getSIM(sessionData.netten.simPartNumber);
       	console.log('returns', simval);
       	sessionData.netten.sim = simval;
       	
       	
       	DataUtil.addSimToEsn(sessionData.netten.esn,sessionData.netten.sim);
		
       	DataUtil.setLeaseStatus("1001",sessionData.netten.esn);
		console.log("----after-----setLeaseStatus()------");
		done();
	});
	
});
