'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};
//var generatedEsnSim = {};

describe('Net10 Activation GSM', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on a net10 phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {	
		generatedEsn['esn'] = sessionData.netten.esn;
		activation.enterEsn(sessionData.netten.esn);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsn;	
	
	it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.netten.zip);		
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it(' enter airtime pin and navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.netten.pinPartNumber);
       	console.log('returns Pin value', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.netten.pin = pinval;
		generatedPin['pin'] = sessionData.netten.pin;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	
	it('select create account  and navigate to account creation page', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	it('enter the account details and navigate to account creation successful popup', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "netten";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.netten.cardpin = "12345";
		sessionData.netten.username = emailval;
		sessionData.netten.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the popup ', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnSummaryBtn();
		sessionData.netten.upgradeEsn = sessionData.netten.esn;//specific to phone upgrade					
		expect(myAccount.isLoaded()).toBe(true);		
		done();	
	}).result.data = generatedMin;

	// });
	
});
