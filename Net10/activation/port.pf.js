//this represents a modal for phoneNumber page(port)
'use strict';
var ElementsUtil = require("../util/element.util");

var Port = function() {
	
	this.keepMyNumberBox = element.all(by.name('phone')).get(1);
	//this.keepMyNumContinueBtn =  element(by.id('btn_continuephonenumber'));//Removed get(0)
	this.keepMyNumContinueBtn =  element(by.css("button[id='btn_continuephonenumber']"));
	this.accountPasswordTxt = element.all(by.id('password')).get(1);
	this.accountPasswordContineBtn = element(by.css("[ng-click='action()']"));
	
	//** method to enter phonenumber  and proceed from the page
	this.enterMobNumber = function(number){
		ElementsUtil.waitForElement(this.keepMyNumberBox);
		this.keepMyNumberBox.sendKeys(number);
		//browser.executeScript("arguments[0].click();", this.keepMyNumContinueBtn.getWebElement());
		this.keepMyNumContinueBtn.click();
	};
};

module.exports = new Port;