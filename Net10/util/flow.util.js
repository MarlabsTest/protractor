'use strict';

var runUtil = require('./run.util');
const path = require('path');

var FLOWS = {
		NT_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_ACTIVATION_WITHNOPIN_WITHACCOUNT: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withnopin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_ACTIVATION_HOMEPHONE_WITHPIN_WITHACCOUNT: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.homephone.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_ACTIVATION_HOMEPHONE_WITHPURCHASE_WITHACCOUNT:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.homephone.withpurchase.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_BYOP_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.byopphone.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_ACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpurchase.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_ACTIVATION_HOTSPOT_WITHPIN_WITHACCOUNT:['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.hotspot.withpin.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_FORGOT_PASSWORD:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.forgotpassword.spec.js'],
        NT_EDIT_CONTACT_INFO:['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.editcontactinfo.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_LOGIN_WITH_EMAIL: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.Email.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_LOGIN_WITH_MIN: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.loginwithmin.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_MYACCOUNT_ADDDEVICE: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.adddevice.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_MYACCOUNT_ADDSERVICEPIN:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.addServicePin.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_ENROLLMENT_OF_AUTOREFILL: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js', 'autoreup/spec/autoreup.specs.js'],
        NT_DEENROLLMENT_OF_AUTOREFILL: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js', 'autoreup/spec/autoreup.specs.js','myaccount/spec/myaccount.loginwithmin.spec.js', 'autoreup/spec/autoreup.deenroll.specs.js','common/spec/myaccount.logout.spec.js'],
        NT_REFILL_WITHPIN:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.spec.js'],
        NT_MANAGE_PAYMENT:['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_DELETE_MANAGE_PAYMENT:['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.Email.spec.js','myaccount/spec/myaccount.delete.payment.spec.js','common/spec/myaccount.logout.spec.js'],
        NT_REFILL_WITHPIN_ALL:['reup/spec/reup.all.withpin.outside.spec.js'],
        NT_REFILL_WITHPIN_BRANDED:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.branded.spec.js'],
        NT_REFILL_WITHPIN_BYOP:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.byopphone.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.byop.spec.js'],
		NT_ACTIVATION_DEVICES_WITHPIN_FLOWS:['activation/spec/activation.devices.withpin.spec.js'],
		NT_INTERNAL_PORTIN_WITHPIN_WITHACCOUNT:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activate.internal.portin.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_REFILL_WITHPIN_HOTSPOT :['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.hotspot.withpin.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.hotspot.spec.js'],
		NT_BYOP_ACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.byopphone.withpurchase.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_REFILL_WITHPIN_HOME :['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.homephone.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.homePhone.spec.js'],
		NT_INTERNAL_PORTIN_WITHPURCHASE_WITHACCOUNT:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activate.internal.portin.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_ACTIVATION_ADD_SERVICE_PLAN: ['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','activation/spec/add.service.plan.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_EXTERNAL_PORT_WITHPIN:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.external.port.withpin.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_EXTERNAL_PORT_WITHPURCHASE:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.external.port.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_REACTIVATION_PHONE: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js', 'myaccount/spec/myaccount.loginwithmin.spec.js','activation/spec/reactivate.phone.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_ACTIVATION_HOTSPOT_WITHPURCHASE_WITHACCOUNT :['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.hotspot.withpurchase.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_UPGRADE_FLOWS:['activation/spec/activation.upgrade.flows.spec.js'],
		NT_BYOP_UPGRADE:['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.byopphone.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.byop.upgrade.spec.js'],
		NT_UPGRADE_ACTIVE_ESN_TO_INACTV_ESN:['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.adddevice.spec.js','myaccount/spec/myaccount.upgrade.esnactive.to.esninactive.nonreservemin.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_SHOP_SELECT_PHONES :['common/spec/netten.loadhomepage.spec.js','shop/spec/shop.selectphones.spec.js'],
		NT_SHOP_SELECT_DEVICE :['common/spec/netten.loadhomepage.spec.js','shop/spec/shop.selectdevice.spec.js'],
		NT_PHONE_UPGRADE:['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.phoneupgrade.spec.js','common/spec/myaccount.logout.spec.js'],
		NT_SWITCHPLAN:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','reup/spec/reup.managereserve.switchplan.spec.js'],
		NT_ACTIVATION_LEASED_ESN :['activation/spec/leasedesn.setleasestatus.spec.js','common/spec/netten.loadhomepage.spec.js','activation/spec/activation.leased.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','activation/spec/leasedesn.redeemleasedesn.spec.js'],
		NT_REACTIVATION_WITH_AUTOREFILL_PHONE: ['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js',  'myaccount/spec/myaccount.loginwithmin.spec.js','activation/spec/reactivate.withautorefill.phone.spec.js'],
		NT_SHOP_ILD: ['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','shop/spec/shop.plan.ILD.spec.js'],
		NT_ILD_DEENROLL: ['common/spec/netten.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','shop/spec/shop.plan.ILD.spec.js','myaccount/spec/myaccount.ILD.deenroll.spec.js'],
		NT_REFILL_WITH_PURCH_OUTSIDE_ACC:['common/spec/netten.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.with.purchase.outside.account.spec.js']
};
	
var FlowUtil = {
	
	run: function(flow) {		
		FLOWS[flow].forEach(function (spec) {
			//console.log("running spec..",path.join(protractor.basePath, spec));
			runUtil.requireSuite(path.join(protractor.basePath, spec));
		});
	}
};

module.exports = FlowUtil;
