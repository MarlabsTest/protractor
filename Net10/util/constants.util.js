'use strict';

var Constants = {
	ENV : {site:'https://siteng.simplemobile.com/',test:'https://test2.simplemobile.com/'},
	LOGIN_CSV_FILE: './testdata/login.td.csv',
	LOGIN_JSON_FILE: './testdata/login.td.json',
	SHOP_PLANS_CSV_FILE: './testdata/shopplans.td.csv',
	SHOP_PLANS_JSON_FILE: './testdata/shopplans.td.json',
	ACTVE_PIN_CSV_FILE: './testdata/activatepin.td.csv',
	ACTVE_PIN_JSON_FILE: './testdata/activatepin.td.json',
	ACTVE_WITHNOPIN_CSV_FILE: './testdata/activatewithnopin.td.csv',
	ACTVE_WITHNOPIN_JSON_FILE: './testdata/activatewithnopin.td.json',
	ACTVE_HOMEPHONE_PIN_CSV_FILE: './testdata/activatehomephone.td.csv',
	ACTVE_HOMEPHONE_PIN_JSON_FILE: './testdata/activatehomephone.td.json',
	BYOP_ACTVE_PIN_CSV_FILE: './testdata/activatebyoppin.td.csv',
	BYOP_ACTVE_PIN_JSON_FILE: './testdata/activatebyoppin.td.json',
	PORTING_CSV_FILE: './testdata/porting.td.csv',
	PORTING_JSON_FILE: './testdata/porting.td.json',
	UTIL_JAR_PATH: './javautils/DataGen.jar',
	ALL: 'All',
	PHONE_UPGRADE_CSV_FILE: './testdata/phoneupgrade.td.csv',
	PHONE_UPGRADE_JSON_FILE: './testdata/phoneupgrade.td.json',
	ACTVE_CDMA_PIN_CSV_FILE: './testdata/activatecdmapin.td.csv',
	ACTVE_CDMA_PIN_JSON_FILE: './testdata/activatecdmapin.td.json',
	ACTVE_HOTSPOT_PIN_CSV_FILE: './testdata/activatehotspot.td.csv',
	ACTVE_HOTSPOT_PIN_JSON_FILE: './testdata/activatehotspot.td.json',
	REFILL_CSV_FILE: './testdata/refill.td.csv',
	REFILL_JSON_FILE: './testdata/refill.td.json',
	ACTVE_DEVICES_PIN_JSON_FILE: './testdata/activatdeviceswithepin.td.json',
	ACTVE_DEVICES_PIN_CSV_FILE: './testdata/activatdeviceswithepin.td.csv',
	SHOP_PLANS:['30dayplan','addon','dataplan'],
    DAY_PLANS_30 : {36:'monthly_268',31.50:'monthly_266',
    	            45:'monthly_101',60:'monthly_269',17.99:'monthly_233',34.99:'monthly_234',
    	            35:'monthly_265',40:'monthly_267',50:'monthly_101',65:'monthly_212'},
    DATAONLY_PLANS : {10:'monthly_303',20:'monthly_304',30:'monthly_305',50:'monthly_306'},
    ADDON_PLANS : {10:'monthly_100'}

};

module.exports = Constants;