'use strict';
var ProcEx = require("./proc.exec.util.js");

var DataUtil = function() {
		this.getESN = function(partnumber) {
		console.log("du.getesn");
		//return ProcEx.getESN(partnumber);
		return ProcEx.executeProc("-o ESN -epn "+partnumber+" -env "+browser.params.environment);
		};
		
		this.getSIM = function(partnumber) {
		console.log("du.getsim");
		//return ProcEx.getSIM(partnumber);
		return ProcEx.executeProc("-o SIM -spn "+partnumber+" -env "+browser.params.environment);
		};
		
		this.getPIN = function(partnumber) {
		console.log("du.getpin");
		//return ProcEx.getPIN(partnumber);
		return ProcEx.executeProc("-o PIN -ppn "+partnumber+" -env "+browser.params.environment)
		};
		
		this.activateESN = function(esn) {
		console.log("du.getpin");
		//return ProcEx.executeProc("ACTIVATE", esn);
		return ProcEx.executeProc("-o ACTIVATE -esn "+esn+" -env "+browser.params.environment);		
		};
		
		this.getMinOfESN = function(esn) {
		console.log("du.getpin");
		//return ProcEx.executeProc("MIN", esn);
		return ProcEx.executeProc("-o MIN -esn "+esn+" -env "+browser.params.environment);
		};
		
		this.addSimToEsn = function(esn, newsim) {
		console.log(esn,"du.addSIMtoESN esn" );
		console.log(newsim,"du.addSIMtoESN sim");
		var esnsim = esn +" "+newsim;
		console.log(esnsim,"du.addSIMtoESN esnsim");
		//return ProcEx.executeProc("JOINSIM", esnsim);
		return ProcEx.executeProc("-o JOINSIM -esn "+esn+" -sim "+newsim+" -env "+browser.params.environment);
		};
		
		
		
		/* gowtham change for byop start */
		this.getByopSim = function(esnPartNumber, simPartNumber) {
		console.log("du.getbyopsim");
		//var inputdata = esnPartNumber+" "+simPartNumber;
		//return ProcEx.executeProc("BYOPSIM", inputdata);
		return ProcEx.executeProc("-o BYOPSIM -epn "+esnPartNumber+" -spn "+simPartNumber+" -env "+browser.params.environment);
		};
		/* gowtham change for byop end */
		
		this.deactivatePhone = function(actionType, esnNumber, min, deactivateReason , brand) {
			console.log("du.deActivation");
			//var inputdata = esnNumber+" "+min+" "+deactivateReason+" "+brand;
			//console.log(inputdata);
			//return ProcEx.executeProc(actionType, inputdata);
			console.log("Executing ..  "+"-o "+actionType+" -esn "+esnNumber+" -min "+min+" -dr "+deactivateReason+" -brand "+brand+" -env "+browser.params.environment);
			return ProcEx.executeProc("-o "+actionType+" -esn "+esnNumber+" -min "+min+" -dr "+deactivateReason+" -brand "+brand+" -env "+browser.params.environment);
		};
		
		this.getActiveESNFromPartNumber= function(partNumber) {
			console.log("du.getactiveESN");
			//return ProcEx.executeProc("GET_ACTIVE_ESN", partNumber);
			return ProcEx.executeProc("-o GET_ACTIVE_ESN -epn "+partNumber+" -env "+browser.params.environment);
		};
		
		this.getEmail= function(esn) {
		console.log("du.getpin");
		//return ProcEx.executeProc("EMAIL", "");
		return ProcEx.executeProc("-o EMAIL -esn "+esn+" -env "+browser.params.environment);
		};
		
		this.checkActivation = function(esnPartNumber, pin, actionType, transType) {
			console.log("du.checkActivation");
			//var inputdata = esnPartNumber+" "+pin+" "+actionType+" "+transType+" ";
			//console.log(inputdata);
			//return ProcEx.executeProc("CHECK_ACTIVATION", inputdata);
			return ProcEx.executeProc("-o CHECK_ACTIVATION -epn "+esnPartNumber+" -pin "+pin+" -at "+actionType
					+"-tt "+transType+" -env "+browser.params.environment);
		
		};
		
		this.checkRedemption = function(esnPartNumber, pin, actionType, transType, buyFlag, redeemType) {
			console.log("du.checkRedemption");
			//var inputdata = esnPartNumber+" "+pin+" "+actionType+" "+transType+" "+buyFlag+" "+redeemType+" ";
			//console.log(inputdata);
			//return ProcEx.executeProc("CHECK_REDEMPTION", inputdata);
			return ProcEx.executeProc("-o CHECK_REDEMPTION -epn "+esnPartNumber+" -pin "+pin+" -at "+actionType+
					               " -tt "+transType+" -bf "+buyFlag+" -rt "+redeemType+" -env "+browser.params.environment);
		};
		
		this.clearOTA = function(esn) {
			console.log(esn,"du.clearOTA esn" );		
			return ProcEx.executeProc("-o CLEAR_OTA -esn "+esn+" -env "+browser.params.environment);
		};
		
		// for updating MIN in ig_transaction table
		this.updateMin = function(esn) {
			console.log(esn,"du.updateMin esn" );		
			return ProcEx.executeProc("-o UPDATE_MIN -esn "+esn+" -env "+browser.params.environment);
		};
		
		
		this.setLeaseStatus=function(status,esn){
			console.log(esn,"du.setLeaseStatus esn" );		
			return ProcEx.executeProc("-o SET_LEASED_ESN_STATUS -status "+status+" -esn "+esn+" -env "+browser.params.environment);
		}
		
		this.redeemLeasedESN=function(esn,pinpart){
			console.log(esn,"du.redeemLeasedESN esn");		
			return ProcEx.executeProc("-o REDEEM_LEASED_ESN -esn "+esn+" -pinPartNumber "+pinpart+" -env "+browser.params.environment);
		}
		
		this.changeServiceEndDate = function(esn) {
			console.log(esn,"du.SET_DUE_DATE_IN_PAST esn" );		
			return ProcEx.executeProc("-o SET_DUE_DATE_IN_PAST -esn "+esn+" -env "+browser.params.environment);
		};
		
	};

	module.exports = new DataUtil;