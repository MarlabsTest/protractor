'use strict';

var csvJsonUtil = require('./csvjson.util.js');
var Constants = require('./constants.util.js');

var LoginUtil = {
		
	prepareLoginData: function() {		
		csvJsonUtil.convertCsvToJson(Constants.LOGIN_CSV_FILE, Constants.LOGIN_JSON_FILE);
	},
	
	getTestData: function() {
		var loginData = browser.params.loginData;
		var jsonObj = csvJsonUtil.getJsonData(Constants.LOGIN_JSON_FILE); //require can also be used
		console.log('jsonObj', jsonObj);
		console.log('Selected data:', loginData);
		
		if(Constants.ALL === loginData) {
			return jsonObj;
		}
		return this.getSelectedData(jsonObj, loginData.toString().split(','));
	},
	
	getSelectedData: function(json, datas) {
		var selectedData = [];
		
		datas.forEach(function(data, index) {
			selectedData.push(json[data - 1]);
		});
		
		return selectedData;
	}
};

module.exports = LoginUtil;