'use strict';
var ElementUtil = require("../util/element.util");
var ConstantsUtil = require("../util/constants.util");

var ShopServicePlan = function() {


    //Newly added $$
	this.ildEnroll = element(By.id('addToCartEnroll_popup_btn'));
	this.ildMin = element.all(By.id('device-info')).get(1);
	this.ildPopUp = element(By.id('saveDeviceSubmit_btn'));
	this.checkPlanPageLoaded = element.all(By.id('monthly_265')).get(1);
    this.smartPhonePlansTab = element(By.xpath("//span[@translate='BAT_24184']"));
    this.payAsGoPlansTab = element(By.xpath("//span[@translate='ACRE_6801']"));
    this.addOnPlansTab = element(By.xpath("//span[@translate='BAT_24571']"));
    
    this.thirtydayPlans = element.all(By.id('monthly_238'));
    this.dataonlyPlans = element.all(By.id('monthly_285'));
    this.addOnPlans = element.all(By.id('monthly_102'));

    this.autoReupPopup = element(by.className('modal-dialog'));
	
	this.autoRefillBtn = element(By.id('addToCartEnroll_popup_btn'));
	this.actualOneTimePurchase = element(By.id('addToCartOneTimePurchpopup_btn'));
	this.autoRefillBtnSmtPln = element.all(By.id('btn_autorefill'));
	this.actualOneTimePurchaseSmtPln = element(By.id('btn_onetimepurchase'));
	this.phoneNumberTextbox = element.all(By.id('phonenum-min')).get(1);
	this.phoneNumberContinueBtn = element.all(By.id('saveDeviceSubmit_btn'));
	this.addPopUpOne = element(By.css("[ng-click='cancel()']"));
	
	this.thirtydayPlanTab = element(By.xpath("//span[@translate='BAT_24531']"));	
	
	//this."//*[@id="monthly_101"]/ng-include/img"
	
	
	
	this.dataonlyplanTab = element(By.xpath("//span[@translate='BAT_24913']"));
	this.newCustomerLink = element(By.css("[ng-click='newCustomer()']"));
	
	this.choosePlanByName = function(planType,planName,isAutoRefill,phoneType) 
	{		
        if (planType == ConstantsUtil.SHOP_PLANS[0]) 
        {
        	//element(By.id(ConstantsUtil.DAY_PLANS_30[planName])).click();
        	element.all(By.css("button[id='"+ConstantsUtil.DAY_PLANS_30[planName]+"']")).get(0).click();
        }
        else if(planType == ConstantsUtil.SHOP_PLANS[1])
        {
        	element(By.id(ConstantsUtil.ADDON_PLANS[planName])).click();

        } 
        else if(planType == ConstantsUtil.SHOP_PLANS[2])
        {
        	element(By.id(ConstantsUtil.DATAONLY_PLANS[planName])).click();
        }
		
        var autoReupPopupElem = element(by.className('modal-dialog'));
       autoReupPopupElem.isPresent().then(function(isVisible)
        {
        	if(isVisible)
			{
        		console.log("is visible");
				if(isAutoRefill == 'YES' || isAutoRefill == 'Y')
				{
					element(By.id('addToCartEnroll_popup_btn')).click();
				}
				else
				{
					console.log("One time purchase");
					//this.actualOneTimePurchase.click();
					element(By.id('addToCartOneTimePurchpopup_btn')).click();
				}
			}
        });
	};
	this.chooseReupPlanByName = function(planType,planName,isAutoRefill,phoneType) 
	{
        if (planType == ConstantsUtil.SHOP_PLANS[0]) 
        {
        	//element(By.id(ConstantsUtil.DAY_PLANS_30[planName])).click();
        	element.all(By.css("button[id='"+ConstantsUtil.DAY_PLANS_30[planName]+"']")).get(0).click();
        }
        else if(planType == ConstantsUtil.SHOP_PLANS[1])
        {
        	element(By.id(ConstantsUtil.ADDON_PLANS[planName])).click();

        } 
        else if(planType == ConstantsUtil.SHOP_PLANS[2])
        {
        	element(By.id(ConstantsUtil.DATAONLY_PLANS[planName])).click();
        }
	};
	
	this.providePhoneNumber = function(phoneNumber){
	//ElementUtil.waitForElement(this.phoneNumberTextbox);
	this.phoneNumberTextbox.sendKeys(phoneNumber);
	//ElementUtil.waitForElement(this.phoneNumberContinueBtn);
	this.phoneNumberContinueBtn.click();
	};
	
	//this will navigate to enter the phone number
	this.isShopServicePlanPageLoaded = function() {
		return ElementUtil.waitForUrlToChangeTo(/shop\/plans$/);
	};
	
	
	
	//check whether the page is navigated to the service plan page
	this.isShopServicePlansListed = function() {
		ElementUtil.waitForElement(this.checkPlanPageLoaded);
		return this.checkPlanPageLoaded.isPresent();
//		return ElementUtil.waitForUrlToChangeTo(/serviceplan$/);
	};
	
	this.ILDPopUpcheck = function(){
//		ElementUtil.waitForElement(this.ildPopUp);
		return this.ildPopUp.isPresent();
	};
	this.autorefillPopUpcheck= function(){
//		ElementUtil.waitForElement(this.ildPopUp);
		return this.ildEnroll.isPresent();
	};
	
	this.fillILDPopUp = function(activeMin){
//		ElementUtil.waitForElement(this.ildMin);
		this.ildMin.sendKeys(activeMin);
		this.ildPopUp.click();
	};
	
	this.EnrollILDPlan = function(){
		ElementUtil.waitForElement(this.ildEnroll);
		return this.ildEnroll.isPresent();
	};
	
	this.ClickOnEnrollILDPlan = function(){
		this.ildEnroll.click();
	};
	this.ServicePlanForNewCustomer = function(planName,planType){
		this.selectPlan(planName,planType);
		this.newCustomerLink.click();
	};
	this.selectPlan = function(planName,planType){
		console.log('IN PF FILE');
		console.log('monthlyPlan=' ,planName);
		console.log('planType=' ,planType);
		if (planType == ConstantsUtil.SHOP_PLANS[0]){
			console.log('SHOP PLNS IS 30 DAYPLAN');
			console.log('30 DAYPLAN=',ConstantsUtil.THIRTY_DAY_PLANS[planName]);
			element(By.id(ConstantsUtil.DAY_PLANS_30[planName])).click();			
      }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
    	  console.log('SHOP PLNS IS 30 DAYPLAN');
			console.log('DATAONLY_PLANS=',ConstantsUtil.DATAONLY_PLANS[planName]);
    	  element(By.id(ConstantsUtil.DATAONLY_PLANS[planName])).click();
      }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
    	  console.log('SHOP PLNS IS 30 DAYPLAN');
			console.log('ADDON_PLANS =',ConstantsUtil.ADDON_PLANS[planName]);
    	  element(By.id(ConstantsUtil.ADDON_PLANS[planName])).click();
      }     
		/*if (planType == ConstantsUtil.SHOP_PLANS[0]) {
            // this.thirtydayPlanTab.click();
             this.thirtydayPlans.click();
             //this.smartPhonePlans.get(ConstantsUtil.30DAY_PLANS[planName]).click();
       }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
             //this.payAsGoPlansTab.click();
             this.dataonlyPlans.click();
            /// this.payAsGoPlans.get(ConstantsUtil.ADDON_PLANS[planName]).click();
       }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
             //this.addOnPlansTab.click();
             this.addOnPlans.click();
             //this.addOnPlans.get(ConstantsUtil.DATAONLY_PLANS[planName]).click();
       }*/
		
	}
	
	this.isActivatePageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/selectdevice$/);
	};
	

};

module.exports = new ShopServicePlan;