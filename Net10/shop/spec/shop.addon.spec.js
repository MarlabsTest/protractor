'use strict';
var myAccount = require("../../myaccount/myaccount.po");
var sessionData = require("../../common/sessiondata.do");
var menu = require("../../common/homepage.po");
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");

describe('purchase ADD ON plan', function() {	
	
	it('enteremail id', function(done) {
		myAccount.enterEmailId(sessionData.netten.username);
		expect(myAccount.isPasswordPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('login', function(done) {
		myAccount.accountLogin(sessionData.netten.password);
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('click on shop link', function(done) {
		menu.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on shop plan', function(done) {
		shop.goToShopServicePlan();
		expect(shop.isServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('choose plan', function(done) {
		var planType		= sessionData.netten.shopPlan;
		var planName		= sessionData.netten.planName;
		var isAutoRefill	= sessionData.netten.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		shop.clickNoThanksAutorefillBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
//	it('enter phone details', function(done){
//		var esnval = sessionData.netten.esn;
//		shop.enterEsnDetails(esnval);	
//		expect(myAccount.checkoutPageLoaded()).toBe(true);
//		done();
//	});
	
	
	/*it('select $10ILD addon', function(done) {
		shop.clickOnILDPlanTab();
		shop.selectILDPlan();
		shop.clickNoThanksAutorefillBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});*/
	
	it('should enter card details', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.netten.cardType),CommonUtil.getCvv(sessionData.netten.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		if('DATA' == sessionData.netten.planName || 'data' == sessionData.netten.planName){
			expect(true).toBe(true);
			done();		
		}else{
			myAccount.placeMyOrder();
			expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
			done();					
		}
	}); 
	
	it('should load back to the same check out page', function(done) {
		if('DATA' == sessionData.netten.planName || 'data' == sessionData.netten.planName){
			expect(true).toBe(true);
			done();					
		}else{
			myAccount.agreeBillingZipcodeChanges();
			expect(myAccount.checkoutPageLoaded()).toBe(true);
			done();		
		}
	});
	
	it('should load order confirmation page', function(done) {
		myAccount.placeMyOrder();
		expect(myAccount.confirmationPageLoaded()).toBe(true);
		done();		
	});
	

	
	it('should load dashboard page', function(done) {
		myAccount.clickDoneConfirmationBtn();
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	});	
	});
