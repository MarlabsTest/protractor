'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");
var myAccount = require("../../myaccount/myaccount.po");
var activation = require("../../activation/activation.po");
var generator = require('creditcard-generator');
var CommonUtil = require('../../util/common.functions.util');
var CCUtil = require("../../util/creditCard.utils");
var dataUtil= require("../../util/datautils.util");

describe('Net10 buy ILD plan inside account', function() {

	//click on the shop link which is shown in the homepage
	it('Go to shop page', function(done) {
		homePage.goToShopAndSelectPlan();
		expect(shop.isShopServicePlansListed()).toBe(true);
		done();
	});
	it('Select ILD plan', function(done) {
		var planType		= sessionData.netten.shopPlan;
		var planName		= sessionData.netten.planName;
		var isAutoRefill	= sessionData.netten.autoRefill;
		var phoneType	    = sessionData.netten.phoneType;
		console.log("planType="+planType);
		console.log("planName="+planName);
		console.log("isAutoRefill="+isAutoRefill);
		console.log("phoneType="+phoneType);
		shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
		expect(shop.ILDPopUpcheck()).toBe(true);
	});
	
	it('Enter min and continue to checkout', function(done) {
		var min= dataUtil.getMinOfESN(sessionData.netten.esn);
		console.log("active min",min)
		shop.fillILDPopUp(min);
		expect(shop.EnrollILDPlan()).toBe(true);
		done();
	});
	
	it('click on enroll', function(done){
		shop.ClickOnEnrollILDPlan();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});

	it('Enter payment details for checkout', function(done) {
		myAccount.enterAutoReupCcDetails(""+generator.GenCC(sessionData.netten.cardType),CommonUtil.getCvv(sessionData.netten.cardType));
		console.log("city",CCUtil.city);
		myAccount.enterAutoReupBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.pin,CCUtil.city);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	});
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the home page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();		
	});
});