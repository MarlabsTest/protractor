'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");

//Firstly go to the shop page 
//click on the select phones
//enter the ZIP Code of the area where you will be using your net10
//confirm the zip code by clicking yes button
//it redirects to the page where we can purchase the phones

describe('Net10 buy device', function() {

	//click on the shop link which is shown in the homepage
	//should check whether the shop phone page is loaded or not
	it('should open shopping page', function(done) {
		homePage.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);
		done();
	});
	
	//click on the select phones now and will be redirected to enter the zipcode page
	it('should open shop phone page, provide zipcode and confirm', function(done) {
		shop.goToSelectdevice();
		expect(shop.isShopDevicePageLoaded ()).toBe(true);
	});
	
	it('should open to view all devices',function(done){
		shop.goToSeeAlldevice();
	});
	
	it('open the zipcode and enter the value',function(done){
	
		shop.enterZipcode(sessionData.netten.zip); 
		expect(shop.isShopPhonePageLoaded()).toBe(true);
		done();
	});	
	
});
