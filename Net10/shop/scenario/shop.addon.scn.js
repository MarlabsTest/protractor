'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var shop = require('../../util/shopplans.util');
var sessionData = require("../../common/sessiondata.do");

describe('Net10 purchase ADD ON plan', function() {
	var activationData = shop.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.simPartNumber = inputActivationData.SIM;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.pinPartNumber = inputActivationData.PIN;
				sessionData.netten.shopPlan = inputActivationData.shopplan;
				sessionData.netten.planName = inputActivationData.PlanName;
				sessionData.netten.autoRefill = inputActivationData.AutoRefill;
				done();
			});
			FlowUtil.run('NT_SHOP_ADDONS');
		}).result.data = inputActivationData;
	});
});