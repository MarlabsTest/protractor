module.exports = {

	//testing framework (jasmine/cucumber/mocha etc)
	test: 'jasmine2',
	
	//framework options - jasmine node options
	options: {
		onComplete: null,
		isVerbose: true,
		showColors: true,
		includeStackTrace: true,
		defaultTimeoutInterval: 75000
	}
};