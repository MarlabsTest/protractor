module.exports = {
	suites: {
		Activation: [
			'activation/scenario/activation.withpin.withaccount.scn.js',
			'activation/scenario/activation.withnopin.withaccount.scn.js',
			'activation/scenario/activation.withpurchase.withaccount.scn.js',
			'activation/scenario/activation.homephone.withpin.withaccount.scn.js',
			'activation/scenario/activation.homephone.withpurchase.withaccount.scn.js',
			'activation/scenario/activation.byopphone.scn.js',
			'activation/scenario/activation.byopphone.withpurchase.scn.js',
			'activation/scenario/activation.hotspot.withpin.scn.js',
			'activation/scenario/activation.hotspot.withpurchase.withaccount.scn.js',
			'activation/scenario/activation.devices.withpin.scn.js',
			'activation/scenario/reactivation.phone.scn.js',
			'activation/scenario/activate.internal.portin.withpin.withaccount.scn.js',
			'activation/scenario/activate.internal.portin.withpurchase.scn.js',
			'activation/scenario/activation.external.port.withpin.scn.js',
			'activation/scenario/activation.external.port.withpurchase.scn.js',
			'activation/scenario/activation.upgrade.flows.scn.js',
			'activation/scenario/activation.add.service.plan.scn.js',
			'activation/scenario/activation.leasedesn.scn.js',
			'activation/scenario/reactivation.withautorefill.phone.scn.js'
		]
		,
		Myaccount: [
			'myaccount/scenario/myaccount.login.withemail.scn.js',
			'myaccount/scenario/myaccount.login.withmin.scn.js',
			'myaccount/scenario/myaccount.forgotpassword.scn.js',
			'myaccount/scenario/myaccount.editcontactinfo.scn.js',
			'myaccount/scenario/myaccount.adddevice.scn.js',
			'myaccount/scenario/myaccount.managepayment.scn.js',
			'myaccount/scenario/myaccount.delete.payment.scn.js',
			'myaccount/scenario/myaccount.addServicePin.scn.js',
			'myaccount/scenario/myaccount.ILD.deenroll.scn.js'
		]
		,
		AutoReUp: [
			'autoreup/scenario/autoreup.scn.js',
			'autoreup/scenario/deenroll.autoreup.scn.js'
		]
		,
		ReUp: [
			'reup/scenario/reup.withpin.outside.scn.js',
			'reup/scenario/reup.managereserve.switchplan.scn.js'
			'reup/scenario/reup.with.purchase.outside.account.scn.js'
		]
		,
		Shop: [
			'shop/scenario/shop.buyphones.scn.js',
			'shop/scenario/shop.plan.ILD.scn.js'
		]
	}
};