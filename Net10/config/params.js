
module.exports = {

	params: {
		environment: 'site',
		reportDir: './reports/',
		loginData: 'All', // comma separated csv line numbers / All
		activationData: '1' // comma separated csv line numbers / All
	}
};