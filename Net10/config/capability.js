
module.exports = {

	//Capabilities or multi capabilities
	capabilities: {
		// 'browserName': 'firefox',
		
		'browserName': 'chrome',
		'chromeOptions' : {
			// 'binary': 'D:/rgs/Chrome/Application/chrome.exe',
			'args': ['acceptSslCerts=true', 'ignore-ssl-errors=true']
		},
		'proxy': {
			'proxyType': 'direct'
		},
	
		'shardTestFiles': true,
		'maxInstances': 5,
	},
	/*multiCapabilities: [
		{'browserName': 'firefox'},
		{'browserName': 'chrome',		
			'chromeOptions' : {
				'binary': 'D:/rgs/Chrome/Application/chrome.exe',
				'args': ['acceptSslCerts=true', 'ignore-ssl-errors=true']
			}
		}
	],*/
	
	//restartBrowserBetweenTests: true;
};