//this represents a modal for the 'All Options' link 

'use strict';
//var ElementsUtil = require("../util/elements.util");

var OptionsQuickLink = function() {

	this.allOptions = element.all(by.css("strong[translate='MYACC_5005']")).get(0);// MYAC_99744
	
	this.addServicePlanLink = element(by.css('[ng-click="addAirtimePlan()"]'));
	
	//** method is to click on the All Options link
	this.clickOnAllOptions = function(){
		//ElementsUtil.waitForElement(this.allOptions);
		browser.executeScript("arguments[0].click();", this.allOptions.getWebElement());
		//this.allOptions.click();
	};

};

module.exports = new OptionsQuickLink;