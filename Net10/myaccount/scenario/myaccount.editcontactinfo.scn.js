'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

/*
 * Scenario : NT Edit contact information
 * Details	: This scenario first read an test data from JSON file.
 * 			  Using part numbers mentioned in JSON it's execute an activation spec file and 
 * 			  then activate that ESN to get MIN for the same.Also,username password stored on session object.
 * 			  Then it executes an myaccount edit contact info spec to edit contact information of the same account.
 * */
describe('NT Edit Contact Information', function() {
	
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.netten.esnPartNumber = inputActivationData.PartNumber;
				sessionData.netten.simPartNumber = inputActivationData.SIM;
				sessionData.netten.zip = inputActivationData.ZipCode;
				sessionData.netten.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('NT_EDIT_CONTACT_INFO');
		}).result.data = inputActivationData;
	});
		
});
	
