//this represents a modal for add service plan option

'use strict';
var ElementsUtil = require("../util/element.util");

var AddServicePin = function() {

	this.addServicePlanLink = element(by.css('[ng-click="addServicePlan()"]'));
	this.renewPin = element.all(by.name('formPlan.pin')).get(1);
	this.renewContinueBtn = element(by.id('AddAirtime_btn'));
	this.proceedFromCOnfirmationPage = element.all(by.id('done_confirmation')).get(0);
	
	//** method is to proceed with the add service plan flow
	this.clickOnservicePlanLink = function(){
		ElementsUtil.waitForElement(this.addServicePlanLink);
		this.addServicePlanLink.click();
	};
  
	//** method checks whether the renew service plan page is loaded or not
	this.renewServicePageLoaded = function(){
		ElementsUtil.waitForElement(this.renewPin);
		return this.renewPin.isPresent();
	}
  
  //** method to enter new pin 
	this.enterAirtimePinRenew = function(pin){
		ElementsUtil.waitForElement(this.renewPin);
		this.renewPin.clear().sendKeys(pin);
		ElementsUtil.waitForElement(this.renewContinueBtn);
		this.renewContinueBtn.click();
	};
  
	//** method to check the redemption confirmation page is loaded after entering the new pin
	this.redemptionSuccessPage = function(){	
		ElementsUtil.waitForElement(this.proceedFromCOnfirmationPage);
		return this.proceedFromCOnfirmationPage.isPresent();
	};
  
	//** method to proceed from the confirmation page
	this.clickOnContinueBtnFromConfirmation = function(){
		ElementsUtil.waitForElement(this.proceedFromCOnfirmationPage);
		this.proceedFromCOnfirmationPage.click();
	};

};

module.exports = new AddServicePin;