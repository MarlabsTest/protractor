//this represents a modal for the checkout page

'use strict';
var ElementsUtil = require("../util/element.util");

var ServiceRenewChkout = function() {
	//*[@id="terms"]
	this.paymentTab = element(by.xpath("//span[@translate='BAT_24249']"));
	this.termsCheckbox = element(by.css("input[id='term']"));//('[for="term"]'));
	this.switchAutoRefillBtn = element(by.className('togSlide'));
	this.proceedFromConfirmationAutoRefil = element.all(by.id('btn_done')).get(1);
	this.proceedFromConfirmation = element.all(by.id('done')).get(1);
	this.surveyContyinue = element.all(by.css('[ng-click="action()"]')).get(1);
	this.chkoutContinue = element.all(by.id('btn_continuetopayment')).get(1);
	//this.btnContinueSetupPhone	= element.all(by.id('btn_continuesetupphone')).get(3);
	//this.btnPurchaseDone	= element.all(by.id('btn_done')).get(1);
	this.cvvText = element.all(by.name('cvv')).get(1);
	//remya
	//this.creditTabOption	= element.all(by.css('[ng-click="selectTab(1)"]')).get(0);
	//remya
	this.newPaymentForm = element(by.css('[ng-click="selectTab(2)"]'));
	this.ccnumber = element.all(by.name('formName.number_mask')).get(1);
	this.nickName = element.all(by.id('nickname')).get(1);
	//button for 'continue to Payment' in reactivation with purchase scenario
	//this.btncontinuetopayment = element.all(by.id('btn_continuetopayment')).get(2);
	
	//this.monthDropDown = element.all(by.className('selectize-input focus')).get(0); //ui-select-match ng-scope
	this.monthDropDown = element.all(by.className('selectize-input')).get(1); 
	this.monthDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(6);
	
	this.yearDropDown = element.all(by.className('selectize-input')).get(2);	
	this.yearDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(2);
	
	this.fname = element(by.css("input[id='fname']"));//element.all(by.name('fname')).get(1);
	this.lname = element(by.css("input[id='lname']"));//element.all(by.name('lname')).get(1);
	this.address = element(by.css("input[id='address1']"));//element.all(by.name('address1')).get(1);
	this.houseNo = element.all(by.name('address2')).get(1);
	this.city = element(by.css("input[id='city']"));//element.all(by.id('city')).get(1);	
	this.zipcode = element(by.css("input[id='zipcode']"));//element.all(by.id('zipcode')).get(1);
	
	this.autoReupMonthDropDown = element.all(by.className('selectize-input')).get(0); 
	this.autoReupMonthDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(6);
	
	this.autoReupYearDropDown = element.all(by.className('selectize-input')).get(1);	
	this.autoReupYearDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(2);
	
	this.autoReupStateElem = element.all(by.className('selectize-input')).get(2);
	this.autoReupStateDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	this.state = element.all(by.className('selectize-input')).get(3);	
	this.stateDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	this.stateAch = element.all(by.className('selectize-input')).get(1);	
	this.stateAchDropDownSelect = element.all(by.className('ui-select-choices-row ng-scope')).get(4);
	
	this.country = element.all(by.model('$select.search')).get(1);
	
	this.placeOrderBtn  = element(by.css("button[id='PlaceMyorder']"));//PlaceMyorder
	
	this.tabBankAccount	= element(by.linkText('Bank Account'));
	
	this.autoRefillStatusYesElement = element(by.css('[ng-if="toggleStatus"]'));
	
	this.selectAccType		= element.all(by.className('selectize-input')).get(0);
	this.selectAccTypeOpt	= element.all(by.className('dropdown-content-item ng-scope')).get(1);
	this.txtAccNo			= element.all(by.name('formName.number_mask')).get(1); 
	this.txtRoutingNo		= element.all(by.name('routingNumber')).get(1);
	this.billingZipcodeChangesAgreeBtn = element(by.id("zipagree"));
	this.billingZipcodeChangesPopup = element(by.className("modal-content"));
	this.autoReupState = element(by.css("input [id='state1']"));
	
	this.goToPaymentTab = function(){
		this.paymentTab.click();
	};
	
	//** method to agree billing zipcode change popup
	this.agreeBillingZipcodeChanges = function(){
		ElementsUtil.waitForElement(this.billingZipcodeChangesAgreeBtn);
		this.billingZipcodeChangesAgreeBtn.click();
	};
	
	//** method to check billing zipcode change popup shown
	this.isBillingZipcodeChangesPopupShown = function(){
		ElementsUtil.waitForElement(this.billingZipcodeChangesPopup);
		return this.billingZipcodeChangesPopup.isPresent();
	};
	
	//** method to click place my order button
	this.placeMyOrder = function(){
		ElementsUtil.waitForElement(this.placeOrderBtn);
		this.placeOrderBtn.click();
	};
	
	//** method to check place my order button loaded
	this.isAutoRefillCheckoutPageLoaded = function(){
		ElementsUtil.waitForElement(this.placeOrderBtn);
		return this.placeOrderBtn.isPresent();
	};
	
	//** method to click on the continue to payment button in the checkout page
	this.continueToPayment = function() {
		//$$//ElementsUtil.waitForElement(this.chkoutContinue);
		this.chkoutContinue.click();
	};
	
	//Method to click on Bank Account tab in Payment - for reactivation with ACH paurchase scenario
	this.clickOnBankAccTab = function(){
		//$$//ElementsUtil.waitForElement(this.tabBankAccount);
		this.tabBankAccount.click();
	};
	
	// method to enter ACH details for reactivation with purchase scenario
	this.enterAchDetails = function(accno,routingno){
		this.selectAccType.click();
		this.selectAccTypeOpt.click();
		this.txtAccNo.clear().sendKeys(accno);
		this.txtRoutingNo.clear().sendKeys(routingno);
    };
    
    this.isAutoRefillStatusYes = function(accno,routingno){
    	ElementsUtil.waitForElement(this.autoRefillStatusYesElement);
		return this.autoRefillStatusYesElement.isPresent();
    };
	
	//** method to check whether the payment form for entering the cc and billing details are displayed or not
	this.paymentOptionLoaded = function() {
		//$$//ElementsUtil.waitForElement(this.ccnumber);
		return this.ccnumber.isPresent();
	};
	
	
	this.clickNewPayment = function(){
		//$$//ElementsUtil.waitForElement(this.newPaymentForm);
		this.newPaymentForm.click();
	};
	
	//** method to enter cc details
	this.enterCcDetails = function(ccNum,cvv){
		ElementsUtil.waitForElement(this.ccnumber);
		this.ccnumber.clear().sendKeys(ccNum);
		//this.nickName.clear().sendKeys('Test');
		this.cvvText.clear().sendKeys(cvv);
		this.monthDropDown.click();
		this.monthDropDownSelect.click();
		this.yearDropDown.click();
		this.yearDropDownSelect.click();
    };
    
    //** method to enter cc details (autoreup flow specific)
	this.enterAutoReupCcDetails = function(ccNum,cvv){
		ElementsUtil.waitForElement(this.ccnumber);
		this.ccnumber.clear().sendKeys(ccNum);
		//this.nickName.clear().sendKeys('Test');
		this.cvvText.clear().sendKeys(cvv);
		this.autoReupMonthDropDown.click();
		this.autoReupMonthDropDownSelect.click();
		this.autoReupYearDropDown.click();
		this.autoReupYearDropDownSelect.click();
    };
    this.enterReupCcDetails = function(ccNum,cvv){
		ElementsUtil.waitForElement(this.ccnumber);
		this.ccnumber.clear().sendKeys(ccNum);
		//this.nickName.clear().sendKeys('Test');
		this.cvvText.clear().sendKeys(cvv);
		this.autoReupMonthDropDown.click();
		this.autoReupMonthDropDownSelect.click();
		this.autoReupYearDropDown.click();
		this.autoReupYearDropDownSelect.click();
    };
    
   /* //Method to enter bill details for reactivation
    this.enterReactivateBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		console.log('billing info');
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		
		this.state.click();//clear().sendKeys("CA");//click();
		this.stateDropDownSelect.click();
			
		this.country.click();
		this.country.clear().sendKeys("USA");
        this.city.clear().sendKeys(city);
        this.zipcode.clear().sendKeys(zipcode);	
		//ElementsUtil.elementHasCome(this.placeOrderBtn, 1);
		this.placeOrderBtn.click();
	};*/
    
    this.switchCheckOutWithAutoRefill = function(){
    	ElementsUtil.waitForElement(this.switchAutoRefillBtn);
    	this.switchAutoRefillBtn.click();
    };

	//** method to enter billing details
	this.enterBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		console.log('billing info');
		this.fname.sendKeys(fname);
		this.lname.sendKeys(lname);
		this.address.sendKeys(address1);
		this.city.sendKeys(city);	
		
			
		//this.state.clear().sendKeys("CA");//click();
		this.state.click();//clear().sendKeys("CA");//click();
		this.stateDropDownSelect.click();
		this.zipcode.sendKeys(zipcode);	
		//this.zipcode.blur();
		//this.termsCheckbox.click();
		browser.executeScript("arguments[0].click();", this.termsCheckbox.getWebElement());	
	};
	
	//** method to enter billing details
	this.enterAutoReupBillingDetails = function(fname,lname,address1,houseNum,zipcode,city){
		console.log('billing info');
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		//this.zipcode.clear().sendKeys(zipcode);
		this.city.clear().sendKeys(city);
		//this.state.clear().sendKeys("CA");//click();
		this.autoReupStateElem.click();//clear().sendKeys("CA");//click();
		//ElementsUtil.waitForElement(this.autoReupStateDropDownSelect);
		this.autoReupStateDropDownSelect.click();
		//this.state.click();//clear().sendKeys("CA");//click();
		//this.stateDropDownSelect.click();
		this.zipcode.clear().sendKeys(zipcode);
		//this.autoReupState.sendKeys("CA");
		//this.termsCheckbox.click();
		browser.executeScript("arguments[0].click();", this.termsCheckbox.getWebElement());	
	};
	//** method to enter billing details
	this.enterReupBillingDetails = function(fname,lname,address1,houseNum,zipcode,city){
		console.log('billing info');
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		//this.zipcode.clear().sendKeys(zipcode);
		
		//this.state.clear().sendKeys("CA");//click();
		this.autoReupStateElem.click();//clear().sendKeys("CA");//click();
		//ElementsUtil.waitForElement(this.autoReupStateDropDownSelect);
		this.autoReupStateDropDownSelect.click();
		//this.state.click();//clear().sendKeys("CA");//click();
		//this.stateDropDownSelect.click();
		this.zipcode.clear().sendKeys(zipcode);
		this.city.clear().sendKeys(city);
		//this.autoReupState.sendKeys("CA");
		//this.termsCheckbox.click();
		browser.executeScript("arguments[0].click();", this.termsCheckbox.getWebElement());	
	};
	//** method to enter billing details
	this.enterReupBillingDetails = function(fname,lname,address1,houseNum,zipcode,city){
		console.log('billing info');
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		//this.zipcode.clear().sendKeys(zipcode);
		this.city.clear().sendKeys(city);
		//this.state.clear().sendKeys("CA");//click();
		this.autoReupStateElem.click();//clear().sendKeys("CA");//click();
		//ElementsUtil.waitForElement(this.autoReupStateDropDownSelect);
		this.autoReupStateDropDownSelect.click();
		//this.state.click();//clear().sendKeys("CA");//click();
		//this.stateDropDownSelect.click();
		this.zipcode.clear().sendKeys(zipcode);
		//this.autoReupState.sendKeys("CA");
		//this.termsCheckbox.click();
		browser.executeScript("arguments[0].click();", this.termsCheckbox.getWebElement());	
	};
	
	//** method to enter ach billing details
	this.enterAchBillingDetails = function(fname, lname, address1, houseNum, city, zipcode){
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		this.stateAch.click();
		this.stateAchDropDownSelect.click();
		this.city.clear().sendKeys(city);
		this.zipcode.clear().sendKeys(zipcode);	
		this.termsCheckbox.click();
		this.placeOrderBtn.click();
	};
	
	//** method to proceed from the survey page
	this.clickOnSurvey = function(){
		//$$//ElementsUtil.waitForElement(this.surveyContyinue);
        this.surveyContyinue.click();		
	};
	
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function(){	
		return ElementsUtil.waitForUrlToChangeTo(/confirmation_activation$/);		
	};		
	//** method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function(){
		//$$//ElementsUtil.waitForElement(this.proceedFromConfirmation);
		this.proceedFromConfirmation.click();
	}; 
	
	//** For auto refill - method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.autoRefillConfirmationPageLoaded = function(){	
		return ElementsUtil.waitForUrlToChangeTo(/purchasesummary$/);		
	};	
	
	this.autoRefilProceedFromConfirmation = function(){
		this.proceedFromConfirmationAutoRefil.click();
	}
	/*this.clickOnDoneBtn = function(){
		ElementsUtil.waitForElement(this.btnPurchaseDone);
		this.btnPurchaseDone.click();
	};*/
	/*//remya
	this.isChkoutPageLoaded = function(){
		ElementsUtil.waitForElement(this.btncontinuetopayment);
		return this.btncontinuetopayment.isPresent();
	
	};*/
	/*this.clickCreditTab = function(){
		ElementsUtil.waitForElement(this.creditTabOption);
		this.creditTabOption.click();
	}*/
	//remya
};

module.exports = new ServiceRenewChkout;