'use strict';
var ElementsUtil = require("../util/element.util");

//=============Serves all functions inside MyAccount============

var MyAccount = function() {
	
//	this.myDevices = element(by.linkText('MY DEVICES'));  //Add Device
	this.manageProfile = element.all(by.linkText('MANAGE PROFILE')).get(0);
	this.paymentMethod = element.all(by.id('paymentmethod')).get(0);
	this.signOut = element(by.linkText('SIGN OUT'));
	this.addDevice = element.all(by.id('Adddevicenav')).get(0);
	this.addDevicePopupBtn = element.all(by.id('btn_adddevice1')).get(1);
	this.activateButton = element(by.css("span[translate='MYAC_99867']"));
	
	this.selectDevice 				= element.all(by.css('[ng-model="$select.search"]')).get(0);
	this.selectActiveMin			= element.all(by.className('dropdown-content-item ng-scope')).get(0);
	this.btnContinuePhonenumber		= element.all(by.id('btn_continuephonenumber')).get(0);
	
	this.manageEnrollBtn = element(by.css("span[translate='MYACC_5377']"));
	this.cancelEnrollBtn = element(by.id('deenroll'));//(by.css("button[id='deenroll']"));
	this.deEnrollProceed = element.all(by.css('[ng-click="action()"]')).get(1);
	
	// this.serviceRenewBtn = element.all(by.buttonText('Pay Service')).get(1);
	/*this.enrolBtn = element.all(by.css("button[id='noEnrollToAutoPay_btn']")).get(1);
	this.serviceRenewBtn = element.all(by.css("span[translate='MYACC_5001']")).get(0);
	this.activateBtn = element.all(by.buttonText('Activate')).get(1);
	this.AutoPayBtn = element.all(by.css("span[translate='MYACC_5000']")).get(1);	
	

	this.continueBtn = element.all(by.buttonText('CONTINUE')).get(1);
	this.buyPlan = element.all(by.buttonText('BUY SERVICE PLAN')).get(1); 	
	this.close = element(by.css("button[title='close']"));
	this.clickPlan = element(by.linkText('SERVICE ADD-ONS')); 
	this.deviceAdded = element.all(by.css("button[id='btn_']")).get(3);
	this.deviceToRemove = element.all(by.css("a[ng-click='editAction()']")).get(2); 
	this.removeDeviceOption = element.all(by.css("a[ng-click='removeDevice()']")).get(1); 
	this.removeConfirm = element.all(by.buttonText('YES')).get(1);
	this.closeBtnDevice = element(by.css("button[title='CLOSE']"));
	// this.popupClose = element(by.css("button[title='close']"));
	this.selectPlan = element.all(by.css("button[id='btn_plancardaddonaddtocart']")).get(5);	
	this.continueToCheckout = element.all(by.buttonText('Continue to Checkout')).get(1);
	this.newpopupwindow = element.all(by.buttonText('Not this time')).get(1);
	this.serviceRenewBtn = element.all(by.css("span[translate='MYACC_5001']")).get(1); //payservice
	//newly added for tracfone
	this.addAirTimePinBtn = element.all(by.buttonText("ADD AIRTIME")).get(1);
	//this.addAirtimePin =  element.all(by.css("button[id='btn_addairtime']")).get(1);
	this.addAirtimePin =  element.all(by.css("[ng-click='action()']")).get(1);
	
	this.goToMultilineAutopay = function() {
		//$$//ElementsUtil.waitForElement(this.AutoPayBtn);
		return this.AutoPayBtn.click();
	};
	
	this.clickAddAirtime = function() {
		//$$//ElementsUtil.waitForElement(this.addAirtimePin);
		return this.addAirtimePin.click();
	};
	
	this.payService = function(){  //payservice
		//$$//ElementsUtil.waitForElement(this.serviceRenewBtn);
		return this.serviceRenewBtn.click();
	};
	
	this.enroll = function(){  
		return this.enrolBtn.click();
	};
	
	this.clickContinue = function() {
		//$$//ElementsUtil.waitForElement(this.continueBtn);
		return this.continueBtn.click();
	};
	
	this.goToAllOptions = function() {
		//$$//ElementsUtil.waitForElement(this.allOptions);
		return this.allOptions.click();
	};
	
	this.chooseBuyPlan = function() {
		ElementsUtil.waitForElement(this.buyPlan);
		return this.buyPlan.click();
	}; 
	
	this.selectAPlan = function() {
		ElementsUtil.waitForElement(this.clickPlan);
		this.close.click();
		this.clickPlan.click();
		// this.popupClose.click();
		return this.selectPlan.click();	
	}; 
	
	this.goToSelectPlan = function() {
		//$$//ElementsUtil.waitForElement(this.selectPlan);
		this.selectPlan.click();	
		return this.continueToCheckout.click();
	}; 
	
	this.goToMyDevices = function() {
		//$$//ElementsUtil.waitForElement(this.myDevices);
		return this.myDevices.click();
	};
	*/
	
	this.selectActivatedMin = function(){
		ElementsUtil.waitForElement(this.selectDevice);
		this.selectDevice.click();
		this.selectActiveMin.click();
		this.btnContinuePhonenumber.click();
	};
	
	this.goToManageProfile = function() {
		ElementsUtil.waitForElement(this.manageProfile);
		return this.manageProfile.click();
	};
	this.goToPaymentMethod = function() {
		ElementsUtil.waitForElement(this.paymentMethod);
		return this.paymentMethod.click();
	};
	/*
	this.goToPaymentMethod = function() {
		ElementsUtil.waitForElement(this.paymentMethod);
		return this.paymentMethod.click();
	};
	
	this.goToPaymentHistory = function() {
		//$$//ElementsUtil.waitForElement(this.paymentHistory);
		return this.paymentHistory.click();
	};*/
	
	this.goToAddDevice = function() {
		//ElementsUtil.waitForElement(this.addDevice);
	    this.addDevice.click();
	    ElementsUtil.waitForElement(this.addDevicePopupBtn);
	    this.addDevicePopupBtn.click();
	    
	};
	
	this.goToSignOut = function() {
		ElementsUtil.waitForElement(this.signOut);//needed for byop activation scenario
		return this.signOut.click();
	};
	/*
	this.removeDevice = function() {
		//$$//ElementsUtil.waitForElement(this.deviceToRemove);
		this.deviceToRemove.click();
		// ElementsUtil.waitForElement(this.removeDeviceOption);
		this.removeDeviceOption.click();
		//$$//ElementsUtil.waitForElement(this.removeConfirm);
		this.removeConfirm.click();
		ElementsUtil.waitForElement(this.closeBtnDevice);
		return this.closeBtnDevice.click(); 
	};
	
	this.isDeviceRemoved = function() {
		return browser.wait(function() {
			return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /dashboard$/.test(url);
		});
		}, 10000, "URL hasn't changed");
	}; 
	*/

	//** method checks whether the account dashboard is loaded or not
	this.isLoaded = function(){
		//ElementsUtil.waitForElement(this.deviceAdded);
		return browser.wait(function() {
			return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /dashboard$/.test(url);
		});
		}, 10000, "URL hasn't changed");
	};
	
	//** method to click on the Activate button in dashboard under inactive devices -- Reactivation scenario
	this.clickOnActivateBtn = function(){
		this.activateButton.click();
	};
	
	this.refresh = function() {
		return browser.getCurrentUrl().then(function (url) {
					console.log('Currenturl: ', url);
                    return browser.get(url);
                });
        };

      //** method to check whether the checkout page is loaded or not
    	this.checkoutPageLoaded = function(){
    		return ElementsUtil.waitForUrlToChangeTo(/checkout$/);	
    	};
    	
	/*
	//** method to proceed with the pay service flow
	this.payService = function(){
		//$$//ElementsUtil.waitForElement(this.serviceRenewBtn);
		this.serviceRenewBtn.click();

	
	//** method to click on the activate button, after adding a device through the dashboard
	this.clickOnActivate = function() {
		this.activateBtn.click();
	};
	
	//** method to check whether  a popup is dispalyed after clicking the pay service button in the account dashboard
	this.newPopupLoaded = function(){
		//$$//ElementsUtil.waitForElement(this.newpopupwindow);
		return this.newpopupwindow.isPresent();
	};
	
	//** method to proceed from the popup page
	this.continueFromPopUp = function(){
		//$$//ElementsUtil.waitForElement(this.newpopupwindow);
		this.newpopupwindow.click();
	};
	
	//** method to click on the Add Airtime Pin button in dashboard
	this.clickOnAddAirtimePin = function(){
		//$$//ElementsUtil.waitForElement(this.addAirTimePinBtn);
		this.addAirTimePinBtn.click();
	};
	*/
	
	//method to click on manage enroll button
	this.clickOnManageEnroll = function(){
		this.manageEnrollBtn.click();
	};
	
	this.cancelEnrollPageLoaded = function(){
		return this.cancelEnrollBtn.isPresent();
	};
	
	this.cancelEnroll = function(){
		this.cancelEnrollBtn.click();
	};
	
	this.selectDeenrollReason = function(){
		element(by.model('data.reason')).$('[value="not needed"]').click();
		this.deEnrollProceed.click();
	};
};

module.exports = new MyAccount;
