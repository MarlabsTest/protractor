/*
 * **********Spec file is for REACTIVATING A DEVICE ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.Once the device get deactivated,it will be listed under inactive devices in myaccount dashboard.
 * 4.Add service plan by clicking on the all options link in dashboard (Add service PIN)
 * 5.we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 
 */

'use strict';
var activation = require("../../activation/activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};
var generatedPin ={};

describe('Net10 Phone ReActivation', function() {	
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should deactivate the device', function(done) {	
        var min = DataUtil.getMinOfESN(sessionData.netten.esn);
        console.log('minVal ,    ::'+min);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.netten.esn,min,'PASTDUE','NET10');	
		generatedMin['min'] = min;
		home.homePageLoad();
		home.myAccount();
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}).result.data = generatedMin;	 
	
	/*Click on the service plan link inside the All Options
	 *Expected result - Redirected to the page to enter a new service pin  
	 */
	it('should navigate to renew service page', function(done) {
		myAccount.clickOnAllOptions();
		myAccount.clickOnservicePlanLink();
		expect(myAccount.renewServicePageLoaded()).toBe(true);
		done();		
	});
	
	/*Enter service pin
	 *Expected result - Redirected to confirmation page  
	 */
	it('should enter navigate to redemption confirmation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.netten.pinPartNumber);
       	console.log('returns', pinval);		
       	sessionData.netten.pin = pinval;
		generatedPin['pin'] = pinval;
		myAccount.enterAirtimePinRenew(pinval);
		expect(myAccount.redemptionSuccessPage()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	/*Continue from the confiramtion page
	 *Expected result - Redirected to my account dashboard page  
	 */
	it('should enter click on the proceed button and will be navigated to dashoard page', function(done) {
		myAccount.clickOnContinueBtnFromConfirmation();
		//db call for inserting redemption record into itq_dq_check table
		DataUtil.checkRedemption(sessionData.netten.esn,sessionData.netten.pin,'401','NET10_Redeem_Pin','false','true');
		console.log('ITQ_DQ_CHECK table updated!!');
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
