'use strict';

var homePage = require("../../common/homepage.po");
var sessionData = require("../../common/sessiondata.do");
var myAccount = require("../myaccount.po");


describe('Net10 buy ILD plan inside account', function() {

	//click on the shop link which is shown in the homepage
	it('Go to dashboard page', function(done) {
		homePage.myAccount();
		expect(myAccount.isLoaded()).toBe(true);	
		done();
	});
	
	
	//click on the shop link which is shown in the homepage
	it('Go to cancel de enroll page', function(done) {
		myAccount.clickOnManageEnroll();
		expect(myAccount.cancelEnrollPageLoaded()).toBe(true);	
		done();
	});
	
	it('Select reason to de enroll', function(done) {
		myAccount.cancelEnroll();
		myAccount.selectDeenrollReason();
		expect(myAccount.isLoaded()).toBe(true);	
		done();
	});
	
	
	
});