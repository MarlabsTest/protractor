'use strict';

var activation = require("../../activation/activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};
var generatedEsnSim = {};

describe('Net10 Add device to myaccount', function() {
	it('click on add line menu from My Account page', function(done) {		
		myAccount.goToAddline();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on the continue button and it will navigates to Sim page',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {
	 	//console.log('returns'+ sessionData.netten.esnPartNumber);
	 	//console.log('returns'+ sessionData.netten.simPartNumber);
		var esnval = DataUtil.getESN(sessionData.netten.esnPartNumber);
       	console.log('returns', esnval);
		
		
		sessionData.netten.esn = esnval;
		sessionData.netten.esnsToReactivate.push(esnval);
		generatedEsn['esn'] = sessionData.netten.esn;
		
		activation.enterEsn(esnval);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter the sim number and click the continue button', function(done) {
		
		var simval = DataUtil.getSIM(sessionData.netten.simPartNumber);
       	console.log('returns', simval);
       	sessionData.netten.sim = simval;		
       	generatedSim['sim'] = sessionData.netten.sim;
		activation.enterSIM(simval);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.netten.zip);		
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it(' enter airtime pin and navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.netten.pinPartNumber);
       	console.log('returns Pin value', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.netten.pin = pinval;
		generatedPin['pin'] = sessionData.netten.pin;
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	
	
	it('click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnSummaryBtn();
//		DataUtil.activateESN(sessionData.netten.esn);
//		console.log("update table with esn :"+sessionData.netten.esn);
//		DataUtil.updateMin(sessionData.netten.esn);
//		var min = DataUtil.getMinOfESN(sessionData.netten.esn);
//		console.log("MIN is  :::::" +min);
		sessionData.netten.upgradeEsn = sessionData.netten.esn;//specific to phone upgrade
		//check_activation		
//		DataUtil.checkActivation(sessionData.netten.esn,sessionData.netten.pin,'1','SIMPLE_MOBILE_ACTIVATION_WITH_PIN');
//		generatedMin['min'] = min;
//		console.log("ITQ table updated");			
		expect(myAccount.isLoaded()).toBe(true);		
		done();	
	}).result.data = generatedMin;

	// });
	
});
