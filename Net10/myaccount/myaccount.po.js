'use strict';

var myAccount = require("./myaccount.pf");
var resetPassword = require("./forgotpassword.pf");
var manageProfile = require("./manageprofile.pf");

var serviceRenewal = require("./checkout.pf");
var managePayment = require("./managepayment.pf");
/*
//var managePayment = require("./managepayment.pf");

var managePayment = require("./managepayment.pf");*/
//var addDevice = require("./adddevice.pf");
var optionsQuickLinks = require("./optionquicklink.pf");
var addservicepin = require("./addservicepin.pf");
//var cardPayment = require("../common/creditcardpayment.pf");
//var cardPayment = require("../common/paymentcheckout.pf");
//var activeDevices = require("./activedevices.pf");
//var addDeviceModal = require("./adddevicemodal.pf");
//var resetUser = require("./forgotusername.pf");



var MyAccount = function() {
	
	this.signOut = function() {
		return myAccount.goToSignOut();
	};
	
	//method to click on the forgot password link
	this.clickOnForgotPassword = function(){
		return resetPassword.clickOnForgotPassword();
	};
	
	//method to load the page which has forgot password link
	this.isForgotPasswordPageLoaded = function(){
		return resetPassword.isForgotPasswordPageLoaded();
	};
	
	//method to provide the username which is already there in the simple mobile
	this.enterTheUserName = function(userName){
		return resetPassword.enterTheUserName(userName);
	};
	
	//method to display the message in pop up window saying that "reset password is sent to your provided email"
	this.popUpWindow = function(){
		return resetPassword.popUpWindow();
	};
	
	this.manageProfile = function() {
		return myAccount.goToManageProfile();
	};

	this.isManageProfileLoaded = function() {
	return manageProfile.isLoaded();
	}; 
	
	this.editContactInfo = function() {
		manageProfile.goToEditContactInfo();
	};
	
	this.isContactEdited = function() {
		return manageProfile.isContactEdited();
		};	
	this.paymentMethod = function() {
		return myAccount.goToPaymentMethod();
	};
	this.isManagePaymentLoaded = function() {
		return managePayment.isLoaded();
	};
	this.editPaymentDetails = function() {
		return managePayment.goToEditPaymentInfo();
	};
	this.isPaymentAdded = function() {
		return managePayment.isPaymentAdded();
	};
	//method to delete the saved card details
	this.deleteCardDetails = function(){
		return managePayment.deleteCardDetails();
	};
	/*
	this.addDevice = function() {
		return myAccount.goToAddDevice();
	};
	
	//method to click on the button which is there in popup window
	this.PopUpWindowClick = function(){
		return resetPassword.PopUpWindowClick();
	}
	
	/*
	this.AutopayCheckout = function() {
		return myAccount.goToMultilineAutopay();
	};
	
	this.clickAddAirtime = function() {
		return myAccount.clickAddAirtime();
	};
		
	this.payService = function(){
		myAccount.payService();
	};
	
	this.enroll= function(){
		myAccount.enroll();
	};
	
	//Method to click on Bank Account tab in Payment - for reactivation with ACH paurchase scenario
	this.clickOnBankAccTab = function(){
		serviceRenewal.clickOnBankAccTab();
	};
	
	//method to enter ACH payment details: for reactivation with ACH purchase scenario
	this.enterAchDetails = function(accno,routingno){
		serviceRenewal.enterAchDetails(accno,routingno);
	};	
		
	this.checkout = function() {
		return cardPayment.goToPayCheckout();
	};
	
	this.goToPaymentTab = function() {
		return serviceRenewal.goToPaymentTab();
	};
	
	this.isSuccessfulCheckout = function() {
		return cardPayment.goToSuccessfulCheckout();
	};
	
	this.clickContinue = function() {
		return myAccount.clickContinue();
	};
	
	this.allOptions = function() {
		return myAccount.goToAllOptions();
	};
	
	this.chooseBuyPlan = function() {
		return myAccount.chooseBuyPlan();
	};
	
	this.isbuyPlanPageLoaded = function() {
		return myAccount.isBuyPlanLoaded();
	};
	
	this.selectAPlan = function() {
		return myAccount.selectAPlan();
	};
	
	this.myDevices = function() {
		return myAccount.goToMyDevices();
	};
	
	
	this.paymentHistory = function() {
		return myAccount.goToPaymentHistory();
	};
	
	
	
	this.isAddDeviceModalShown = function() {
		return addDeviceModal.isAddDeviceModalShown();
	}
	
	this.addInActiveEsn = function(esn) {
		addDeviceModal.addInActiveEsn(esn);
	};
	
	this.isInActivceEsnAdded = function() {
		return addDeviceModal.isAddDeviceSuccessModalShown();
	}
	
	this.closeAddDeviceSuccessModal = function() {
		addDeviceModal.closeAddDeviceSuccessModal();
	}
	
	this.isManagePaymentLoaded = function() {
	return managePayment.isLoaded();
	};
	
	this.paymentMethod = function() {
		return myAccount.goToPaymentMethod();
	};
	

	this.editPaymentDetails = function() {
		return managePayment.goToEditPaymentInfo();
	};
	
	
	this.isPaymentAdded = function() {
		return managePayment.isPaymentAdded();
	};
	
	/*
		
	this.clickActivateAddedDevice = function() {
		addDevice.clickActivateAddedDevice();
	};
	
	this.newDeviceActivation = function(simval,pinval,zip) {
		addDevice.newDeviceActivation(simval,pinval,zip);
	};
	
	this.removeDevice = function() {
		myAccount.removeDevice();
	};
	
	this.isDeviceRemoved = function() {
		return myAccount.isDeviceRemoved();
	};
	
	this.refresh = function() {
		myAccount.refresh();
	};
	this.isNewDeviceAdded = function(esn) {
		addDevice.isDeviceAdded(esn);
	};
	*/
	
	//** method checks whether the account dashboard is loaded or not
	this.isLoaded = function(){
		return myAccount.isLoaded();
	};
	
	//** method to check whether the checkout page is loaded or not
	this.checkoutPageLoaded = function(){
		return myAccount.checkoutPageLoaded();
	};
	
	this.selectActivatedMin = function(){		
		return myAccount.selectActivatedMin();
	};
	
	//** method to check whether the checkout page is loaded or not
	this.isAutoRefillCheckoutPageLoaded = function(){
		return serviceRenewal.isAutoRefillCheckoutPageLoaded();	
	};
	
	//** method to check whether the payment form for entering the cc and billing details are displayed or not
	this.switchCheckOutWithAutoRefill = function(){
		return serviceRenewal.switchCheckOutWithAutoRefill();
	};

	//** method to check whether the payment form for entering the cc and billing details are displayed or not	
	this.paymentOptionLoaded = function(){
		return serviceRenewal.paymentOptionLoaded();
	};
	
	//** method to enter billing details
	this.enterBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		serviceRenewal.enterBillingDetails(fname,lname,address1,houseNum,city,zipcode);
	};
	
	//** method to agree billing zipcode change popup
	this.agreeBillingZipcodeChanges = function(){
		serviceRenewal.agreeBillingZipcodeChanges();
	};
	
	//** method to check billing zipcode change popup shown
	this.isBillingZipcodeChangesPopupShown = function(){
		return serviceRenewal.isBillingZipcodeChangesPopupShown();
	};
	
	//** method to click place my order button
	this.placeMyOrder = function(){
		serviceRenewal.placeMyOrder();
	};
	
	//check toggle status of auto refill is yes
	this.isAutoRefillStatusYes = function(){
		return serviceRenewal.isAutoRefillStatusYes();
	};
	
	//** method to click on the continue to payment button in the checkout page
	this.continueToPayment = function(){
		serviceRenewal.continueToPayment();
	};
	
	//** method to enter cc details
	this.enterCcDetails = function(ccNum,cvv){
		serviceRenewal.enterCcDetails(ccNum,cvv);
	};
	
	//** method to enter cc details (autoreup flow specific)
	this.enterAutoReupCcDetails = function(ccNum,cvv){
		serviceRenewal.enterAutoReupCcDetails(ccNum,cvv);
	};
	this.enterReupCcDetails = function(ccNum,cvv){
		serviceRenewal.enterReupCcDetails(ccNum,cvv);
	};	
	//** method to enter billing details (autoreup flow specific)
	this.enterAutoReupBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		serviceRenewal.enterAutoReupBillingDetails(fname,lname,address1,houseNum,city,zipcode);
	};
	this.enterReupBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		serviceRenewal.enterReupBillingDetails(fname,lname,address1,houseNum,city,zipcode);
	};
	this.goToAddline = function() {
		myAccount.goToAddDevice();
	};
	
	//** method is to click on the All Options link
	this.clickOnAllOptions = function(){
		optionsQuickLinks.clickOnAllOptions();
	};
	
	//** method is to proceed with the add service plan flow
	this.clickOnservicePlanLink = function(){
		addservicepin.clickOnservicePlanLink();
	};
	
	//** method checks whether the renew service plan page is loaded or not
	this.renewServicePageLoaded = function(){
		return addservicepin.renewServicePageLoaded();
	}
  
	//** method to enter new pin 
	this.enterAirtimePinRenew = function(pin){
		return addservicepin.enterAirtimePinRenew(pin);
	};
  
	//** method to check the redemption confirmation page is loaded after entering the new pin
	this.redemptionSuccessPage = function(){	
		return addservicepin.redemptionSuccessPage(); 
	};
	
	//** method to proceed from the confirmation page
	this.clickOnContinueBtnFromConfirmation = function(){
		addservicepin.clickOnContinueBtnFromConfirmation();
	};
	
	//** method to click on the Activate button in dashboard under inactive devices -- Reactivation scenario
	this.clickOnActivateBtn = function(){
		myAccount.clickOnActivateBtn();
	};
	
	/*
	this.isDeviceAdded = function(){
		return addDevice.isDeviceAdded();
	};
	
	
  
	
	//** method to proceed with the pay service flow
	this.payService = function(){
		myAccount.payService();
	}
	
	this.clickNewPayment = function(){
		serviceRenewal.clickNewPayment();
	};
	
	//** method to enter ach billing details
	this.enterAchBillingDetails = function(fname,lname,address1,houseNum, city, zipcode){
		serviceRenewal.enterAchBillingDetails(fname,lname,address1,houseNum, city, zipcode);
	};
	
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function(){
		return serviceRenewal.confirmationPageLoaded();
	};
	
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.autoRefillConfirmationPageLoaded = function(){
		return serviceRenewal.autoRefillConfirmationPageLoaded();
	};
	
	//** method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function(){
		serviceRenewal.proceedFromConfirmationPage();
	};
	//** method to proceed from check out in Auto Refill option
	this.autoRefilProceedFromConfirmation = function(){
		serviceRenewal.autoRefilProceedFromConfirmation();
	}
	
	//** method to click on the activate button, after adding a device through the dashboard
	this.clickOnActivate = function() {
		myAccount.clickOnActivate();
	};
	/*
	//** method to proceed from the survey page
	this.clickOnSurvey = function(){
		serviceRenewal.clickOnSurvey();
	};

	//** method to check whether  a popup is dispalyed after clicking the pay service button in the account dashboard
	this.newPopupLoaded = function(){		
		return myAccount.newPopupLoaded();
	};
	
	//** method to proceed from the popup page
	this.continueFromPopUp = function(){
		return myAccount.continueFromPopUp();
	};
	
	//** method to click on the Add Airtime Pin button in dashboard
	this.clickOnAddAirtimePin = function(){
		myAccount.clickOnAddAirtimePin();
	};
	//** method to click check whether the page to enter the airtime pin is loaded or not
	this.enterAirtimePinPageLoaded = function(){
		return addservicepin.enterAirtimePinPageLoaded();
	};
	//** method to enter Airtime Pin 
	this.enterAirtimePin = function(pin){
		addservicepin.enterAirtimePin(pin);
	};
	//** method to proceed from Airtime pin page
	this.proceedFromAirTimePage = function(){
		addservicepin.proceedFromAirTimePage();
	};
	//** method to check whether the redemption confirmation page is loaded or not
	this.redemptionConfirmationPageLoaded = function(){
		return addservicepin.redemptionConfirmationPageLoaded();
	};
	//** method to continue from the confirmation page
	this.clickOnContinueFromConfirmation = function(){
		addservicepin.clickOnContinueFromConfirmation();
	};

	/*
	this.removeEsn = function() {
		activeDevices.removeEsn();
	}
	
	this.isEsnRemoved = function() {
		return activeDevices.isEsnRemoved();
	}
	
	this.closeEsnRemovalSuccessPopup = function() {
		return activeDevices.closeEsnRemovalSuccessPopup();
	}
	
	//***********checks wheather the forgot username homepage loaded***********
	this.resetUserIsLoaded = function() {
		return resetUser.isHomePageLoaded();
	};
	
	this.continueWithForgotUsername = function(){
		return resetUser.continueWithForgotUsername();
	};
	
	// asks us to enter the sim number
	this.gotToPage= function(sim) {
		return resetUser.gotToRetrievePage(sim);
	};
	
	
	this.retrievePageLoaded=function(){
		return resetUser.retrieveUsernamePageLoaded();
	};
	
	//asks us to enter the pin number to reset the username.
	this.goToPinPage = function(pin) {
		return resetUser.goToPinPage(pin);
	}
	
	this.securityPinPageLoaded = function() {		
		return resetUser.securityPinPageLoaded();
	};
	
	//pop up window with log in button comes for navigating to the homepage
	this.popUpWindowForResetUser = function(){
		return resetUser.popUpWindow();
	};
	
	//method to delete the saved card details
	this.deleteCardDetails = function(){
		return managePayment.deleteCardDetails();
	};
	//checks whether the payment page is loaded or not
	this.paymentPageIsLoaded = function(){
		return managePayment.paymentPageIsLoaded();
	};
	
	this.deleteCardDetails =function(){
		return managePayment.deleteCardDetails();
	}
	*/
	
	this.clickOnManageEnroll = function(){
		myAccount.clickOnManageEnroll();
	};
	
	this.cancelEnrollPageLoaded = function(){
		return myAccount.cancelEnrollPageLoaded();
	};
	
	this.cancelEnroll = function(){
		myAccount.cancelEnroll();
	};
	
	this.selectDeenrollReason = function(){
		myAccount.selectDeenrollReason();
	};
	
};


module.exports = new MyAccount;
