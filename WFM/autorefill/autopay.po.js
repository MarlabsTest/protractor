'use strict';

/*
This PO will Do Enrolment of user for Auto Pay
*/

var autoPay = require("../autorefill/autopay.pf");
var AutoPayPO = function() {
   	
		/*
		 This function will click on Auto Pay link in home page
		*/
	   this.goToPayServiceAndAutoPay = function() {
		return autoPay.goToPayServiceAndAutoPay();
	  };
	 	 
	/*
	* This function will return true on success redirection of page to specified url
	*/		 
	   this.isPayServiceAndAutoPayLoaded = function() {
		return autoPay.isPayServiceAndAutoPayLoaded();
	  };
	  
	  
	/*
	 *Parameters : MailId
	 *This function will send input to mailId text field and clicks on continue button
	*/
	  this.enterPhoneNumberOrMailId = function(mailId) {
		return autoPay.enterPhoneNumberOrMailId(mailId);
	  };
	  
	   /*
		* This function will return true on success redirection of page to specified url
		*/
	  this.isPhoneNumberOrMailIdLoaded = function() {
		return autoPay.isPhoneNumberOrMailIdLoaded();
	  };
	  
	  /*
	*Parameters : password
	*This function will send input to password text field and clicks on continue button
	*/
	  this.enterPasswordForAutoPay = function(password) {
		return autoPay.enterPasswordForAutoPay(password);
	  };
	  
	  
	  /*
	* This function will return true on success redirection of page to specified url
	*/
	  this.isPasswordForAutoPayLoaded = function() {
		return autoPay.isPasswordForAutoPayLoaded();
	  };
	  
	  /*
	*This function will click on continue button after successful login
	*/
	  this.clickOnContinueBtn=function(){
		return autoPay.clickOnContinueBtn();
	  }
	  
	  /*
	*This function will click on "Continue to Payment" button in checkout page
	*/
	  this.clickOnContinuetoPaymentbttn = function() {
		return autoPay.clickOnContinuetoPaymentbttn();
	  };
	  
	
	  /*
	*This function will click on "New Payment" tab in checkout page
	*/
	  this.clickOnDoneBtn=function(){
		return autoPay.clickOnDoneBtn();
	  }
	  
	  
	  /*
		* This function will return true on success redirection of page to specified url
		*/
	  this.isPaymentDone = function() {
		return autoPay.isPaymentDone();
	  };
	  

	  /*
		* This function will return true on success redirection of page to specified url
		*/
	  this.isServeyPageLoaded = function() {
			return autoPay.isServeyPageLoaded();
		  };
		  
};

module.exports = new AutoPayPO;