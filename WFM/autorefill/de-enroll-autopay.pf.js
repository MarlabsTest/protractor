'use strict';
var ElementsUtil = require("../util/elements.util");

	/*	
	This PF will De Enroll user from Auto Pay service	
	*/

var DeEnrollAutoPay = function() {
	this.cancelAutorefilLink =  element(by.id('enrolled_yes'));
	this.reasonForDeEnrollDropDown = element(by.id('deenroll-selectedreason')).$('[value="Not needed"]');
	this.clickOnYesBtnForDeEnroll = element(by.xpath("//*[@id='modal-body']/div[2]/form/div[5]/div/div[3]/tf-button-primary/span[1]/button"));
	//this.autoPayButtn = element.all(by.buttonText('Auto Pay')).get(1);
	this.autoPayButtn = element.all(by.css('[ng-click="action()"]')).get(0);
                                                        
	/*
		This function will click on "Cancel Auto Pay Enrollment" link .
	*/
	this.clickOnCancelAutorefil = function(){
		ElementsUtil.waitForElement(this.cancelAutorefilLink);
		 this.cancelAutorefilLink.click();	
	};
	
	/*
		This function will select reason for De Enrollment of Auto pay from drop down 
	*/
	
	this.selectReasonForCancelAutorefil = function(){	
		ElementsUtil.waitForElement(this.reasonForDeEnrollDropDown,10000);
		 this.reasonForDeEnrollDropDown.click();
		 this.clickOnYesBtnForDeEnroll.click();
	};
	
	/*
	This function will check whether user De-Enrolled Auto pay or not 
*/
	this.isAutorefillDeEnrolled = function(){		
		return ElementsUtil.waitForUrlToChangeTo(/dashboard$/);
	};
};

module.exports = new DeEnrollAutoPay;