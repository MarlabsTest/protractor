'use strict';

	/*
		This PF will De Enroll user from Auto Pay service	
	*/
	
var deEnrollAutoPay = require("../autorefill/de-enroll-autopay.pf");
var DeEnrollAutoPayPO = function() {

	/*
		This function will click on "Cancel Auto Pay Enrollment" link .
	*/
	this.clickOnCancelAutorefil = function(){
		deEnrollAutoPay.clickOnCancelAutorefil();	
	};
	
	
	/*
		This function will select reason for De Enrollment of Auto pay from drop down 
	*/	
	this.selectReasonForCancelAutorefil = function(){
		 deEnrollAutoPay.selectReasonForCancelAutorefil();
	 };
	 
	 /*
		This function will check whether user De-Enrolled Auto pay or not 
	*/
	 this.isAutorefillDeEnrolled = function(){
		 deEnrollAutoPay.isAutorefillDeEnrolled();
	 };
	 
};

module.exports = new DeEnrollAutoPayPO;