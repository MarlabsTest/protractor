'use strict';

var autoPay = require("../autopay.po");
var sessionData = require("../../common/sessiondata.do");
var activation = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var dataUtil= require("../../util/datautils.util");
describe('Enrolling for Autopay service : ', function() {

	/*
	*This scenario will click on Auto Pay link in home page and checks whether
	*page is redirected to expected url
	*/

	it('should click on pay service and select Auto pay ',function(done){		
			autoPay.goToPayServiceAndAutoPay();
			expect(autoPay.isPayServiceAndAutoPayLoaded()).toBe(true);
			done();
		});
	
	/*
		This scenario will send  mailId  or phone number and click on continue button
		checks whether page is redirected to expected url
	*/
	it('should enter mailId  or phone number and click on continue button',function(done){		
		autoPay.enterPhoneNumberOrMailId(dataUtil.getMinOfESN(sessionData.wfm.esn));
		expect(autoPay.isPhoneNumberOrMailIdLoaded()).toBe(true);
		done();
	});
	
	/*
	This scenario will send  password and click on continue button
		checks whether page is redirected to expected url
	*/
	it('should enter password and login to account',function(done){		
		autoPay.enterPasswordForAutoPay(sessionData.wfm.password);
		expect(autoPay.isPasswordForAutoPayLoaded()).toBe(true);
		done();
	});

	/*
	 * Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 *
	 */	
	it('should load the payment form', function(done) {
		myAccount.continueToPayment();	
		myAccount.enterCcDetails("4012888888881881","123");
		myAccount.enterBillingDetails("Test","Test","1295 Charleston Road"," ","mountain view","94043");
		expect(autoPay.isPaymentDone()).toBe(true);
		done();		
	});

	/*
	* This scenario will click on  "Done" button after successful payment
	*/
	
	it('should click on done button after successful payment',function(done){		
			autoPay.clickOnDoneBtn();
			expect(autoPay.isServeyPageLoaded()).toBe(true);
			
			done();
		});

	it('should click on the thank you button and will be redirected to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();	
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(sessionData.wfm.esn,'0','6','WFM_Purchase','true','false');
		console.log('ITQ_DQ_CHECK table updated!!');	
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
		
});
