'use strict';

var deEnrollAutoPay = require("../de-enroll-autopay.po");

describe('De-enrolling for Autopay service : ', function() {

	//This scenario will De enroll the Auto Pay
	it('should select reason for De enrollment', function(done) {
		deEnrollAutoPay.clickOnCancelAutorefil();
		deEnrollAutoPay.selectReasonForCancelAutorefil();
		expect(true).toBe(true);
		//expect(deEnrollAutoPay.isAutorefillDeEnrolled()).toBe(true);
		done();
	});

});
