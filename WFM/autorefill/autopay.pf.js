'use strict';
var ElementsUtil = require("../util/elements.util");
var sessionData = require("../common/sessiondata.do");
	/*
	This PF will Do Enrolment of user for Auto Pay
	*/

var AutoPay = function() {
	this.autoReUpMenu = element.all(by.id("lnk_PayService")).get(0);
	this.autoReUp = element(by.id("nav-Sign_Up_for_Auto-Refill"));
	this.refilllink = element(by.id('REFILL'));
	this.enterNumber = element.all(by.id("refill-account")).get(1);
	this.continueBtnForAutoPay = element(by.buttonText('Continue'));
	this.passwordForAutoPay = element.all(by.xpath("//*[@id='wfmloginpinpwd-device']")).get(1);
	this.continueBtnForPassword = element.all(by.id('btn_continue23')).get(0);
	this.continueBtn  = element.all(by.buttonText('Continue')).get(0);
	this.doneBtn = element(by.id('done'));
	
	/*
	*This function will click on Auto Pay link in home page
	*/
	this.goToPayServiceAndAutoPay = function(){
		ElementsUtil.hoverAndSelect(this.autoReUpMenu, this.autoReUp);
	};
	
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isPayServiceAndAutoPayLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/autorefill$/);
	};

	/*
	*Parameters : mailId
	*This function will send input to mailId text field and clicks on continue button
	*/
  
	this.enterPhoneNumberOrMailId=function(mailId){
		this.enterNumber.sendKeys(mailId);	
		this.continueBtnForAutoPay.click();
	};
	
	
	/*
	* This function will return true on success redirection of page to specified url
	*/	
		this.isPhoneNumberOrMailIdLoaded = function() {	
			return ElementsUtil.waitForUrlToChangeTo(/autorefill$/);
	};
	
	/*
	*Parameters : password
	*This function will send input to password text field and clicks on continue button
	*/
	this.enterPasswordForAutoPay=function(password){
		this.passwordForAutoPay.sendKeys(password);	
		this.continueBtnForPassword.click();		
	};
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isPasswordForAutoPayLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/checkout$/);
	};
  
	/*
	*This function will click on continue button after successful login
	*/
	
	this.clickOnContinueBtn=function(){
		this.continueBtn.click();
	};
	
	/*
	* This function will click on  "Done" button after successful payment
	*/
	this.clickOnDoneBtn = function(){
		ElementsUtil.waitForElement(this.doneBtn);
		this.doneBtn.click();
	};
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isPaymentDone = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/confirmation_activation$/);
	};
	
	/*
	* This function will return true on success redirection of page to specified url
	*/
	this.isServeyPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/redirectsurvey?/);
	};
	
};

module.exports = new AutoPay;