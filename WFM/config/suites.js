module.exports = {
	suites: {
		Activation: [
			"activation/scenario/activation.byopphone.withpin.scn.js",
			"activation/scenario/activation.familyphone.withinvalidpin.scn.js",
			"activation/scenario/activation.familyphone.withsurvey.scn.js",
			"activation/scenario/activation.familyphone.withpurchase.scn.js",
			"activation/scenario/activation.internal.portin.withpin.scn.js",
			"activation/scenario/activation.internal.portin.withpurchase.scn.js",
			"activation/scenario/activation.withport.withpin.scn.js",
			"activation/scenario/activation.withport.withpurchase.scn.js",
			"activation/scenario/reactivate.familyphone.withpin.scn.js",
			"activation/scenario/reactivate.familyphone.withpurchase.scn.js",
			"activation/scenario/activation.upgrade.flows.scn.js",
			"activation/scenario/sanity.scn.js",
			"activation/scenario/multiline.reactivation.scn.js",
			"activation/scenario/single.line.activation.enrollment.scn.js",
			"activation/scenario/activation.add.service.plan.scn.js",
		]
		,
		AutoRefill: [
			"autorefill//scenario/autorefill.deenroll.scn.js",
			"autorefill/scenario/autorefill.scn.js"
		],
		Myaccount: [
			"myaccount/scenario/myaccount.login.withsecondarymin.scn.js",
			"myaccount/scenario/myaccount.addnewesn.scn.js",
			"myaccount/scenario/myaccount.buyaddon.withinaccount.scn.js",
			"myaccount/scenario/myaccount.editcontactinfo.scn.js",
			"myaccount/scenario/myaccount.forgotpassword.scn.js",
			"myaccount/scenario/myaccount.login.withemail.scn.js",
			"myaccount/scenario/myaccount.login.withmin.scn.js",
			"myaccount/scenario/myaccount.managepayment.scn.js",
			"myaccount/scenario/myaccount.delete.managepayment.scn.js",
			"myaccount/scenario/myaccount.service.renewal.scn.js"
		],
		Refill: [
			"refill/scenario/refill.payservice.outsideaccount.withpin.scn.js",
			"refill/scenario/refill.payservice.outsideaccount.withpurchase.scn.js"
		],
		Shop: [
			"shop/scenario/shop.buyserviceplan.outsideaccount.newuser.scn.js",
			"shop/scenario/shop.buyserviceplan.outsideaccount.scn.js",
			"shop/scenario/shop.buyaddonplan.outsideaccount.scn.js",
			"shop/scenario/shop.buyaddonplan.outsideaccount.newuser.scn.js"
		]
	}
};
