'use strict';

module.exports = {

	generateReport: function(baseReportDir, reportDir) {
		require('./reporter').generateReport(baseReportDir, reportDir);
	}
};