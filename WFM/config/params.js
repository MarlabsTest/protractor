
module.exports = {

	params: {
		environment: 'sitz',
		brand: 'WFM',
		deploymentDir: '',
		reportDir: './reports/',
		reportDirSource: './reports/',
		loginData: 'All', // comma separated csv line numbers / All
		activationData: '1,2,3' // comma separated csv line numbers / All
	}
};