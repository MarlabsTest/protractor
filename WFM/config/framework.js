module.exports = {

	//testing framework (jasmine/cucumber/mocha etc)
	test: 'jasmine',
	
	//framework options - jasmine node options
	options: {
		onComplete: null,
		isVerbose: true,
		showColors: true,
		includeStackTrace: true,
		defaultTimeoutInterval: 75000
	}
};