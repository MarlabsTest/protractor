'use strict';

module.exports = {

	prepareTestData: function() {
		require("../util/data.generator.util").generateTestData();
		var today = new Date();
		global.reportDir = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ',' + today.getHours() + '-' + today.getMinutes() + '-' + today.getSeconds();
	}
};