'use strict';

var menu = require("./menu.pf");
var customerType = require("./customertype.pf");

var HomePage = function() {
	//this.url = 'https://sitz.myfamilymobile.com/';
	
	this.isHomeLoaded = function() {
		return customerType.isLoaded();
	};
	
	this.Continue = function() {
		return customerType.Continue();
	};
  
	this.newCustomer = function() {
		return customerType.goAsNewCustomer();
	};
  
	this.ok = function() {
		return customerType.ok();
	};

	this.load = function(url) {
		browser.get(url);
		//browser.sleep(3000);
	};
  
	this.isLoaded = function() {
		return menu.isLoaded();
	};
  
	this.myAccount = function() {
		return menu.goToMyAccount();
	};
  
	this.isMyAccountLoaded = function() {	
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /accountrecovery$/.test(url); 
		});
	};
  
	this.goToActivate = function() {
		return menu.activate();
	};
  
	this.goToShop = function() {
		return menu.shop();
	};

	this.goToRefill = function() {
		return menu.goToRefill();
	}; 
  
};

module.exports = new HomePage;