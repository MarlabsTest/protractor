'use strict';

var myAccount = require("../../myaccount/myaccount.po");
var login = require("../../login/login.po");

/*
* Spec		: WFM Logout
* Details	: This spec is for logout my account on WFM.
* */
describe('WFM SignOut', function() {

	/*
	 * Should logout my account page. 
	 * */
	it('Should logout my account page', function(done) {
		myAccount.signOut();	
		expect(login.isSignOut()).toBe(true);
		done();
	});
	
});
	