'use strict';

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('../WFM/properties/Protractor.properties');
var homePage = require("../../common/homepage.po");
var Constants = require("../../util/constants.util");

/*
 * Spec		: WFM Home Page
 * Details	: This spec is for loading home page of WFM site.
 * */
describe('WFM HomePage', function() {
	
	/*
	 * Should load the WFM home page to continue other screens. 
	 * if customer type popup is there, should proceed as a new customer and navigated to the homepage
	 * */
	it('should load the WFM home page', function(done) {
		//homePage.load(Constants.ENV[browser.params.environment]);
		homePage.load(properties.get('weburl.'+browser.params.environment));
		element(by.id('collapsenew')).isPresent().then(function(isVisible) {
			if (isVisible) {
				homePage.newCustomer();
				homePage.Continue();
				//homePage.ok();  //element removed as per latest code
				expect(homePage.isLoaded()).toBe(true);
				done();
			} else {
				expect(homePage.isLoaded()).toBe(true);
				done();
			}
		});

	});

});
