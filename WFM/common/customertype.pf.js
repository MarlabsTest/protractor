'use strict';
var ElementsUtil = require("../util/elements.util");

var CustomerType = function() {
	this.newCustomerBtn = element.all(by.id("collapsenew")).get(0);//1
	this.continueBtn = element.all(by.id("btn_wfmsim2")).get(0);//1
	this.okLink = element.all(by.css("[ng-click='redirectHome()']")).get(0); 	
	
	this.goAsNewCustomer = function() {
//			ElementsUtil.waitForElement(this.newCustomerBtn);
		return this.newCustomerBtn.click();
	};
	
	this.isLoaded = function() {
//			ElementsUtil.waitForElement(this.newCustomerBtn);
		 var bflag = this.newCustomerBtn.isPresent(); 
		return true;
	};
	
	this.Continue = function() {
//			ElementsUtil.waitForElement(this.continueBtn);
		return this.continueBtn.click();
	};
  
	this.ok = function() {
//			ElementsUtil.waitForElement(this.okLink);
		return this.okLink.click();
	};

};

module.exports = new CustomerType;
