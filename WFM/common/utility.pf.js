'use strict';
var expectedConditions = protractor.ExpectedConditions; 
var Utility = function() {
	
 this.waitForUrlToChangeTo = function(urlRegex) {
    var currentUrl;
console.log('waitForUrlToChangeTo');
    return browser.getCurrentUrl().then(function storeCurrentUrl(url) {
            currentUrl = url;
			
			console.log('currentUrl******* '+currentUrl);
        }
    ).then(function waitForUrlToChangeTo() {
            return browser.wait(function waitForUrlToChangeTo() {
                return browser.getCurrentUrl().then(function compareCurrentUrl(url) {
				console.log('urlRegex******* '+urlRegex+'  ********   '+url);
                    return urlRegex.test(url);
                });
            });
        }
    );
}

};

module.exports = Utility;

