'use strict';
var ElementsUtil = require("../util/elements.util");

var Menu = function() {

	this.myAccount = element(By.css('a[id="lnk_myaccount"]'));
    this.activateLink = element(by.id('lnk_wfmhome_activate'));	
	this.shopLink = element(by.id('shop'));	
	this.refilllink = element(by.id('REFILL'));
	
	this.goToMyAccount = function() {
//		ElementsUtil.waitForElement(this.myAccount);
		return this.myAccount.click();
	};
	
	this.isLoaded = function() {
//		ElementsUtil.waitForElement(this.myAccount);
		return this.myAccount.isDisplayed();
	};
	
	this.activate = function() {
//		ElementsUtil.waitForElement(this.activateLink);
		return this.activateLink.click();
	};
  
	this.shop = function() {
//			ElementsUtil.waitForElement(this.shopLink);
		return this.shopLink.click();
	};

	this.goToRefill = function() {	
//		ElementsUtil.waitForElement(this.refilllink);
		return this.refilllink.click();
	};
  
};

module.exports = new Menu;
