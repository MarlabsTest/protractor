'use strict';
var ElementsUtil = require("../util/elements.util");
var generator = require('creditcard-generator');
var CCUtil = require("../util/creditCard.utils");
var CommonUtil= require("../util/common.functions.util");
var sessionData = require("./sessiondata.do");

//===================CardPayment Page==============================

var CardPayment = function() {

	//-----------Enter Credit Card Details-----------------
	
	this.payment = element.all(by.css("a[ng-click='toggleOpen()']")).get(1);	
	this.newPaymentTab = element.all(by.linkText('NEW PAYMENT METHOD')).get(0);
	this.enterCardInfo = element.all(by.css("input[id='formName.number_mask']")).get(0);
	this.enterCvv = element.all(by.css("input[id='cvv']")).get(0);
	this.month = element.all(by.model('$select.search')).get(0); 
	this.year =  element.all(by.model('$select.search')).get(1);
	this.enterMonth = element(by.xpath("//div[@id='exp_month']/div[2]/div/div/div[8]/div/div/p")); 
	this.enterYear =  element(by.xpath("//div[@id='exp_year']/div[2]/div/div/div[4]/div/div/p"));
	this.enterFirstName = element(by.css("input[id='fname']"));
	this.enterLastName = element(by.css("input[id='lname']"));
	this.enterAddress = element(by.css("input[id='address1']"));
	this.enterCity = element(by.css("input[id='city']"));
	this.city = element.all(by.name('city')).get(1);
	this.enterZip = element(by.css("input[id='zipcode']"));
	this.state = element.all(by.model('$select.search')).get(2);
	this.enterState = element(by.xpath("//div[@id='state']/div[2]/div/div/div[6]/div/div/p"));
	// this.state = element.all(by.css("div[class='dropdown-content-item']")).get(4);
	// this.chooseState = state.element(by.cssContainingText('p','CA'));
	// this.enterCountry = element(by.model('billingInfo.country')).$('[label="USA"]');
	this.confirmAutopay = element.all(by.css("button[id='done']")).get(1);
	this.confirmAddOn = element.all(by.css("button[id='done_confirmation']")).get(0);
	this.addCreditCardBtn = element.all(by.css("button[id='PlaceMyorder']")).get(0);
	this.surveyBtn = element.all(by.buttonText('No, Thanks')).get(1);
		
	this.goToPayCheckout = function() {
		ElementsUtil.waitForElement(this.payment);
		this.payment.click();
		// this.newPaymentTab.click();
		this.enterCardInfo.clear().sendKeys(""+generator.GenCC(sessionData.wfm.cardType));
		this.enterCvv.sendKeys(CommonUtil.getCvv(sessionData.wfm.cardType));
		this.month.click();
		this.enterMonth.click();
		this.year.click();
		this.enterYear.click();
		this.enterFirstName.sendKeys(CCUtil.firstName);
		this.enterLastName.sendKeys(CCUtil.lastName);
		this.enterAddress.sendKeys(CCUtil.addOne);
		this.city.sendKeys(CCUtil.city);
		this.state.click();
		this.enterState.click();
		this.enterZip.sendKeys(CCUtil.pin);		
		// this.enterCountry.click();
		this.addCreditCardBtn.click();
	};
	//-----------------Check if successful checkout is done--------------------------
	
	this.goToSuccessfulCheckout = function() {
		return browser.wait(function() {
		// return ElementsUtil.waitForUrlToChangeTo("/activationcc\/confirmation_activation/");
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /activationcc\/confirmation_activation/.test(url); 
		});
	}, 10000, "URL hasn't changed");
	};
	
	//-----------------Check if checkout page is loaded--------------------------
	
	this.isLoaded = function() {	
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /checkout$/.test(url);
		});
	};
	
	this.autopayConfirm = function() {	
		 this.confirmAutopay.click(); 
	};
	
	this.buyAddOnConfirm = function() {	
		 this.confirmAddOn.click(); 
	};
	
	this.isConfirmDone = function() {	
		return browser.wait(function() {
		 return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /redirectsurvey\/*$/.test(url); // https://sitz.myfamilymobile.com/redirectsurvey  https://sitz.myfamilymobile.com/redirectsurvey/3055359452
		}); 
		}, 10000, "URL hasn't changed");
	};
	
	this.survey = function() {	
		 this.surveyBtn.click(); 
	};
	
	this.isSuccessful = function() {	
		return browser.wait(function() {
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /dashboard$/.test(url); // https://sitz.myfamilymobile.com/myaccount/dashboard
		});
		}, 10000, "URL hasn't changed");
	};
	
	
};

module.exports = new CardPayment;
