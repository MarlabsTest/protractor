var expectedConditions = protractor.ExpectedConditions;

var discount = function(){
	
	this.phoneNumber = element(by.css('input[id="phone"]'));
	this.firstName = element(by.css('input[id="firstname"]'));
	this.lastName = element(by.css('input[id="lastname"]'));
	this.accountPin = element(by.css('input[id="accountpin"]'));
	this.facilityNumber =element(by.css('input[id="deptcode"]'));
	this.continueBtn = element.all(by.css('[ng-click="action()"]')).get(0);
	this.continuePopup = element(by.css('[class="btn tf-btn-primary ng-scope"]'))
	
	this.discountPageLoaded = function(){
		browser.wait(expectedConditions.visibilityOf(this.phoneNumber),10000);
		return this.phoneNumber.isDisplayed();
	};
		
	this.phoneNumberDetails = function(min){
		this.phoneNumber.sendKeys(min);
		return true;
		//browser.wait(expectedConditions.visibilityOf(this.firstName),5000);
	};
	
	this.getphoneNumberDetails = function(){
		this.phoneNumber.getText();
		return true;
		//browser.wait(expectedConditions.visibilityOf(this.firstName),5000);
	};
	
	
	this.firstNameDetails = function(min){
		this.firstName.sendKeys(min);
		return true;
		//browser.wait(expectedConditions.visibilityOf(this.lastName),5000);
	};
	
	this.lastNameDetails = function(min){
		this.lastName.sendKeys(min);
		return true;
		//browser.wait(expectedConditions.visibilityOf(this.accountPin),5000);
	};
	this.accountPinDetails = function(min){
		this.accountPin.sendKeys(min)	;
		return true;

		//browser.wait(expectedConditions.visibilityOf(this.facilityNumber),5000);
	};
	
	this.facilityNumberDetails = function(min){
		this.facilityNumber.sendKeys(min);
		return true;
		//browser.wait(expectedConditions.visibilityOf(this.facilityNumber),5000);
	};
	
	this.continueBtnClick = function(min){
		//browser.wait(expectedConditions.visibilityOf(this.facilityNumber),5000);
		this.continueBtn.click();
		browser.wait(expectedConditions.visibilityOf(this.continuePopup),5000);
		return this.continuePopup.isPresent();
	};

};

module.exports = new discount;