
var customerdiscount = require("./employeediscount.pf");


var discount = function(){

	this.url = 'https://sitz.myfamilymobile.com/employeediscount';
	
	this.discountPageLoad = function(){
		browser.get(this.url);
	};
	this.discountHomePageLoaded = function(){
		return customerdiscount.discountPageLoaded();
	};
	
	this.phoneNumberDetails = function(min){
		return customerdiscount.phoneNumberDetails(min);
	};
		
	this.getphoneNumberDetails = function(){
		return customerdiscount.getphoneNumberDetails();
		//browser.wait(expectedConditions.visibilityOf(this.firstName),5000);
	};
	
	this.firstNameDetails = function(min){
		return customerdiscount.firstNameDetails(min);
	};
	
	this.lastNameDetails = function(min){
		return customerdiscount.lastNameDetails(min);
	};
	
	this.accountPinDetails = function(min){
		return customerdiscount.accountPinDetails(min);
	};
	
	this.facilityNumberDetails = function(min){
		return customerdiscount.facilityNumberDetails(min);
	};
	
	this.continueBtnClick = function(){
		return customerdiscount.continueBtnClick();
	};
	
};

module.exports = new discount;