var discount = require("../employeediscount.po");

var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");

describe('discount Page', function() {
	
	it('Home page', function(done) {
	console.log("home page load");
		discount.discountPageLoad();
		expect(discount.discountHomePageLoaded()).toBe(true);
		done();
	});
	
	it(' phone number for the employee discount',function(done){
		console.log("employee phone number  durgaaaa");	
		var min = dataUtil.getMinOfESN(sessionData.wfm.esn);
		discount.phoneNumberDetails(min); //enter the phone number in employee discount
		console.log(" durgaaaa",discount.getphoneNumberDetails());	
		expect(true);
		done();
	});
	it(' first name are given for the employee discount',function(done){
		console.log("employee first name");	
		var min = 'Durga';
		discount.firstNameDetails(min); //enter the first name in employee discount
		expect(true);
		done();
	});
	it(' last name are given for the employee discount',function(done){
		console.log("employee last name");	
		var min = 'ramesh';
		discount.lastNameDetails(min); //enter the last name in employee discount
		expect(true);
		done();
	});
	it(' account pin for the employee discount',function(done){
		console.log("employee account pin");	
		var min = '1234';
		discount.accountPinDetails(min); //enter the account pin number in employee discount
		expect(true);
		done();
	});
	it(' facilityNumberDetails for the employee discount',function(done){
		console.log("employee facilityNumberDetails");	
		var min = '1234567890';
		discount.facilityNumberDetails(min); //enter the facility number in employee discount
		expect(true);
		done();
	});
	it(' details are given for the employee discount',function(done){
		console.log("click on continue button");	
		expect(discount.continueBtnClick()).toBe(true); //click on the continue button
		done();
	});
});