'use strict';
var ElementsUtil = require("../util/elements.util"); 

var Login = function() {

 	this.login = element.all(by.id("createac-device")).get(1);
	this.validateLoginBtn = element.all(by.id("btn_continue25")).get(0);
	this.password = element.all(by.id("wfmloginpinpwd-device")).get(1);
	this.passwordForMin = element.all(by.model('modelValue')).get(0);
	this.validatePassswordBtnForMin = element.all(by.id("btn_login")).get(0);
	this.validatePassswordBtn = element.all(by.id("btn_continue23")).get(0);
	this.forgotPasswordLnk = element(by.id('login_forgotpassword'));
	this.resetMessage = element(by.id("modal-title")); 
	this.securityPinTxt = element.all(by.id("wfmloginpin-pin")).get(1);
	//this.securityPinTxt = element.all(by.css('[ng-click="action()"]')).get(4);
	//this.securityPinContinueBtnAll = element.all(by.id("btn_continue"));
	this.securityPinContinueBtn = element.all(by.id("btn_continue")).get(1);
	
	
	this.resetPassword = function() {
	//	ElementsUtil.waitForElement(this.forgotPasswordLnk); 
		this.forgotPasswordLnk.click();
	};
	
	this.isPasswordReset = function() {
	//	ElementsUtil.waitForElement(this.resetMessage); 
		return this.resetMessage.getText();
	};

		
	this.validateSecurityPin = function(securityPin) {
		this.securityPinTxt.clear().sendKeys(securityPin);
		ElementsUtil.waitForElement(this.securityPinContinueBtn);
		this.securityPinContinueBtn.click();
	};
	
	this.provideLogin = function(login) {
	//	ElementsUtil.waitForElement(this.login); 
		this.login.clear().sendKeys(login);
	};
    
	this.validateLogin = function(login) {
		this.provideLogin(login);
	//	ElementsUtil.waitForElement(this.validateLoginBtn); 
		this.validateLoginBtn.click();
	};
  
	this.providePassword = function(password) {
	//	ElementsUtil.waitForElement(this.password); 
		this.password.clear().sendKeys(password);
	};
    
	this.validatePassswordForMin = function(password) {
	//	ElementsUtil.waitForElement(this.passwordForMin); 
		this.passwordForMin.clear().sendKeys(password);  
	//	ElementsUtil.waitForElement(this.validatePassswordBtnForMin); 
		this.validatePassswordBtnForMin.click();
	};
	
	this.validatePasssword = function(password) {
	//	ElementsUtil.waitForElement(this.password); 
		this.password.clear().sendKeys(password);
	//	ElementsUtil.waitForElement(this.validatePassswordBtn); 
		this.validatePassswordBtn.click();
	};
  
	this.isValidPassword = function() {
		return ElementsUtil.waitForUrlToChangeTo(/dashboard$/);
	};
	
	this.isSignOut = function() {
		return ElementsUtil.waitForUrlToChangeTo(/$/);
	};
	
	this.isValidLogin = function() {
		return ElementsUtil.waitForUrlToChangeTo(/accessaccount$/);
	};
};

module.exports = new Login;