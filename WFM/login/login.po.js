'use strict';

var login = require("./login.pf");

var Login = function() {

	this.resetPassword = function() {
		login.resetPassword();
	};
	
	this.isPasswordReset = function() {
		return login.isPasswordReset();
	};

	
	this.validateSecurityPin = function(securityPin) {
		login.validateSecurityPin(securityPin);
	};
	
	this.validateLogin = function(mailOrNumber) {
		login.validateLogin(mailOrNumber);
	};
	
	this.validatePassword = function(password) {
		login.validatePasssword(password);
	};
	
	this.validatePassswordForMin = function(password) {
		login.validatePassswordForMin(password);
	};
	
	this.isValidLogin = function() {
		return login.isValidLogin();
	};  
	
	this.isSignOut = function() {
		return login.isSignOut();
	}; 
	
	  
	this.isValidPassword = function() {
		return login.isValidPassword();
	};
};

module.exports = new Login;