/*
 * **********Scenario is for MULTILINE SERVICE RENEWAL ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,click on the payservice button in the my account dashboard
 * 3.Redirected to the cehckout page
 * 4.Do the payment and will be redirected to the confirmation page,survey page and finally to my account dashboard  
 * 
 */
'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Multiline Service Renewal', function() {
	
	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				sessionData.wfm.cardType = inputActivationData.cardType;
				done();
			});
			FlowUtil.run('WFM_MYACCOUNT_MULTILINE_SERVICE_RENEWAL');
		}).result.data = inputActivationData;
	});
		
});
