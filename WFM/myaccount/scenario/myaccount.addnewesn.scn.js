'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

/*
 * Scenario : WFM Add New ESN
 * Details	: This scenario first read an test data from JSON file.
 * 			  Using Form part numbers mentioned in JSON it's execute an activation spec file and 
 * 			  then activate that ESN to get MIN for the same.It's executes an add new ESN spec with new ESN part number 
 * 			  and add that ESN on same account.Also, it activate the newly added ESN.
 * */
describe('Add New Esns', function() {
	
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		sessionData.wfm.noOfLines = inputActivationData.noOfLines;//set outside "it" to get outside "it"
			console.log('Inside scn sessionData.wfm.noOfLines======',sessionData.wfm.noOfLines);

		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				console.log("inside firsdt IT session data setting",sessionData.wfm.noOfLines);
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('WFM_MYACCOUNT_ADD_NEW_ESN');
		}).result.data = inputActivationData;
	});
		
});
	