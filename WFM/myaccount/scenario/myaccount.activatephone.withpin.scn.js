'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('ActivateDeviceInsideAccount', function() {
	
	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_ACTIVATE_DEVICE_INSIDE_ACCOUNT');
		}).result.data = inputActivationData;
	});
		
});
