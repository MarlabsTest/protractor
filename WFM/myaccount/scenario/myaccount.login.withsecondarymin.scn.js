'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

/*
 * Scenario : WFM Login with SECONDARY MIN
 * Details	: This scenario first read an test data from JSON file.
 * 			  Using part numbers mentioned in JSON it's execute an activation spec file and 
 * 			  then activate that ESN to get MIN for the same.then it execute the add new esn spec to add Secoundary MIN 
 * 			  in the same account.Also,secondary MIN, security pin stored on session object.
 * 			  Then it executes an myaccount login with secondary MIN spec with the same secondary MIN/security pin
 * 			   to test login functionality.
 * */
describe('Login With Secondary Min', function() {
	
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('WFM_LOGIN_WITH_SECONDARY_MIN');
		}).result.data = inputActivationData;
	});
		
});
	
