'use strict';

var myAccount = require("./myaccount.pf");
var manageProfile = require("./manageprofile.pf");
var managePayment = require("./managepayment.pf");
var addDevice = require("./adddevice.pf");
var optionsQuickLinks = require("./optionquicklink.pf");
var addservicepin = require("./addservicepin.pf");
var serviceRenewal = require("./checkout.pf");
var cardPayment = require("../common/creditcardpayment.pf");
var survey = require("../activation/survey.pf");

var MyAccount = function() {
	
	this.autopayCheckout = function() {
		return myAccount.goToMultilineAutopay();
	};
		
	this.dashboard = function() {
		return myAccount.dashboard();
	};
	
	this.payService = function(){
		myAccount.payService();
	};
	
	this.autopayConfirm = function(){
		return cardPayment.autopayConfirm();
	};
	
	this.clickOnThankYouBtn = function(){
		survey.clickOnThankYouBtn();
	}
	
	this.buyAddOnConfirm = function(){
		return cardPayment.buyAddOnConfirm();
	};
	
	this.isConfirmDone = function(){
		return cardPayment.isConfirmDone();
	};
	
	this.survey = function(){
		return cardPayment.survey();
	};
	
	this.isSuccessful = function(){
		return cardPayment.isSuccessful();
	};	
		
	this.multilineAutopayCheckout = function() {
		return cardPayment.goToPayCheckout();
	};
	
	this.isSuccessfulCheckout = function() {
		return cardPayment.goToSuccessfulCheckout();
	};
	
	this.refresh = function() {
		return myAccount.refresh();
	};
	
	this.allOptions = function() {
		return myAccount.goToAllOptions();
	};
	
	this.chooseBuyAddon = function() {
		return myAccount.chooseBuyAddon();
	};
	
	this.isbuyPlanPageLoaded = function() {
		return myAccount.isBuyPlanLoaded();
	};
	
	this.selectAPlan = function() {
		return myAccount.goToSelectPlan();
	};
	
	this.myDevices = function() {
		return myAccount.goToMyDevices();
	};
	
	this.manageProfile = function() {
		return myAccount.goToManageProfile();
	};
	
	this.paymentMethod = function() {
		return myAccount.goToPaymentMethod();
	};
	
	this.paymentHistory = function() {
		return myAccount.goToPaymentHistory();
	};
	
	this.addDevice = function() {
		return myAccount.goToAddDevice();
	};
	
	this.signOut = function() {
		myAccount.goToSignOut();
	};
  	
	this.isManageProfileLoaded = function() {
	return manageProfile.isLoaded();
	};
	
	this.isManagePaymentLoaded = function() {
	return managePayment.isLoaded();
	};
	
	this.editContactInfo = function() {
		manageProfile.goToEditContactInfo();
	};
	
	this.isPaymentAdded = function() {
		return managePayment.isPaymentAdded();
	};
	
	this.isContactEdited = function() {
	return manageProfile.isContactEdited();
	};
	
	this.editPaymentDetails = function() {
		return managePayment.goToEditPaymentInfo();
	};
	
	this.addNewDevice = function(esn) {
		addDevice.addDevice(esn);
	};
	
	this.isNewDeviceAdded = function(esn) {
		addDevice.isDeviceAdded(esn);
	};
	
	//** method checks whether the account dashboard is loaded or not
	this.isLoaded = function(){
		return myAccount.isLoaded();
	};
	
	//** method checks whether the select device page is loaded or not
	this.isDeviceLoaded = function(){
		return myAccount.isDeviceLoaded();
	};
	//** method is to click on the All Options link
	this.clickOnAllOptions = function(){
		optionsQuickLinks.clickOnAllOptions();
	};
	
	//** method is to proceed with the add service plan flow
	this.clickOnservicePlanLink = function(){
		addservicepin.clickOnservicePlanLink();
	};
	
	//** method checks whether the renew service plan page is loaded or not
	this.renewServicePageLoaded = function(){
		return addservicepin.renewServicePageLoaded();
	}
  
	//** method to enter new pin 
	this.enterAirtimePinRenew = function(pin){
		return addservicepin.enterAirtimePinRenew(pin);
	};
  
	//** method to check the redemption confirmation page is loaded after entering the new pin
	this.redemptionSuccessPage = function(){	
		return addservicepin.redemptionSuccessPage(); 
	};
  
	//** method to proceed from the confirmation page
	this.clickOnContinueBtnFromConfirmation = function(){
		addservicepin.clickOnContinueBtnFromConfirmation();
	};
	
	//** method to proceed with the pay service flow
	this.payService = function(){
		myAccount.payService();
	}
	
	//** method to check whether the checkout page is loaded or not
	this.checkoutPageLoaded = function(){
		return myAccount.checkoutPageLoaded();
	};
	
	//** method to click on the continue to payment button in the checkout page
	this.continueToPayment = function(){
		serviceRenewal.continueToPayment();
	};
	
	//** method to check whether the payment form for entering the cc and billing details are displayed or not
	this.paymentOptionLoaded = function(){
		return serviceRenewal.paymentOptionLoaded();
	};
	
	
	this.clickNewPayment = function(){
		serviceRenewal.clickNewPayment();
	};
	
	//** method to enter cc details
	this.enterCcDetails = function(ccNum,cvv){
		serviceRenewal.enterCcDetails(ccNum,cvv);
	};
	
	//** method to enter billing details
	this.enterBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		serviceRenewal.enterBillingDetails(fname,lname,address1,houseNum,city,zipcode);
	};
	//method to enter ACH payment details: for reactivation with ACH purchase scenario
	this.enterAchDetails = function(accno,routingno){
		serviceRenewal.enterAchDetails(accno,routingno);
	};	
	//Method to click on Bank Account tab in Payment - for reactivation with ACH paurchase scenario
	this.clickOnBankAccTab = function(){
		serviceRenewal.clickOnBankAccTab();
	};
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function(){
		return serviceRenewal.confirmationPageLoaded();
	};
	
	//** method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function(){
		serviceRenewal.proceedFromConfirmationPage();
	};
	
	//** method to click on the activate button, after adding a device through the dashboard
	this.clickOnActivate = function() {
		myAccount.clickOnActivate();
	};
	
	//** method to proceed from the survey page
	this.clickOnSurvey = function(){
		serviceRenewal.clickOnSurvey();
	};

	//**method to check the enrollment checkbox at checkout page
	this.selectEnrollmentCheckBox = function(){
		serviceRenewal.selectEnrollmentCheckBox();
	};
	
	//** method to check whether  a popup is dispalyed after clicking the pay service button in the account dashboard
	this.newPopupLoaded = function(){		
		return myAccount.newPopupLoaded();
	};
	
	//** method to proceed from the popup page
	this.continueFromPopUp = function(){
		return myAccount.continueFromPopUp();
	};
	
	//**method to click on edit option 
	this.goTodeleteOption = function() {
		return managePayment.goTodeleteOption();
	};
	
	//**method to check whether the delete option is present or not
	this.deleteOptionIsPresent = function(){
		return managePayment.deleteOptionIsPresent();
	};
	
	//**method to click on the delete option
	this.clickOnDeleteOption = function(){
		return managePayment.clickOnDeleteOption();
	};
	
	//**method to opt for autopay option in checkout page -- single line activation enrollment changes
	this.optForAutoPay = function() {
		serviceRenewal.optForAutoPay();
	};
	this.clickOnBankAccount = function(){
		return serviceRenewal.clickOnBankAccount();
	};
	this.accountTypeDetails = function(accountNumber,routingNumber){
		return serviceRenewal.accountTypeDetails(accountNumber,routingNumber);
	};
	//Multiline Activation with CC purchase starts:Remya
	//method to click on 'Add Line' option in Account dashboard menu
	this.clickOnAddLineMenu = function(){
		return myAccount.clickOnAddLineMenu();
	};
	//method to check whether 'Is your Phone Active?' popup loaded or not
	this.isPhoneActivePopUpLoaded = function(){		
		return myAccount.isPhoneActivePopUpLoaded();
	};
	//Method to click on 'No' option in 'Is your Phone Active?' popup
	this.clickOnPhoneActivePopUpOptn = function(){		
		return myAccount.clickOnPhoneActivePopUpOptn();
	};
	//Multiline Activation with CC purchase ends
	//Upgrade ESN active to ESN inactive(non reserve) starts :Remya
	this.selectDevicePageLoaded = function(){		
		return myAccount.selectDevicePageLoaded();
	};
	this.selectActivatedMin = function(){		
		return myAccount.selectActivatedMin();
	};
	//Upgrade ESN active to ESN inactive(non reserve) ends
};

module.exports = new MyAccount;