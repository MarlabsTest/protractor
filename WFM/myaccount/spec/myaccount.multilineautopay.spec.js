'use strict';

var myAccount = require("../myaccount.po");

//====================Multiline AutoPay Scenario=============================

describe('WFM enroll in AutoPay', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	
	it('should enroll in AutoPay of all devices in MyAccount', function(done) {
		console.log("should enroll in AutoPay of all devices in MyAccount");
		myAccount.autopayCheckout();  //---------Click on AutoPay button--------------
		myAccount.multilineAutopayCheckout();        //---------Do checkout--------------
		expect(myAccount.isSuccessfulCheckout()).toBe(true);
		done();
	});	
	
	it('should confirm autopay enrollment', function(done) {
		console.log("should confirm autopay enrollment");
		myAccount.autopayConfirm();  //---------Click on AutoPay confirm button--------------
		expect(myAccount.isConfirmDone()).toBe(true);
		done();
	});
	
	it('should direct to survey page and then to dashboard after successful payment', function(done) {
		console.log("should direct to survey page and then to dashboard after successfull payment");
		myAccount.survey();  //---------Click on survey button--------------
		expect(myAccount.isSuccessful()).toBe(true);
		done();
	});
	
});
	