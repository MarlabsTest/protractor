'use strict';

var myAccount = require("../myaccount.po");

/*
 * Spec		: WFM Edit contact information
 * Details	: This spec is for editing contact information of already existing account.
 * */
describe('WFM Edit Contact Info', function() {

	/*
	 * Should navigate to Manage Profile page for editing contact information of the account. 
	 * */
	it('Should navigate to Manage Profile page', function(done) {
		myAccount.manageProfile();
		expect(myAccount.isManageProfileLoaded()).toBe(true);
		done();
	});
	
	/*
	 * Should edit the contact information. 
	 * */
	it('Should edit the contact information', function(done) {
		myAccount.editContactInfo();
		expect(myAccount.isContactEdited()).toEqual('test');
		done();
	});
	
});
	