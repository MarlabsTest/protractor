'use strict';

var myAccount = require("../myaccount.po");
var drive = require('jasmine-data-provider');
var dataUtil= require("../../util/datautils.util");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedPin ={};
var generatedMin ={};
/*
 * Spec		: WFM Add new ESN
 * Details	: This spec is for adding a new device to already existing account.
 * */

describe('WFM Add New ESN', function() {
	console.log('Spec sessionData.wfm.noOfLines======',sessionData.wfm.noOfLines);
	
	if (sessionData.wfm.noOfLines == ''){
		sessionData.wfm.noOfLines = 1;
	}
		
	for(var line = 0;line < sessionData.wfm.noOfLines;line++)
	{
		/*
		 * Should add New ESN on Account for activate that. 
		 * */
		it('Should add New ESN on Account', function(done) {
			myAccount.addDevice();
			expect(myAccount.isDeviceLoaded()).toBe(true);
			done(); 
		});
	
		/*
		 * should click 'I have a family phone' button on activation page to enter WFM family phone ESN.
		 * */
		it("Should click 'I have a family phone' button on activation page ",function(done){		
			activation.gotToEsnPage();
			expect(activation.esnPageLoaded()).toBe(true);
			done();
		});
	
		/*
		 * Should Marry ESN and SIM then enter Married SIM value and then navigate to pop up for providing the security PIN
		 * */
		it('Should enter married SIM value and then navigate to pop up for providing the security PIN', function(done) {
			var esnval = dataUtil.getESN(sessionData.wfm.esnPartNumber);
			sessionData.wfm.esn = esnval;
			sessionData.wfm.esnsToReactivate.push(esnval);
			var simval = dataUtil.getSIM(sessionData.wfm.simPartNumber);
			dataUtil.addSimToEsn(esnval,simval);
			activation.enterMarriedSim(simval);
			sessionData.wfm.sim = simval;
			generatedEsnSim['esn'] = esnval;
			generatedEsnSim['sim'] = simval;
			activation.checkBoxCheck();
			activation.continueESNClick();
			expect(activation.securityPinPopUp()).toBe(true);
			done();	
		}).result.data = generatedEsnSim;
	
		/*
		 * Should provide an security PIN and navigate to Zipcode entering page to provide zipcode.
		 * */
		it('Should provide an security PIN and navigate to zipcode entering page', function(done) {
			activation.enterPin("1234");
			activation.clickOnPinContinue();
			expect(activation.keepMyPhonePageLoaded()).toBe(true);
			done();		
		});
	
		/*
		 * Should provide an valid zip and navigate to service PIN entering page to provide PIN number.
		 * */
		it('Should provide an valid zip and navigate to service PIN entering page', function(done) {
			activation.enterZipCode(sessionData.wfm.zip);
			expect(activation.servicePlanPageLoaded()).toBe(true);
			done();		
		});
		
		/*
		 * Should provide an valid service PIN and navigate to activation request submitted page.
		 * */
		it('Should provide an valid service PIN and navigate to activation request submitted page', function(done) {
			var pinval = dataUtil.getPIN(sessionData.wfm.pinPartNumber);
			sessionData.wfm.pin = pinval; 
			activation.enterAirTimePin(pinval);
			generatedPin['pin'] = pinval;
			expect(activation.finalInstructionPageLoaded()).toBe(true);
			done();		
		}).result.data = generatedPin;
	
		/*
		 * Should click on the continue button in the popup page and navigate to final instraction page.
		 * 
		it('Should click on the continue button in the popup page', function(done) {
			activation.clickOnAccountCreatedPopupBtn();
			expect(activation.finalInstructionPageLoaded()).toBe(true);
			done();		
		});*/
	
		/*
		 * Should click on the continue button on final instruction page  and then load summary page
		 * */
		xit('Should click on the continue button and should load summary page', function(done) {
			activation.finalInstructionProceed();
			expect(activation.summaryPageLoaded()).toBe(true);
			done();		
		});
	
		/*
		 * Should click on the done button and should load survey page.
		 * */
		xit('Should click on the done button and should load survey page', function(done) {
			activation.clickOnSummaryBtn();
			expect(activation.surveyPageLoaded()).toBe(true);
			done();		
		});
	
		/*
		 * Should click on the thank you button and redirect to the dashboard page.Also execute procedure to 
		 * activate ESN.
		 * */
		it('Should direct to the dashboard page', function(done) { //click on the thank you button and re
			// activation.clickOnThankYouBtn();
			dataUtil.activateESN(sessionData.wfm.esn);
			var min = dataUtil.getMinOfESN(sessionData.wfm.esn);
			generatedMin['min'] = min;
			dataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_Activation_with_PIN');
			myAccount.dashboard();
			expect(myAccount.isLoaded()).toBe(true);
			done();		
		}).result.data = generatedMin;
	}
	
});
