'use strict';
//go to the manage payment page
//click on the edit payment deatils
//the card details which we saved during our payment will be deleted
var myAccount = require("../myaccount.po");

//================deleting the saved cards in manage payment scenario=====================

describe('WFM Manage Payment', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	// require("./myaccount.login.spec.js");

	//load the payment page 
	//go to the manage payment page
	it('should navigate to Manage Payment page', function(done) {
		myAccount.paymentMethod();
		expect(myAccount.isManagePaymentLoaded()).toBe(true);
		done();
	});	
	
	
	//click on the edit payment details
	//check whether the deletion option is present.
	it('checks whether the deletion option is present', function(done) {
		myAccount.goTodeleteOption();
		expect(myAccount.deleteOptionIsPresent()).toBe(true);	
		done();
	});
	
	//click on the delete payment
	//upon clicking the saved credit card details will be deleted
	it('should delete the added payment details', function(done) {
		myAccount.clickOnDeleteOption();
		expect(myAccount.isManagePaymentLoaded()).toBe(true);	
		done();
	});	
});
	