'use strict';

var login = require("../../login/login.po");
var loginData = require("../../common/sessiondata.do");
var homePage = require("../../common/homePage.po");

/*
 * Spec		: WFM Forgot password
 * Details	: This spec is for testing the forgot password functionality using an valid username.which is already stored on 
 * 			  session by activation spec.
 * */
describe('WFM Forgot Password', function() {

	/*
	 * Should navigate to My Account page for entering Email of existing account. 
	 * */
	it('Should navigate to My Account page', function(done) {
		homePage.myAccount();
		expect(homePage.isMyAccountLoaded()).toBe(true);
		done();
	});
	
	/*
	 * Should navigate to access account page for clicking forgot password link. 
	 * */
	it('Should navigate to access account page', function(done) {		
		login.validateLogin(loginData.wfm.username);
		expect(login.isValidLogin()).toBe(true);
		done();
	});
	
	/*
	 * Should click forgot password link and reset the password of the same account. 
	 * */
	it('Should click forgot password link and reset the password', function(done) {
		login.resetPassword();
		expect(login.isPasswordReset()).toEqual("Link Sent!");
		done();
	});

});
	