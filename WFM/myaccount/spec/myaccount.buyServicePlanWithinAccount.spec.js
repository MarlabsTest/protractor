'use strict';

var myAccount = require("../myaccount.po");

//===============Buy service plan with in account Scenario========================

describe('WFM Buy Service Plan inside Account', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	//require("./myaccount.login.spec.js");   //-------------loads login page---------------
	require("../../activation/spec/activate-family-phone-spec.js");
	
	it('should navigate to Active Devices Buy Service Plan', function(done) {
		console.log("should navigate to Active Devices All Options");
		myAccount.allOptions();   //-------Click on AllOptions--------------
		myAccount.chooseBuyPlan();   //---------Choose a Plan----------
		myAccount.selectAPlan();     //--------Continue with the plan-----------
		myAccount.multilineAutopayCheckout(); //---------Checkout-------------
		myAccount.isSuccessfulCheckout();
		done();
	});	
	
});
	