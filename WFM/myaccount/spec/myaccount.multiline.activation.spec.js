'use strict';

var myAccount = require("../myaccount.po");

describe('WFM Add a device & activate through myAccount', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	// require("./myaccount.login.spec.js");
		
	it('should click on the PayService button in the account dashboard and will be redirected to checkout page', function(done) {		
		myAccount.payService();
		expect(myAccount.checkoutPageLoaded()).toBe(true);		
		done();
	});
		
	it('should make payment', function(done) {
		myAccount.multilineAutopayCheckout();
		expect(myAccount.isSuccessfulCheckout()).toBe(true);
		done();		
	});
	
});
