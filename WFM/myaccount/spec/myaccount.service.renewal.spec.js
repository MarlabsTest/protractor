/*
 * **********Spec file is for MULTILINE SERVICE RENEWAL ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,click on the payservice button in the my account dashboard
 * 3.Redirected to the cehckout page
 * 4.Do the payment and will be redirected to the confirmation page,survey page and finally to my account dashboard  
 * 
 */
'use strict';

var myAccount = require("../myaccount.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");

describe('WFM Multiline Service Renewal', function() {
	var expectedConditions = protractor.ExpectedConditions;
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	/*Click on the pay service button in myaccount dashboard
	 *Expected result - AutoEnrollment POPup page will be displayed  
	 */
	it('should display autoenrollment popup page', function(done) {
		myAccount.payService();		
		expect(myAccount.newPopupLoaded()).toBe(true);
		done();		
	});
	
	/*Click on the no thanks button in popup page
	 *Expected result - Checkout page will be displayed  
	 */
	it('should navigate to the checkout page', function(done) {
		myAccount.continueFromPopUp();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 */	
	it('should load the payment form', function(done) {
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - confirmation page will be loaded
	 */	
	it('should load the confirmation page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.wfm.cardType),CommonUtil.getCvv(sessionData.wfm.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);		
		expect(myAccount.confirmationPageLoaded()).toBe(true);
		done();		
	});
	
	/*Continue from the confirmation page ,will be redirected to the survey page
	 *Continue from survey page 
	 *Expected result - myaccount dashboard will be loaded
	 */
	it('should navigate to the account dashboard page', function(done) {
		// myAccount.proceedFromConfirmationPage();
		// myAccount.clickOnSurvey();			
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(sessionData.wfm.esn,'0','6','WFM_Purchase','true','false');
		console.log('ITQ_DQ_CHECK table updated!!');
		console.log("service renewal",sessionData.wfm.esn);
		myAccount.dashboard();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	
});
