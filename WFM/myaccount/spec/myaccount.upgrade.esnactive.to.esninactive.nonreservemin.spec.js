
/* Spec file for  Upgrade from ESN Active with service end date in future to 
 * ESN Inactive with Min not reserved - Belongs to Same account
 * author:rpillai
 * */

'use strict';

var sessionData 	= require("../../common/sessiondata.do");
var activationPo	= require("../../activation/activation.po");
var myAccountPo 	= require("../../myaccount/myaccount.po");
var homePagePo 		= require("../../common/homepage.po");
var dataUtil		= require("../../util/datautils.util");
var generatedMin 	= {};

describe('WFM Upgrade activated ESN to inactive ESN with MIN not reserved: ', function() {
	//should first deactivate the device and then reload the page for to activate the device,upon clicking activate link
	it('Deactivate the device', function(done) {	
        var min = dataUtil.getMinOfESN(sessionData.wfm.esn);
        var varPhoneStstus	= sessionData.wfm.phoneStatus;
        if(varPhoneStstus == "INACTIVE_NONRESERVE"){
        	dataUtil.deactivatePhone('DEACTIVATE',sessionData.wfm.esn,min,'PAST_DUE','WFM');
        }else{
        	dataUtil.deactivatePhone('DEACTIVATE',sessionData.wfm.esn,min,'CUSTOMER REQUESTED','WFM');
        }
       	
		homePagePo.myAccount();
		expect(myAccountPo.isLoaded()).toBe(true);		
		done();
	}); 
	//Click on activate button of deactivated device.Opt for 'Keep My PhoneNumber' and select active device's MIN.
	it('Proceed for activation,', function(done) {
		myAccountPo.clickOnActivate();
		expect(myAccountPo.selectDevicePageLoaded()).toBe(true);		
		done();
	});
	//For upgrading inactive device, select MIN of active device from dropdown
	it('Select MIN of Active device', function(done) {	
		myAccountPo.selectActivatedMin();
		expect(activationPo.finalInstructionPageLoadedPortPin()).toBe(true);
		done();
	});
	//should click on the continue button in the final instruction page
	it('Load Final instruction page', function(done) {
		activationPo.finalInstructionProceedPortPin();		
		expect(activationPo.summaryPageLoaded()).toBe(true);		
		done();	
	});
	//should click on the done button in the summary page
	it('Load survey page', function(done) {
		activationPo.clickOnSummaryBtn();
		expect(activationPo.surveyPageLoaded()).toBe(true);
		done();	
	});
	//should click on the thank you button and will be redirected to the account dashboard page
	it('Take activation survey and activate device', function(done) {
		activationPo.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.wfm.esn);
		console.log("update table with esn :"+sessionData.wfm.esn);
		var min = dataUtil.getMinOfESN(sessionData.wfm.esn);
		generatedMin['min'] = min;	
		expect(myAccountPo.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
});
