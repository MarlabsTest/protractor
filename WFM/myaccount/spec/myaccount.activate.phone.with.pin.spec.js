'use strict';

var myAccount = require("../myaccount.po");
var activation = require("../../activation/activation.po");
//newly added for data integration
var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('WFM Add a device & activate through myAccount', function() {

	//var activationData = activationUtil.getTestData();	
	//console.log('activationData:', activationData);
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	
	//require("./myaccount.addnewesn.spec.js");
	//drive(activationData, function(inputActivationData) {
	it('Add New ESN on Account', function(done) {
	console.log('inside *********************spec*************************************');
		myAccount.addDevice();		
		var esnval = DataUtil.getESN(sessionData.wfm.esnPartNumber);
       	console.log('returns', esnval);
		sessionData.wfm.esn = esnval;
		var simval = DataUtil.getSIM(sessionData.wfm.simPartNumber);
       	console.log('returns', simval);		
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.wfm.sim = simval;		
		myAccount.addNewDevice(esnval);
		expect(myAccount.isLoaded()).toBe(true);
		done();
	});
	
	
	
	it('should click on the activate button in the account dashboard and will be redirected to SIM page', function(done) {		
		myAccount.clickOnActivate();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);		
		done();
	});
		/*
	it('should navigate to next page for selecting keep current phone or get new number after entering valid sim number', function(done) {
		//var simval = DataUtil.getSIM(inputActivationData.SIM);
       	//console.log('returns', simval);
		activation.enterMarriedSim(sessionData.wfm.sim);	
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});*/
	
	it('should enter the zipcode and will be navigated to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.wfm.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('should enter airtime pin and will be navigated to accoutn creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.wfm.pinPartNumber);
       	console.log('returns', pinval);
		sessionData.wfm.pin = pinval;
		activation.enterAirTimePin(pinval);
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
		
	it('should click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('should click on the done button in the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('should click on the thank you button and will be redirected to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);
		console.log("update table with esn :"+sessionData.wfm.esn);
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		console.log("MIN is  :::::" +min);
		//check activation
		DataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_Activation_with_PIN');
		console.log("DB call");	
		
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	//});	
});
