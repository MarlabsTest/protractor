'use strict';

var myAccount = require("../myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var activation = require("../../activation/activation.po");
//===============Buy service plan with in account Scenario========================

describe('WFM Buy AddOn inside Account', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	it('should navigate to Active Devices Buy Add Ons', function(done) {
		console.log("should navigate to Active Devices All Options");
		browser.sleep(5000);
		activation.clickOnMyAccountLink();
		myAccount.allOptions();   //-------Click on AllOptions--------------
		myAccount.chooseBuyAddon();   //---------Choose a Plan----------  
		done();
	});
		
	it('should choose Add On plan and complete payment', function(done) {
		console.log("should choose Add On plan and complete payment");
		myAccount.selectAPlan();     //--------Continue with the plan-----------
		myAccount.multilineAutopayCheckout(); //---------Checkout-------------
		expect(myAccount.isSuccessfulCheckout()).toBe(true);
		done();
	});	
	
	/*it('should confirm autopay enrollment', function(done) {
		console.log("should confirm autopay enrollment");
		myAccount.buyAddOnConfirm();  //---------Click on buyAddon confirm button--------------
		expect(myAccount.isConfirmDone()).toContain('redirectsurvey');
		done();
	});*/
	
	it('should direct to survey page and then to dashboard after successful payment', function(done) {
		console.log("should direct to survey page and then to dashboard after successfull payment");
		myAccount.buyAddOnConfirm();  //---------Click on buyAddon confirm button--------------
		myAccount.clickOnThankYouBtn();  //---------Click on survey button--------------
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(sessionData.wfm.esn,'0','6','WFM_Redeem_Pin','true','false');
		console.log('ITQ_DQ_CHECK table updated!!');
		expect(myAccount.isSuccessful()).toBe(true);
		done();
	});
	
});
	