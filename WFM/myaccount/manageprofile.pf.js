'use strict';
var ElementsUtil = require("../util/elements.util");
var sessionData = require("../common/sessiondata.do");

//=============To Edit your Account Profile============

var ManageProfile = function() {

//----------------Enter Details--------------------------
	
	this.editContactInfo = element.all(by.css('[ng-click="editAction()"]')).get(0);
	this.enterFirstName = element.all(by.id('contact_form-first_name')).get(1);
	this.enterLastName = element.all(by.id('contact_form-last_name')).get(1);
	this.addressLine1 = element.all(by.id('contact_form-address1')).get(1);
	this.addressLine2 = element.all(by.id('contact_form-address2')).get(1);
	this.city = element(by.css('input[id="contact_form-city"]'));
	this.enterPhoneNum = element.all(by.id('contact_form-phone')).get(1);
	// this.state = element(by.model('form.contact.state')).$('[value="FL"]');
	this.state = element(by.model('$select.search')); 
	this.enterState = element(by.xpath("//div[@id='state']/div[2]/div/div/div[6]/div/div/p")); 
	this.zipcode = element(by.css('input[id="contact_form-zipcode"]'));
	this.saveButton = element.all(by.css('[ng-click="action()"]')).get(0);	
	this.confirmPassword = element(by.css('input[id="pswpopup-password"]'));
	this.confirmSave = element.all(by.id('btn_save')).get(0);
	this.editedFirstName = $('.col-xs-12.col-sm-6.col-md-6.col-lg-6').all(by.tagName('p')); 
	
	this.goToEditContactInfo = function() {
		this.editContactInfo.click();
		this.enterFirstName.sendKeys('test');
		this.enterLastName.sendKeys('lastname');  // 1
		this.addressLine1.sendKeys('Address line one'); 
		this.addressLine2.sendKeys('Address line two');
		this.enterPhoneNum.clear().sendKeys(9098765432);		// 1		
		this.saveButton.click();  
		this.confirmPassword.sendKeys(sessionData.wfm.password);
		this.confirmSave.click();
	};
	
	//---------------Check if profile edited--------------
	this.isContactEdited = function() {
		var editDone = element.all(by.css("div[class='col-xs-12 col-sm-6 col-md-6 col-lg-6']")).get(1).element(by.tagName('p'));
		console.log("edit",editDone.getText());		
		return editDone.getText().then(function (text) { 
			console.log("EditedName========================",text);
			return text;
			}); 
				
	};
	
	//----------------Check if MyProfile page loaded----------
	this.isLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/myprofile$/);	
	};
	
};

module.exports = new ManageProfile;