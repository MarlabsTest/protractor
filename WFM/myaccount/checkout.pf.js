//this represents a modal for the checkout page

'use strict';
var ElementsUtil = require("../util/elements.util");

var ServiceRenewChkout = function() {
	this.proceedFromConfirmation = element(by.id('done_confirmation'));
	//this.proceedFromConfirmation = element.all(by.id('done_confirmation')).get(1);
	this.surveyContyinue = element.all(by.css('[ng-click="action()"]')).get(0);
	
	this.chkoutContinue = element.all(by.id('ContinueToPayment')).get(0);
	this.cvvText = element.all(by.name('cvv')).get(1);
	this.newPaymentForm = element(by.css('[ng-click="selectTab(2)"]'));
	this.ccnumber = element.all(by.name('formName.number_mask')).get(1); 
	this.monthDropDown = element.all(by.className('selectize-input')).get(0); //ui-select-match ng-scope
	this.monthDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(6);
	this.yearDropDown = element.all(by.className('selectize-input')).get(1);
	this.yearDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(2);
	
	this.fname = element.all(by.name('fname')).get(1);
	this.lname = element.all(by.name('lname')).get(1);
	this.address = element.all(by.name('address1')).get(1);
	this.houseNo = element.all(by.name('address2')).get(1);
	this.city = element.all(by.name('city')).get(1);
	this.zipcode = element(By.css('input[id="zipcode"]'));
	this.state = element.all(by.id('state')).get(1);
	this.stateDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	this.country = element.all(by.model('$select.search')).get(1);
	this.placeOrderBtn  = element(By.css('button[id="PlaceMyorder"]')); 
	this.autoPaySlideBar = element(by.className('togSlide'));
	
	this.bankAccount = element.all(by.css('[ng-click="selectTab(2)"]')).get(0);
	this.accountType = element.all(by.css('[ng-model="$select.search"]')).get(0);
	this.accountTypeSelect = element.all(by.className('option ui-select-choices-row-inner')).get(1);
	//this.routingNumber = element.all(by.className('form-control padding-right-xs-1 small_txt ng-pristine ng-untouched ng-valid')).get(1);
	this.accountNo = element.all(by.id('formName.number_mask')).get(1);
	this.routingNumber = element.all(by.id('routingNumber')).get(1);
	//reactivation with ACH purchase starts: remya
	//this.tabBankAccount		= element(by.css('[ng-click="setPaymentType(ach)"]'));
	this.tabBankAccount		= element.all(by.css('[ng-click="selectTab(2)"]')).get(0);	
	this.selectAccType		= element.all(by.className('selectize-input')).get(0);
	this.selectAccTypeOpt	= element.all(by.className('dropdown-content-item ng-scope')).get(1);
	this.txtAccNo			= element.all(by.name('formName.number_mask')).get(1); 
	this.txtRoutingNo		= element.all(by.name('routingNumber')).get(1); 
	this.enrollmentChkBox   = element(by.className('togSlide'))
//		element(by.css('[ng-click="action({autoRef:toggleStatus})"]'));
	//reactivation with ACH purchase ends
	
	this.selectEnrollmentCheckBox = function(){
//		ElementsUtil.waitForElement(this.enrollmentChkBox);
		this.enrollmentChkBox.click();
	};
	
	//** method to click on the continue to payment button in the checkout page
	this.continueToPayment = function() {
		ElementsUtil.waitForElement(this.chkoutContinue);
		this.chkoutContinue.click();
	};
	
	//** method to check whether the payment form for entering the cc and billing details are displayed or not
	this.paymentOptionLoaded = function() {
		ElementsUtil.waitForElement(this.ccnumber);
		return this.ccnumber.isPresent();
	};
	
	
	this.clickNewPayment = function(){
		ElementsUtil.waitForElement(this.newPaymentForm);
		this.newPaymentForm.click();
	};
	
	//** method to enter cc details
	this.enterCcDetails = function(ccNum,cvv){
		this.ccnumber.clear().sendKeys(ccNum);
		this.cvvText.clear().sendKeys(cvv);
		this.monthDropDown.click();
		this.monthDropDownSelect.click();
		this.yearDropDown.click();
		this.yearDropDownSelect.click();
    };

	//** method to enter billing details
	this.enterBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		console.log('billing info');
		this.fname.sendKeys(fname);
		this.lname.sendKeys(lname);
		this.address.sendKeys(address1);
		this.state.click();
		this.stateDropDownSelect.click();
		this.city.sendKeys(city);
		this.zipcode.sendKeys(zipcode);
		this.placeOrderBtn.click();
	};
	//Method to click on Bank Account tab in Payment - for reactivation with ACH paurchase scenario
	this.clickOnBankAccTab = function(){
		ElementsUtil.waitForElement(this.tabBankAccount);
		this.tabBankAccount.click();
	};
	// method to enter ACH details for reactivation with purchase scenario
	this.enterAchDetails = function(accno,routingno){
		this.selectAccType.click();
		this.selectAccTypeOpt.click();
		this.txtAccNo.clear().sendKeys(accno);
		this.txtRoutingNo.clear().sendKeys(routingno);
    };
	
	//** method to proceed from the survey page
	this.clickOnSurvey = function(){
		ElementsUtil.waitForElement(this.surveyContyinue);
        this.surveyContyinue.click();		
	};
	
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function(){	
		return ElementsUtil.waitForUrlToChangeTo(/confirmation_activation$/);		
	};
	
	//** method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function(){
		ElementsUtil.waitForElement(this.proceedFromConfirmation);
		this.proceedFromConfirmation.click();
	}; 
	
    //**method to opt for autopay option in checkout page -- single line activation enrollment changes
	this.optForAutoPay = function() {
		this.autoPaySlideBar.click();
	};
	
	this.clickOnBankAccount = function(){
		console.log("net banking 2");
		ElementsUtil.waitForElement(this.bankAccount);
		this.bankAccount.click();
	};
	
	this.accountTypeDetails = function(accountNumber,routingNumber){
		console.log("net banking 4");
		ElementsUtil.waitForElement(this.bankAccount);
		this.bankAccount.click();
		//ElementsUtil.waitForElement(this.accountType);
		this.accountType.click();
		this.accountTypeSelect.click();
		this.accountNo.clear().sendKeys(accountNumber);
		this.routingNumber.clear().sendKeys(routingNumber);
	};
	
};

module.exports = new ServiceRenewChkout;