'use strict';
var ElementsUtil = require("../util/elements.util");

/*
	This represents modal popup used to add a device
	Methods
		1. addDeviceWithEsn - 
		2.
		3.
*/
var AddDevice = function() {

	this.simTextbox= element(by.xpath("//input[@id='adddevice-esnsimmin']"));	
	this.addDeviceBtn = element.all(by.css("[ng-click='action()']")).get(1);  
	this.closeButton= element(by.xpath("/html/body/div[1]/div/div/modal-popup/div[1]/button"));
	this.pinTextbox = element(by.xpath("//input[@id='confirmaddpin-esnsimmin']"));  
	this.addPinBtn = element.all(by.css("[ng-click='action()']")).get(2);

	/* Adding device providing esn*/
	this.addDeviceWithEsn = function(esn) {
	};
	
	//addDeviceWithMin
	
	this.addDevice = function(esn) {
		//ElementsUtil.waitForElement(this.esnTextbox);
		this.esnTextbox.click();
		//ElementsUtil.waitForElement(this.esnTextbox);
		this.esnTextbox.sendKeys(esn);
		//ElementsUtil.waitForElement(this.addDeviceBtn);
		this.addDeviceBtn.click();
		//ElementsUtil.waitForElement(this.pinTextbox);
		this.pinTextbox.sendKeys(1234);
		//ElementsUtil.waitForElement(this.addPinBtn);
		this.addPinBtn.click();
	};

	this.isDeviceAdded = function(esn) {
		var devicePannel = element(by.xpath("//span[@value='"+esn+"']"));
		//ElementsUtil.waitForElement(devicePannel);
		return devicePannel.isPresent();
	};
};

module.exports = new AddDevice;