'use strict';
var ElementsUtil = require("../util/elements.util");

//=============Serves all functions inside MyAccount============

var MyAccount = function() {
	
	this.home = element.all(by.css("a[href='/']")).get(0);
	this.dashboardMyAcct = element(by.css("a[href='/accountrecovery']"));
	this.myDevices = element(by.linkText('MY DEVICES'));  //Add Device
//	this.manageProfile = element(by.css("a[id='Manageprofile']"));	
	this.manageProfile = element(by.id("Manageprofile"));	
	this.paymentMethod = element(by.css('a[id="paymentmethod"]'));
	this.paymentHistory = element(by.linkText('PAYMENT HISTORY'));
/*	this.signOut = element(by.linkText('SIGN OUT'));
	this.addDevice = element.all(by.css("[ng-click='triggerAddDevice()']")).get(0);*/

	this.signOut = element.all(By.css('a[id="Signoutnav"]')).get(0);
	this.addDevice = element.all(by.css("[ng-click='triggerAddDevice()']")).get(0);	

	this.isPhoneActiveNo = element.all(by.id('btn_adddevice1')).get(1);
	//this.activateBtn = element.all(by.buttonText('Activate')).get(0);
	this.activateBtn = element(by.buttonText('Activate'));
	this.AutoPayBtn = element.all(by.css("span[translate='MYACC_5000']")).get(1);	
	this.allOptions = element.all(by.css("strong[translate='MYACC_5005']")).get(0);
	this.buyPlan = element(by.css('strong[translate="MYACC_5015"]')); 	
//	this.buyPlan = element(by.css('[ng-click="buyAddOns()"]'); 	
	this.selectPlan = element(by.css("label[for='add_on_486']")); 	
	this.continueToCheckout = element.all(by.css("button[ng-click='action()']")).get(2);//5
	this.newpopupwindow = element.all(by.css("button[id='addToCartOneTimePurchpopup_btn']")).get(0);
	this.serviceRenewBtn = element.all(by.css("span[translate='MYACC_5001']")).get(0); //payservice
	//Multiline Activation with CC purchase starts
	this.linkAddLine		= element(by.id('Adddevicenav'));
	this.btnadddeviceYesNo	= element.all(by.id('btn_adddevice1')).get(1);
	//Multiline Activation with CC purchase ends
		this.myAccClick = element(by.id('lnk_myaccount'));
	//Upgrade ESN active to inactiveESN(non reserve) starts :Remya
	this.selectDevice 				= element.all(by.css('[ng-model="$select.search"]')).get(0);
	this.selectActiveMin			= element.all(by.className('dropdown-content-item ng-scope')).get(0);
	this.btnContinuePhonenumber		= element.all(by.id('btn_continuephonenumber')).get(0);
	//Upgrade ESN active to inactiveESN(non reserve)ends
this.dashboard = function() {
		this.home.click();
		ElementsUtil.waitForElement(this.dashboardMyAcct);
		this.dashboardMyAcct.click();
	};
	this.goToMultilineAutopay = function() {
		ElementsUtil.waitForElement(this.AutoPayBtn);
		this.AutoPayBtn.click();
	};
	
	this.payService = function(){  //payservice
		this.serviceRenewBtn.click();
	};
	
	this.refresh = function(){  //payservice
		this.myAccClick.click();
	};
	
	
	this.goToAllOptions = function() {
		ElementsUtil.waitForElement(this.allOptions);
		return this.allOptions.click();
	};
	
	this.chooseBuyAddon = function() {
		return this.buyPlan.click();
	}; 
	
	this.goToSelectPlan = function() {
		ElementsUtil.waitForElement(this.selectPlan);
		this.selectPlan.click();	
		return this.continueToCheckout.click();
	}; 
	
	this.goToMyDevices = function() {
		ElementsUtil.waitForElement(this.myDevices);
		return this.myDevices.click();
	};
	
	this.goToManageProfile = function() {
		ElementsUtil.waitForElement(this.manageProfile);
		return this.manageProfile.click();
	};
	
	this.goToPaymentMethod = function() {
		ElementsUtil.waitForElement(this.paymentMethod);
		return this.paymentMethod.click();
	};
	
	this.goToPaymentHistory = function() {
		ElementsUtil.waitForElement(this.paymentHistory);
		return this.paymentHistory.click();
	};
	
	this.goToAddDevice = function() {
	//	ElementsUtil.waitForElement(this.addDevice);
		this.addDevice.click();
		return this.isPhoneActiveNo.click();
		
	};
	
	this.goToSignOut = function() {
		ElementsUtil.waitForElement(this.signOut);
		this.signOut.click();
	};
	
	//** method checks whether the account dashboard is loaded or not
	this.isLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/dashboard$/);
	};
	
	this.isDeviceLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/activation\/selectdevice$/);
	};
	
	//** method to proceed with the pay service flow
	this.payService = function(){
		ElementsUtil.waitForElement(this.serviceRenewBtn);
		this.serviceRenewBtn.click();
	};
	
	//** method to check whether the checkout page is loaded or not
	this.checkoutPageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/checkout$/);	
	};
	
	//** method to click on the activate button, after adding a device through the dashboard
	this.clickOnActivate = function() {
		this.activateBtn.click();
	};
	
	//** method to check whether  a popup is dispalyed after clicking the pay service button in the account dashboard
	this.newPopupLoaded = function(){
		ElementsUtil.waitForElement(this.newpopupwindow);
		return this.newpopupwindow.isPresent();
	};
	
	//** method to proceed from the popup page
	this.continueFromPopUp = function(){
		ElementsUtil.waitForElement(this.newpopupwindow);
		this.newpopupwindow.click();
	};
	//Multiline Activation with CC purchase starts:Remya
	this.clickOnAddLineMenu = function(){
		ElementsUtil.waitForElement(this.linkAddLine);
		this.linkAddLine.click();
	};
	this.isPhoneActivePopUpLoaded = function(){
		ElementsUtil.waitForElement(this.btnadddeviceYesNo);
		return this.btnadddeviceYesNo.isPresent();
	};
	this.clickOnPhoneActivePopUpOptn = function(){
		ElementsUtil.waitForElement(this.btnadddeviceYesNo);
		this.btnadddeviceYesNo.click();
	};
	//Multiline Activation with CC purchase ends
	//Upgrade ESN active to ESN inactive(non reserve) starts :Remya
	this.selectDevicePageLoaded = function(){
		ElementsUtil.waitForElement(this.selectDevice);
		return this.selectDevice.isPresent();
	};
	this.selectActivatedMin = function(){
		ElementsUtil.waitForElement(this.selectDevice);
		this.selectDevice.click();
		this.selectActiveMin.click();
		this.btnContinuePhonenumber.click();
	};
	//Upgrade ESN active to ESN inactive(non reserve) ends
	
};

module.exports = new MyAccount;
