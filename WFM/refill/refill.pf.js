'use strict';
var ElementsUtil = require("../util/elements.util");


var Refill = function() {
     
    this.refillPinBox = element.all(by.id('form_redeem.pin')).get(1);
	this.refillMinBox = element.all(by.name('devicemin')).get(1);
	this.refillButton = element.all(by.id('AddAirtime')).get(0);
	this.doneButton = element(by.id('done'));
	this.payService = element(by.id('lnk_PayService'));
	this.phoneNo = element.all(by.id('formphone-min')).get(1);
	this.renewServiceBtn = element.all(by.id('RenewService')).get(0);
	this.selectPlanBtn = element.all(by.css('[class="btn tf-btn-primary btn-block"]')).get(1);
	this.autoPayBtn = element(by.id('addToCartEnroll_popup_btn'));
	this.continueToCheckOutBtn = element.all(by.id('addonPopUpCheckout_btn')).get(1);
	this.checkBoxSelectAddons = element(by.css("label[for='add_on_486']"));
	this.checkOutLoginBtn = element.all(by.id('btn_continue23')).get(0);
	this.skipAutoRefill = element(by.id('skip_autorefill'));
	//this.continueToPayment = element.all(by.id('ContinueToPayment')).get(1);
	this.continueToPayment = element(by.id('ContinueToPayment'));
	//this.doneBtnConfirmation = element.all(by.id('done_confirmation')).get(1);
	this.doneBtnConfirmation = element(by.id('done_confirmation'));
	
	this.minNumber = element.all(by.id('phonenum-min')).get(1);
	this.continueMinPopUp = element(by.id('saveDeviceSubmit_btn'));
	this.goToRefillPage = function() {
		ElementsUtil.waitForElement(this.payService);
		return this.payService.isPresent();
	};
	
	//*** method to check whether the refill page loaded or not
	this.isRefillPageLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/collectminpinpromo$/);
	};
	
	 //*** method to  check the refill of service plan is successful or not
	this.isConfirmationPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/redemptionconfirmationSuccess$/);	
	};
	
	this.goToRefillPageSendDatas = function(phno) {
		this.phoneNo.sendKeys(phno);
		this.renewServiceBtn.click();
	};
	
	//*** this method checks whether the service plan page is loaded or not
	this.isServiceplanPageLoaded = function() {	
		//ElementsUtil.waitForElement(this.planPge);
		return ElementsUtil.waitForUrlToChangeTo(/serviceplan$/);
	};
	
	//*** this method purchases the plans
	this.selectPlan = function() {
		this.selectPlanBtn.click();
	};
	
	//** this method checks the auto-refill is needed for the plan or not
	this.proceedSelectPlan = function(){
		ElementsUtil.waitForElement(this.autoPayBtn);
		return this.autoPayBtn.isPresent();
	}
	
	//*** this method auto refills the plan after the end date
	this.autoPayYes = function(){
		ElementsUtil.waitForElement(this.autoPayBtn);
		this.autoPayBtn.click();
	};
	
	this.selectPlanPopUpAddon = function() {
		this.checkBoxSelectAddons.click();
		this.continueToCheckOutBtn.click();
	};
	
	//** this method checks the checkout page is loaded or not
	this.continueToCheckOutPage = function(){
		return this.continueToCheckOutBtn.isPresent();
	};
	
	this.skipAutoRefillSelect = function(){
		return this.skipAutoRefill.click();
	};
	
	//** this method checks the payment page is loaded or not
	this.continueToPaymentPage = function(){
		ElementsUtil.waitForElement(this.continueToPayment);
		return this.continueToPayment.isPresent();
	};
	
	this.doneBtnConfirmationPage = function(){
		ElementsUtil.waitForElement(this.doneBtnConfirmation);
		return this.doneBtnConfirmation.click();
	};
	
	this.checkOutLoginProceed = function(){
		ElementsUtil.waitForElement(this.skipAutoRefill);
		return this.skipAutoRefill.isPresent();
	};
	
	this.paymentSection = function() {
		this.paymentBtn.click();
	};
	
	//*** method to  refill the service plan by entering min, pin and add
	this.refillServicePlan = function(min,pin) {
	    console.log('min: ', min);
		console.log('pin: ', pin);
		ElementsUtil.waitForElement(this.refillMinBox);
		this.refillMinBox.sendKeys(min);
		ElementsUtil.waitForElement(this.refillPinBox);
		this.refillPinBox.clear().sendKeys(pin);
		ElementsUtil.waitForElement(this.refillButton);
		return this.refillButton.click();
	};
	
	//*** method to  click the done button once the refill is successful
	this.clickDoneButton = function() {
		ElementsUtil.waitForElement(this.doneButton);
		return this.doneButton.click();
	};
	this.isDoneButtonPresent = function() {
		ElementsUtil.waitForElement(this.doneBtnConfirmation);
		return this.doneBtnConfirmation.isPresent();
	};
	this.minNumberEnter = function(min){
		this.minNumber.sendKeys(min);
		this.continueMinPopUp.click();
	};
	this.minNumberPage = function(){
		ElementsUtil.waitForElement(this.minNumber);
		return this.minNumber.isPresent();
	};
};

module.exports = new Refill;