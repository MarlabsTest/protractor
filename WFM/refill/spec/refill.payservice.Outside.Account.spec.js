'use strict';

var homePage = require("../../common/homepage.po");
var refillPage = require("../refill.po");
var loginData = require("../../common/sessiondata.do");
var drive = require('jasmine-data-provider');
var dataUtil= require("../../util/datautils.util");
var activationUtil = require('../../util/activation.util');
var generatedPin ={};
describe('WFM Refill Service with PIN outside AC', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = false;		
	});
	
	var activationData = activationUtil.getTestData();	
	console.log('activationData:', activationData);
	
	drive(activationData, function(inputActivationData) {
	
	it('should navigate to Refill page', function(done) {
		console.log("should navigate to My Refill page");
		homePage.goToRefill();
		expect(refillPage.isRefillPageLoaded()).toBe(true);		
		done();
	});
	
	
	 it('Enter min and pin then click on add service plan button', function(done) {
		var min = dataUtil.getMinOfESN(loginData.wfm.esn);
		loginData.wfm.min = min; 
        console.log('minVal ,    ::'+loginData.wfm.min);	 
		//refillPage.refillServicePlan('3053818349','999999937368249');
		var pinval = dataUtil.getPIN(inputActivationData.RedemptionPin);
       	console.log('pinVal', pinval);
       	generatedPin['pin'] = pinval;
		refillPage.refillServicePlan(loginData.wfm.min,pinval);        		
		expect(refillPage.isConfirmationPageLoaded()).toBe(true);		
		done();
	}).result.data = generatedPin;
	 
	
	it('click on Done button for Confirmation', function(done) {
		refillPage.clickDoneButton();
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(loginData.wfm.esn,loginData.wfm.pin,'401','WFM_Redeem_Pin','false','true');
		console.log('ITQ_DQ_CHECK table updated!!');
        expect(homePage.isLoaded()).toBe(true);       
		done();
	});
	
	});

});
	