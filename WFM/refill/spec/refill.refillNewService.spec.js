'use strict';
//Provide the mobile number which is active
//Then purchase any of the new plans from the provided plans
//then make the credit card payment for the plans which you have purchased
//And proceed to checkout

var refill = require("../../refill/refill.po");//added by karthick
var homePage = require("../../common/homepage.po");
var refillPage = require("../refill.po");
var myAccount = require("../../myaccount/myaccount.po");
var loginData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var activationUtil = require('../../util/activation.util');
var shop = require("../../shop/shop.po");

//** wfm refill the account by purchase scenario
describe('WFM Refill Service with PIN outside AC', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = false;		
	});
	
	//here the refill page is loaded
	it('should navigate to Refill page', function(done) {
		console.log("should navigate to My Refill page");
		homePage.goToRefill();
		expect(refillPage.isRefillPageLoaded()).toBe(true);		
		done();
	});
	
	//enter the mobile number which is active and go to purchse service plan  page
	it('Enter mobile number', function(done) {
		var min = dataUtil.getMinOfESN(loginData.wfm.esn);	
		loginData.wfm.min = min;
		refillPage.refillPageSendDatas(loginData.wfm.min);	
		expect(refillPage.isServiceplanPageLoaded()).toBe(true);
		done();
	});
	
	//purchase any plans from the given plans and procced to check out
	it('proceed to select the plans ',function(done){
		console.log("pop window for yes option");
		refillPage.selectPlan();
		expect(refillPage.minNumberPage()).toBe(true);
		//expect(refillPage.proceedSelectPlan()).toBe(true);
		done();
	});
	
	it('proceed to select the plans ',function(done){
		console.log("pop window for yes option");
		refillPage.minNumberEnter(loginData.wfm.min);
		expect(refillPage.proceedSelectPlan()).toBe(true);
		done();
	});
	
	//pop up window appears to select the refill plan or one time plan
	it('pop up window for ADD Ons option', function(done){
		console.log("ADD ONs option yes");
		refillPage.autoPayYes();
		expect(refillPage.checkOutLoginProceed()).toBe(true);
		//expect(refillPage.continueToCheckOutPage()).toBe(true);
		done();
	});
	
	/*it('select and continue to checkout', function(done){
		console.log("continue to check out");
		refillPage.selectPlanPopUpAddon();
		expect(refillPage.checkOutLoginProceed()).toBe(true);
		done();
	});*/
	
	//select the Skip link and it continues to the payment page 
	it('select skip link', function(done){
		console.log("check skip link");
		refillPage.skipAutoRefillSelect();
		expect(refillPage.continueToPaymentPage()).toBe(true);
		done();
	});
	//should click on the continue to payemnt button in checkout page and payment options will be displayed
	
	it('payment page should loaded ', function(done) {
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	//should fill the Credit card details and billing details and click on the place order button and will be navigated to confirmation page
	it('enter the credit card details', function(done) {
		myAccount.enterCcDetails('4012888888881881','123');
		myAccount.enterBillingDetails('Test','Test','1295 Charleston Road ','','Mountain View','94043');		
		expect(refillPage.isDoneButtonPresent()).toBe(true);
		done();		
	});
	
	//should click on the button in the confirmation page and will be navigated to the homepage
	it('proceed to confirmation page', function(done) {
		refillPage.doneBtnConfirmationPage();
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(loginData.wfm.esn,'0','6','WFM_Purchase','true','false');
		console.log('ITQ_DQ_CHECK table updated!!');
		expect(homePage.isLoaded()).toBe(true);
		done();		
	});
});

