'use strict';
var refillPf = require("./refill.pf");
var payment = require("../common/creditcardpayment.pf");

var Refill = function() {

	this.refillPageReturn = function() {
		return refillPf.goToRefillPage();
	};
	
	//*** method to check the refill page loaded
	this.isRefillPageLoaded = function() {	
		return refillPf.isRefillPageLoaded();
	};
	
	this.refillPageSendDatas = function(phno) {
		return refillPf.goToRefillPageSendDatas(phno);
	};
	
	//**Service plan page is loaded
	this.isServiceplanPageLoaded = function() {	
		return refillPf.isServiceplanPageLoaded();
	};
	
	//** purchase the plan
	this.selectPlan = function() {
		return refillPf.selectPlan();
	};
	
	this.proceedSelectPlan = function(){
		return refillPf.proceedSelectPlan();
	};
	//** To auto-refill the plan after the end date
	this.autoPayYes = function(){
	
		return refillPf.autoPayYes();
	};
	
	this.selectPlanPopUpAddon = function() {
		return refillPf.selectPlanPopUpAddon();
	};
	//** checkout page is loaded
	this.continueToCheckOutPage = function() {
		return refillPf.continueToCheckOutPage();
	};
	
	this.checkOutLoginProceed = function () {
		return refillPf.checkOutLoginProceed();
	};
	
	this.skipAutoRefillSelect = function(){
		return refillPf.skipAutoRefillSelect();
	};
	//** payment page is loaded	
	this.continueToPaymentPage = function (){
		return refillPf.continueToPaymentPage();
	};
	
	this.paymentSection = function() {
		return refillPf.paymentSection();
	};
	
    //*** method to refill the service plan
	this.refillServicePlan = function(min,pin) {
		refillPf.refillServicePlan(min,pin);
	};
	
	this.doneBtnConfirmationPage = function(){
		return refillPf.doneBtnConfirmationPage();
	};
	
	//*** method to check the refill of service plan is successfully done
	this.isConfirmationPageLoaded = function() {
		return refillPf.isConfirmationPageLoaded();
	};
	
	 //*** method to  click the done button once the refill is successful
	this.clickDoneButton = function() {
		return refillPf.clickDoneButton();
	};
	
	this.isDoneButtonPresent = function(){
		return refillPf.isDoneButtonPresent();
	};
   
	this.minNumberEnter = function(min){
		return refillPf.minNumberEnter(min);
	};
	
	this.minNumberPage = function(){
		return refillPf.minNumberPage();
	};
	
};

module.exports = new Refill;