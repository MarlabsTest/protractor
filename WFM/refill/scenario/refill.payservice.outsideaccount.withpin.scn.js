'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Refill Service With Pin Outside Account', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				sessionData.wfm.redemptionPin = inputActivationData.RedemptionPin;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_REFILL_WITH_PIN_OUTSIDE_ACC');
		}).result.data = inputActivationData;
	});
});



//Buy a service plan Outside Account - Main Spec.
/*describe('WFM Refill Service with PIN outside Account Main Spec', function() {
		
	
	require("../../activation/spec/activate-family-phone-spec.js");
	require("../../common/spec/myaccount.logout.spec.js");
	require("./refill.payservice.Outside.Account.spec.js");
	
});*/
	