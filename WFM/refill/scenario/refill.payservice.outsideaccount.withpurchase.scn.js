'use strict';
//Provide the mobile number which is active
//Then purchase any of the new plans from the provided plans
//then make the credit card payment for the plans which you have purchased

var activationUtil = require('../../util/activation.util');
var drive = require('jasmine-data-provider');
var sessionData = require("../../common/sessiondata.do");
var FlowUtil = require('../../util/flow.util');
//Buy a service plan Outside Account - Main Spec.
describe('Refill Service With Plan Outside Account', function() {
		
	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('REFILL_PLAN_WITH_PURCHASE');
		}).result.data = inputActivationData;
	});
	
});
	