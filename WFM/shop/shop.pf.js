'use strict';
var ElementsUtil = require("../util/elements.util");

var ShopPf = function() {
	this.shopPhonesNowBtn = element(by.linkText('Shop Phones Now'));
	//this.shopPhonesNowBtn = element.all(by.id('lnk_phones')).get(0);
	this.shopServicePlanLink = element.all(by.linkText('Shop Plans')).get(0);
	//.shopPhonesNowBtn = element.all(by.linkText('SHOP PHONES NOW')).get(0);
	//this.shopPhonesNowBtn = element(by.xpath('/html/body/tf-update-lang/div[1]/div/div[2]/div/div/div[2]/div/div/ng-include/tf-update-title/tf-update-meta/tf-update-meta/tf-update-meta/tf-update-meta/tf-update-meta/tf-update-meta/tf-update-meta/div[1]/div/div/div[2]/div/div[2]/p[2]/a'));	
	this.walmartPage = element(by.xpath('//*[@id="search"]'));
    //this.additionalServiceContinueButton = element(by.xpath('//*[@id="modal-body"]/div[2]/div/div/div[2]/tf-button-primary/span[2]/button/span')); // kapil added	
	//this.autoPopUpOptionButton = element(by.xpath('//*[@id="modal-body"]/div[2]/div/div[2]/wfm-button-primary[1]/span[2]/button/span'));	
	this.autoPopUpOptionButton =element(by.id('addToCartOneTimePurchpopup_btn')); 
	this.additionalServiceContinueButton =element.all(by.id('addonPopUpCheckout_btn')).get(1);
	
	this.goToShopServicePlan = function() {
		return this.shopServicePlanLink.click();
	};
	
	this.isLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/shop$/);
	};
	
	// kapil added starts
	
	// check the presence of (Not this time) Button in the auto pay popup (user can choose for auto refill or not)
	this.autoPayPopUp=function(){
		ElementsUtil.waitForElement(this.autoPopUpOptionButton);
		return this.autoPopUpOptionButton.isPresent();	
	};
  
  // check the presence of continue button in the additional servcie popup, (user can even choose for addition service or not and continue)
	this.additionalSerivePopUp = function() {
		ElementsUtil.waitForElement(this.additionalServiceContinueButton);
		return this.additionalServiceContinueButton.isPresent();
	};
	
	// Click the Continue Button (not choosing additional service here)  in the additional service popup
	this.additionalSerivePopUpButtonClick = function() {
		
	return this.additionalServiceContinueButton.click();
	};
	
	
	// waiting for the checkout page to be loaded
	this.isCheckoutPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/checkout$/);
	};
  
  
  // Click the Not this time Button in the auto pay popup
  this.autoPayPopUpOptionClick=function(){
	
	return this.autoPopUpOptionButton.click();
  };
  
  // kapil added end
	
	
	this.shopPhonesNow = function(phno) {
	ElementsUtil.waitForElement(this.shopPhonesNowBtn);
		this.shopPhonesNowBtn.click();
	};
	
	this.isWalmartPageLoaded = function() {	
		console.log("URL.........................."+browser.getCurrentUrl());
		 return browser.getAllWindowHandles().then(function (handles) {
           var newWindowHandle = handles[1]; 
		    browser.switchTo().window(newWindowHandle);
			 return browser.getCurrentUrl().then(function(url) {
				return (url.includes("walmart"));
				});
		 });
	};
	
};

module.exports = new ShopPf;