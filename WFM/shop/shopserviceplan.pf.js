'use strict';
var ElementsUtil = require("../util/elements.util");

var ShopServicePlanPf = function() {
	
	 this.anyMonthlyPlanLink = element(by.id('monthly_481'));
	 this.newCustomerLink = element(by.linkText("I'm a new customer"));
	 this.servicePlanPopUpMinBox = element.all(by.id('phonenum-min')).get(1); //added by kapil 
	 this.servicePlanPopUpSubmitBtn = element.all(by.id('saveDeviceSubmit_btn')).get(0);
	 this.monthlyPlan = element.all(by.css('[ng-click="action()"]')).get(2);
	 this.monthlyPlanPannel =  element(by.id("sp_sf_list"));
	 this.addonPlanPannel =  element(by.id("sp_payg_list"));
	 
	
	this.chooseAnyMonthlyServicePlan = function() {
		return this.anyMonthlyPlanLink.click();
	}; 
	
	this.isLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/serviceplan$/); 
	};
	
	this.ServicePlanForNewCustomer = function(planName){
		this.selectPlan(planName);
		this.newCustomerLink.click();
	};
	
	this.selectPlan = function(planName){
		element(by.css("button[aria-label*='"+planName+"']")).click();
		//this.monthlyPlan.click();
		//this.monthlyPlanPannel.element(by.css("button[title*='"+planName+"']")).click();
	}
	
	this.addonPlanForNewCustomer = function(addonName){
		this.selectAddonPlan(addonName);
		this.newCustomerLink.click();
	};
	
	this.selectAddonPlan = function(addonName){
		element.all(by.css("button[aria-label*='"+addonName+"']")).get(0).click();
		//this.addonPlanPannel.element(by.css("button[title*='"+addonName+"']")).click();
	};
	
	this.isActivatePageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/selectdevice$/);
	};
	
	//kapil added start
	
	// check for the presence of min input box and submit button of service plan popup
   this.servicePlanPopUp=function(){
		ElementsUtil.waitForElement(this.servicePlanPopUpMinBox);
		return this.servicePlanPopUpMinBox.isPresent();
	  };
	
	
	// input the min number to the text box and click the submit button in service plan popup
	this.ServicePlanForExistingCustomer = function(min){
		//ElementsUtil.waitForElement(this.servicePlanPopUpSubmitBtn);
		console.log("min::",min);
		ElementsUtil.waitForElement(this.servicePlanPopUpMinBox);
		this.servicePlanPopUpMinBox.sendKeys(min);		
		this.servicePlanPopUpSubmitBtn.click();
		
	};
	
	//kapil added end
};

module.exports = new ShopServicePlanPf;