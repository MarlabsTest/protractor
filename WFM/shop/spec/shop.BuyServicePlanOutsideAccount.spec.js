'use strict';
var refillPage = require("../../refill/refill.po");
var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var myAccount = require("../../myaccount/myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var loginData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");

describe('WFM Buy service plan Outside Account', function() {
	

	it('should open shopping page', function(done) {
		console.log("should open shopping page");
		homePage.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);	
		done();  
	});
	
	it('should open shop service plan page', function(done) {
		console.log("should open shop service plan page");
		shop.shopServicePlan();
		expect(shop.isShopServicePlanPageLoaded()).toBe(true);	
		done();  
	});	
	
	
	it('Choose any service plan from service plan page and should navigates to servicePlanPopUp', function(done) {
		console.log("choosing monthly service plan");
		//shop.chooseAnyMonthlyServicePlan();
		console.log("choose plan",sessionData.wfm.monthlyPlan);
		shop.selectServiceOnPlan(sessionData.wfm.monthlyPlan);		
		expect(shop.servicePlanPopUp()).toBe(true);
		done();  
	});

	
	
	 it('enter min and click submit in the collect min popup and should navigates to autoPayPopUp', function(done) {
		console.log("Shop service plan as a existing customer");
		var minval = dataUtil.getMinOfESN(sessionData.wfm.esn);
       	console.log('minVal', minval);
		//shop.shopServicePlanAsExistingCustomer('3054547857');	
		shop.shopServicePlanAsExistingCustomer(minval);
		expect(shop.autoPayPopUp()).toBe(true);
		done();  
	});
	
	it('Click the Not this time Button in the auto pay popup and should navigates to additionalSerivePopUp', function(done) {
		console.log("Click the Not this time Button in the auto pay popup");		
		shop.autoPayPopUpOptionClick();
		expect(shop.isCheckoutPageLoaded()).toBe(true);
		done();  
	});
	/*
	it('Click Continue to checkout button and should go to the Checkout page', function(done) {
		console.log("should go to the Checkout page");
		
		shop.additionalSerivePopUpButtonClick();
		expect(shop.isCheckoutPageLoaded()).toBe(true);
		done();  
	});
	*/
	
	it('should click on the continue to payemnt button in checkout page and payment options will be displayed ', function(done) {
		
		myAccount.continueToPayment();			
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	
	it('should fill the Credit card details and billing details and click on the place order button and will be navigated to confirmation page', function(done) {
		
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.wfm.cardType),CommonUtil.getCvv(sessionData.wfm.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);	
		expect(myAccount.confirmationPageLoaded()).toBe(true);
		done();		
	});
	
	it('should click on the button in the confirmation page and will be navigated to the Home page', function(done) {
		myAccount.proceedFromConfirmationPage();	
		dataUtil.checkRedemption(loginData.wfm.esn,'0','6','WFM_Purchase','true','false');
		console.log('ITQ_DQ_CHECK table updated!!');
		expect(homePage.isLoaded()).toBe(true);
		done();		
	});
	
	
	
});
	