'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");
var activation = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsnSim ={};
var generatedMin ={};

describe('WFM Buy Service plan Outside Account', function() {

	it('should open shopping page', function(done) {
		console.log("should open shopping page");
		homePage.goToShop();	
		expect(shop.isShopPageLoaded()).toBe(true);	
		done();  
	});
	
	it('should open shop service plan page', function(done) {
		console.log("should open shop service plan page");
		shop.shopServicePlan();	//start flows
		expect(shop.isShopServicePlanPageLoaded()).toBe(true);	
		done();  
	});
	
	it('Shop service plan as a new customer', function(done) {
		console.log("Shop service plan as a new customer");
		console.log(sessionData.wfm.monthlyPlan);
		shop.shopServicePlanAsNewCustomer(sessionData.wfm.monthlyPlan);		
		expect(shop.isActivatePageLoaded()).toBe(true);	
		done();  
	});
	it('should navigate to the page to enter SIM number',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.wfm.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.wfm.esn = esnval;
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	/*Enter a 4 digit security pin 
	 *Expected result - Page to opt for porting or new number will be displayed
	 */
	it('should navigate to page for entering zipcode', function(done) {
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		//expect(activation.isSIMPage()).toBe(true);
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	/*Enter a valid zipcode 
	 *Expected result - Page to proceed with plan purchase will be displayed
	 */	
	it('should navigate to the plan selection page', function(done) {
		activation.enterZipCode(sessionData.wfm.zip);		
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	});
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.wfm.username = emailval;
		sessionData.wfm.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Checkout Page will be shown
	 */
	it('should load the checkout page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 */	
	it('should load the payment form', function(done) {
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - final instruction page will be loaded
	 */	
	it('should load the final instruction page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.wfm.cardType),CommonUtil.getCvv(sessionData.wfm.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);		
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		console.log("MIN is  :::::" +min);
		generatedMin['min'] = min;
		expect(homePage.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	
});
	
