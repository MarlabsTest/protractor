'use strict';

var homePage = require("../../common/homepage.po");

var shop = require("../../shop/shop.po");


describe('WFM shop', function() {

	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	require("../../common/spec/myaccount.homePageLoad.spec.js"); 
	
	it('should open shopping page', function(done) {
		console.log("should open shopping page");
		homePage.goToShop();	
		expect(shop.isShopPageLoaded()).toBe(true);	
		done();  
	});
	
	 it('should load the WFM Phones page', function(done) {
		console.log("Phones Page");
		shop.shopPhonesNow();	
		expect(shop.isWalmartPageLoaded()).toBe(true);
		done();
	}); 
		
});
	