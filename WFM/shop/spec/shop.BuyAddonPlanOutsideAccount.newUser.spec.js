'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");


describe('WFM Buy service plan Outside Account', function() {

	it('should open shopping page', function(done) {
		console.log("should open shopping page");
		homePage.goToShop();	
		expect(shop.isShopPageLoaded()).toBe(true);	
		done();  
	});
	
	it('should open shop Add-on plan page', function(done) {
		console.log("should open shop service plan page");
		shop.shopServicePlan();	
		expect(shop.isShopServicePlanPageLoaded()).toBe(true);	
		done();  
	});
	
	it('Shop Add-on plan as a new customer', function(done) {
		console.log("Shop service plan as a new customer");
		shop.addonPlanForNewCustomer(sessionData.wfm.addonPlan);	
		expect(shop.isActivatePageLoaded()).toBe(true);	
		done();  
	});
	
});
	