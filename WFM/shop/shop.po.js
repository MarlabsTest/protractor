'use strict';

var shopPf = require("./shop.pf");
var shopServicePlanPf = require("./shopserviceplan.pf");


var Shop = function() {
	
	this.selectAddonPlan = function(addonName){
		shopServicePlanPf.selectAddonPlan(addonName);
	}
	
	this.addonPlanForNewCustomer = function(addonName){
		shopServicePlanPf.addonPlanForNewCustomer(addonName);
	}
	
	this.selectPlan = function(planName){
		shopServicePlanPf.selectPlan(planName);
	}

	this.shopPhonesNow = function() {
		return shopPf.shopPhonesNow();
	};
	
	 this.isWalmartPageLoaded = function() {
		return shopPf.isWalmartPageLoaded();
	  };
	
	this.shopServicePlan = function() {
		return shopPf.goToShopServicePlan();
	};
	
	this.isShopPageLoaded = function(){
		return shopPf.isLoaded();
	};
	
	this.isShopServicePlanPageLoaded = function(){
		return shopServicePlanPf.isLoaded();
	};
	
	this.shopServicePlanAsNewCustomer = function(planName){
		shopServicePlanPf.ServicePlanForNewCustomer(planName);
	};
	
	this.isActivatePageLoaded = function(){
		return shopServicePlanPf.isActivatePageLoaded();
	};
	
	this.chooseAnyMonthlyServicePlan = function() {
		
		return shopServicePlanPf.chooseAnyMonthlyServicePlan();
	}; 
	
	//kapil added start
	//*** method to call service plan popup
	this.shopServicePlanAsExistingCustomer = function(min){
		shopServicePlanPf.ServicePlanForExistingCustomer(min);
	};
	
	//*** method to call auto refill popup
	
	this.autoPayPopUp = function() {
		return shopPf.autoPayPopUp();
	};
	
	//*** method to call additional service popup
	this.additionalSerivePopUp = function() {
		return shopPf.additionalSerivePopUp();
	};
	
	//*** method to click not this time button in auto refill popup
	this.autoPayPopUpOptionClick = function() {
		return shopPf.autoPayPopUpOptionClick();
	};
	
	//*** method to click continue additional service popup
	this.additionalSerivePopUpButtonClick = function() {
		return shopPf.additionalSerivePopUpButtonClick();
	};
	
	//*** method to check the checkout page loaded
	this.isCheckoutPageLoaded = function(){
		return shopPf.isCheckoutPageLoaded();
	};
	
	//*** method to presence of elements in service plan popup
	this.servicePlanPopUp = function() {
		return shopServicePlanPf.servicePlanPopUp();
	};
	
	//kapil added end
	
	this.selectServiceOnPlan = function(planName){
		shopServicePlanPf.selectPlan(planName);
	};
	
};

module.exports = new Shop;