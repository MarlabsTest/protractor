//this represents a modal for phoneNumber page(port)
'use strict';
var ElementsUtil = require("../util/elements.util");

var Port = function() {
	
	this.KeepMyNumberBox = element.all(by.name('phone')).get(1);
	//this.keepMyNumContinueBtn =  element(by.id('btn_continuephonenumber'));
	//====================Gowtham Karthick
	//this.keepMyNumContinueBtn =  element.all(by.id('btn_continuephonenumber')).get(0);
	this.keepMyNumContinueBtn =  element(by.id('btn_continuephonenumber'));//Removed get(0)
	//====================
	this.accountPasswordTxt = element.all(by.id('password')).get(1);
	//this.accountPasswordContineBtn = element.all(by.css("[ng-click='action()']")).get(0);
	this.accountPasswordContineBtn = element(by.css("[ng-click='action()']"));
	//** method to enter phonenumber  and proceed from the page
	this.enterMobNumber = function(number){
		//browser.wait(expectedConditions.visibilityOf(this.KeepMyNumberBox),10000);
		//ElementsUtil.waitForElement(this.KeepMyNumberBox);
		this.KeepMyNumberBox.sendKeys(number);
		//browser.wait(expectedConditions.visibilityOf(this.keepMyNumContinueBtn),10000);
		//ElementsUtil.waitForElement(this.keepMyNumContinueBtn);
		browser.executeScript("arguments[0].click();", this.keepMyNumContinueBtn.getWebElement());
		//this.keepMyNumContinueBtn.click();
	};
  
	this.validateAccountPassword = function(password){
		this.accountPasswordTxt.sendKeys(password);
		this.accountPasswordContineBtn.click();
	};
	
	this.validateAccountPasswordPageLoaded = function(){
		//return ElementsUtil.waitForUrlToChangeTo(/PHONE_UPGRADE$/);
		return this.accountPasswordContineBtn.isDisplayed();
	};
};

module.exports = new Port;