//this represents a modal for the ESN page

'use strict';
var ElementsUtil = require("../util/elements.util");

var Esn = function() {

	this.esn = element(by.xpath("//input[@id='tfvalesn-esn']"));
	this.terms = element(by.id('term'));
	this.esnContinueBtn = element(by.css('[ng-click="action()"]'));//.get(0);
	this.securityPinBox=element.all(by.name('updateSecPin-pin')).get(1);
    this.pinContinueButton =  element(by.css('[ng-click="action()"]'));//.get(0);

	this.esnPageLoaded=function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /tracfonecollectinfo/.test(url);
		});
	};
	/* Change in FLOW	
	//**method to enter the esn
	this.enterEsn=function(esnVal){
		this.esn.clear().sendKeys(esnVal);		
	};*/
  
	//**method to enter the married SIM
	this.enterMarriedSim=function(simVal){
		this.esn.clear().sendKeys(simVal);		
	};
  
	//** method to click on terms and condition checkbox
	this.checkBoxCheck=function(){
		browser.executeScript("arguments[0].click();", this.terms.getWebElement());
	};
	
	//method to proceed from the esn page
	this.continueESNClick=function(){
		this.esnContinueBtn.click();
	};
	  
	//** method to check whether the security pin popup page is loaded
	this.securityPinPopUp=function(){
		ElementsUtil.waitForElement(this.securityPinBox);
		return this.securityPinBox.isPresent();
	};  
  
	//** method to enter pin 
	this.enterPin = function(pinVal){
		this.securityPinBox.clear().sendKeys(pinVal);
	};
  
	//** method to proceed from security pin popup page
	this.clickOnPinContinue = function(){
		this.pinContinueButton.click();	
	};
  
};
module.exports = new Esn;