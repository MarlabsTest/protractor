//this represents a modal for final instruction page

'use strict';

var ElementsUtil = require("../util/elements.util");

var FinalInstruction = function() {

	//this.finalInstructionContinueBtn =  element.all(by.css('[ng-click="action()"]')).get(1); 

	/*this.finalInstructionContinueBtnPortPin =  element.all(by.id('btn_summaryviewpincontinue')).get(0);
	this.finalInstructionContinueBtn =  element.all(by.id('btn_activatepin')).get(0);//1 
	this.finalInstructionContinueBtnPurchase =  element.all(by.id('btn_activatenopin')).get(0);//1
*/	
	this.finalInstructionContinueBtnPortPin =  element(by.id('btn_summaryviewpincontinue'));//Removed get(0)
	this.finalInstructionContinueBtn =  element(by.id('btn_activatepin'));//Removed get(0)
	this.finalInstructionContinueBtnPurchase =  element(by.id('btn_activatenopin'));//Removed get(0)
	//this.actualFICPurchaseBtn = this.finalInstructionContinueBtnPurchase.get(3);
	
	//this.portUpgradeInstructionContinueBtn =  element.all(by.id('btn_summaryviewpincontinue')).get(1);//1 
	//this.portUpgradeInstructionContinueBtn =  element.all(by.id('btn_summaryviewpincontinue')).get(0);//1
	this.portUpgradeInstructionContinueBtn =  element(by.id('btn_summaryviewpincontinue'))//Removed get(0)
	
	//** this method is to check whether the final instruction page is loaded or not
	this.finalInstructionPageLoaded = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtn),40000);
		ElementsUtil.waitForElement(this.finalInstructionContinueBtn);
		return this.finalInstructionContinueBtn.isPresent();
	};
	
	//** this method is to proceed from the port upgrade final instruction page
	this.portUpgradeInstructionProceed = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtn),40000);
		//ElementsUtil.waitForElement(this.portUpgradeInstructionContinueBtn);
		this.portUpgradeInstructionContinueBtn.click();
	};
  
	//** this method is to proceed from the final instruction page
	this.finalInstructionProceed = function(){
		//browser.wait(expectedConditions.visibilityOf(this.finalInstructionContinueBtn),40000);
		ElementsUtil.waitForElement(this.finalInstructionContinueBtn);
		this.finalInstructionContinueBtn.click();
	};
	
	this.finalInstructionPageLoadedPurchase = function(){		
		return this.finalInstructionContinueBtnPurchase.isPresent();
	};
	
	this.finalInstructionProceedPurchase = function(){
		this.finalInstructionContinueBtnPurchase.click();
	};
	
	this.finalInstructionPageLoadedPortPin = function(){
		ElementsUtil.waitForElement(this.finalInstructionContinueBtnPortPin);
		return this.finalInstructionContinueBtnPortPin.isPresent();
	};
	
	this.finalInstructionProceedPortPin = function(){
		ElementsUtil.waitForElement(this.finalInstructionContinueBtnPortPin);
		this.finalInstructionContinueBtnPortPin.click();
	};
  
};
module.exports = new FinalInstruction;
