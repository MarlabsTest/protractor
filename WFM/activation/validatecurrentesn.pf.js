//this is the page fragment that validate the current device esn

'use strict';
var ElementsUtil = require("../util/elements.util");
var validateEsn = function() {
		
	this.esnTextBox = element.all(by.id('securitypin_1')).get(1);
	//this.esnContinueBtn =  element.all(by.css('[type="submit"]')).get(0);//.get(2);
	this.esnContinueBtn =  element(by.css('[type="submit"]'));//Removed get(0)
	this.fourDigitCodeTextBox = element.all(by.id('lastfour_serial')).get(1);
	//this.fourDigitCodeContinueBtn =  element.all(by.css('[type="submit"]')).get(0);//.get(2);
	this.fourDigitCodeContinueBtn =  element(by.css('[type="submit"]'));//Removed get(0)
	
	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.validateEsnLastNumbersPageLoaded = function(){
		ElementsUtil.waitForElement(this.esnTextBox);
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /collectlastfourinvalidacct$/.test(url);
		});
	};
  
	//** this method is to check whether the validate last four esn number of the current device
	this.enterCurrentEsnLastFourDigits = function(number) {
		this.esnTextBox.clear().sendKeys(number);
		this.esnContinueBtn.click();
	};
	
	//** this method is to check whether the validate four digit code after message
	this.enterFourDigitCodeFromMsg = function(number) {
		this.fourDigitCodeTextBox.clear().sendKeys(number);
		this.fourDigitCodeContinueBtn.click();
	};
};
module.exports = new validateEsn;
