//this represnts a modal for the SIM page

'use strict';
var ElementsUtil = require("../util/elements.util");

var sim = function() {
	
	this.identifySIMPage = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[2]/subheading/div/div/div[1]/table/tbody/tr[1]/td"));
	this.simErrorField = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div"));
	this.simTextBox = element.all(by.name('sim')).get(1);//element.all(by.xpath('//*[@id="sim"]')).get(1);//*[@id="sim"]
	//this.simContinueBtn = element.all(by.css('[ng-click="action()"]')).get(0);
	this.simContinueBtn = element(by.css('[ng-click="action()"]'));//Removed get(0)
//	this.byopSimTextBox = element.all(by.id('number')).get(1);
	//this.byopSimContinueBtn = element.all(by.css('[ng-click="action()"]')).get(0);
	this.byopSimContinueBtn = element(by.css('[ng-click="action()"]'));//Removed get(0)
	this.byopPurchasePinBtn = element.all(by.css('[ng-click="action()"]')).get(3);
	this.byopSimTextBox = element(by.css('input[id="simbyop"]'));

	//to provide the BYOP SIM number
	this.enterByopSIM = function(simNum){
//		ElementsUtil.waitForElement(this.byopSimTextBox);
		this.byopSimTextBox.sendKeys(simNum);
		this.byopSimContinueBtn.click();
	};
  
	//checks whether the SIM page is loaded
	this.isByopSIMPage = function(){
        return browser.getCurrentUrl().then(function(url) {
			return /byopcollectsim/.test(url);
		});
	};
	
	//** method to check whether the SIM page is loaded
	this.isSIMPage = function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /collecttfsim/.test(url);
		});
	};
	
	//** method to enter the SIM
	this.enterSIM = function(simNum){
		this.simTextBox.clear().sendKeys(simNum);
		this.simContinueBtn.click();
	};
  
	//** method to display error after entering invalid SIM 
	this.isSIMError = function(){
		//browser.wait(expectedConditions.visibilityOf(this.simErrorField),10000);
		//ElementsUtil.waitForElement(this.simErrorField);
		return this.simErrorField.getText();
	};
	
};
module.exports = new sim;