//this represnts a modal for the summary page

'use strict';
var ElementsUtil = require("../util/elements.util");

var summaryPge = function() {
	
	
	//this.confirmationMessageText = element(by.xpath('/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div[1]/div/div/div[2]'));
	this.summaryBtn = element(by.id('withpin_donebtn'));//.get(0);//1 
	this.summaryBtnPurchase =  element(By.css('button[id="done_confirmation"]'));
	this.clickOnLogo = element(By.css('img[id="img_walmartfamilymobile"]'));
	this.clickOnmyAccount= element(By.css('a[id="lnk_myaccount"]'));

 
	//** this method is to check whether the summary page is loaded or not
	this.summaryPageLoaded  = function(){		
		return this.summaryBtn.isPresent();		
	};
  
	//** this method is to proceed from the summary page
	this.clickOnSummaryBtn = function(){		
		this.summaryBtn.click();		
	};
	
	this.summaryPageLoadedPurchase  = function(){
		return this.summaryBtnPurchase.isPresent();
	};
  
	this.clickOnSummaryBtnPurchase = function(){
		this.summaryBtnPurchase.click();
	};
	
	this.clickOnWfmLogo = function(){
		this.clickOnLogo.click();
	};
	
	this.checkOnMyAccountLink = function(){
		return this.clickOnmyAccount.isPresent();
	};
	
	this.clickOnMyAccountLink = function(){
		this.clickOnmyAccount.click();
	};
};
module.exports = new summaryPge;
