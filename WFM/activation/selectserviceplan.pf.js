//this represents a modal for servicePlan page
'use strict';

//var ElementsUtil = require("../util/elements.util");

var purchasePlan = function() {
	
	//this.plan =  element.all(by.css('[ng-click="action()"]')).get(0);
	this.plan =  element(by.css('[ng-click="action()"]'));//Removed get(0)
	
	//** method to check whether the service plan page is loaded or not
	this.selectServicePlanPageLoaded = function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /serviceplan/.test(url);
		});
	};
  
	//** this method is to select a plan
	this.pickPlan = function(){
		//browser.wait(expectedConditions.visibilityOf(this.plan),10000);
		//ElementsUtil.waitForElement(this.plan);
		this.plan.click();
	};
  
};

module.exports = new purchasePlan;

