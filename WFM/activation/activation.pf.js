//this modal represents a modal for whether to go with activate new phone or activate a BYOP

'use strict';

var ElementsUtil = require("../util/elements.util");

var Activate = function() {
	

	this.continueBtn =element(by.id('btn_continuetracfonephone'));//.get(0);//10


	this.byopBtn = element(by.id('btn_continuesmartphone'));
	this.termsConditionsModal = element(by.css('.modal-dialog.modal-md'));	
	this.termsConditionsAcceptBtn = element(by.id('btn_acceptterms&conditions'));
	
	this.maximiseBrowser = function() {
		browser.driver.manage().window().maximize();
	};
    
	//** this method checks whether the page is loaded or not after the click on activate link from homepage
	this.isActivateLoaded = function() {
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /selectdevice/.test(url);
		});
	};
	
	//** this method checks whether the page is directed to request submitted page
	this.activationRequestSubmittedPageLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/activation\/pininstructions\?redirect=PIN_FLOW$/);
	};

	//to select device type as "Bring your own phone"
	this.selectDeviceTypeBYOP = function() {
		console.log("clicking byop button");
		this.byopBtn.click();
	};
	
	//to accept terms and conditions
	this.acceptTermsConditions = function() {
		if(this.checkTermsConditionsModalExist())
		{
			this.termsConditionsAcceptBtn.click();
		}
	}
	
	this.checkTermsConditionsModalExist = function() {
		return this.termsConditionsModal.isPresent();
	}

	//**this method is to proceed with new phone activation flow
	this.gotToEsnPage= function() {
		this.continueBtn.click();		
	};   	
  
  };
module.exports = new Activate;
