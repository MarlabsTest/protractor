//**this represents a modal for account creation
 
'use strict';

var ElementUtil = require("../util/elements.util");

var CreateAccount = function() {

	this.fbButtonText = element(by.id('lnk_signupfacebook'));
	this.emailTextBox= element.all(by.name('createacfm.user_email')).get(1);
	this.passwordBox=element.all(by.name('createacfm-user_pwd')).get(1);
	this.DOBBox=element.all(by.name('createacfm-DOB')).get(1);
	this.confirmPswdBox=element.all(by.name('createacfm-confirmuser_pwd')).get(1);
	this.secPinBox=element.all(by.name('createacfm-user_pin')).get(1);
	this.accountCreatedPopUp = element(by.xpath('//*[@id="modal-body"]/div[2]/form/div/div[1]/div[2]'));
	this.accountCreatedPopupContinue = element.all(by.id('btn_continue11'));//.get(2);
	this.actualAccoutnCreatedPopup = this.accountCreatedPopupContinue.get(0);//1
	//this.accountCreatedPopupContinue = element.all(by.id('btn_continue11')).get(0);
	//this.actCreationBtn = element.all(by.id('btn_createaccount'));//.get(3)
	//this.accountCreationContinueBtn = element.all(by.css('[ng-click="action()"]'));//.get(3);
	//this.actCreationBtn = element.all(by.id('btn_createaccount')).get(0);//1
	this.actCreationBtn = element(by.id('btn_createaccount'));//Removed get(0)
	//this.actualAccountCreationContinueBtn = this.actCreationBtn.get(1);
	this.newAccountContinueBtn = element.all(by.id('btn_createnew')); 
	this.actualAccountCreated = element(by.id('btn_createnew'));//this.newAccountContinueBtn.get(0);
	
	//** this method checks whether the account creation page is loaded or not
	this.activationAccountPageLoaded = function(){  
		/*return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /activation!airtimeserviceplan$/.test(url);
		});*/
		return ElementUtil.waitForUrlToChangeTo(/activation!airtimeserviceplan$/);
	};
  
	//** this method is to proceed with account creation flow
	this.clickonAccountCreationContinueBtn = function(){
				
		this.actCreationBtn.click();
		//this.actualAccountCreationContinueBtn.click();
		
	};
	
	//** this method checks whether the account details fields to be enetered  are loaded or not
	this.fbButtonLoaded = function(){
		return this.fbButtonText.isPresent();
	};
	//** this method checks whether the account details fields to be enetered  are loaded or not
	this.emailTextBoxLoaded = function(){
		return this.emailTextBox.isPresent();
	};
	
	
	//** this method is to enter the required fields to create new account
	this.enterAccountDetails= function(email,password,DOB,pin){
		this.emailTextBox.clear().sendKeys(email);
		this.passwordBox.clear().sendKeys(password);
		this.confirmPswdBox.clear().sendKeys(password);
		this.DOBBox.clear().sendKeys(DOB);
		this.secPinBox.clear().sendKeys(pin);
		
		
		browser.executeScript("arguments[0].click();", this.actualAccountCreated.getWebElement());
		
	};
	
	//** this method is to check whether the popup page is loaded after successful account creation
	this.accountCreationDone = function(){
		ElementUtil.elementHasCome(this.accountCreatedPopupContinue, 0);
		return this.actualAccoutnCreatedPopup.isPresent();
		
	};
	
	//** this method is to proceed from the popup page
	this.clickOnAccountCreatedPopupBtn = function(){
	//browser.wait(Waitutil.elementHasCome(this.accountCreatedPopupContinue, 2), 20000);
		ElementUtil.elementHasCome(this.accountCreatedPopupContinue, 0);
		this.accountCreatedPopupContinue.click();
		//this.accountCreatedPopupContinue.click();
		
	};
};
module.exports = new CreateAccount;