//** This represents a  common page object for all the activation flows.

'use strict';
var activationPF = require("./activation.pf");
var esnObj = require("./esn.pf");
var simObj = require("./sim.pf");
var zipCodeObj = require("./zipcode.pf");
var purchasePlan = require("./purchaseplan.pf");
var airtimePinObj = require("./airtimepin.pf");
var newAccountObj = require("./accountcreation.pf");
var summaryObj = require("./summarypage.pf");
var surveyPageObj = require("./survey.pf");
var finalInstructionPage = require("./finalinstruction.pf");
var existingAccnt = require("./existingaccount.pf");
var portIn = require("./port.pf");
var phoneAccountDetails  = require("./phoneaccountdetails.pf");
var selectPlan = require("./selectserviceplan.pf");
var validateCurrentEsn = require("./validatecurrentesn.pf");

var Activate = function() {

	this.validateAccountPassword = function(password){
		portIn.validateAccountPassword(password);
	};
	
	this.validateAccountPasswordPageLoaded = function(){
		return portIn.validateAccountPasswordPageLoaded();
	};
	
	//to select device type as "Bring your own phone"
	this.selectByopDeviceType = function() {
		activationPF.selectDeviceTypeBYOP();
	};
	
	//to accept terms and conditions
	this.acceptTermsConditions = function() {
		activationPF.acceptTermsConditions();
	};
  
	//to provide the BYOP SIM number
	this.enterByopSim = function(simNum){
		simObj.enterByopSIM(simNum);
	};
  
	//to provide zipcode for byop flow
	this.enterByopZipCode = function(zipCode){
		zipCodeObj.enterByopZipCode(zipCode);
	};
  	
  	//checks whether the SIM page is loaded
	this.isByopSIMPage = function(){
		return simObj.isByopSIMPage();
	};

	//this.waitForElementLoad  = function(min) {
		//browser.sleep(min);
	//};
  
	this.maximiseBrowser = function() {
		browser.driver.manage().window().maximize();
	};
	
	//** this method checks whether the page is loaded or not after the click on activate link from homepage	
	this.isActivateLoaded = function() {
		return activationPF.isActivateLoaded();
	};
	
	//**this method is to proceed with new phone activation flow
	this.gotToEsnPage= function() {
		return activationPF.gotToEsnPage();
	};
	
	//**checks whether the esn page is loaded or not
	this.esnPageLoaded=function(){
		return esnObj.esnPageLoaded();
	};
	
   //**method to enter the esn 
	this.enterEsn=function(esnVal){
		esnObj.enterEsn(esnVal);		
	};
	
	
	//**method to enter the married SIM 
	this.enterMarriedSim=function(simVal){
		esnObj.enterMarriedSim(simVal);		
	};
	
	//** method to click on terms and condition checkbox
	this.checkBoxCheck=function(){
		esnObj.checkBoxCheck();
	};
	
	//method to proceed from the esn page
	this.continueESNClick=function(){
		esnObj.continueESNClick();
	};
	
	//method to display error message after entering already activated ESN
	this.errorMessage=function(){
		return esnObj.errorMessage();
	};
  
	//method to display error message after entering a random number
	this.randomEsnErrorMsg = function(){
		return esnObj.randomEsnErrorMsg();
	};
	
	//** method to check whether the security pin popup page is loaded
	this.securityPinPopUp=function(){
		return esnObj.securityPinPopUp();
	};
	
	//** method to display error message after entering invalid pin
	this.displayPinError = function(){
		return esnObj.displayPinError();
	};
	
	//** method to enter pin 
	this.enterPin = function(pinVal){
		esnObj.enterPin(pinVal);
	};
  
	//** method to proceed from security pin popup page
	this.clickOnPinContinue = function(){
		esnObj.clickOnPinContinue();
	};
  
	//** method to check whether the SIM page is loaded
	this.isSIMPage = function(){
		return simObj.isSIMPage();
	};
  
	//** method to enter the SIM
	this.enterSIM = function(simNum){
		simObj.enterSIM(simNum);
	};
  
	//** method to display error after entering invalid SIM 
	this.isSIMError = function(){
		return simObj.isSIMError();
	}; 
  
	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.keepMyPhonePageLoaded = function(){
		return zipCodeObj.keepMyPhonePageLoaded();
	};
  
	//** method to enter zipcode
	this.enterZipCode = function(zipCode){
		zipCodeObj.enterZipCode(zipCode);
	};
	
	//** method to check whether the plan(purchase plan/already have a pin -flows) page is loaded
	this.servicePlanPageLoaded = function(){
		return purchasePlan.servicePlanPageLoaded();
	};
	
	//** method to enter airtime pin
	this.enterAirTimePin = function(pin){
		airtimePinObj.enterAirTimePin(pin);      
	};
  
	//** this method is used to choose the option purchase
	this.airtimePurchase = function(){
		purchasePlan.airtimePurchase();
	};
  
	//** this method checks whether the account creation/existing account flows page is loaded or not
	this.activationAccountPageLoaded = function(){ 
		return newAccountObj.activationAccountPageLoaded(); 
	};
	
	//** this method checks whether the activation request submitted page is loaded or not
	this.activationRequestSubmittedPageLoaded = function(){ 
		return activationPF.activationRequestSubmittedPageLoaded(); 
	};
  
	//** this method is to proceed with account creation flow
	this.clickonAccountCreationContinueBtn = function(){
		newAccountObj.clickonAccountCreationContinueBtn();
	};
  
	//** this method checks whether the account details fields to be enetered are loaded or not
	this.fbButtonLoaded = function(){
		return newAccountObj.fbButtonLoaded();
	};
	//** this method checks whether the account details fields to be enetered are loaded or not
	this.emailTextBoxLoaded = function(){
		return newAccountObj.emailTextBoxLoaded();
	};
	
	
	//** this method is to enter the required fields to create new account
	this.enterAccountDetails= function(email,password,DOB,pin){
		newAccountObj.enterAccountDetails(email,password,DOB,pin);
	};
	
	//** this method is to check whether the popup page is loaded after successful account creation
	this.accountCreationDone = function(){
		return newAccountObj.accountCreationDone();
	};
  
	//** this method is to proceed from the popup page
	this.clickOnAccountCreatedPopupBtn = function(){
		newAccountObj.clickOnAccountCreatedPopupBtn();
	};
    
	//** this method is to check whether the final instruction page is loaded or not
	this.finalInstructionPageLoaded = function(){
		return finalInstructionPage.finalInstructionPageLoaded();
	};
  
	//** this method is to proceed from the final instruction page
	this.finalInstructionProceed = function(){
		finalInstructionPage.finalInstructionProceed();
	};
	
	//** this method is to proceed from the port upgrade final instruction page
	this.portUpgradeInstructionProceed = function(){
		finalInstructionPage.portUpgradeInstructionProceed();
	};
	
	//** this method is to check whether the summary page is loaded or not
	this.summaryPageLoaded  = function(){
		return summaryObj.summaryPageLoaded();
	};
  
	//** this method is to proceed from the summary page
	this.clickOnSummaryBtn = function(){
		summaryObj.clickOnSummaryBtn();
	};
  
	//** this method is to check whether the survey page is loaded or not
	this.surveyPageLoaded = function(){
		return surveyPageObj.surveyPageLoaded();
	};
  
	//** this method is to proceed from the survey page using the THANK YOU option
	this.clickOnThankYouBtn = function(){
		return surveyPageObj.clickOnThankYouBtn();
	}; 
  
	//** this method is to proceed with existing account flow
	this.clickonExistingAccountContinueBtn = function(){
		existingAccnt.clickonExistingAccountContinueBtn();
	};
	
	//** this method is to check whether the field to enter existing userid is loaded or not
	this.existingUserIdBoxLoaded = function(){
		return existingAccnt.existingUserIdBoxLoaded();
	};
  
	//** this method is to enter the userid/sim(field identifier is SIM hence using SIM in the methods)
	this.enterSimInExistingflow = function(sim){
		existingAccnt.enterSimInExistingflow(sim);
	};
  
	//** this method is to check whether field to enter the password is loaded or not
	this.enterPasswordBoxLoaded = function(){
		return existingAccnt.enterPasswordBoxLoaded();
	};
  
	//** this method is to enter the password
	this.enterPassword = function(pwd){
		existingAccnt.enterPassword(pwd);
	};
   
	//** this method is to check whether the page to select service plan is loaded or not
	this.selectServicePlanPageLoaded = function(){
		return selectPlan.selectServicePlanPageLoaded();
	};
  
	//** this method is to select a plan
	this.pickPlan = function(){
		selectPlan.pickPlan();
	};
  
	//** this method is check whether the page to enter service provider details is loaded or not
	this.ServiceProviderPageLoaded = function(){
		return phoneAccountDetails.ServiceProviderPageLoaded();
	};
  
	//** this method is to enter the phone type
	this.selectPhoneType = function(){
		phoneAccountDetails.selectPhoneType();
	};
	
	//** this method is to check whether the fields to enter phone account details is loaded or not
	this.accountDetailsLoaded = function(){
		return phoneAccountDetails.accountDetailsLoaded();
	};
  
	//** this method is to enter the phone account details
	this.enterPhoneAccountDetails = function(accountNum,pswd,socialSec,fname,lname,phnNo,addOne,addtwo,city,zip) {
		phoneAccountDetails.enterPhoneAccountDetails(accountNum,pswd,socialSec,fname,lname,phnNo,addOne,addtwo,city,zip);
	};
  
	//** this method is to check whether the pop up for selecting address is loaded or not
	this.selectAddressPopUpLoaded = function(){
		return phoneAccountDetails.selectAddressPopUpLoaded();
	};
  
	//** this method is to click on the keep this address button  in the pop up
	this.keepThisAddress = function(){
		phoneAccountDetails.keepThisAddress();
	};
  
	//** this method is to check whether the page to enter address details is loaded or not
	this.addressDetailsPageLoaded = function(){
		return phoneAccountDetails.addressDetailsPageLoaded();
	};
  
	//** this method is to enter the address details
	this.addressDetails = function(unitNo,street,houseNo){
		phoneAccountDetails.addressDetails(unitNo,street,houseNo);
	};
  
	//** this method is to enter the mobile number 
	this.enterMobNumber = function(number){
		portIn.enterMobNumber(number);
	};
	
	//** this method is to check whether the validate last four esn number of the current device - page loaded
	this.validateEsnLastNumbersPageLoaded = function(){
		return validateCurrentEsn.validateEsnLastNumbersPageLoaded();
	};
	
	//** this method is to check whether the validate last four esn number of the current device
	this.enterCurrentEsnLastFourDigits = function(number){
		validateCurrentEsn.enterCurrentEsnLastFourDigits(number);
	};
	
	//** this method is to check whether the validate four digit code after message
	this.enterFourDigitCodeFromMsg = function(number){
		validateCurrentEsn.enterFourDigitCodeFromMsg(number);
	};
   
	//** this method is to  check whether the final instruction page is loaded or not
	this.finalInstructionPageLoadedPurchase = function(){
		return finalInstructionPage.finalInstructionPageLoadedPurchase();
	};
  
	//** this method is to proceed from the final instruction page for purchase scenarios
	this.finalInstructionProceedPurchase = function(){
		finalInstructionPage.finalInstructionProceedPurchase();
	};
	
	//** this method is to  check whether the final instruction page is loaded or not
	this.finalInstructionPageLoadedPortPin = function(){
		return finalInstructionPage.finalInstructionPageLoadedPortPin();
	};
  
	//** this method is to proceed from the final instruction page for purchase scenarios
	this.finalInstructionProceedPortPin = function(){
		finalInstructionPage.finalInstructionProceedPortPin();
	};
	
	//** this method is to check whether the summary page is loaded or not for purchase scenarios
	this.summaryPageLoadedPurchase  = function(){
	return summaryObj.summaryPageLoadedPurchase ();
	
	};
  
	//** this method is to proceed from the summary page for purchase scenarios
	this.clickOnSummaryBtnPurchase = function(){
	summaryObj.clickOnSummaryBtnPurchase();
	};
	
	//** this method is to display the error message when the invalid pin is entered. 
	this.errorMessageDisplayed = function(){
		return airtimePinObj.errorMessageDisplayed();
	
	};
	//Multiline Activation with CC purchase starts:Remya
	//method to click on 'Add Line' option in Account dashboard menu
	this.clickOnAddLineMenu = function(){
		return myAccount.clickOnAddLineMenu();
	};
	//method to check whether 'Is your Phone Active?' popup loaded or not
	this.isPhoneActivePopUpLoaded = function(){		
		return myAccount.isPhoneActivePopUpLoaded();
	};
	//Method to click on 'No' option in 'Is your Phone Active?' popup
	this.clickOnPhoneActivePopUpOptn = function(){		
		return myAccount.clickOnPhoneActivePopUpOptn();
	};
	//Multiline Activation with CC purchase ends
  
	//code changes durga ** without survey page
	this.clickOnWfmLogo = function(){
		return summaryObj.clickOnLogo.click();
	};
	
	this.checkOnMyAccountLink = function(){
		return summaryObj.clickOnmyAccount.isPresent();
	};
	
	this.clickOnMyAccountLink = function(){
		return summaryObj.clickOnmyAccount.click();
	};
	
  };
module.exports = new Activate;