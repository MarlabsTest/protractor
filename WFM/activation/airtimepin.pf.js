//this represents a modal for airtimepin page

'use strict';
var ElementsUtil = require("../util/elements.util");

var AirtimePin = function() {
	
	this.errorMessage = element(by.css('[title="The Service Plan PIN you entered is invalid. Please review the number and try again."]'));
	this.airtimePinBox = element.all(by.id('pinmin')).get(1);//2	
	this.airtimePinContinueBtn =  element(by.id('airtimePinActivation_btn'));//.get(0);

	//** method to check whether the plan(purchase plan/already have a pin -flows) page is loaded
	this.servicePlanPageLoaded = function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /airtimeserviceplan$/.test(url);
		});
	};
	
	//** method to enter airtime pin and proceed from the page
	this.enterAirTimePin = function(pin){
		this.airtimePinBox.sendKeys(pin);
		this.airtimePinContinueBtn.click();
	};

	//** this method is to check whether the error message is displayed when invalid pin is entered.
	this.errorMessageDisplayed = function(){
		return this.errorMessage.isPresent();
	};
	
};
module.exports = new AirtimePin;
