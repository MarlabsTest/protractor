/*
 * Scenario file for WFM multiline activation with credit card option author:
 * @author :rpillai
 * Steps to be followed.
 * 1. Activate a Family mobile phone
 * 2. Once phone get activated, go to 'Account Summary' page and select 'Add Line' option in menu tab.
 * 3. Opt for phone to be added is not activated in the system.(from a pop-up)
 * 4. Activate the phone with Credit Card purchase option
 * 5. Once the activation completed, the 'Account Summary' page should load with all activated devices under that account
 */
'use strict';
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Multiline Activation With Cc Purchase', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip 		  = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				sessionData.wfm.cardType = inputActivationData.cardType;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_GSM_MULILINE_ACTIVATION_CC');
		}).result.data = inputActivationData;
	});
});