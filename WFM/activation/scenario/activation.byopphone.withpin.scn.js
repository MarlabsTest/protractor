//This scenario is for WFM BYOP activation. User has to choose device type "Bring your own phone" and 
//provide values such as BYOP SIM, security PIN, zipcode, AT PIN to activate the BYOP.

'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/byop.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('BYOP Activation', function() {

	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_BYOP_ACTIVATION');
		}).result.data = inputActivationData;
	});
});