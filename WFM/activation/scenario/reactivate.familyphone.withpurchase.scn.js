/*
 * **********Scenario is for REACTIVATING A DEVICE WITH PLAN PURCHASE ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.The deactivated device will be listed under inactive devices in myaccount dashboard.
 * 4.Activate the device by clicking on the activate button in dashboard (Reactivation)
 * 5.It will be redirected to the page where we need to enter zipcode.
 * 6.Select an airtime plan and enroll 
 * 7.Proceed to the payment.
 * 8.After successful purchase,we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 9.After doing all these,we are done with the reactivation process. 
 * 
 */
'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Reactivation Device', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				sessionData.wfm.cardType = inputActivationData.cardType;
				sessionData.wfm.cardType = inputActivationData.cardType;
				sessionData.wfm.cardType = inputActivationData.cardType;
				sessionData.wfm.autoRefill = inputActivationData.AutoRefill;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_REACTIVATION_GSM_WITH_PURCHASE');
		}).result.data = inputActivationData;
	});
});