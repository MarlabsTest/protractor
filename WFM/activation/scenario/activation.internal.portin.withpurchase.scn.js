//This spec file is for WFM Internal Port In with Purchase. User has to choose the device type "I have a 
//family phone" and provide the values such as SIM, security PIN, MIN number(to be port) and purchase 
//the plan to do the internal port in with Purchase.

'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/internal.portin.util');
var sessionData = require("../../common/sessiondata.do");

describe('Internal PortIn With Purchase', function() {

	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				sessionData.wfm.pinPartNumber = inputActivationData.PIN;
				sessionData.wfm.oldPartNumber = inputActivationData.OldPart;
				sessionData.wfm.cardType = inputActivationData.cardType;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_INTERNAL_PORTIN_WITH_PURCHASE');
		}).result.data = inputActivationData;
	});
});