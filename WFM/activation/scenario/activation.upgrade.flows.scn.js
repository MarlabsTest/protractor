/*
 * **********Scenario is the consolidation of Upgrade  flows in WFM ******** 
 * 
 * 1.Read the input data for upgrade from CSV
 * 2.Based on the partnumber and AR flag,decide which flow to proceed with.
 * 3.If the partnumber starts with 'PH' ,it will be a  BYOP upgrade.In that we are initially doing a BYOP activation process
 * 4.If the AR field value is 'Y' in CSV, we will proceed with Upgrade from ESN Active and Enrolled in AR .In this we are doing a single activation with enrollment with credit card purchase. 
 * 5.The third upgrade flow is the normal WFM phone upgrade.  We will be doing activation with PIN flow.
 * 6.In all the above cases,After activating the device, will get the MIN number.
 * 7.After getting MIN, It's executes an phone upgrade spec with new part numbers and the same MIN.Finally, the older ESN gets upgraded with the new ESN with the same MIN number.
 */
'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var phoneupgradeUtil = require('../../util/phoneupgrade.util');
var sessionData = require("../../common/sessiondata.do");

describe('Upgrade Flow', function() {
	var phoneupgradeData = phoneupgradeUtil.getTestData();
	console.log('phoneupgradeData:', phoneupgradeData);
	console.log('protractor.basePath ', protractor.basePath);
	
	drive(phoneupgradeData, function(inputPhoneUpgradeData) {
		sessionData.wfm.esnPartNumber = inputPhoneUpgradeData.FromPartNumber;
		console.log('sessionData.wfm.esnPartNumber: ',sessionData.wfm.esnPartNumber );
		sessionData.wfm.arFlag = inputPhoneUpgradeData.AR;
		console.log('sessionData.wfm.arFlag::',sessionData.wfm.arFlag);
		sessionData.wfm.phoneStatus = inputPhoneUpgradeData.PhoneStatus;
		describe('Drive Spec', function() {
			it('Copying phone upgrade test data to session', function(done) {
				sessionData.wfm.simPartNumber = inputPhoneUpgradeData.FromSIM;
				sessionData.wfm.zip = inputPhoneUpgradeData.FromZipCode;
				sessionData.wfm.pinPartNumber = inputPhoneUpgradeData.FromPIN;
				sessionData.wfm.toEsnPartNumber = inputPhoneUpgradeData.ToPartNumber;
				sessionData.wfm.toSimPartNumber = inputPhoneUpgradeData.ToSIM;
				sessionData.wfm.toPinPartNumber = inputPhoneUpgradeData.ToPIN;
				console.log('Copying upgrade', inputPhoneUpgradeData);
				done();
			
		});
			FlowUtil.run('WFM_UPGRADE_FLOWS');
		}).result.data = inputPhoneUpgradeData;
	});
});