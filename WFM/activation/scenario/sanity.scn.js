'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var sanityUtil = require('../../util/sanity.util');
var sessionData = require("../../common/sessiondata.do");

describe('Upgrade Flow', function() {
	var sanityData = sanityUtil.getTestData();
	console.log('sanityData:', sanityData);
	console.log('protractor.basePath ', protractor.basePath);
	
	drive(sanityData, function(inputSanityData) {
		sessionData.wfm.esnPartNumber = inputSanityData.FromPartNumber;
		console.log('sessionData.wfm.esnPartNumber: ',sessionData.wfm.esnPartNumber );
		sessionData.wfm.arFlag = inputSanityData.AR;
		console.log('sessionData.wfm.arFlag::',sessionData.wfm.arFlag);
		sessionData.wfm.phoneStatus = inputSanityData.PhoneStatus;
		describe('Drive Spec', function() {
			it('Copying phone upgrade test data to session', function(done) {
				sessionData.wfm.simPartNumber = inputSanityData.FromSIM;
				sessionData.wfm.zip = inputSanityData.FromZipCode;
				sessionData.wfm.pinPartNumber = inputSanityData.FromPIN;
				sessionData.wfm.toEsnPartNumber = inputSanityData.ToPartNumber;
				sessionData.wfm.toSimPartNumber = inputSanityData.ToSIM;
				sessionData.wfm.toPinPartNumber = inputSanityData.ToPIN;
				console.log('Copying upgrade', inputSanityData);
				done();
			
		});
			FlowUtil.run('WFM_SANITY');
		}).result.data = inputSanityData;
	});
});