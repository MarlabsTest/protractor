/*
 * **********Scenario is for Upgrade from ESN Active and Enrolled in AR  with service end date in future to ESN New - Belongs to Same account ******** 
 * 
 * 1.Do a normal single line activation with enrollment with CC purchase
 * 2.Logout 
 * 3.Again load the home page.	
 * 4.Proceed with normal activation 
 * 5.Enter a married SIM 
 * 6.Enter the previous mobile number 
 * 7.Enter the account password
 * 8.After this step,we will be redirected to final Instruction page,summary page and survey page
 * 9.Final page will be the MyAccount dashboard. 
 * 
 */
'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Upgrade Esn Active Enrolled In Ar To New Esn', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_UPGRADE_ESN_ACTIVE_ENROLLED_IN_AR_TO_NEW_ESN');
		}).result.data = inputActivationData;
	});
});