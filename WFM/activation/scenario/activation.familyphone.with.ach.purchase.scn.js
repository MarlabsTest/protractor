/*
 * **********Scenario is for ACTIVATING A DEVICE WITH ACH PURCHASE PLAN - NEW ACCOUNT ******** 
 * 
 * 1.Initially we will be reading partnumber from csv file & storing it in session.(This is for handling multiple testdata)
 * 2.Generate ESN,SIM & PIN from DB using the partnumbers
 * 3.DO MARRY the ESN with the SIM,and provide the SIM number in the flow.
 * 4.Provide the zipcode 
 * 5.Purchase an airtime plan
 * 6.Create a new account
 * 7.Do the payment after successful account creation.
 * 8.After purchasing plan,we will be redirected to final Instruction page,summary page and survey page
 * 9.Final page will be the MyAccount dashboard, where the device will the displayed under Active devices section. 
 * 
 */
'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Activation With Ach Purchase', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_ACTIVATION_GSM_WITH_ACH_PURCHASE');
		}).result.data = inputActivationData;
	});
});