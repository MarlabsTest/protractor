'use strict';
//Firstly provide esn and sim for the activation process
//it will ask to enter the four digit pin 
//then enter the zipcode, it will navigate to the service plan page  
//here provide the invalid pin,so that it will show you the error message.

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Activation With Invalid Pin', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.wfm.esnPartNumber = inputActivationData.PartNumber;
				sessionData.wfm.simPartNumber = inputActivationData.SIM;
				sessionData.wfm.zip = inputActivationData.ZipCode;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('WFM_ACTIVATION_GSM_WITH_INVALID_PIN');
		}).result.data = inputActivationData;
	});
});