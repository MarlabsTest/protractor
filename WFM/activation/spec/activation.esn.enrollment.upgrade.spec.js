
/*
 * **********Spec file is for Upgrade from ESN Active and Enrolled in AR with service end date in future to ESN New - Belongs to Same account ******** 
 * 
 * 1.Do a normal single line activation with enrollment with CC purchase
 * 2.Logout 
 * 3.Again load the home page.	
 * 4.Proceed with normal activation 
 * 5.Enter a married SIM 
 * 6.Enter the previous mobile number 
 * 7.Enter the account password
 * 8.After this step,we will be redirected to final Instruction page,summary page and survey page
 * 9.Final page will be the MyAccount dashboard. 
 * 
 */ 'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};

describe('Upgrade Active ESN Enrolled in AR' , function() {
	
	/*Click on the activate link in the home page
	 *Expected result - Select Device Page(Either activate a family Phone Or a BYOP device )  
	 */
	it('should navigate to the Select Device Page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	/*Proceed with the Activate Family Phone flow
	 *Expected result - Page to enter SIM number will be displayed  
	 */
	it('should navigate to the page to enter SIM number',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.toEsnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.wfm.toSimPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.wfm.esn = esnval;
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	/*Enter a 4 digit security pin 
	 *Expected result - Page to opt for porting or new number will be displayed
	 */
	it('should navigate to page for entering mobile number', function(done) {		
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	/*Enter previous mobile number 
	 *Expected result - Page to enter the account password will be loaded
	 */	
	it('should navigate to enter password for previous mobile number', function(done) {
		console.log("esn from previous session is"+sessionData.wfm.upgradeEsn );
		var minVal = DataUtil.getMinOfESN(sessionData.wfm.upgradeEsn);
		generatedMin['min'] = minVal;
		activation.enterMobNumber(minVal);
		expect(activation.validateAccountPasswordPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	/*Enter account password 
	 *Expected result - final Instruction page will be loaded
	 */	
	it('should load the final instruction page', function(done) {
		activation.validateAccountPassword("tracfone");
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();	
	}); 
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {		
		activation.finalInstructionProceedPortPin();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

		/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) { 
		activation.clickOnThankYouBtn(); 
		DataUtil.activateESN(sessionData.wfm.esn);
		console.log("update table with esn :"+sessionData.wfm.esn);
		//check activation
		//DataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_Upgrade');
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
		
});
