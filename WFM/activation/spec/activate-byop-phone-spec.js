//This spec file is for WFM BYOP activation. 
//User has to choose device type "Bring your own phone"  
//Provide BYOP SIM, security PIN, zipcode, AT PIN to activate the BYOP.

'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};
var generatedPin ={};


describe('BYOP activation', function() {
	//to click activate link in the menu and check whether the activation page is loaded 
	it('Go to BYOP activation page', function(done) {
		homePage.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});
	
	//to choose the device type as "Bring your own phone" and check whether the SIM page is loaded
	it('Choose device type BYOP',  function(done) {
		activation.selectByopDeviceType();
		activation.acceptTermsConditions();
		expect(activation.isByopSIMPage()).toBe(true);	
		done();
	});
	
	//to provide the SIM number and check whether the security popup window shown
	it('Provide SIM number',  function(done) {
		var simval = DataUtil.getByopSim(sessionData.wfm.esnPartNumber, sessionData.wfm.simPartNumber);
		activation.enterByopSim(simval);
		sessionData.wfm.sim = simval;	
		console.log(simval);
		var esnVal = CommonUtil.getLastDigits(simval, 15);//to get dummy esn from sim number
		sessionData.wfm.esn = esnVal;
		sessionData.wfm.upgradeEsn = esnVal; 
		console.log(esnVal);
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		expect(activation.securityPinPopUp()).toBe(true);
		done();
	}).result.data = generatedEsnSim;
	
	//to provide the security PIN and check whether the keep my phone page loaded
it('Provide security pin',  function(done) {
		activation.securityPinPopUp();
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	//to provide the zipcode and check whether the service plan page loaded
	it('Provide zipcode',  function(done) {
		activation.enterByopZipCode(sessionData.wfm.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();
	});
	
	//to provide the airtime pin and check whether the account creation page loaded
	it('Provide airtime pin', function(done) {
		var pinval = DataUtil.getPIN(sessionData.wfm.pinPartNumber);
		activation.enterAirTimePin(pinval);
		sessionData.wfm.pin = pinval;
		generatedPin['pin'] = pinval;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	//to switch to create account option and check whether the email text box loaded to create account
	it('Switch to create account option', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	//create the account and check whether the account creation success popup shown
	it('Provide the account details', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.wfm.username = emailval;
		sessionData.wfm.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	//click continue on account creation success popup and check whether the instruction page loaded
	it('Click continue button in account creation success popup', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	//click continue in final instruction page and check whether the summary page loaded
	it('Click continue button in final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	//code changed as its not navigating to dashboard page********
	it('should click the WFM logo', function(done) {
		activation.clickOnWfmLogo();
		expect(activation.checkOnMyAccountLink()).toBe(true); 
		done();		
	});
	
//	//click done in the summary page and check whether the survey page loaded
//	it('Click done button in the summary page', function(done) {
//		activation.clickOnSummaryBtn();
//		expect(activation.surveyPageLoaded()).toBe(true);
//		done();		
//	});
//	
//	//click "No thanks" button and check whether the dashboard page loaded
//	it('Click "No thanks" button in survey page', function(done) {
//		activation.clickOnThankYouBtn();
	it('Click "No thanks" button in survey page', function(done) {
		activation.clickOnMyAccountLink();
		DataUtil.activateESN(sessionData.wfm.esn);
		sessionData.wfm.upgradeEsn = sessionData.wfm.esn; //for upgrade
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		DataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_Activation_with_PIN');
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
});
	

