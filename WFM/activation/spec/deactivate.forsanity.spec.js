'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};
var generatedPin ={};

describe('WFM DeActivation ', function() {	
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should deactivate the device', function(done) {	
        var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
        console.log('deactivate minVal ,    ::'+min);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.wfm.esn,min,'PAST_DUE','WFM');	
		home.myAccount();
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}); 
});