/*
 * **********Spec file is for REACTIVATING A DEVICE WITH PLAN PURCHASE ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.The deactivated device will be listed under inactive devices in myaccount dashboard.
 * 4.Activate the device by clicking on the activate button in dashboard (Reactivation)
 * 5.It will be redirected to the page where we need to enter zipcode.
 * 6.Select an airtime plan and do the payment.
 * 7.After successful purchase,we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 8.After doing all these,we are done with the reactivation process. 
 * 
 */
'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var generatedMin ={};

describe('WFM ReActivation ', function() {
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should deactivate the device', function(done) {	
        var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
        console.log('minVal ,    ::'+min);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.wfm.esn,min,'PAST_DUE','WFM');	
        generatedMin['min'] = min;
        DataUtil.changeServiceEndDate(sessionData.wfm.esn);
		home.myAccount();
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}).result.data = generatedMin; 

	/*click on the activate button in the dashboard for the deactivated device
	 *Expected result - redirected to the page where we need to enter the zipcode.  
	 */
	it('should navigate to page for entering zipcode', function(done) {		
		myAccount.clickOnActivate();
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*Buy an airtime Plan 
	 *Expected result - Checkout page will be loaded
	 */	
	it('should be navigated to payment page', function(done) {
		activation.pickPlan();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});

	
	/*check enrollment and Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 */	
	it('should load the payment form', function(done) {
		var isAutoRefill	= sessionData.wfm.autoRefill;
		
		if(isAutoRefill == 'YES' || isAutoRefill == 'Y')
		{
			myAccount.selectEnrollmentCheckBox();
		}
		
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - final instruction page will be loaded
	 */	
	it('should load the final instruction page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.wfm.cardType),CommonUtil.getCvv(sessionData.wfm.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);
		console.log("update table with esn :"+sessionData.wfm.esn);
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		generatedMin['min'] = min;	
		expect(myAccount.isLoaded()).toBe(true);
		done();		

	}).result.data = generatedMin;	

	

});
