'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnMin ={};


describe('WFM DeActivation Device', function() { 

	describe('WFM DeActivation Device', function() {
		it('should first deactivate the device and then reload the page for to activate the device,upon clicking activate link', function(done) {
			console.log('sessionData.wfm.esnsToReactivate.length===========',sessionData.wfm.esnsToReactivate.length);
				for(var line = 0;line < sessionData.wfm.esnsToReactivate.length;line++)
					{
					var min = DataUtil.getMinOfESN(sessionData.wfm.esnsToReactivate[line]);
			        console.log('minVal ,    ::'+min);
					DataUtil.deactivatePhone('DEACTIVATE',sessionData.wfm.esnsToReactivate[line],min,'UPGRADE','WFM');	
					console.log('deactivated===',sessionData.wfm.esnsToReactivate[line]);
					generatedEsnMin['esn'] = sessionData.wfm.esnsToReactivate[line];
					generatedEsnMin['min'] = min;
					done();
					}
			myAccount.refresh();
	}).result.data = generatedEsnMin;  
	});


});