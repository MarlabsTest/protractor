'use strict';
//Firstly provide esn and sim for the activation process
//it will ask to enter the four digit pin 
//then enter the zipcode, it will navigate to service plan page  
//here provide the invalid pin,so that it will show you the error message.

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};

describe('WFM Activation With Invalid PIN', function() {
	//from the menu it goes to the activation page for selecting the device upon clicking activate link
	it('activation page for selecting the device', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	//here we have to click on the button of I Have A Family Phone and ESN page will be loaded
	it('should click on I have a family phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	//enter the sim number which is married to ESN upon clicking the continue button the popup window appears. 
	it('should enter the sim', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.wfm.simPartNumber);
       	console.log('returns', simval);
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.wfm.esn = esnval;
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	//enter the four digit security pin and proceed to click on the keep my phone
	it('should enter 4 digit PIN', function(done) {
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	//enter the zipcode and it will be navigated to enter the sirtime service plan page
	it('should enter the zipcode ', function(done) {
		activation.enterZipCode(sessionData.wfm.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	//Enter the invalid pin so that it shows the error message saying that invalid pin.
	it('should enter the invalid PIN', function(done) {
		activation.enterAirTimePin("123456765432");
		expect(activation.errorMessageDisplayed()).toBe(true);
		done();		
	});
	
});