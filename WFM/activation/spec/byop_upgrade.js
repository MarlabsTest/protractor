 'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//newly added for data integration
var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};



describe('BYOP upgrade', function() {
	
	
	it('should navigate to the activation page for selecting the device upon clicking activate link', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	it('should click on button of I have a family phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('should navigate to pop up for providing the security PIN number upon clicking on continue button after providing valid ESN', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.toEsnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.wfm.toSimPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.wfm.esn = esnval;
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	
	it('should navigate to page for entering phone number after entering 4 digit PIN', function(done) {
		
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	it('should enter the mobile number and should click on continue', function(done) {
		console.log("esn from previous session is"+sessionData.wfm.upgradeEsn );
		var minVal = DataUtil.getMinOfESN(sessionData.wfm.upgradeEsn);
		generatedMin['min'] = minVal;
		activation.enterMobNumber(minVal);
		expect(activation.validateAccountPasswordPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	it('should enter account password for phone upgrade', function(done) {
		activation.validateAccountPassword("tracfone");
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();	
	}); 
	
	it('should click on the continue button in the final instruction page and should be navigated to Succcess page', function(done) {
		activation.finalInstructionProceedPortPin();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('should click on the done button in the summary page and shud be navigated to survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('should click on the thank you button and will be redirected to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn(); 
		DataUtil.activateESN(sessionData.wfm.esn);
		//check activation
		//DataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_BYOP_Upgrade');
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
		
});
