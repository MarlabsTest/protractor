/*
 * **********Scenario is the consolidation of Upgrade  flows in WFM ******** 
 * 
 * 1.Read the input data for upgrade from CSV
 * 2.Based on the partnumber,AR flag and PhoneStatus decide which flow to proceed with.
 * 3.If the partnumber starts with 'PH' ,it will be a  BYOP upgrade.In that we are initially doing a BYOP activation process
 * 4.If the AR field value is 'Y' in CSV, we will proceed with Upgrade from ESN Active and Enrolled in AR .In this we are doing a single activation with enrollment with credit card purchase.
 * 5.if PhoneStstus is is INACTIVE(both MIN reserve and non reserve),proceed with upgrading the inactive device with MIN of active device.
 * 6.The third upgrade flow is the normal WFM phone upgrade.  We will be doing activation with PIN flow.
 * 7.In all the above cases,After activating the device, will get the MIN number.
 * 8.After getting MIN, It's executes an phone upgrade spec with new part numbers and the same MIN.Finally, the older ESN gets upgraded with the new ESN with the same MIN number.
 */
 
 'use strict';

var sessionData 	= require("../../common/sessiondata.do");
var FlowUtil 		= require('../../util/flow.util');
var commonUtils		= require('../../util/common.functions.util');

describe('WFM Upgrade' , function() {
	
	var esnPartNumber 	= sessionData.wfm.esnPartNumber;
	var arFlag 			= sessionData.wfm.arFlag;
	var varPhoneStatus	= sessionData.wfm.phoneStatus;
	
	if(commonUtils.isByopEsn(esnPartNumber)){
		console.log('BYOP upgrade');
		FlowUtil.run('WFM_BYOP_UPGRADE');				
	}else if(commonUtils.isARFlow(arFlag)){
		console.log('AR enrollment flow');
		FlowUtil.run('WFM_UPGRADE_ESN_ACTIVE_ENROLLED_IN_AR_TO_NEW_ESN');
	}else if(commonUtils.isInactive(varPhoneStatus)){
		/*Upgrade ESN active to ESN inactive- MIN reserve and non reserve scenario.
		 * This method check the PhoneStatus field in input file and return the result*/
		console.log("Upgrade from active esn to inactive esn");
		FlowUtil.run('WFM_UPGRADE_ACTIVE_ESN_TO_INACTV_ESN');
	}
	else{
		FlowUtil.run('WFM_PHONE_UPGRADE');		
	}
});
