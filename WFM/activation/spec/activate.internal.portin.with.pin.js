//This spec file is for WFM Internal Port In with PIN. 
//User has to choose the device type "I have a family phone"
//and provide SIM, security PIN, MIN number(to be port), AT PIN to 
//do the internal port in with PIN.
'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//newly added for data integration
var DataUtil= require("../../util/datautils.util");
var CommonUtil = require('../../util/common.functions.util');

var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};
var generatedPin ={};


describe('WFM Porting', function() {
	var esnToBePort = "";
	
	//To click activate link in the menu and check whether the activation page is loaded 
	it('Go to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});
	
	//To choose "I have a family phone" option and check whether the esn page is loaded
	it('Choose I have a family phone option',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	//To provide SIM number and check whether the security PIN popup shown
	it('Provide SIM number', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.esnPartNumber);
		var simval = DataUtil.getSIM(sessionData.wfm.simPartNumber);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		sessionData.wfm.esn = esnval;
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		activation.checkBoxCheck();
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	//To provide security PIN and check whether the keep my phone page loaded
	it('Provide security PIN', function(done) {
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
		esnToBePort = DataUtil.getActiveESNFromPartNumber(sessionData.wfm.oldPartNumber);
		var min = DataUtil.getMinOfESN(esnToBePort);
		activation.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activation.validateEsnLastNumbersPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	//To provide four digit authentication code and check whether the service plan page loaded
	it('Provide four digit authentication code', function(done) {
		activation.enterFourDigitCodeFromMsg(CommonUtil.getLastDigits(esnToBePort, 4));//last four digits of the current esn
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	//To provide the airtime pin and check whether the account creation page loaded
	it('Provide airtime pin', function(done) {
		var pinval = DataUtil.getPIN(sessionData.wfm.pinPartNumber);
		activation.enterAirTimePin(pinval);
		sessionData.wfm.pin = pinval;
		generatedPin['pin'] = pinval;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	//to switch to create account option and check whether the email text box loaded to create account
	it('Switch to create account option', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	//create the account and check whether the account creation success popup shown
	it('Provide the account details', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.wfm.username = emailval;
		sessionData.wfm.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	//click continue on account creation success popup and check whether the instruction page loaded
	it('Click continue button in account creation success popup', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	//click continue in port upgrade instruction page and check whether the summary page loaded
	it('Click continue button in final instruction page', function(done) {
		activation.portUpgradeInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	//click done in the summary page and check whether the survey page loaded
	it('Click done button in the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	//click "No thanks" button and check whether the dashboard page loaded
	it('Click "No thanks" button in survey page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		DataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_Activation_with_PIN');
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
