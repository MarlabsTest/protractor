'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};
var generatedPin ={};

describe('WFM clear carrier pending', function() {	
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should clear the carrier pending', function(done) {	
		var min= DataUtil.getMinOfESN(sessionData.wfm.esn);
		console.log("check carrier min",min);
		DataUtil.checkUpgrade(sessionData.wfm.esn,min);
        var minVal = DataUtil.getMinOfESN(sessionData.wfm.esn);
        console.log('deactivate minVal ,    ::'+minVal);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.wfm.esn,minVal,'PORT_CANCEL','WFM');	
		home.myAccount();
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}); 
});