'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};
var generatedPin ={};

 
describe('WFM ReActivation ', function() {
 
 for(var line = 0;line < sessionData.wfm.noOfEsns+1;line++)
   {
	describe('WFM ReActivation  for each esn', function() {

	 it('should click on the activate button in the account dashboard and will be redirected to SIM page', function(done) {	
		console.log('indexSpec=============',index);
		myAccount.clickOnActivate(index);
		expect(activation.keepMyPhonePageLoaded()).toBe(true);		
		done();
	});
	
	
	it('should enter the zipcode and will be navigated to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.wfm.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('should enter airtime pin and will be navigated to Activation Request Submitted page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.wfm.pinPartNumber);
       	console.log('returns', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.wfm.pin = pinval;
		generatedPin['pin'] = pinval;
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
		
	it('should click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('should click on the done button in the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('should click on the thank you button and will be redirected to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esnsToReactivate[line]);
		console.log("update table with esn :",sessionData.wfm.esnsToReactivate[line]);
		var min = DataUtil.getMinOfESN(sessionData.wfm.esnsToReactivate[line]);
		generatedMin['min'] = min;
		//check activation
		DataUtil.checkActivation(sessionData.wfm.esnsToReactivate[line],sessionData.wfm.pin,'1','WFM_Activation_with_PIN');
		console.log("DB call");	
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;	 
	
   // });
 });
 }
});