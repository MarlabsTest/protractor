//This spec file is for WFM Internal Port In with Purchase. 
//User has to choose the device type "I have a family phone"
//and provide SIM, security PIN, MIN number(to be port) and purchase 
//the plan to do the internal port in with Purchase.

'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var CommonUtil = require('../../util/common.functions.util');
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};

describe('WFM Porting with purchase PIN', function() {
	var esnToBePort = "";
	
	//To click activate link in the menu and check whether the activation page is loaded
	it('Go to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});
	
	//To choose "I have a family phone" option and check whether the esn page is loaded
	it('Choose I have a family phone option',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	//To provide SIM number and check whether the security PIN popup shown
	it('Provide SIM number', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.esnPartNumber);
		var simval = DataUtil.getSIM(sessionData.wfm.simPartNumber);
		DataUtil.addSimToEsn(esnval,simval);
		sessionData.wfm.esn = esnval;
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	//To provide security PIN and check whether the keep my phone page loaded
	it('Provide security PIN', function(done) {
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
		esnToBePort = DataUtil.getActiveESNFromPartNumber(sessionData.wfm.oldPartNumber);
		var min = DataUtil.getMinOfESN(esnToBePort);
		activation.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activation.validateEsnLastNumbersPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	//To provide four digit authentication code and check whether the service plan page loaded
	it('Provide four digit authentication code', function(done) {
		activation.enterFourDigitCodeFromMsg(CommonUtil.getLastDigits(esnToBePort, 4));//last four digits of the current esn
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	//Choose purchase plan option and check whether the service plans page loaded
	it('Select purchase plan option', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	//To pick a service plan and check whether the account creation page loaded
	it('Pick a service plan', function(done) {
		activation.pickPlan();
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	});
	
	//to switch to create account option and check whether the email text box loaded to create account
	it('Switch to create account option', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	//create the account and check whether the account creation success popup shown
	it('Provide the account details', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  //Newly added by gopi
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.wfm.username = emailval;
		sessionData.wfm.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	//To click continue on account creation success popup and check whether the instruction page loaded
	it('Click continue button in account creation success popup', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	//Click continue to payment button and check whether the payment options loaded
	it('Click continue to payment button', function(done) {
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	//Enter cc and billing address details and check whether the final instruction page loaded 
	it('Enter cc and billing address details', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.wfm.cardType),CommonUtil.getCvv(sessionData.wfm.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	//To click continue in final instruction page and check whether the summary page loaded
	it('Click continue button in final instruction page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	//To click done in the summary page and check whether the survey page loaded
	it('Click done button in the summary page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	//click "No thanks" button and check whether the dashboard page loaded
	it('Click "No thanks" button in survey page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		//db call for inserting redemption record into itq_dq_check table
		DataUtil.checkRedemption(sessionData.wfm.esn,'0','6','WFM_Purchase','true','false');
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
