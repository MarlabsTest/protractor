/*
 * **********Spec file is for ACTIVATING A Device WITH PIN - NEW ACCOUNT ******** 
 * 
 * 1.Initially we will be reading partnumber from csv file & storing it in session.(This is for handling multiple testdata)
 * 2.Generate ESN,SIM & PIN from DB using the partnumbers
 * 3.DO MARRY the ESN with the SIM,and provide the SIM number in the flow.
 * 4.Provide the zipcode and airtime PIN
 * 5.Create a new account
 * 6.After successful account creation,we will be redirected to final Instruction page,summary page and survey page
 * 7.Final page will be the MyAccount dashboard, where the device will the displayed under Active devices section. 
 * 
 */
'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedPin ={};
var generatedMin ={};

describe('WFM Activation ', function() {
	
	/*Click on the activate link in the home page
	 *Expected result - Select Device Page(Either activate a family Phone Or a BYOP device )  
	 */
	it('should navigate to the Select Device Page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	/*Proceed with the Activate Family Phone flow
	 *Expected result - Page to enter SIM number will be displayed  
	 */
	it('should navigate to the page to enter SIM number',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.wfm.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.wfm.esn = esnval;
		sessionData.wfm.esnsToReactivate.push(esnval);
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	/*Enter a 4 digit security pin 
	 *Expected result - Page to opt for porting or new number will be displayed
	 */
	it('should navigate to page for entering zipcode', function(done) {
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	/*Enter a valid zipcode 
	 *Expected result - Page to enter Airtime PIN will be displayed
	 */	
	it('should navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.wfm.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*Enter an airtime PIN 
	 *Expected result - Account creation Page will be loaded
	 */
	it('should navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.wfm.pinPartNumber);
       	activation.enterAirTimePin(pinval);
		sessionData.wfm.pin = pinval;
		generatedPin['pin'] = pinval;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  //Newly added by gopi
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		//browser.sleep(1000);
		//Newly added by gopi
		sessionData.wfm.username = emailval;
		sessionData.wfm.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Final Instruction Page will be shown
	 */
	it('should load the final instruction page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		console.log("account created");
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
//		browser.sleep(2000);
//		console.log("final instruction page");
		activation.finalInstructionProceed();
//		console.log("summary page created");
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	//code changed as its not navigating to dashboard page********
	it('should click the WFM logo', function(done) {
		activation.clickOnWfmLogo();
		expect(activation.checkOnMyAccountLink()).toBe(true); 
		done();		
	});
	
	/*  ******this should uncomment when there is change in code // durga******
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded

	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true); 
		done();		
	});

	/*Click on 'No,Thanks' button in the Survey Page 
	/ *Expected result - My account dashboard will be loaded
	 
	it('should redirect to the account dashboard page', function(done) {
	     activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);*/
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnMyAccountLink();
		console.log("result from cleartnumber==="+DataUtil.activateESN(sessionData.wfm.esn));
		console.log("update table with esn :"+sessionData.wfm.esn+":");
//		browser.sleep(30000);
		sessionData.wfm.upgradeEsn = sessionData.wfm.esn; //for upgrade
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		generatedMin['min'] = min;
		//check activation
		DataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_Activation_with_PIN');
		console.log("DB call", min);	
		myAccount.dashboard();
		expect(myAccount.isLoaded()).toBe(true);
		browser.sleep(30000);
		done();		
	}).result.data = generatedMin;

});
