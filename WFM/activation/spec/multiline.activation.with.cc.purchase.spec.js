
/* Spec file for Multiline Activation withCredit Card Payment option
 * author:rpillai
 * */

'use strict';

var sessionData 	= require("../../common/sessiondata.do");
var activationPo	= require("../../activation/activation.po");
var myAccountPo 	= require("../../myaccount/myaccount.po");
var homePo 			= require("../../common/homepage.po");
var DataUtil		= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil = require("../../util/common.functions.util");
var generatedEsnSim = {};
var generatedMin    = {};

describe('Enrolling for Autopay service : ', function() {
	//Click on 'Add Line' option in Account Dashboard
	it('Add Line to Account',function(done){
		myAccountPo.clickOnAddLineMenu();
		expect(myAccountPo.isPhoneActivePopUpLoaded()).toBe(true);
		done();
	});
	//click on 'No' option in 'Is your Phone Active?'popup and check whether 'Select Device' page is loaded or not
	it('Load Select Device Page',function(done){
		myAccountPo.clickOnPhoneActivePopUpOptn();
		expect(activationPo.isActivateLoaded()).toBe(true);
		done();
	});
	//navigate to the Select Device Page, either BYOP or Family Mobile Phone
	it('Select Device for Activation', function(done) {
		homePo.goToActivate();
		expect(activationPo.isActivateLoaded()).toBe(true);		
		done();
	});	
	//navigate to the page to enter SIM number
	it('Load Page to enter SIM Number',function(done){		
		activationPo.gotToEsnPage();
		expect(activationPo.esnPageLoaded()).toBe(true);
		done();
	});
	//Navigate to Phone Security Pin popup
	it('Enter SIM Number', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.esnPartNumber);
		var simval = DataUtil.getSIM(sessionData.wfm.simPartNumber);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.wfm.esn = esnval;
		activationPo.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activationPo.checkBoxCheck();
		activationPo.continueESNClick();
		expect(activationPo.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	//navigate to page for entering zipcode
	it('Enter Security PIN', function(done) {
		activationPo.enterPin("1234");
		activationPo.clickOnPinContinue();
		expect(activationPo.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	//navigate to the plan selection page
	it('Enter Zip code', function(done) {
		activationPo.enterZipCode(sessionData.wfm.zip);		
		expect(activationPo.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	//load the selected plans
	it('Purchase an airtime plan', function(done) {
		activationPo.airtimePurchase();
		expect(activationPo.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	//Select a particular plan and go for checkout
	it('Select a particular plan', function(done) {
		activationPo.pickPlan();
		expect(myAccountPo.checkoutPageLoaded()).toBe(true);
		done();		
	});
	//Continue to Payment option for checkout
	it('Continue with Payment for checkout', function(done) {
		myAccountPo.continueToPayment();
		expect(myAccountPo.paymentOptionLoaded()).toBe(true);
		done();		
	});
	//Enter CC information
	it('Enter CC information', function(done) {
		myAccountPo.enterCcDetails(""+generator.GenCC(sessionData.wfm.cardType),CommonUtil.getCvv(sessionData.wfm.cardType));
		myAccountPo.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activationPo.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	//Once payment done ,load the Final instruction page
	it('Load Final instruction page', function(done) {
		activationPo.finalInstructionProceedPurchase();
		expect(activationPo.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});	
	//Summary page
	it('Load survey page', function(done) {
		activationPo.clickOnSummaryBtnPurchase();
		expect(activationPo.surveyPageLoaded()).toBe(true);
		done();		
	});

	//Take survey and redirect to Account Dashboard
	it('Redirect to account dashboard page', function(done) {
		activationPo.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);
		var min = DataUtil.getMinOfESN(sessionData.wfm.esn);
		generatedMin['min'] = min;
		//db call for inserting redemption record into itq_dq_check table
		DataUtil.checkRedemption(sessionData.wfm.esn,'0','6','WFM_Purchase','true','false');
		expect(myAccountPo.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
});
