'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};

/*
 * Spec		: WFM Phone Upgrade
 * Details	: This spec Uses New part numbers and activated MIN mentioned in Session and it's execute 
 * 			  Phone Upgrade flow with the same.
 * 			  Finally, the older ESN gets upgraded with the new ESN with the same MIN number.
 * */

describe('WFM Phone Upgrade', function() {
	/*
	 * Should navigate to the activation page by clicking activate link on home page to activate new device.
	 * */
	it('Should navigate to the activation page by clicking activate link on home page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  

	/*
	 * should click 'I have a family phone' button on activation page to enter WFM family phone ESN.
	 * */
	it("Should click 'I have a family phone' button on activation page ",function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});

	/*
	 * Should Marry ESN and SIM then enter Married SIM value and then navigate to pop up for providing the security PIN
	 * */
	it('Should enter married SIM value and then navigate to pop up for providing the security PIN', function(done) {
		var esnval = DataUtil.getESN(sessionData.wfm.toEsnPartNumber);
		var simval = DataUtil.getSIM(sessionData.wfm.toSimPartNumber);
		DataUtil.addSimToEsn(esnval,simval);
		sessionData.wfm.esn = esnval;
		activation.enterMarriedSim(simval);
		sessionData.wfm.sim = simval;
		generatedEsnSim['esn'] = sessionData.wfm.esn;
		generatedEsnSim['sim'] = sessionData.wfm.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.securityPinPopUp()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	/*
	 * Should provide an security PIN and navigate to existing MIN entering page for entering active MIN number.
	 * */
	it('Should provide an security PIN and navigate to keep my MIN page.', function(done) {
		activation.enterPin("1234");
		activation.clickOnPinContinue();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});
	
	/*
	 * Should enter active WFM mobile number and redirect to account password page to enter Account password.
	 * */
	it('Should enter active WFM mobile number and redirect to account password page', function(done) {
		console.log("esn from previous session is"+sessionData.wfm.upgradeEsn );
		var minVal = DataUtil.getMinOfESN(sessionData.wfm.upgradeEsn);
		activation.enterMobNumber(minVal);
		console.log("previous min" +minVal);
		generatedMin['min'] = minVal;
		expect(activation.validateAccountPasswordPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	/*
	 * Should enter account password for login to account and upgrade the phone.
	 * */
	it('Should enter account password for login to account and upgrade the phone', function(done) {
		activation.validateAccountPassword("tracfone")
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();	
	});
	
	/*
	 * Should click on the continue button on final instruction page  and then load summary page
	 * */
	it('Should click on the continue button and should load summary page', function(done) {
		activation.portUpgradeInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	/*
	 * Should click on the done button and should load survey page.
	 * */
	it('Should click on the done button and should load survey page', function(done) {
		activation.clickOnSummaryBtn();
//		expect(activation.surveyPageLoaded()).toBe(true);
//		done();		
//	});
//	
//	/*
//	 * Should click on the thank you button and redirect to the dashboard page.Also execute procedure to 
//	 * activate ESN.
//	 * */
//	it('Should click on the thank you button and redirect to the dashboard page', function(done) {
//		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.wfm.esn);
		DataUtil.checkActivation(sessionData.wfm.esn,sessionData.wfm.pin,'1','WFM_Phone_Upgrade');
		var min= DataUtil.getMinOfESN(sessionData.wfm.esn);
		console.log("check carrier min",min);
		DataUtil.checkUpgrade(sessionData.wfm.esn,min);
//		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	
});
