
/*Spec for FamilyMobile GSM reactivation with purchase scenario with ACH payment option
 * @author rpillai
 * */

'use strict';
var activationPo 		= require("../activation.po");
var homePagePo		= require("../../common/homepage.po");
var myAccountPo		= require("../../myaccount/myaccount.po");
var dataUtil		= require("../../util/datautils.util");
var sessionData 	= require("../../common/sessiondata.do");
var generatedMin ={};

describe('WFM reactivation with Ach Purchase', function() {
	
	//should first deactivate the device and then reload the page for to activate the device,upon clicking activate link
	it('Deactivate the device', function(done) {	
        var min = dataUtil.getMinOfESN(sessionData.wfm.esn);
        dataUtil.deactivatePhone('DEACTIVATE',sessionData.wfm.esn,min,'PAST_DUE','WFM');	
		homePagePo.myAccount();
		expect(myAccountPo.isLoaded()).toBe(true);		
		done();
	});  
	//should click on the activate button in the account dashboard and will be redirected to a page to provide the SIM number	
	/*it('Activate button click', function(done) {		
		myAccountPo.clickOnActivate();
		expect(activationPo.keepMyPhonePageLoaded()).toBe(true);		
		done();
	});	*/
	//should enter the zipcode and will be navigated to airtime  serviceplan page
	it('Enter Zipcode', function(done) {
		myAccountPo.clickOnActivate();
		activationPo.enterZipCode(sessionData.wfm.zip);
		expect(activationPo.servicePlanPageLoaded()).toBe(true);
		done();		
	});	
	//should select purchase plan  and will be navigated to account creation page
	it('Purchase an airtime plan', function(done) {
		activationPo.airtimePurchase();
		expect(activationPo.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});	
	//should select a service plan and should click on proceed and should be navigated to payment page
	it('Select a particular plan', function(done) {
		activationPo.pickPlan();
		expect(myAccountPo.checkoutPageLoaded()).toBe(true);
		done();		
	});
	//should click on continue to payment button for checkout
	it('Continue with Payment for checkout', function(done) {
		myAccountPo.continueToPayment();
		expect(myAccountPo.paymentOptionLoaded()).toBe(true);
		done();		
	});
	//should enter Bank account information for payment
	it('Enter ACH information', function(done) {
		myAccountPo.clickOnBankAccTab();
		myAccountPo.enterAchDetails("4102","121042882");
		myAccountPo.enterBillingDetails("Test","Test","1295 Charleston Road","12345","mountain view","94043");
		expect(activationPo.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	//should click on the continue button in the final instruction page
	it('Load Final instruction page', function(done) {
		activationPo.finalInstructionProceedPurchase();
		expect(activationPo.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	//should click on the done button in the summary page
	it('Load survey page', function(done) {
		activationPo.clickOnSummaryBtnPurchase();
		expect(activationPo.surveyPageLoaded()).toBe(true);
		done();		
	});
	//should click on the thank you button and will be redirected to the account dashboard page
	it('Take activation survey and activate device', function(done) {
		activationPo.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.wfm.esn);
		console.log("update table with esn :"+sessionData.wfm.esn);
		var min = dataUtil.getMinOfESN(sessionData.wfm.esn);
		generatedMin['min'] = min;	
		expect(myAccountPo.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	//});
});
