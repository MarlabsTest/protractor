//this represnts the modal for zipcode(new number selection) flow

'use strict';
var ElementsUtil = require("../util/elements.util");
var zipCode = function() {
		
	this.zipCodeTextBox = element.all(by.name('zip')).get(1);
	this.zipCodeContinueBtn =  element(by.id('btn_continuezipcode'));//.get(0);

	this.zipCodeByopTextBox = element.all(by.name('zip')).get(1);

	this.zipCodeByopContinueBtn =  element.all(by.id('btn_continuezipcode')).get(0);


	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.keepMyPhonePageLoaded = function(){
		ElementsUtil.waitForElement(this.zipCodeTextBox);
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /keepcollectphone$/.test(url);
		});
	};
  
	//** method to enter zipcode
	this.enterZipCode = function(zipCode){
		this.zipCodeTextBox.clear().sendKeys(zipCode);
		this.zipCodeContinueBtn.click();		
	};

	//to provide zipcode for byop flow
	this.enterByopZipCode = function(zipCode){
		ElementsUtil.waitForElement(this.zipCodeByopTextBox);
		this.zipCodeByopTextBox.clear().sendKeys(zipCode);
		ElementsUtil.waitForElement(this.zipCodeByopContinueBtn);
		this.zipCodeByopContinueBtn.click();
	};

};
module.exports = new zipCode;
