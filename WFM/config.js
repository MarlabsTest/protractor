
var context = require('./config/context');
var framework = require('./config/framework');
var prepare = require('./config/onprepare');
//var complete = require('./config/oncomplete');
var bLaunch = require('./config/beforelaunch');
var aLaunch = require('./config/afterlaunch');
var params = require('./config/params');

exports.config = {

	//specCount: 0,

	// seleniumAddress: context.seleniumAddress,
	
	//time outs
	allScriptsTimeout: context.syncScriptsTimeout,
	getPageTimeout: context.pageTimeout,
	untrackOutstandingTimeouts: context.untrackOutstandingTimeouts,
	
	capabilities: require('./config/capability').capabilities,
	framework: framework.test,
	jasmineNodeOpts: framework.options,

	directConnect: true,
	
	suites: require('./config/suites').suites,
	
	//specs: require('./config/specs').specs,
	//disableChecks: true,
	
	params: params.params,
	
	beforeLaunch: function() {
		bLaunch.prepareTestData();
		console.log('global.reportDir:', reportDir);
	},
	
	onPrepare: function() {
		return browser.getProcessedConfig().then(function(config) {
			protractor.basePath = __dirname;
			prepare.prepare();
			prepare.initXMLReporter(config.capabilities.browserName, './reports/current');
			prepare.initHtmlReporter(config.capabilities.browserName, './reports/current');
			prepare.disableAnimations();			
		});
	},
	
	onComplete: function() {
	},
	
	afterLaunch: function(exitCode) {	
		var baseReportDir = __dirname + '/reports/';
		aLaunch.generateReport((baseReportDir + 'current'), (baseReportDir + reportDir));
	},
	
	plugins: require('./config/plugins').plugins	
};