'use strict';

var loginUtil = require('./login.util');
var activationUtil = require('./activation.util');
var byopActivationUtil = require('./byop.activation.util');
var internalPortInUtil = require('./internal.portin.util');
var phoneUpgradeUtil = require('./phoneupgrade.util');
var sanityUtil = require('./sanity.util');
var TestDataGenerator = {
	
	generateTestData: function() {
		//loginUtil.prepareLoginData();
		activationUtil.prepareActivationData();
		byopActivationUtil.prepareActivationData();
		internalPortInUtil.prepareActivationData();
		phoneUpgradeUtil.preparePhoneUpgradeData();
		sanityUtil.prepareSanityData();
	}
};

module.exports = TestDataGenerator;