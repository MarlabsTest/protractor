var jasmineReporter = require('jasmine-reporters');
var HTMLReporter = require('protractor-html-reporter');
var csvJsonUtil = require('./csvjson.util.js');
var Constants = require('./constants.util.js');
var CompareCsvJsonUtil = require('./comparecsvjson.util.js');

var fs = require('fs');
var path = require('path');
var util = require('util');

module.exports = {

prepareActivationData: function() {		
	if (!CompareCsvJsonUtil.isJsonFileExist(Constants.BYOP_ACTVE_PIN_JSON_FILE)
			|| CompareCsvJsonUtil.isCsvModifyed(
					Constants.BYOP_ACTVE_PIN_CSV_FILE,
					Constants.BYOP_ACTVE_PIN_JSON_FILE)) {
		console.log("BYOP Activation : Updating New data from CSV to JSON file..");
		csvJsonUtil.convertCsvToJson(Constants.BYOP_ACTVE_PIN_CSV_FILE,
				Constants.BYOP_ACTVE_PIN_JSON_FILE);
	}
		//csvJsonUtil.convertCsvToJson(Constants.BYOP_ACTVE_PIN_CSV_FILE, Constants.BYOP_ACTVE_PIN_JSON_FILE);
	},
	
	getTestData: function() {
		var activationDataIndex = "1";
		//var activationDataIndex = browser.params.loginData;
		var jsonObj = csvJsonUtil.getJsonData(Constants.BYOP_ACTVE_PIN_JSON_FILE); //require can also be used
		console.log('jsonObj', jsonObj);
		console.log('Selected data:', activationDataIndex);
		
		if(Constants.ALL === activationDataIndex) {
			return jsonObj;
		}
		return this.getSelectedData(jsonObj, activationDataIndex.toString().split(','));
	},
	
	getSelectedData: function(json, datas) {
		var selectedData = [];
		
		datas.forEach(function(data, index) {
			selectedData.push(json[data - 1]);
		});
		
		return selectedData;
	},
	
	initXmlReporter: function(browserName, reportDir) {
		var xmlReporter = new jasmineReporter.JUnitXmlReporter({
			consolidate: true,
			consolidateAll: false,
			savePath: reportDir,
			filePrefix: '',
			modifySuiteName: function(generatedSuiteName, suite) {
				console.log('generatedSuiteName:', generatedSuiteName);
				return browserName + '-' + generatedSuiteName;
			},
			modifyReportFileName: function(generatedFileName, suite) {
				console.log('generatedFileName:', generatedFileName);
				return browserName + '-' + generatedFileName;
			}
		});
		
		jasmine.getEnv().addReporter(xmlReporter);
	},
	
	createHtmlReport: function(browserName, reportDir) {		
		var files = fs.readdirSync(reportDir)
		.map(function(fileName) { 
			return { name: fileName,
				time: fs.statSync(reportDir + '/' + fileName).mtime.getTime()
			}; 
		})
		.sort(function(file1, file2) { return file1.time - file2.time; })
		.map(function(file) { return file.name; });
			   
		files.forEach(function(file) {
				if(path.extname(file) === '.xml') {
					console.log('File: ' + file);
					new HTMLReporter().from(reportDir + '/' + file, {
						reportTitle: 'Report',
						outputPath: reportDir,
						screenshotPath: './screenshots',
						testBrowser: browserName,
						modifiedSuiteName: true,
						screenshotsOnlyOnFailure: true
					});
				}
			});
	
		/*fs.readdir(reportDir, function(err, files) {
			if (err) return;
			files.forEach(function(file) {
				if(path.extname(file) === '.xml') {
					console.log('File: ' + file);
					new HTMLReporter().from(reportDir + '/' + file, {
						reportTitle: 'Report',
						outputPath: reportDir,
						screenshotPath: './screenshots',
						testBrowser: browserName,
						modifiedSuiteName: true,
						screenshotsOnlyOnFailure: true
					});
				}
			});
		});*/
	}	
};