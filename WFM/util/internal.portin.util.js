var jasmineReporter = require('jasmine-reporters');
var HTMLReporter = require('protractor-html-reporter');
var csvJsonUtil = require('./csvjson.util.js');
var Constants = require('./constants.util.js');
var CompareCsvJsonUtil = require('./comparecsvjson.util.js');

var fs = require('fs');
var path = require('path');
var util = require('util');

module.exports = {

prepareActivationData: function() {	
	
	if (!CompareCsvJsonUtil.isJsonFileExist(Constants.INTERNAL_PORT_IN_ACTVE_JSON_FILE)
			|| CompareCsvJsonUtil.isCsvModifyed(
					Constants.INTERNAL_PORT_IN_ACTVE_CSV_FILE,
					Constants.INTERNAL_PORT_IN_ACTVE_JSON_FILE)) {
		//console.log("Internal Porting : Updating New data from CSV to JSON file..");
		console.log("Internal Porting : Updating New data from CSV to JSON file activation util..",Constants.INTERNAL_PORT_IN_ACTVE_JSON_FILE+":"+Constants.INTERNAL_PORT_IN_ACTVE_CSV_FILE);

		csvJsonUtil.convertCsvToJson(Constants.INTERNAL_PORT_IN_ACTVE_CSV_FILE,
				Constants.INTERNAL_PORT_IN_ACTVE_JSON_FILE);
	}
		//csvJsonUtil.convertCsvToJson(Constants.INTERNAL_PORT_IN_ACTVE_CSV_FILE, Constants.INTERNAL_PORT_IN_ACTVE_JSON_FILE);
	},
	
	getTestData: function() {
//		var activationDataIndex = "1";
		var activationDataIndex = browser.params.activationData;
		var jsonObj = csvJsonUtil.getJsonData(Constants.INTERNAL_PORT_IN_ACTVE_JSON_FILE); //require can also be used
		console.log('jsonObj', jsonObj);
		console.log('Selected data:', activationDataIndex);
		
		if(Constants.ALL === activationDataIndex) {
			return jsonObj;
		}
		return this.getSelectedData(jsonObj, activationDataIndex.toString().split(','));
	},
	
	getSelectedData: function(json, datas) {
		var selectedData = [];
		
		datas.forEach(function(data, index) {
			selectedData.push(json[data - 1]);
		});
		
		return selectedData;
	}
		
};