'use strict';

var Constants = {
		ENV : {sit1:'https://sit1.myfamilymobile.com/',sitz:'https://sitz.myfamilymobile.com/',sitc:'https://sitci.myfamilymobile.com/',test:'https://test.myfamilymobile.com'},
	LOGIN_CSV_FILE: './testdata/login.td.csv',
	LOGIN_JSON_FILE: './testdata/login.td.json',
	ACTVE_PIN_CSV_FILE: './testdata/activatepin.td.csv',
	ACTVE_PIN_JSON_FILE: './testdata/activatepin.td.json',
	PHONE_UPGRADE_CSV_FILE: './testdata/phoneupgrade.td.csv',
	PHONE_UPGRADE_JSON_FILE: './testdata/phoneupgrade.td.json',
	BYOP_ACTVE_PIN_CSV_FILE: './testdata/activatebyoppin.td.csv',
	BYOP_ACTVE_PIN_JSON_FILE: './testdata/activatebyoppin.td.json',
	INTERNAL_PORT_IN_ACTVE_CSV_FILE: './testdata/internalportin.td.csv',
	INTERNAL_PORT_IN_ACTVE_JSON_FILE: './testdata/internalportin.td.json',
	SANITY_CSV_FILE : './testdata/sanity.td.csv',
	SANITY_JSON_FILE : './testdata/sanity.td.json',
	UTIL_JAR_PATH: './javautils/DataGen.jar',
	ALL: 'All'
};

module.exports = Constants;