'use strict';

var Constants = require('./constants.util.js');
var jarpath=Constants.UTIL_JAR_PATH;

var ProcExec = function() {
			
		this.getESN = function(partnumber) {
		var exec = require('child_process').execSync;
		return exec('java -jar '+jarpath+' ESN '+partnumber).toString();
	};

		this.getSIM = function(partnumber) {
		var exec = require('child_process').execSync;
		var simv = exec('java -jar '+jarpath+' SIM '+partnumber).toString();
		//return exec('java -jar '+jarpath+' SIM '+partnumber).toString();
		console.log('inside procex',simv);
		return simv;		
	
	};

		this.getPIN = function(partnumber) {
		var exec = require('child_process').execSync;
		return exec('java -jar '+jarpath+' PIN '+partnumber).toString();
		
	
	};

	this.executeProc = function(operation, inputdata) {
		var exec = require('child_process').execSync;
		return exec('java -jar '+jarpath+" "+operation+" "+inputdata).toString();
		
	
	};


	
};
module.exports = new ProcExec;