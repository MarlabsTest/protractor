'use strict';
var ProcEx = require("./proc.exec.util.js");
const basePath = require('path');

var DataUtil = function() {
		this.getESN = function(partnumber) {
		console.log("du.getesn");
		//return ProcEx.getESN(partnumber);
		return ProcEx.executeProc("-o ESN -epn "+partnumber+" -env "+browser.params.environment);
		};
		
		this.getSIM = function(partnumber) {
		console.log("du.getsim");
		//return ProcEx.getSIM(partnumber);
		return ProcEx.executeProc("-o SIM -spn "+partnumber+" -env "+browser.params.environment);
		};
		
		this.getPIN = function(partnumber) {
		console.log("du.getpin");
		//return ProcEx.getPIN(partnumber);
		return ProcEx.executeProc("-o PIN -ppn "+partnumber+" -env "+browser.params.environment)
		};
		
		this.activateESN = function(esn) {
		console.log("du.getpin");
		//return ProcEx.executeProc("ACTIVATE", esn);
		return ProcEx.executeProc("-o ACTIVATE -esn "+esn+" -env "+browser.params.environment);		
		};
		
		this.getMinOfESN = function(esn) {
		console.log("du.getpin");
		//return ProcEx.executeProc("MIN", esn);
		return ProcEx.executeProc("-o MIN -esn "+esn+" -env "+browser.params.environment);
		};
		
		this.addSimToEsn = function(esn, newsim) {
		console.log(esn,"du.addSIMtoESN esn" );
		console.log(newsim,"du.addSIMtoESN sim");
		var esnsim = esn +" "+newsim;
		console.log(esnsim,"du.addSIMtoESN esnsim");
		//return ProcEx.executeProc("JOINSIM", esnsim);
		return ProcEx.executeProc("-o JOINSIM -esn "+esn+" -sim "+newsim+" -env "+browser.params.environment);
		};
		
		
		
		/* gowtham change for byop start */
		this.getByopSim = function(esnPartNumber, simPartNumber) {
		console.log("du.getbyopsim");
		//var inputdata = esnPartNumber+" "+simPartNumber;
		//return ProcEx.executeProc("BYOPSIM", inputdata);
		return ProcEx.executeProc("-o BYOPSIM -epn "+esnPartNumber+" -spn "+simPartNumber+" -env "+browser.params.environment);
		};
		/* gowtham change for byop end */
		
		this.deactivatePhone = function(actionType, esnNumber, min, deactivateReason , brand) {
			console.log("du.deActivation");
			//var inputdata = esnNumber+" "+min+" "+deactivateReason+" "+brand;
			console.log("deactivate command "+"-o "+actionType+" -esn "+esnNumber+" -min "+min+" -dr "+deactivateReason+" -brand "+brand+" -env "+browser.params.environment);
			//return ProcEx.executeProc(actionType, inputdata);
			return ProcEx.executeProc("-o "+actionType+" -esn "+esnNumber+" -min "+min+" -dr "+deactivateReason+" -brand "+brand+" -env "+browser.params.environment);
		};
		
		this.getActiveESNFromPartNumber= function(partNumber) {
			console.log("du.getactiveESN");
			//return ProcEx.executeProc("GET_ACTIVE_ESN", partNumber);
			return ProcEx.executeProc("-o GET_ACTIVE_ESN -epn "+partNumber+" -env "+browser.params.environment);
		};
		
		this.getEmail= function(esn) {
		console.log("du.getpin");
		//return ProcEx.executeProc("EMAIL", "");
		return ProcEx.executeProc("-o EMAIL -esn "+esn+" -env "+browser.params.environment);
		};
		
		this.checkActivation = function(esnPartNumber, pin, actionType, transType) {
			console.log("du.checkActivation");
			var inputdata = esnPartNumber+":pin: "+pin+" :action type:"+actionType+" :tt:"+transType+" :env:"+browser.params.environment+" ";
			console.log("DB",inputdata);
			//return ProcEx.executeProc("CHECK_ACTIVATION", inputdata);
			return ProcEx.executeProc("-o CHECK_ACTIVATION -esn "+esnPartNumber+" -pin "+pin+" -ac "+actionType
					+"-tt "+transType+" -env "+browser.params.environment);
		
		};
		
		this.checkRedemption = function(esnPartNumber, pin, actionType, transType, buyFlag, redeemType) {
			console.log("du.checkRedemption");
			var inputdata = esnPartNumber+" :pin:"+pin+" :ac:"+actionType+" :tt:"+transType+" :bf:"+buyFlag+" :redeemtype:"+redeemType+" :CHK_redemption:";
			console.log(inputdata);
			//return ProcEx.executeProc("CHECK_REDEMPTION", inputdata);
			return ProcEx.executeProc("-o CHECK_REDEMPTION -esn "+esnPartNumber+" -pin "+pin+" -ac "+actionType+
					               " -tt "+transType+" -bf "+buyFlag+" -rt "+redeemType+" -env "+browser.params.environment);
		};
		
		this.checkUpgrade = function(esnPartNumber, min) {
			console.log("du.checkCarrierPending");
			var inputdata = esnPartNumber+" -min "+min;
			console.log("-o CLEAR_CARRIER_PENDING -esn "+esnPartNumber+" -min "+min+" -env "+browser.params.environment);
			//return ProcEx.executeProc("CHECK_REDEMPTION", inputdata);
			return ProcEx.executeProc("-o CLEAR_CARRIER_PENDING -esn "+esnPartNumber+" -min "+min+" -env "+browser.params.environment);
		};
		
		this.checkCarrierPendingAfterReactivation = function(esnPartNumber) {
			console.log("du.check carrier pending after reactivation");
			var inputdata = esnPartNumber;
			console.log("-o CLEAR_CARRIER_ON_REACTIVATION -esn "+esnPartNumber+" -env "+browser.params.environment);
			//return ProcEx.executeProc("CHECK_REDEMPTION", inputdata);
			return ProcEx.executeProc("-o CLEAR_CARRIER_ON_REACTIVATION -esn "+esnPartNumber+" -env "+browser.params.environment);
		};
		
		this.changeServiceEndDate = function(esn) {
			console.log(esn,"du.SET_DUE_DATE_IN_PAST esn" );		
			return ProcEx.executeProc("-o SET_DUE_DATE_IN_PAST -esn "+esn+" -env "+browser.params.environment);
		};
		
	};

	module.exports = new DataUtil;