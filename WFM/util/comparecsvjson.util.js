'use strict';

var fs = require('fs');
var path = require('path');
var util = require('util')
var fileExists = require('file-exists');

var CompareCsvJsonUtil = {

	isCsvModifyed : function(csvFilePath, jsonFilePath) {
		var csvFileDetails = fs.statSync(csvFilePath);
		var jsonFileDetails = fs.statSync(jsonFilePath);

		var lastModifyedDateOfCsv = new Date(util.inspect(csvFileDetails.mtime));
		var lastModifyedDateOfJson = new Date(util
				.inspect(jsonFileDetails.mtime));
		if (lastModifyedDateOfCsv > lastModifyedDateOfJson) {
			console.log("Data in JSON file is not up to date..");
			return true;
		} else {
			console.log("Data in JSON file is up to date..");
			return false;
		}
	},
	isJsonFileExist : function(jsonFilePath) {

		if (fileExists.sync(jsonFilePath)) {
			return true;
		} else {
			return false;
		}

	}

};

module.exports = CompareCsvJsonUtil;