'use strict';

var CsvJsonUtil = {

	convertCsvToJson : function(source, target) {
		var Converter = require("csvtojson").Converter;
		var csvConverter = new Converter({
			constructResult : false,
			toArrayString : true
		});
		var readStream = require("fs").createReadStream(source);
		var writeStream = require("fs").createWriteStream(target);
		readStream.pipe(csvConverter).pipe(writeStream);
	},

	getJsonData : function(jsonFile) {
		return JSON.parse(require('fs').readFileSync(jsonFile, 'utf8'));
	}
};

module.exports = CsvJsonUtil;