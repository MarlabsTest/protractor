'use strict';

var runUtil = require('./run.util');
const path = require('path');

var FLOWS = {
		WFM_SHOP_ADDONPLAN_OUTSIDE_ACC: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','shop/spec/shop.BuyAddonPlanOutsideAccount.spec.js'],
		WFM_SHOP_ADDONPLAN_OUTSIDE_ACC_NEWUSER: ['common/spec/myaccount.homePageLoad.spec.js','shop/spec/shop.BuyAddonPlanOutsideAccount.newUser.spec.js','activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_SHOP_SERVICEPLAN_OUTSIDE_ACC_NEWUSER: ['common/spec/myaccount.homePageLoad.spec.js','shop/spec/shop.BuyServicePlanOutsideAccount.newUser.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_ACTIVATION_GSM_WITH_PIN: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-with-survey-spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_MYACCOUNT_ADD_NEW_ESN:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.addnewesn.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_LOGIN_WITH_EMAIL: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_LOGIN_WITH_MIN: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.loginwithmin.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_FORGOT_PASSWORD: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.forgotpassword.spec.js'],
		WFM_EDIT_CONTACT_INFO:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.editcontactinfo.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_MANAGE_PAYMENT:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.managepayment.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_BUY_ADDON_WITHIN_ACCOUNT:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.buyAddOnWithinAccount.spec.js','common/spec/myaccount.logout.spec.js'],
		REFILL_PLAN_WITH_PURCHASE: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','refill/spec/refill.refillNewService.spec.js'],
		WFM_ACTIVATION_WITH_PORT_PIN: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate_with_port.js','common/spec/myaccount.logout.spec.js'],
		WFM_ACTIVATION_WITH_PORT_PURCHASE: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate_with_port_purchase.js','common/spec/myaccount.logout.spec.js'],
		WFM_ACTIVATION_GSM_WITH_PURCHASE: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-withPurchase-spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_ACTIVATION_ADD_SERVICE_PLAN: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js', 'activation/spec/add.service.plan.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_MYACCOUNT_MULTILINE_SERVICE_RENEWAL:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.service.renewal.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_ACTIVATE_DEVICE_INSIDE_ACCOUNT:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.activate.phone.with.pin.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_BYOP_ACTIVATION:['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-byop-phone-spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_INTERNAL_PORTIN_WITH_PIN:['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate.internal.portin.with.pin.js','common/spec/myaccount.logout.spec.js'],
		WFM_ENROLLMENT_OF_AUTOREFILL: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','autorefill/spec/autorefil.specs.js','common/spec/myaccount.logout.spec.js'],
	    WFM_DEENROLLMENT_OF_AUTOREFILL: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','autorefill/spec/autorefil.specs.js','autorefill/spec/deenrollautorefil.specs.js','common/spec/myaccount.logout.spec.js'],
	    WFM_INTERNAL_PORTIN_WITH_PURCHASE:['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate.internal.portin.with.purchase.js','common/spec/myaccount.logout.spec.js'],
	    WFM_ACTIVATION_GSM_WITH_INVALID_PIN: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activation-with-invalid-pin-spec.js'],
		WFM_REACTIVATION_GSM_WITH_PIN: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','activation/spec/reactivate-family-phone-spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_REACTIVATION_GSM_WITH_PURCHASE: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','activation/spec/reactivate-family-phone-with-purchase-spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_REFILL_WITH_PIN_OUTSIDE_ACC: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','refill/spec/refill.payservice.Outside.Account.spec.js'],
	    WFM_SHOP_SERVICEPLAN_OUTSIDE_ACC: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','shop/spec/shop.BuyServicePlanOutsideAccount.spec.js'],
	    WFM_DELETE_MANAGE_PAYMENT: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.managepayment.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.spec.js','myaccount/spec/myaccount.delete.managepayment.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_SINGLE_LINE_ACTIVATION_WITH_ENROLLMENT: ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/single.line.activation.enrollment.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_REACTIVATION_MULTILINE_CC:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.addnewesn.spec.js','activation/spec/multiline-deactivation-spec.js','myaccount/spec/myaccount.service.renewal.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_LOGIN_WITH_SECONDARY_MIN: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.addnewesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.withsecondarymin.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_ACTIVATION_GSM_WITH_ACH_PURCHASE : ['common/spec/myaccount.homePageLoad.spec.js', 'activation/spec/activation.familyphone.with.ach.purchase.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_UPGRADE_FLOWS: ['activation/spec/activation.upgrade.flows.spec.js'],
		WFM_BYOP_UPGRADE:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-byop-phone-spec.js','common/spec/myaccount.logout.spec.js','activation/spec/byop_upgrade.js','common/spec/myaccount.logout.spec.js'],
		WFM_UPGRADE_ESN_ACTIVE_ENROLLED_IN_AR_TO_NEW_ESN:['common/spec/myaccount.homePageLoad.spec.js','activation/spec/single.line.activation.enrollment.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.esn.enrollment.upgrade.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_PHONE_UPGRADE: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.phoneupgrade.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_SANITY: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.phoneupgrade.spec.js','myaccount/spec/myaccount.login.spec.js','activation/spec/deactivate.forsanity.spec.js','myaccount/spec/myaccount.service.renewal.spec.js','activation/spec/clear.carrier.pending.after.reactivation.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activate_with_port.js','common/spec/myaccount.logout.spec.js'],
		WFM_GSM_MULILINE_ACTIVATION_CC :['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','activation/spec/multiline.activation.with.cc.purchase.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_UPGRADE_ACTIVE_ESN_TO_INACTV_ESN: ['common/spec/myaccount.homePageLoad.spec.js','activation/spec/activate-family-phone-spec.js','myaccount/spec/myaccount.addnewesn.spec.js','myaccount/spec/myaccount.upgrade.esnactive.to.esninactive.nonreservemin.spec.js','common/spec/myaccount.logout.spec.js'],
		WFM_SHOP_PHONE: ['common/spec/myaccount.homePageLoad.spec.js', 'shop/spec/shop.buyPhone.spec.js'],
};	
var FlowUtil = {
	
	run: function(flow) {		
		FLOWS[flow].forEach(function (spec) {
			console.log("running spec..",path.join(protractor.basePath, spec));
			runUtil.requireSuite(path.join(protractor.basePath, spec));
		});
	}
};

module.exports = FlowUtil;
