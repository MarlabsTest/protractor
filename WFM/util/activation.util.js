var jasmineReporter = require('jasmine-reporters');
var HTMLReporter = require('protractor-html-reporter');
var csvJsonUtil = require('./csvjson.util.js');
var Constants = require('./constants.util.js');
var CompareCsvJsonUtil = require('./comparecsvjson.util.js');

var fs = require('fs');
var path = require('path');
var util = require('util');

module.exports = {

prepareActivationData: function() {	
		/*console.log("<<<<<< IAM HERE >>>>>>");
		csvJsonUtil.convertCsvToJson(Constants.ACTVE_PIN_CSV_FILE, Constants.ACTVE_PIN_JSON_FILE);*/
	if (!CompareCsvJsonUtil.isJsonFileExist(Constants.ACTVE_PIN_JSON_FILE)
			|| CompareCsvJsonUtil.isCsvModifyed(
					Constants.ACTVE_PIN_CSV_FILE,
					Constants.ACTVE_PIN_JSON_FILE)) {
		console.log("Activation : Updating New data from CSV to JSON file activation util..",Constants.ACTVE_PIN_JSON_FILE+":"+Constants.ACTVE_PIN_CSV_FILE);
		csvJsonUtil.convertCsvToJson(Constants.ACTVE_PIN_CSV_FILE,
				Constants.ACTVE_PIN_JSON_FILE);
	}
	},
	
	getTestData: function() {
		console.log("<<<<<< IAM HERE >>>>>>2");
		console.log("LOG");
		//var activationDataIndex = "1";
		var activationDataIndex = browser.params.activationData;
		var jsonObj = csvJsonUtil.getJsonData(Constants.ACTVE_PIN_JSON_FILE); //require can also be used
		console.log('jsonObj', jsonObj);
		console.log('Selected data index:', activationDataIndex);
		
		if(Constants.ALL === activationDataIndex) {
			console.log("Inside Constant.ALL>>>>>>>>>>>>>>>>>>>>");
			return jsonObj;
		}
		return this.getSelectedData(jsonObj, activationDataIndex.toString().split(','));
	},
	
	getSelectedData: function(json, datas) {
		var selectedData = [];
		
		datas.forEach(function(data, index) {
			selectedData.push(json[data - 1]);
		});
		
		return selectedData;
	},
	
	initXmlReporter: function(browserName, reportDir) {
		var xmlReporter = new jasmineReporter.JUnitXmlReporter({
			consolidate: true,
			consolidateAll: false,
			savePath: reportDir,
			filePrefix: '',
			modifySuiteName: function(generatedSuiteName, suite) {
				console.log('generatedSuiteName:', generatedSuiteName);
				return browserName + '-' + generatedSuiteName;
			},
			modifyReportFileName: function(generatedFileName, suite) {
				console.log('generatedFileName:', generatedFileName);
				return browserName + '-' + generatedFileName;
			}
		});
		
		jasmine.getEnv().addReporter(xmlReporter);
	},
	
	createHtmlReport: function(browserName, reportDir) {		
		var files = fs.readdirSync(reportDir)
		.map(function(fileName) { 
			return { name: fileName,
				time: fs.statSync(reportDir + '/' + fileName).mtime.getTime()
			}; 
		})
		.sort(function(file1, file2) { return file1.time - file2.time; })
		.map(function(file) { return file.name; });
			   
		files.forEach(function(file) {
				if(path.extname(file) === '.xml') {
					console.log('File: ' + file);
					new HTMLReporter().from(reportDir + '/' + file, {
						reportTitle: 'Report',
						outputPath: reportDir,
						screenshotPath: './screenshots',
						testBrowser: browserName,
						modifiedSuiteName: true,
						screenshotsOnlyOnFailure: true
					});
				}
			});
	
		/*fs.readdir(reportDir, function(err, files) {
			if (err) return;
			files.forEach(function(file) {
				if(path.extname(file) === '.xml') {
					console.log('File: ' + file);
					new HTMLReporter().from(reportDir + '/' + file, {
						reportTitle: 'Report',
						outputPath: reportDir,
						screenshotPath: './screenshots',
						testBrowser: browserName,
						modifiedSuiteName: true,
						screenshotsOnlyOnFailure: true
					});
				}
			});
		});*/
	},
	
	requireSuite: function(suite) {
		delete require.cache[require.resolve(suite)]
	    return require(suite);
	}	
};