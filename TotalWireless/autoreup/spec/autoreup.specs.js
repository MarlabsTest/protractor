'use strict';

var autoReUp = require("../autoreup.po");
var sessionData = require("../../common/sessiondata.do");
var activation = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var dataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil = require("../../util/common.functions.util");
var home = require("../../common/homepage.po");

describe('Enrolling for Auto ReUp service : ', function() {

	/*
	*This scenario will click on Auto Pay link in home page and checks whether
	*page is redirected to expected url
	*/

	it('should click on auto reup',function(done){		
		autoReUp.clickEnrollRefillBttn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	

	it('should show the billing zipcode changes popup', function(done) {
		myAccount.enterCcDetailsForAutoRefill(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();	
	}); 
	
	it('should load back to the same check out page', function(done) {
			myAccount.agreeBillingZipcodeChanges();
			expect(myAccount.checkoutPageLoaded()).toBe(true);
			done();		
	});
	
	it('should load order confirmation page', function(done) {
		myAccount.placeMyOrder();
		expect(myAccount.confirmationPageLoaded()).toBe(true);
		done();		
	}); 
	
	it('should load survey  page', function(done) {
		myAccount.clickDoneConfirmationBtn();
		expect(myAccount.isSurveyPageLoaded()).toBe(true);
		done();		
	});
	
	it('should load dashboard page', function(done) {
		myAccount.clickOnSurvey();
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	}); 

});
