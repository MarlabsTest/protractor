'use strict';

var shopPf = require("./shop.pf");
var shopServicePlanPf = require("./shopserviceplan.pf");
var paymentCheckoutPf = require("../common/paymentcheckout.pf");
var selectphonespf = require("./selectphones.pf");
//var selectdevicepf=require("./selectdevice.pf");

var Shop = function() {
	

	this.goToShopServicePlan = function() {
		return shopPf.goToShopServicePlan();
	};
	//this method selects the device to be used
	/*this.goToSelectdevice = function()
	{
		
		return selectdevicepf.goToSelectdevice();
							  
	};*/
	//this method is to display all the devices been used
	/*this.goToSeeAlldevice = function()
	{
		return selectdevicepf.goToSeeAlldevice();
	};
*/
	this.enterEsnDetails = function(esnval){
		return shopPf.enterEsnDetails(esnval);
	};
	
	this.selectPlanWithId = function(planId) {
		return shopPf.selectPlanWithId(planId);
	};
	
	this.isServicePlanPageLoaded = function() {
		return shopPf.isServicePlanPageLoaded();
	};
	
	this.clickNoThanksAutorefillBtn = function() {
		return shopPf.clickNoThanksAutorefillBtn();
	};
	
	this.selectILDPlan = function() {
		return shopPf.selectILDPlan();
	};
	
	this.clickOnILDPlanTab = function() {
		return shopPf.clickOnILDPlanTab();
	};

	
	this.isShopPageLoaded = function() {
		return shopPf.isShopPageLoaded();
	};
	//this method will navigate to enter the phone number
	this.isShopServicePlanPageLoaded = function() {
		return shopServicePlanPf.isShopServicePlanPageLoaded();
	};
	this.shopServicePlanAsNewCustomer = function(planName,planType){
		console.log('monthlyPlan=' ,planName);
		console.log('planType=' ,planType);
		shopServicePlanPf.ServicePlanForNewCustomer(planName,planType);
	};	
    //Newly added by
    this.choosePlanByName = function(planType,planName,isAutoRefill,phoneType) {
          shopServicePlanPf.choosePlanByName(planType,planName,isAutoRefill,phoneType);
    };

    this.providePhoneNumber = function(phoneNumber){
    	shopServicePlanPf.providePhoneNumber(phoneNumber);
    }
     this.isActivatePageLoaded = function(){
		return shopServicePlanPf.isActivatePageLoaded();
	};
	this.goToLowPriceShopPhones = function() {
		return selectphonespf.goToLowPriceShopPhones();
	};
	
	this.doAutoReUpPopupSelection = function(isAutoRefillNeeded) {
		shopServicePlanPf.doAutoReUpPopupSelection(isAutoRefillNeeded);
	};
	
	this.isAddNowWarningPageLoaded = function() {
		return shopServicePlanPf.isAddNowWarningPageLoaded();
	};
	
	this.doAddNow = function() {
		shopServicePlanPf.doAddNow();
	};
	
	//this method checks whether the service plan page is loaded 
	this.isShopServicePlansListed = function() {
		return shopServicePlanPf.isShopServicePlansListed();
	};
	
	//this method is to select auto refill option
	this.registerAutoRefill = function() {
		shopServicePlanPf.registerAutoRefill();
	};
	
	//this method clicks the shop link
	this.goToShopPhones = function() {
		return selectphonespf.goToShopPhones();
	};
	
	//this method clicks the simcard link
	this.goToShopSimCard = function() {
		return selectphonespf.goToShopSimCard();
	};
	
	//this method checks whether the page is navigated to enter the byop
	this.isShopByopPageLoaded = function() {
		return selectphonespf.isShopByopPageLoaded();
	};
	
	//this method checks whether  itis navigated to enter the carrier page
	this.isShopCarrierPageLoaded = function() {
		return selectphonespf.isShopCarrierPageLoaded();
	};
	
	
	//this this method checks whether the page is navigated to enter the simcard shop page
	this.isShopSimCardPageLoaded = function() {
		return selectphonespf.isShopSimCardPageLoaded();
	};
	
	//this method checks whether the page is navigated to enter the zipcode 
	this.isShopPhonePageLoaded = function() {
		return selectphonespf.isShopPhonePageLoaded();
	};
	
	//this method enters the zipcode and confirm the same.
	this.enterZipcode = function(zipcode) {
		return selectphonespf.enterZipcode(zipcode);
	};
	
	//this method navigates to buy the phones 
	this.isSelectPhonesPageLoaded = function() {
		return selectphonespf.isSelectPhonesPageLoaded();
	};
	
};

module.exports = new Shop;