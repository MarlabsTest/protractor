'use strict';//new
var ElementUtil = require("../util/element.util");

var ShopPhonePf = function() {

	this.shopPhonesLink= element(by.id('lnk_shopphones'));
	this.simcardLink= element(by.id('lnk_byop'));	
	this.compatabilityBtn= element(by.id('lnk_compatibility_choose_phone'));
	this.gsmBtn= element(by.id('btn_selectgsmphone'));
	this.simcardContinueBtn= element(by.id('btn_enterbyopdshopsim'));
	this.zipcode = element.all(by.id('zipPopup_zc')).get(0);
	this.continueBtn = element.all(by.id('pdp_zipbutton')).get(0);
	
	
	//this method clicks on the shop link
	this.goToShopPhones = function() {
		ElementUtil.waitForElement(this.shopPhonesLink);
		this.shopPhonesLink.click();
	};
	
	//this method checks whether the url is changed or not
	this.isShopPhonePageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/phones$/);
	};
	
	
	//this method clicks on the simcaed link
	this.goToShopSimCard = function() {
		ElementUtil.waitForElement(this.simcardLink);
		this.simcardLink.click();
	};
	
	//this method checks whether the url is changed or not
	this.isShopByopPageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/byop$/);
	};
	
	
	//this method checks whether the url is changed or not
	this.isShopCarrierPageLoaded = function(){
		this.compatabilityBtn.click();
		return ElementUtil.waitForUrlToChangeTo(/coveragecheck$/);
	};
	
	//this method checks whether the url is changed or not
	this.isShopSimCardPageLoaded = function(){
		this.gsmBtn.click();
		ElementUtil.waitForElement(this.simcardContinueBtn);
		this.simcardContinueBtn.click();
		return ElementUtil.waitForUrlToChangeTo(/simcards$/);
	};
	
	//this method enters the zipcode and confirm the same
	this.enterZipcode = function(zipcode){
		this.zipcode.sendKeys(zipcode);
		this.continueBtn.click();
		
	};	
	
	
};

module.exports = new ShopPhonePf;