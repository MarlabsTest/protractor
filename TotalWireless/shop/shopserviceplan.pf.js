'use strict';
var ElementUtil = require("../util/element.util");
var ConstantsUtil = require("../util/constants.util");
var shop = require("../shop/shop.pf");
var sessionData = require("../common/sessiondata.do");

var ShopServicePlan = function() {


    //Newly added $$
    this.smartPhonePlansTab = element(By.xpath("//span[@translate='BAT_24184']"));
    this.payAsGoPlansTab = element(By.xpath("//span[@translate='ACRE_6801']"));
    this.addOnPlansTab = element(By.xpath("//span[@translate='BAT_24571']"));
    
    this.thirtydayPlans = element.all(By.id('monthly_238'));
    this.dataonlyPlans = element.all(By.id('monthly_285'));
    this.addOnPlans = element.all(By.id('monthly_102'));

    this.autoReupPopup = element(by.className('modal-dialog'));
    
    this.select10Ild = element(By.id('monthly_500'));
	this.autoRefillBtn = element(By.id('addToCartEnroll_popup_btn'));
	this.actualOneTimePurchase = element(By.id('addToCartOneTimePurchpopup_btn'));
	this.autoRefillBtnSmtPln = element.all(By.id('btn_autorefill'));
	this.actualOneTimePurchaseSmtPln = element(By.id('btn_onetimepurchase'));
	this.phoneNumberTextbox = element.all(By.id('phonenum-min')).get(1);
	this.phoneNumberContinueBtn = element.all(By.id('saveDeviceSubmit_btn'));
	this.addPopUpOne = element(By.css("[ng-click='cancel()']"));
	this.doAddNowBtn = element.all(by.css('[type="submit"]')).get(0);
	
	this.thirtydayPlanTab = element(By.xpath("//span[@translate='BAT_24531']"));	
	
	this.dataonlyplanTab = element(By.xpath("//span[@translate='BAT_24913']"));
	this.newCustomerLink = element(By.css("[ng-click='newCustomer()']"));
	
	
	this.choosePlanByName = function(planType,planName,isAutoRefill,phoneType) {
        if (planType == ConstantsUtil.SHOP_PLANS[0]) 
        {
        	console.log("30 day plans");
        	element(By.id(ConstantsUtil.DAY_PLANS_30[planName])).click();
        	
        	var chosenPlanId = 'monthly_'+ConstantsUtil.DAY_PLANS_30[planName];
        	var chosenPlanElement = element(By.id(chosenPlanId));
        	ElementUtil.waitForElement(chosenPlanElement, 2000);
    		chosenPlanElement.click();
        }
        else if(planType == ConstantsUtil.SHOP_PLANS[1])
        {
        	console.log("add on ILD plan");              
        	if('ILD' == planName || 'ild'== planName)
        	{
        		console.log("ILD plan"); 
        		shop.clickOnILDPlanTab();
        		shop.selectPlanWithId(ConstantsUtil.ADDON_PLANS[planName]);
        	}
        	else
        	{
        		console.log("add on DATA plan"); 
        		shop.clickOnAddOnDataPlanTab();
        		shop.selectPlanWithId(ConstantsUtil.ADDON_PLANS[planName]);
        		shop.enterMinAndContinue();
        	}
        } 
        else if(planType == ConstantsUtil.SHOP_PLANS[2])
        {
        	console.log("data plan ");              
			element(By.xpath("//*[@id='" + ConstantsUtil.DATAONLY_PLANS[planName] + "']/ng-include/img")).click();
        }
  };
  
  	this.doAutoReUpPopupSelection=function(isAutoRefillNeeded){
//  		ElementUtil.waitForElement(this.autoReupPopup, 2000);
  		
  		console.log("isAutoRefillNeeded:: "+isAutoRefillNeeded);
		if(isAutoRefillNeeded == 'YES' || isAutoRefillNeeded == 'Y')
  		{
			console.log("auto refill");
			this.autoRefillBtn.isPresent().then(result => {
                if (result) {
                    this.autoRefillBtn.click();
                }
			});
//			this.autoRefillBtn.click();
  		}
		else
		{
			console.log("one time");
			this.actualOneTimePurchase.click();
		}
  	}
  
  	/*This  method will click do add now button*/
	this.doAddNow = function() {
		this.doAddNowBtn.click();
	};
	
	this.providePhoneNumber = function(phoneNumber){
	//ElementUtil.waitForElement(this.phoneNumberTextbox);
	this.phoneNumberTextbox.sendKeys(phoneNumber);
	//ElementUtil.waitForElement(this.phoneNumberContinueBtn);
	this.phoneNumberContinueBtn.click();
	};
	
	//this will navigate to enter the phone number
	this.isShopServicePlanPageLoaded = function() {
		return ElementUtil.waitForUrlToChangeTo(/shop\/plans$/);
	};
	
	//*** method to check whether the add now warning page loaded or not
	this.isAddNowWarningPageLoaded = function() {
		ElementUtil.waitForElement(this.doAddNowBtn);
		return this.doAddNowBtn.isPresent();
	};
	
	//check whether the page is navigated to the service plan page
	this.isShopServicePlansListed = function() {
	//	ElementUtil.waitForElement(this.addPopUpOne);
	//	this.addPopUpOne.click();
		return ElementUtil.waitForUrlToChangeTo(/serviceplan$/);
		
	};
	
	this.clickOn10ILDPlan = function() {
		ElementUtil.waitForElement(this.select10Ild);
		return this.select10Ild.click();
	};
	
	this.ServicePlanForNewCustomer = function(planName,planType){
		this.selectPlan(planName,planType);
		this.newCustomerLink.click();
	};
	this.selectPlan = function(planName,planType){
		console.log('IN PF FILE');
		console.log('monthlyPlan=' ,planName);
		console.log('planType=' ,planType);
		if (planType == ConstantsUtil.SHOP_PLANS[0]){
			console.log('SHOP PLNS IS 30 DAYPLAN');
			console.log('30 DAYPLAN=',ConstantsUtil.THIRTY_DAY_PLANS[planName]);
			element(By.id(ConstantsUtil.DAY_PLANS_30[planName])).click();			
      }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
    	  console.log('SHOP PLNS IS 30 DAYPLAN');
			console.log('DATAONLY_PLANS=',ConstantsUtil.DATAONLY_PLANS[planName]);
    	  element(By.id(ConstantsUtil.DATAONLY_PLANS[planName])).click();
      }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
    	  console.log('SHOP PLNS IS 30 DAYPLAN');
			console.log('ADDON_PLANS =',ConstantsUtil.ADDON_PLANS[planName]);
    	  element(By.id(ConstantsUtil.ADDON_PLANS[planName])).click();
      }     
		/*if (planType == ConstantsUtil.SHOP_PLANS[0]) {
            // this.thirtydayPlanTab.click();
             this.thirtydayPlans.click();
             //this.smartPhonePlans.get(ConstantsUtil.30DAY_PLANS[planName]).click();
       }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
             //this.payAsGoPlansTab.click();
             this.dataonlyPlans.click();
            /// this.payAsGoPlans.get(ConstantsUtil.ADDON_PLANS[planName]).click();
       }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
             //this.addOnPlansTab.click();
             this.addOnPlans.click();
             //this.addOnPlans.get(ConstantsUtil.DATAONLY_PLANS[planName]).click();
       }*/
		
	}
	
	this.isActivatePageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/selectdevice$/);
	};
	

};

module.exports = new ShopServicePlan;