'use strict';
var ElementUtil = require("../util/element.util");
var dataUtil= require("../util/datautils.util");
var sessionData = require("../common/sessiondata.do");

var ShopPf = function() {

	this.shopServicePlanLink = element.all(by.id('lnk_shopplans')).get(0);
	this.addOnDataPlanTab = element.all(by.className('panel-title')).get(7);
	
	this.minInput = element.all(by.className('form-control padding-right-xs-1  ng-pristine ng-untouched ng-valid')).get(0)
	this.continueBtn = element(By.id('saveDeviceSubmit_btn'));
	
	this.planILD  = element(by.id('monthly_500'));
	this.noThanksAutorefillBtn  = element(by.id('addToCartOneTimePurchpopup_btn'));
	
	this.deviceInfoText = element(by.css("input[id='device-info']"));
	this.deviceInfoTextContinue = element(by.css("button[id='saveDeviceSubmit_btn']"));
	
	this.enterMinAndContinue = function() {
		ElementUtil.waitForElement(this.minInput);
		this.minInput.sendKeys(dataUtil.getMinOfESN(sessionData.totalwireless.esn));
		this.continueBtn.click();
	};
	
	this.goToShopServicePlan = function() {
		ElementUtil.waitForElement(this.shopServicePlanLink);
		this.shopServicePlanLink.click();
	};
	
	this.enterEsnDetails = function(esnval){
		ElementUtil.waitForElement(this.deviceInfoText);
		this.deviceInfoText.sendKeys(esnval);
		this.deviceInfoTextContinue.click();
	};
	
	this.addOn10ILDTab = element.all(by.className('panel-title')).get(9);
	this.clickOnILDPlanTab = function() {
		ElementUtil.waitForElement(this.addOn10ILDTab);
		this.addOn10ILDTab.click();
	};
	
	this.clickOnAddOnDataPlanTab = function() {
		ElementUtil.waitForElement(this.addOnDataPlanTab);
		this.addOnDataPlanTab.click();
	};
	
	this.clickNoThanksAutorefillBtn = function() {
		//ElementUtil.waitForElement(this.addOn10ILDTab);
		this.noThanksAutorefillBtn.click();
	};
	
	this.selectILDPlan = function() {
		this.planILD.click();
	};
	
	this.selectPlanWithId = function(planId) {
		element(by.id(planId)).click();
	};
	
	
	this.isShopPageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/shop$/);
	};
	
	this.isServicePlanPageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/reset$/);
	};
	
};

module.exports = new ShopPf;