'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var shop = require('../../util/shopplans.util');
var sessionData = require("../../common/sessiondata.do");

describe('TW purchase ADD ON plan', function() {
	var activationData = shop.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				sessionData.totalwireless.shopPlan = inputActivationData.shopplan;
				sessionData.totalwireless.planName = inputActivationData.PlanName;
				sessionData.totalwireless.autoRefill = inputActivationData.AutoRefill;
				done();
			});
			FlowUtil.run('TW_SHOP_ADDONS');
		}).result.data = inputActivationData;
	});
});