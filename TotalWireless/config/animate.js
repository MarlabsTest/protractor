
module.exports = {
	
	disableAnimations: function() {
	
		var disableAnimate = function() {
			angular.module('disableAnimate', []).run(['$animate', function($animate) {
				$animate.enabled(false);
			}]);
		};
			
		var disableCssAnimate = function() {
			angular.module('disableCssAnimate', [])
				.run(function() {
					var style = document.createElement('style');
					style.type = 'text/css';
					style.innerHTML = '* {' +
						'-webkit-transition: none !important;' +
						'-moz-transition: none !important' +
						'-o-transition: none !important' +
						'-ms-transition: none !important' +
						'transition: none !important' +
						'}';
					document.getElementsByTagName('head')[0].appendChild(style);
				});
		};
		
		browser.addMockModule('disableAnimate', disableAnimate);	
		browser.addMockModule('disableCssAnimate', disableCssAnimate);
	}
};