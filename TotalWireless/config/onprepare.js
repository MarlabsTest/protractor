
var Reporter= require('./reporter');

module.exports = {
	
	prepare: function() {	
		
		browser.driver.manage().window().setSize(1364, 768);
	
		//waits for only Xml Http Requests - workaround to get rid of time outs due to some http polls
		//protractor.waitForXHROnly(true);		
		
		//delete all cookies
		browser.manage().deleteAllCookies();
	},
	
	initXMLReporter: function(browserName) {
		Reporter.initXmlReporter(browserName);
	},
	
	disableAnimations: function() {
		require('./animate').disableAnimations();
	},
	
	initHtmlReporter: function(browserName, baseReportDir) {
		Reporter.initHtmlReporter(browserName, baseReportDir);
	}
};