module.exports = {
	suites: {
		Activation: [
			'activation/scenario/activation.withpin.withaccount.scn.js',
			'activation/scenario/activation.mixedlines.withpin.withaccount.scn.js',
			'activation/scenario/activation.oneline.withpurchase.withaccount.scn.js',
			'activation/scenario/activation.hotspot.withpin.scn.js',
			'activation/scenario/activation.oneline.hotspot.withpin.scn.js',
			'activation/scenario/activation.oneline.hotspot.withpurchase.scn.js',
			'activation/scenario/activation.oneline.withpin.withaccount.scn.js',
			'activation/scenario/activation.byopphone.scn.js',
			'activation/scenario/activation.byopphone.withpurchase.scn.js',
			'activation/scenario/activate.internal.portin.withpin.withaccount.scn.js',
			'activation/scenario/activate.internal.portin.withpurchase.scn.js',
			'activation/scenario/activation.external.port.withpin.scn.js',
			'activation/scenario/activation.external.port.withpurchase.scn.js',
			'activation/scenario/activation.upgrade.flows.scn.js',
			'activation/scenario/reactivation.phone.scn.js',
			'activation/scenario/reactivation.phone.withpurchase.scn.js'
		]
		,
		Myaccount: [
			'myaccount/scenario/myaccount.login.withemail.scn.js',
			'myaccount/scenario/myaccount.login.withmin.scn.js',
			'myaccount/scenario/myaccount.firsttime.login.withmin.scn.js',
			'myaccount/scenario/myaccount.addServicePin.inside.scn.js',
			'myaccount/scenario/myaccount.forgotPassword.scn.js',
			'myaccount/scenario/myaccount.buyaddondata.scn.js',
			'myaccount/scenario/myaccount.addServicePin.addNow.scn.js',
			'myaccount/scenario/myaccount.managepayment.scn.js',
			'myaccount/scenario/myaccount.delete.payment.scn.js',
			'myaccount/scenario/myaccount.editcontactinfo.scn.js',
			'myaccount/scenario/myaccount.buyILDWithEnrollment.scn.js',
		    'myaccount/scenario/myaccount.ILDDeEnrollment.scn.js'
		]
		,
		AutoReUp: [
			'autoreup/scenario/autoreup.scn.js',
			'autoreup/scenario/deenroll.autoreup.scn.js'
		]
		,
		ReUp: [
			'reup/scenario/reup.withpin.outside.scn.js',
			'reup/scenario/reup.withpurchase.outside.scn.js',
			'reup/scenario/reup.withpurchase.inside.scn.js',
			'reup/scenario/reup.managereserve.switchplan.scn.js'
		]
		,
		Shop: [
		'shop/scenario/shop.addon.scn.js'
		]
	}
};
