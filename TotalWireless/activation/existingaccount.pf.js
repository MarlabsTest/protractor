'use strict';

var ElementsUtil = require("../util/element.util");
var ExistingAccount = function() {

	this.existingAccountContinueBtn = element.all(by.buttonText('Log In')).get(1);
	this.existingAccountSimContinueBtn = element.all(by.css('[ng-click="action()"]')).get(4);
	this.existingAccountSim = element.all(by.name('sim')).get(1);
	this.existingAccountPwdIdentifyFB = element(by.buttonText('Log in with Facebook'));
	this.existingAccountPasswordTextBox = element.all(by.name('password')).get(1);
	this.existingAccountLoginBtn = element(by.buttonText('Continue'));
	
	this.clickonExistingAccountContinueBtn = function(){
		//browser.wait(expectedConditions.visibilityOf(this.existingAccountContinueBtn),40000);
		//$$//ElementsUtil.waitForElement(this.existingAccountContinueBtn);
		this.existingAccountContinueBtn.click();
	};
  
	this.existingUserIdBoxLoaded = function(){
		return this.existingAccountSim.isPresent();
	};
  
	this.enterSimInExistingflow = function(sim){
		this.existingAccountSim.clear().sendKeys(sim);	
		this.existingAccountSimContinueBtn.click();
	};
  
	this.enterPasswordBoxLoaded = function(){
		return this.existingAccountPwdIdentifyFB.isPresent();
	};
  
	this.enterPassword = function(pwd){
		this.existingAccountPasswordTextBox.clear().sendKeys(pwd);
		this.existingAccountLoginBtn.click();
	};
  
};
module.exports = new ExistingAccount;