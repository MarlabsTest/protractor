/*
 * **********Spec file is for REACTIVATING A DEVICE ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.Once the device get deactivated,it will be listed under inactive devices in myaccount dashboard.
 * 4.Activate the device by clicking on the activate button in dashboard (Reactivation)
 * 5.we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 6.After doing all these,we are done with the reactivation process. 
 * 
 */

'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");

var generatedMin ={};
var generatedPin ={};

describe('Total Wireless Phone ReActivation with Purchase', function() {	
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should deactivate the device', function(done) {	
        var min = DataUtil.getMinOfESN(sessionData.totalwireless.esn);
        console.log('minVal ,    ::'+min);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.totalwireless.esn,min,'PASTDUE','TOTAL_WEB');	
        console.log('Deactivated    ::');
		generatedMin['min'] = min;
		//home.homePageLoad();
		 console.log('Home pageloaded    ::');
		//home.myAccount();
		 browser.refresh();
		console.log('Myaccount loaded ::');
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}).result.data = generatedMin;	  
	
	/*click on the activate button in the dashboard for the deactivated device
	 *Expected result - redirected to the page where we need to enter the zipcode.  
	 */
	
	it('should navigate to refillpage', function(done) {	
		myAccount.clickRefillNow();
		expect(activation.servicePlanPageLoaded()).toBe(true);	
		done();
	});

	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.totalwireless.shopPlan;
		var planName		= sessionData.totalwireless.planName;
		var isAutoRefill	= sessionData.totalwireless.autoRefill;
		var phoneType	    = sessionData.totalwireless.phoneType;
		shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
		//shop.doAutoReUpPopupSelection(isAutoRefill);
		console.log("SELECTED!!!");
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		//expect(shop.isAddNowWarningPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		//myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType), true);
		
		//enter cc details
		myAccount.enterCcNumbers(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
		
		//enter expiry date details
		var moreServicesLabel = element(by.css('[aria-label="more services"]')).element(by.className('selectize-input'));
		moreServicesLabel.isPresent().then(function(isVisible){
			if(isVisible)
			{
				//console.log("visible");
				myAccount.enterCcExpiryDateIndexOne();
			}
			else
			{
				//console.log("not visible");
				myAccount.enterCcExpiryDateIndexTwo();
			}
		});
		
		//enter billing details	
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click place my order button
	 *Expected result - Summary Page will be shown
	 */	
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
