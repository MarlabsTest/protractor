 'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//newly added for data integration
var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsnSim ={};
var generatedMin ={};
var generatedEsn = {};
var generatedSim = {};
var generatedPin = {};

describe('BYOP upgrade', function() {
	
	
	it('should navigate to the activation page for selecting the device upon clicking activate link', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	it('should click on button of I have a family phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {
	 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
	 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
		var esnval = DataUtil.getESN(sessionData.totalwireless.toEsnPartNumber);
       	console.log('returns', esnval);
		
		
		sessionData.totalwireless.esn = esnval;
		sessionData.totalwireless.esnsToReactivate.push(esnval);
		generatedEsn['esn'] = sessionData.totalwireless.esn;
		
		activation.enterEsn(esnval);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter the sim number and click the continue button', function(done) {
		
		var simval = DataUtil.getSIM(sessionData.totalwireless.toSimPartNumber);
       	console.log('returns', simval);
       	sessionData.totalwireless.sim = simval;		
       	generatedSim['sim'] = sessionData.totalwireless.sim;
		activation.enterSIM(simval);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
		var minVal = DataUtil.getMinOfESN(sessionData.totalwireless.upgradeEsn);
		generatedMin['min'] = minVal;
		activation.enterMobNumber(minVal);
		generatedMin['min'] = minVal;
		expect(activation.validateEsnLastNumbersPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	//To provide four digit authentication code and check whether the service plan page loaded
	it('Provide four digit authentication code', function(done) {
		//console.log("CommonUtil.getLastDigits(esnToBePort, 4) :: "+CommonUtil.getLastDigits(esnToBePort, 4));
		activation.enterCurrentEsnLastFourDigits(CommonUtil.getLastDigits(sessionData.totalwireless.upgradeEsn, 4));//last four digits of the current esn
		//expect(activation.servicePlanPageLoaded()).toBe(true);
		expect(activation.finalInstructionPageLoadedPortInPin()).toBe(true);
		done();		
	});
	/*
	it(' enter airtime pin and navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.totalwireless.toPinPartNumber);
       	console.log('returns Pin value', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.totalwireless.pin = pinval;
		generatedPin['pin'] = sessionData.totalwireless.pin;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	it('select create account  and navigate to account creation page', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	it('enter the account details and navigate to account creation successful popup', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.cardpin = "12345";
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Final Instruction Page will be shown
	 * /
	it('should load the final instruction page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoadedPortInPin()).toBe(true);
		done();		
	}); 
	*/
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPortInPin();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	//click done in the summary page and check whether the survey page loaded
	it('Click done button in the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(home.isHomePageLoaded()).toBe(true);
		done();		
	});
		
});
