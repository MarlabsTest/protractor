'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var esnJsonArray = [];
var simJsonArray = [];
var countIndex = 1;
var generatedPin ={};

var lineTypeOne = sessionData.totalwireless.lineTypeOne;
var lineTypeTwo = sessionData.totalwireless.lineTypeTwo;
var lineTypeThree = sessionData.totalwireless.lineTypeThree;
var lineTypeFour = sessionData.totalwireless.lineTypeFour;
//var generatedEsnSim = {};

describe('Total Wireless Mixed Lines Activation', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	//Add Lines
	for(var lineNo = 1;lineNo <= sessionData.totalwireless.noOfLines;lineNo++)
	{
		var esnPartNumber = "";
		var simPartNumber = "";
		var esnVal = "";
		var simVal = "";
		
		var activationLineType = "";
		if(lineNo == 1)
		{
			console.log("line one"+lineTypeOne);
			activationLineType = lineTypeOne;
			esnPartNumber = sessionData.totalwireless.esnPartNumber;
			simPartNumber = sessionData.totalwireless.simPartNumber;
		}
		else if(lineNo == 2)
		{
			console.log("line two"+lineTypeTwo);
			activationLineType = lineTypeTwo;
			esnPartNumber = sessionData.totalwireless.esnPartNumberTwo;
			simPartNumber = sessionData.totalwireless.simPartNumberTwo;
		}
		else if(lineNo == 3)
		{
			activationLineType = lineTypeThree;
			esnPartNumber = sessionData.totalwireless.esnPartNumberThree;
			simPartNumber = sessionData.totalwireless.simPartNumberThree;
		}
		else if(lineNo == 4)
		{
			activationLineType = lineTypeFour;
			esnPartNumber = sessionData.totalwireless.esnPartNumberFour;
			simPartNumber = sessionData.totalwireless.simPartNumberFour;
		}
		
		console.log("activationLineType"+activationLineType);
		console.log("esnPartNumber :: "+esnPartNumber);
		console.log("simPartNumber :: "+simPartNumber);
		
		//describe('Total Wireless Activation - New line', function() {
			
		if(lineNo != 1)
		{
			it('click on the continue button in the popup ', function(done) {
				activation.pinCheckoutAddDevice();
				expect(activation.isActivateLoaded()).toBe(true);
				done();		
			});
			
		}
		
		console.log("activationLineType :: "+activationLineType);
		console.log(" Branded :: "+CommonUtil.isEqualString('Branded', activationLineType));
		if(CommonUtil.isEqualString("Branded", activationLineType))
		{
			console.log("esnPartNumber inside :: "+esnPartNumber);
			console.log("simPartNumber inside :: "+simPartNumber);
			
			esnVal = DataUtil.getESN(esnPartNumber);
			simVal = DataUtil.getSIM(simPartNumber);
			
			var esnJson = {'esn':esnVal};
			esnJsonArray[lineNo] = esnJson;

			var simJson = {'sim':simVal};
			simJsonArray[lineNo] = simJson;
			
			
			describe('Total Wireless Activation - Add a New line', function() {
				
				it(' click on a Total Wireless phone',function(done){		
					activation.gotToEsnPage();
					expect(activation.esnPageLoaded()).toBe(true);
					done();
				});
				
				/*Enter the married SIM 
				 *Expected result - Security Popup page will be displayed  
				 */
				it('enter the esn and click continue button', function(done) {
				 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
				 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
					
					var esn = esnJsonArray[countIndex].esn;
			       	console.log('returns', esn);
					
					sessionData.totalwireless.esn = sessionData.totalwireless.esn + "|" +esn;
					sessionData.totalwireless.esnsToReactivate.push(esn);
					
					activation.enterEsn(esn);
					activation.checkBoxCheck();
					activation.continueESNClick();
					expect(activation.isSIMPage()).toBe(true);
					done();	
				}).result.data = esnJson;
				
				it('enter the sim number and click the continue button', function(done) {
					
					var sim = simJsonArray[countIndex].sim;
			       	console.log('returns', sim);
			       	sessionData.totalwireless.sim = sessionData.totalwireless.sim + "|" +sim;
			       	
					activation.enterSIM(sim);
					
					countIndex = countIndex + 1;
					expect(activation.keepMyPhonePageLoaded()).toBe(true);
					done();		
				}).result.data = simJson;
				
				if(lineNo == 1)
				{
					it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
						activation.enterZipCode(sessionData.totalwireless.zip);		
						expect(activation.servicePlanPageLoaded()).toBe(true);
						done();		
					});
					
					it(' enter airtime pin and navigate to account creation page', function(done) {
						var pinval = DataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
				       	console.log('returns Pin value', pinval);
						activation.enterAirTimePin(pinval);
						sessionData.totalwireless.pin = pinval;
						generatedPin['pin'] = sessionData.totalwireless.pin;
						expect(activation.activationAccountPageLoaded()).toBe(true);
						done();		
					}).result.data = generatedPin;
					
					
					it('select create account  and navigate to account creation page', function(done) {
						activation.clickonAccountCreationContinueBtn();
						expect(activation.emailTextBoxLoaded()).toBe(true);
						done();		
					});
					
					it('enter the account details and navigate to account creation successful popup', function(done) {
						var emailval = DataUtil.getEmail();
						var password = "tracfone";
						activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
						sessionData.totalwireless.cardpin = "12345";
						sessionData.totalwireless.username = emailval;
						sessionData.totalwireless.password = password;
						expect(activation.accountCreationDone()).toBe(true);
						done();		
					});
					
					it('click on the continue button in the popup ', function(done) {
						activation.clickOnAccountCreatedPopupBtn();
						expect(activation.pinCheckoutPageLoaded()).toBe(true);
						done();		
					});
				}
				else
				{
					it('enter the zipcode and navigate to pin checkout page', function(done) {
						activation.enterZipCode(sessionData.totalwireless.zip);		
						expect(activation.pinCheckoutPageLoaded()).toBe(true);
						done();		
					});
				}
			});
		}
		else if(CommonUtil.isEqualString("Byop", activationLineType))
		{
			esnVal = DataUtil.getCdmaByopESN();
			simVal = DataUtil.getSIM(simPartNumber);
			
			var esnJson = {'esn':esnVal};
			esnJsonArray[lineNo] = esnJson;

			var simJson = {'sim':simVal};
			simJsonArray[lineNo] = simJson;
			
			
			//to choose the device type as "Bring your own phone" and check whether the ESN page is loaded
			it('Choose device type BYOP',  function(done) {
				activation.selectByopDeviceType();
				activation.acceptTermsConditions();
				expect(activation.isByopSIMPage()).toBe(true);	
				done();
			});
			
			//to provide the ESN
			it('Enter ESN',  function(done) {
				var esn = esnJsonArray[countIndex].esn;
				var sim = simJsonArray[countIndex].sim;
				
				console.log("esnVal::: "+esn);
				sessionData.totalwireless.esn = sessionData.totalwireless.esn + "|" + esn;	
				activation.enterByopESN(esn);
				sessionData.totalwireless.esnsToReactivate.push(esn);
				console.log("simVal::: "+sim);
				sessionData.totalwireless.sim = sessionData.totalwireless.sim + "|" + sim;
				
				var transactionPendingMessage = element(by.css('[title="Transaction is pending for this device"]'));
				//ElementsUtil.waitForElement(transactionPendingMessage,200000);
				
				var EC = protractor.ExpectedConditions;
				browser.wait(EC.elementToBeClickable(transactionPendingMessage), 200000);
				
				transactionPendingMessage.isPresent().then(function(isVisible){
					if(isVisible)
					{
						var isTRIO = 'No';
						var isLTE = 'yes';
						var carrier = 'RSS';
						var phoneType = 'SAM';
						var phonemodel = '"""Samsung Galaxy S III 16GB Black"""';
						var isHD = 'No';
						
						DataUtil.updateCdmaByopESN(esn, sim, isTRIO, isLTE, carrier, phoneType, phonemodel, isHD);
					}
				});
				
				activation.enterByopESN(esn); 
				expect(activation.isCdmaByopSimLoaded()).toBe(true);
				//expect(activation.isActiveWithCarrierLoaded()).toBe(true);
				done();
			},200000).result.data = esnJson;
			
			it('Enter Sim',  function(done) {
				var sim = simJsonArray[countIndex].sim;
				console.log("sim::: "+sim);
				activation.enterCdmaByopSim(sim);		
				
				expect(activation.isActiveWithCarrierLoaded()).toBe(true);
				done();
			}).result.data = simJson;
			
			it('select not active',  function(done) {
				countIndex = countIndex + 1;
				activation.selectNotActive();
				expect(activation.zipcodeDivLoaded()).toBe(true);
				done();
			});
			
			if(lineNo == 1)
			{
				it('enter the zipcode  navigate to airtimeserviceplan page', function(done) {
					//activation.chooseDontKeepMyNumber();
					activation.enterZipCodeTabView(sessionData.totalwireless.zip);
					expect(activation.servicePlanPageLoaded()).toBe(true);
					done();		
				});
				
				it(' enter airtime pin navigate to account creation page', function(done) {
					var pinval = DataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
					activation.enterAirTimePin(pinval);
					sessionData.totalwireless.pin = pinval;
					generatedPin['pin'] = sessionData.totalwireless.pin;
					expect(activation.activationAccountPageLoaded()).toBe(true);
					done();		
				}).result.data = generatedPin;
				
				it('select create account option', function(done) {
					activation.clickonAccountCreationContinueBtn();
					expect(activation.fbButtonLoaded()).toBe(true);
					done();		
				}); 
				
				it('enter account creation details and create the account', function(done) {
					var emailval = DataUtil.getEmail();
					var password = "tracfone";  
					activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
					sessionData.totalwireless.username = emailval;
					sessionData.totalwireless.password = password;
					expect(activation.accountCreationDone()).toBe(true);
					done();		
				});
				
				it('load the final instruction page', function(done) {
					activation.clickOnAccountCreatedPopupBtn();
					expect(activation.pinCheckoutPageLoaded()).toBe(true);
					done();		
				});
			}
			else
			{
				it('enter the zipcode  navigate to pin checkout page', function(done) {
					//activation.chooseDontKeepMyNumber();
					activation.enterZipCodeTabView(sessionData.totalwireless.zip);
					expect(activation.pinCheckoutPageLoaded()).toBe(true);
					done();		
				});
			}
		}
		else if(CommonUtil.isEqualString("Hotspot", activationLineType))
		{
			esnVal = DataUtil.getESN(esnPartNumber);
			simVal = "";//Have No SIM
			
			var esnJson = {'esn':esnVal};
			esnJsonArray[lineNo] = esnJson;

			var simJson = {'sim':simVal};
			simJsonArray[lineNo] = simJson;
			
			describe('Total Wireless Hotspot Activation - Add a New line', function() {
				
				it(' click on a Total Wireless Hotspot',function(done){		
					activation.gotToHotSpotEsnPage();
					expect(activation.esnPageLoaded()).toBe(true);
					done();
				});
				
				/*
				 *Expected result - Security Popup page will be displayed  
				 */
				it('enter the esn and click continue button', function(done) {
				 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
					
					var esn = esnJsonArray[countIndex].esn;
			       	console.log('returns', esn);
					
					sessionData.totalwireless.esn = sessionData.totalwireless.esn + "|" +esn;
					sessionData.totalwireless.esnsToReactivate.push(esn);
					
					activation.enterEsn(esn);
					activation.checkBoxCheck();
					activation.continueESNClick();
					countIndex = countIndex + 1;
					expect(activation.keepMyPhonePageLoaded()).toBe(true);
					done();	
				}).result.data = esnJson;
				
				if(lineNo == 1)
				{
					it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
						activation.enterZipCodeHotSpot(sessionData.totalwireless.zip);		
						expect(activation.servicePlanPageLoaded()).toBe(true);
						done();		
					});
					
					it(' enter airtime pin and navigate to account creation page', function(done) {
						var pinval = DataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
				       	console.log('returns Pin value', pinval);
						activation.enterAirTimePin(pinval);
						sessionData.totalwireless.pin = pinval;
						generatedPin['pin'] = sessionData.totalwireless.pin;
						expect(activation.activationAccountPageLoaded()).toBe(true);
						done();		
					}).result.data = generatedPin;
					
					
					it('select create account  and navigate to account creation page', function(done) {
						activation.clickonAccountCreationContinueBtn();
						expect(activation.emailTextBoxLoaded()).toBe(true);
						done();		
					});
					
					it('enter the account details and navigate to account creation successful popup', function(done) {
						var emailval = DataUtil.getEmail();
						var password = "tracfone";
						activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
						sessionData.totalwireless.cardpin = "12345";
						sessionData.totalwireless.username = emailval;
						sessionData.totalwireless.password = password;
						expect(activation.accountCreationDone()).toBe(true);
						done();		
					});
					
					it('click on the continue button in the popup ', function(done) {
						activation.clickOnAccountCreatedPopupBtn();
						expect(activation.pinCheckoutPageLoaded()).toBe(true);
						done();		
					});
				}
				else
				{
					it('enter the zipcode and navigate to pin checkout page', function(done) {
						activation.enterZipCodeHotSpot(sessionData.totalwireless.zip);		
						expect(activation.pinCheckoutPageLoaded()).toBe(true);
						done();		
					});
				}
			});
		}
		//});
	}
	
	describe('Total Wireless Mixed Lines Activation - Instructions and Summary', function() {
		it('click on the continue button in the pin checkout page', function(done) {
			activation.pinCheckoutProceed();
			expect(activation.finalInstructionPageLoaded()).toBe(true);
			done();		
		});
		
		it('click on the continue button in the final instruction page', function(done) {
			activation.finalInstructionProceed();
			expect(activation.summaryPageLoaded()).toBe(true);
			done();		
		});
		
		it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
			activation.clickOnSummaryBtn();
			expect(myAccount.isLoaded()).toBe(true);		
			done();	
		});
	});
});