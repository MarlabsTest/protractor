'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var CommonUtil = require('../../util/common.functions.util');
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};
//var generatedEsnSim = {};

describe('Total Wireless Activation External Port with Purchase', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on a Total Wireless phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {
	 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
	 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
		var esnval = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
       	console.log('returns', esnval);
		
		
		sessionData.totalwireless.esn = esnval;
		sessionData.totalwireless.esnsToReactivate.push(esnval);
		generatedEsn['esn'] = sessionData.totalwireless.esn;
		
		activation.enterEsn(esnval);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter the sim number and click the continue button', function(done) {
		
		var simval = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
       	console.log('returns', simval);
       	sessionData.totalwireless.sim = simval;		
       	generatedSim['sim'] = sessionData.totalwireless.sim;
		activation.enterSIM(simval);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;

	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
		var min = Math.floor((Math.random() * (9999999999-9000000000)) + 90000000000);
		console.log("Random MIN ::",min);
		sessionData.totalwireless.min = min;
		activation.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activation.ServiceProviderPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	/*Select the phone type 
	 *Expected result - Previous phone account details form will be loaded
	 */	
	it('should load the previous phone account details form', function(done) {
		activation.selectPhoneType();
		expect(activation.accountDetailsLoaded()).toBe(true);
		done();		
	});
	
	/*Enter the phone account details
	 *Expected result - Select Address popup page will be loaded
	 */	
	it('should navigate to the address popup page', function(done) {
		activation.enterPhoneAccountDetails("0123459","test@1234","3559","Ginny","Potter","9809806751","Rocky Hill","Miami","Florida","33178");
		expect(activation.selectAddressPopUpLoaded()).toBe(true);
		done();		
	});
	
	/*Select the Keep this address option
	 *Expected result - Page to enter address details  will be loaded
	 */	
	it('should load the address details page', function(done) {
		activation.keepThisAddress();
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.totalwireless.shopPlan;
		var planName		= sessionData.totalwireless.planName;
		var isAutoRefill	= sessionData.totalwireless.autoRefill;
		var phoneType	    = sessionData.totalwireless.phoneType;
		shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
		shop.doAutoReUpPopupSelection(isAutoRefill);
		console.log("SELECTED!!!");
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		//expect(shop.isAddNowWarningPageLoaded()).toBe(true);
		done();
	});
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Checkout Page will be shown
	 */
	it('should load the checkout page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		//myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType), true);
		
		//enter cc details
		myAccount.enterCcNumbers(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
		
		//enter expiry date details
		var moreServicesLabel = element(by.css('[aria-label="more services"]')).element(by.className('selectize-input'));
		moreServicesLabel.isPresent().then(function(isVisible){
			if(isVisible)
			{
				//console.log("visible");
				myAccount.enterCcExpiryDateIndexOne();
			}
			else
			{
				//console.log("not visible");
				myAccount.enterCcExpiryDateIndexTwo();
			}
		});
		
		//enter billing details	
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click place my order button
	 *Expected result - Summary Page will be shown
	 */	
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	
});
