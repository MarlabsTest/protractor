//This spec file is for Total Wireless Internal Port In with PIN. 
//User has to choose the device type "I have a Total Wireless phone"
//and provide SIM, security PIN, MIN number(to be port), AT PIN to 
//do the internal port in with PIN.
'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var CommonUtil= require("../../util/common.functions.util");
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");

var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};
var esnToBePort = "";

describe('Total Wireless Internal Porting with PIN', function() {
	
	//To click activate link in the menu and check whether the activation page is loaded 
	it('Go to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	//To choose "I have a family phone" option and check whether the esn page is loaded
	it('Choose Total Wireless phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {
	 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
	 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
		var esn = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
       	console.log('esnval=', esn);
		
		sessionData.totalwireless.esn = esn;
		sessionData.totalwireless.esnsToReactivate.push(esn);
		generatedEsn['esn'] = esn;
		activation.enterEsn(esn);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		//expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter the sim number and click the continue button', function(done) {		
		var sim = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
       	console.log('returns', sim);
       	sessionData.totalwireless.sim = sim;	
       	generatedSim['sim'] = sim;
		activation.enterSIM(sim);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
		
	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
		var esnToBePortVal = DataUtil.getActiveESNFromPartNumber(sessionData.totalwireless.oldPartNumber, sessionData.totalwireless.noOfLines);
		var esnToBePortList = esnToBePortVal.split("|");
		esnToBePort = esnToBePortList[0];
		
		var min = DataUtil.getMinOfESN(esnToBePort);
		generatedMin['min'] = min;
		activation.enterMobNumber(min);
		
		expect(activation.validateEsnLastNumbersPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	//To provide four digit authentication code and check whether the service plan page loaded
	it('Provide four digit authentication code', function(done) {
		//console.log("CommonUtil.getLastDigits(esnToBePort, 4) :: "+CommonUtil.getLastDigits(esnToBePort, 4));
		activation.enterFourDigitCodeFromMsg(CommonUtil.getLastDigits(esnToBePort, 4));//last four digits of the current esn
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.totalwireless.shopPlan;
		var planName		= sessionData.totalwireless.planName;
		var isAutoRefill	= sessionData.totalwireless.autoRefill;
		var phoneType	    = sessionData.totalwireless.phoneType;
		shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
		shop.doAutoReUpPopupSelection(isAutoRefill);
		console.log("SELECTED!!!");
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		//expect(shop.isAddNowWarningPageLoaded()).toBe(true);
		done();
	});
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Checkout Page will be shown
	 */
	it('should load the checkout page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		//myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType), true);
		
		//enter cc details
		myAccount.enterCcNumbers(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
		
		//enter expiry date details
		var moreServicesLabel = element(by.css('[aria-label="more services"]')).element(by.className('selectize-input'));
		moreServicesLabel.isPresent().then(function(isVisible){
			if(isVisible)
			{
				//console.log("visible");
				myAccount.enterCcExpiryDateIndexOne();
			}
			else
			{
				//console.log("not visible");
				myAccount.enterCcExpiryDateIndexTwo();
			}
		});
		
		//enter billing details	
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click place my order button
	 *Expected result - Summary Page will be shown
	 */	
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
