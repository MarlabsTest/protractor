'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var esnJsonArray = [];
var simJsonArray = [];
var minJsonArray = [];
var countIndex = 0;

var generatedPin ={};

var primaryEsnVal = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
var primaryEsnJson = {'esn':primaryEsnVal};
esnJsonArray[countIndex] = primaryEsnJson;
var primarySimVal = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
var primarySimJson = {'sim':primarySimVal};
simJsonArray[countIndex] = primarySimJson;
var primaryMinVal = Math.floor((Math.random() * (9999999999-9000000000)) + 90000000000);
var primaryMinJson = {'min':primaryMinVal};
minJsonArray[countIndex] = primaryMinJson;
//var generatedEsnSim = {};

describe('Total Wireless Activation External Port with PIN', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on a Total Wireless phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {
	 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
	 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
		var esn = esnJsonArray[countIndex].esn;
       	console.log('returns', esn);
		
		sessionData.totalwireless.esn = esn;
		sessionData.totalwireless.esnsToReactivate.push(esn);
		
		activation.enterEsn(esn);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = primaryEsnJson;
	
	it('enter the sim number and click the continue button', function(done) {
		
		var sim = simJsonArray[countIndex].sim;
       	console.log('returns', sim);
       	sessionData.totalwireless.sim = sim;		
		activation.enterSIM(sim);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = primarySimJson;

	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
		var min = minJsonArray[countIndex].min;
		console.log("Random MIN ::",min);
		sessionData.totalwireless.min = min;
		activation.enterMobNumber(min);
		
		countIndex = countIndex + 1;
		expect(activation.ServiceProviderPageLoaded()).toBe(true);
		done();		
	}).result.data = primaryMinJson;
	
	/*Select the phone type 
	 *Expected result - Previous phone account details form will be loaded
	 */	
	it('should load the previous phone account details form', function(done) {
		activation.selectPhoneType();
		expect(activation.accountDetailsLoaded()).toBe(true);
		done();		
	});
	
	/*Enter the phone account details
	 *Expected result - Select Address popup page will be loaded
	 */	
	it('should navigate to the address popup page', function(done) {
		activation.enterPhoneAccountDetails("0123459","test@1234","3559","Ginny","Potter","9809806751","Rocky Hill","Miami","Florida","33178");
		expect(activation.selectAddressPopUpLoaded()).toBe(true);
		done();		
	});
	
	/*Select the Keep this address option
	 *Expected result - Page to enter address details  will be loaded
	 */	
	it('should load the address details page', function(done) {
		activation.keepThisAddress();
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*Enter an airtime PIN 
	 *Expected result - Account creation Page will be loaded
	 */
	it('should navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
       	console.log('returns', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.totalwireless.pin = pinval;
		generatedPin['pin'] = pinval;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	it('enter the account details and navigate to account creation successful popup', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.cardpin = "12345";
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the popup ', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.pinCheckoutPageLoaded()).toBe(true);
		done();		
	});
	
	//Add Lines
	for(var i = 1;i < sessionData.totalwireless.noOfLines;i++)
	{
		var esnVal = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
		var esnJson = {'esn':esnVal};
		esnJsonArray[i] = esnJson;

		var simVal = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
		var simJson = {'sim':simVal};
		simJsonArray[i] = simJson;
		
		var minVal = Math.floor((Math.random() * (9999999999-9000000000)) + 90000000000);
		var minJson = {'min':minVal};
		minJsonArray[i] = minJson;
		
		describe('Total Wireless Activation - Add a New line', function() {
			it('click on the continue button in the popup ', function(done) {
				activation.pinCheckoutAddDevice();
				expect(activation.isActivateLoaded()).toBe(true);
				done();		
			});
			
			it(' click on a Total Wireless phone',function(done){		
				activation.gotToEsnPage();
				expect(activation.esnPageLoaded()).toBe(true);
				done();
			});
			
			/*Enter the married SIM 
			 *Expected result - Security Popup page will be displayed  
			 */
			it('enter the esn and click continue button', function(done) {
			 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
			 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
				var esn = esnJsonArray[countIndex].esn;
		       	console.log('returns', esn);
				
				sessionData.totalwireless.esn = sessionData.totalwireless.esn +"|"+ esn;
				sessionData.totalwireless.esnsToReactivate.push(esn);
				
				activation.enterEsn(esn);
				activation.checkBoxCheck();
				activation.continueESNClick();
				expect(activation.isSIMPage()).toBe(true);
				done();	
			}).result.data = esnJson;
			
			it('enter the sim number and click the continue button', function(done) {
				
				var sim = simJsonArray[countIndex].sim;
		       	console.log('returns', sim);
		       	sessionData.totalwireless.sim = sessionData.totalwireless.sim +"|"+ sim;		
				activation.enterSIM(sim);		
				expect(activation.keepMyPhonePageLoaded()).toBe(true);
				done();		
			}).result.data = simJson;

			//To provide MIN number and check whether the validate ESN page loaded
			it('Provide mobile number', function(done) {
				var min = minJsonArray[countIndex].min;
				console.log("Random MIN ::",min);
				sessionData.totalwireless.min = sessionData.totalwireless.min +"|"+ min;
				activation.enterMobNumber(min);
				
				countIndex = countIndex + 1;
				expect(activation.ServiceProviderPageLoaded()).toBe(true);
				done();		
			}).result.data = minJson;
			
			/*Select the phone type 
			 *Expected result - Previous phone account details form will be loaded
			 */	
			it('should load the previous phone account details form', function(done) {
				activation.selectPhoneType();
				expect(activation.accountDetailsLoaded()).toBe(true);
				done();		
			});
			
			/*Enter the phone account details
			 *Expected result - Select Address popup page will be loaded
			 */	
			it('should navigate to the address popup page', function(done) {
				activation.enterPhoneAccountDetails("0123459","test@1234","3559","Ginny","Potter","9809806751","Rocky Hill","Miami","Florida","33178");
				expect(activation.selectAddressPopUpLoaded()).toBe(true);
				done();		
			});
			
			/*Select the Keep this address option
			 *Expected result - Page to enter address details  will be loaded
			 */	
			it('should load the address details page', function(done) {
				activation.keepThisAddress();
				expect(activation.pinCheckoutPageLoaded()).toBe(true);
				done();		
			});
			
		});
	}
	
	it('click on the continue button in the pin checkout page', function(done) {
		activation.pinCheckoutProceed();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnSummaryBtn();
		expect(myAccount.isLoaded()).toBe(true);		
		done();	
	});
});
