'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};
//var generatedEsnSim = {};

describe('Total Wireless Activation GSM With No Pin', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on a Total Wireless phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {
	 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
	 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
		var esnval = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
       	console.log('returns', esnval);
		
		
		sessionData.totalwireless.esn = esnval;
		sessionData.totalwireless.esnsToReactivate.push(esnval);
		generatedEsn['esn'] = sessionData.totalwireless.esn;
		
		activation.enterEsn(esnval);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter the sim number and click the continue button', function(done) {
		
		var simval = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
       	console.log('returns', simval);
       	sessionData.totalwireless.sim = simval;		
       	generatedSim['sim'] = sessionData.totalwireless.sim;
		activation.enterSIM(simval);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.totalwireless.zip);		
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it(' enter airtime pin and navigate to account creation page', function(done) {
		activation.clickSkipButton();
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('select create account  and navigate to account creation page', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	it('enter the account details and navigate to account creation successful popup', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.cardpin = "12345";
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the popup ', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnSummaryBtn();
		sessionData.totalwireless.upgradeEsn = sessionData.totalwireless.esn;//specific to phone upgrade					
		expect(myAccount.isLoaded()).toBe(true);		
		done();	
	}).result.data = generatedMin;

	// });
	
});
