 'use strict';
var activation = require("../activation.po");
var myAccountPo = require("../../myaccount/myaccount.po");
var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsnSim ={};
var generatedMin ={};
var generatedEsn = {};
var generatedSim = {};
var generatedPin = {};

describe('TW Phone upgrade', function() {
	it('select phone upgrade option from my account', function(done) {	
		browser.driver.navigate().refresh(); 
		myAccountPo.clickOnShowDevice();
		myAccountPo.clickOnOptionLink();
		myAccountPo.clickOnUpgrdaeDevice();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	}); 
	it('select Total Wireless phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn value and continue', function(done) {
		var esnval = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
       	console.log('returns', esnval);
		sessionData.totalwireless.esn = esnval;
		sessionData.totalwireless.esnsToReactivate.push(esnval);
		generatedEsn['esn'] = sessionData.totalwireless.esn;
		
		activation.enterEsn(esnval);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter the sim number and continue', function(done) {		
		var simval = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
       	console.log('returns', simval);
       	sessionData.totalwireless.sim = simval;		
       	generatedSim['sim'] = sessionData.totalwireless.sim;
		activation.enterSIM(simval);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;	
	//To provide MIN number
	it('Provide mobile number', function(done) {
		activation.selectMin();
		activation.clickOnMinContinue();
		expect(activation.isPhoneUpgradeRequestPageLoaded()).toBe(true);
		done();		
	});
	it('continue to Phone Upgrade Request Page', function(done) {
		activation.continuePhoneUpgradeRequestPage();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	it('load summary page and navigate to account dashboard', function(done) {
		activation.clickOnSummaryBtn();
		sessionData.totalwireless.upgradeEsn = sessionData.totalwireless.esn;//specific to phone upgrade					
		expect(myAccountPo.isLoaded()).toBe(true);		
		done();	
	}).result.data = generatedMin;
	
});
