/*
 * **********Scenario is the consolidation of Upgrade  flows in Total Wireless ******** 
 * 
 * 1.Read the input data for upgrade from CSV
 * 2.Based on the partnumber,decide which flow to proceed with.
 * 3.If the partnumber starts with 'PH' ,it will be a  BYOP upgrade.In that we are initially doing a BYOP activation process
 * 4.The third upgrade flow is the normal Total Wireless phone upgrade.  We will be doing activation with PIN flow.
 * 5.In all the above cases,After activating the device, will get the MIN number.
 * 6.After getting MIN, It's executes an phone upgrade spec with new part numbers and the same MIN.Finally, the older ESN gets upgraded with the new ESN with the same MIN number.
 */

'use strict';

var sessionData = require("../../common/sessiondata.do");
var CommonUtil =  require('../../util/common.functions.util');
var FlowUtil = require('../../util/flow.util');

describe('Total Wireless Upgrade' , function() {
	var esnPartNumber = sessionData.totalwireless.esnPartNumber;
	var arFlag = sessionData.totalwireless.arFlag;
	var varPhoneStatus	= sessionData.totalwireless.phoneStatus;
	/*if(CommonUtil.isByopEsn(esnPartNumber)){
		console.log('BYOP upgrade');
		FlowUtil.run('TW_BYOP_UPGRADE');				
	}
	else if(CommonUtil.isInactive(varPhoneStatus)){
		Upgrade ESN active to ESN inactive- MIN reserve and non reserve scenario.
		 * This method check the PhoneStatus field in input file and return the result
		console.log("Upgrade from active esn to inactive esn");
		FlowUtil.run('TW_UPGRADE_ACTIVE_ESN_TO_INACTV_ESN');
	}
	else{*/
		console.log("normal phone upgrade flow");
		FlowUtil.run('TW_PHONE_UPGRADE');		
	//}
});
