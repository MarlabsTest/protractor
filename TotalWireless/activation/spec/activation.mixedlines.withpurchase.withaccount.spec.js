'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var shop = require("../../shop/shop.po");

var esnJsonArray = [];
var simJsonArray = [];
var countIndex = 1;
var generatedPin ={};

var lineTypeOne = sessionData.totalwireless.lineTypeOne;
var lineTypeTwo = sessionData.totalwireless.lineTypeTwo;
var lineTypeThree = sessionData.totalwireless.lineTypeThree;
var lineTypeFour = sessionData.totalwireless.lineTypeFour;
//var generatedEsnSim = {};

describe('Total Wireless Mixed Lines Activation with Purchase', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	var esnPartNumber = "";
	var simPartNumber = "";
	var esnVal = "";
	var simVal = "";
	
	var activationLineType = lineTypeOne;
	var lineNo = 1;
	esnPartNumber = sessionData.totalwireless.esnPartNumber;
	simPartNumber = sessionData.totalwireless.simPartNumber;
	console.log("activationLineType"+activationLineType);
	console.log("esnPartNumber :: "+esnPartNumber);
	console.log("simPartNumber :: "+simPartNumber);
	
	//describe('Total Wireless Activation - New line', function() {
	console.log("activationLineType :: "+activationLineType);
	console.log(" Branded :: "+CommonUtil.isEqualString('Branded', activationLineType));
	if(CommonUtil.isEqualString("Branded", activationLineType))
	{
		console.log("esnPartNumber inside :: "+esnPartNumber);
		console.log("simPartNumber inside :: "+simPartNumber);
		
		esnVal = DataUtil.getESN(esnPartNumber);
		simVal = DataUtil.getSIM(simPartNumber);
		
		var esnJson = {'esn':esnVal};
		esnJsonArray[lineNo] = esnJson;

		var simJson = {'sim':simVal};
		simJsonArray[lineNo] = simJson;
		
		
		describe('Total Wireless Activation - Add a New line', function() {
			
			it(' click on a Total Wireless phone',function(done){		
				activation.gotToEsnPage();
				expect(activation.esnPageLoaded()).toBe(true);
				done();
			});
			
			/*Enter the married SIM 
			 *Expected result - Security Popup page will be displayed  
			 */
			it('enter the esn and click continue button', function(done) {
			 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
			 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
				
				var esn = esnJsonArray[countIndex].esn;
		       	console.log('returns', esn);
				
				sessionData.totalwireless.esn = sessionData.totalwireless.esn + "|" +esn;
				sessionData.totalwireless.esnsToReactivate.push(esn);
				
				activation.enterEsn(esn);
				activation.checkBoxCheck();
				activation.continueESNClick();
				expect(activation.isSIMPage()).toBe(true);
				done();	
			}).result.data = esnJson;
			
			it('enter the sim number and click the continue button', function(done) {
				
				var sim = simJsonArray[countIndex].sim;
		       	console.log('returns', sim);
		       	sessionData.totalwireless.sim = sessionData.totalwireless.sim + "|" +sim;
		       	
				activation.enterSIM(sim);
				
				countIndex = countIndex + 1;
				expect(activation.keepMyPhonePageLoaded()).toBe(true);
				done();		
			}).result.data = simJson;
			
			it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
				activation.enterZipCode(sessionData.totalwireless.zip);		
				expect(activation.servicePlanPageLoaded()).toBe(true);
				done();		
			});
			
			/*Select an airtime Plan 
			 *Expected result - Selected Plan details form will be loaded
			 */
			it('should load the selected plan details and buy it', function(done) {
				activation.airtimePurchase();
				expect(activation.selectServicePlanPageLoaded()).toBe(true);
				done();		
			});
			
			it('choose 30day plan and add to cart', function(done) {
				//shop.choosePlan("PAYASGO");
				var planType		= sessionData.totalwireless.shopPlan;
				var planName		= sessionData.totalwireless.planName;
				var isAutoRefill	= sessionData.totalwireless.autoRefill;
				var phoneType	    = sessionData.totalwireless.phoneType;
				shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
				shop.doAutoReUpPopupSelection(isAutoRefill);
				console.log("SELECTED!!!");
				//expect(myAccount.checkoutPageLoaded()).toBe(true);
				expect(activation.activationAccountPageLoaded()).toBe(true);
				//expect(shop.isAddNowWarningPageLoaded()).toBe(true);
				done();
			});
			
			/*Click on the new account creation 
			 *Expected result - New Account creation form will be shown
			 */
			it('should load the account creation form', function(done) {
				activation.clickonAccountCreationContinueBtn();
				expect(activation.emailTextBoxLoaded()).toBe(true);
				done();		
			});
			
			/* Enter email,password,DOB and securitypin 
			 * Expected result - Account created successfully popup page
			 */
			it('should create a new account', function(done) {
				var emailval = DataUtil.getEmail();
				var password = "tracfone"; 
		       	console.log('returns', emailval);
				activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
				sessionData.totalwireless.username = emailval;
				sessionData.totalwireless.password = password;
				expect(activation.accountCreationDone()).toBe(true);
				done();		
			});
			
			/*Click on continue button in the popup page 
			 *Expected result - Checkout Page will be shown
			 */
			it('should load the checkout page', function(done) {
				activation.clickOnAccountCreatedPopupBtn();
				expect(myAccount.checkoutPageLoaded()).toBe(true);
				done();		
			}); 
			
			/*Enter the credit card and billing details in the payment form 
			 *Expected result - billing zipcode changes popup shown
			 */	
			it('should show the billing zipcode changes popup', function(done) {
				//myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType), true);
				
				//enter cc details
				myAccount.enterCcNumbers(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
				
				//enter expiry date details
				var moreServicesLabel = element(by.css('[aria-label="more services"]')).element(by.className('selectize-input'));
				moreServicesLabel.isPresent().then(function(isVisible){
					if(isVisible)
					{
						//console.log("visible");
						myAccount.enterCcExpiryDateIndexOne();
					}
					else
					{
						//console.log("not visible");
						myAccount.enterCcExpiryDateIndexTwo();
					}
				});
				
				//enter billing details	
				myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
				myAccount.placeMyOrder();
				//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
				expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
				done();		
			}); 
			
			/*Agree billing zipcode changes popup
			 *Expected result - checkout page will be loaded
			 */	
			it('should load back to the same check out page', function(done) {
				myAccount.agreeBillingZipcodeChanges();
				expect(myAccount.checkoutPageLoaded()).toBe(true);
				done();		
			});
			
			/*Click place my order button
			 *Expected result - Summary Page will be shown
			 */	
			it('should load the summary page', function(done) {
				myAccount.placeMyOrder();
				expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
				done();		
			});
			
			/*Click on continue button in the Final Instruction Page 
			 *Expected result - Summary Page will be shown
			 */
			it('should load the summary page', function(done) {
				activation.finalInstructionProceedPurchase();
				expect(activation.summaryPageLoaded()).toBe(true);
				done();		
			});

			/*Click on continue button in the Summary Page 
			 *Expected result - Survey Page will be loaded
			 */
			it('should load the survey page', function(done) {
				activation.clickOnSummaryBtn();
				expect(activation.surveyPageLoaded()).toBe(true);
				done();		
			});
			
			/*Click on 'No,Thanks' button in the Survey Page 
			 *Expected result - My account dashboard will be loaded
			 */
			it('should redirect to the account dashboard page', function(done) {
				activation.clickOnThankYouBtn();
				expect(myAccount.isLoaded()).toBe(true);
				done();		
			});
		});
	}
	else if(CommonUtil.isEqualString("Byop", activationLineType))
	{
		esnVal = DataUtil.getCdmaByopESN();
		simVal = DataUtil.getSIM(simPartNumber);
		
		var esnJson = {'esn':esnVal};
		esnJsonArray[lineNo] = esnJson;

		var simJson = {'sim':simVal};
		simJsonArray[lineNo] = simJson;
		
		
		//TODO:: BYOP purchase flow to be added
	}
	else if(CommonUtil.isEqualString("Hotspot", activationLineType))
	{
		esnVal = DataUtil.getESN(esnPartNumber);
		simVal = "";//Have No SIM
		
		var esnJson = {'esn':esnVal};
		esnJsonArray[lineNo] = esnJson;

		var simJson = {'sim':simVal};
		simJsonArray[lineNo] = simJson;
		
		describe('Total Wireless Hotspot Activation - Add a New line', function() {
			
			it(' click on a Total Wireless Hotspot',function(done){		
				activation.gotToHotSpotEsnPage();
				expect(activation.esnPageLoaded()).toBe(true);
				done();
			});
			
			/*
			 *Expected result - Security Popup page will be displayed  
			 */
			it('enter the esn and click continue button', function(done) {
			 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
				
				var esn = esnJsonArray[countIndex].esn;
		       	console.log('returns', esn);
				
				sessionData.totalwireless.esn = sessionData.totalwireless.esn + "|" +esn;
				sessionData.totalwireless.esnsToReactivate.push(esn);
				
				activation.enterEsn(esn);
				activation.checkBoxCheck();
				activation.continueESNClick();
				countIndex = countIndex + 1;
				expect(activation.keepMyPhonePageLoaded()).toBe(true);
				done();	
			}).result.data = esnJson;
			
			it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
				activation.enterZipCodeHotSpot(sessionData.totalwireless.zip);		
				expect(activation.servicePlanPageLoaded()).toBe(true);
				done();		
			});
			
			/*Select an airtime Plan 
			 *Expected result - Selected Plan details form will be loaded
			 */
			it('should load the selected plan details and buy it', function(done) {
				activation.airtimePurchase();
				expect(activation.selectServicePlanPageLoaded()).toBe(true);
				done();		
			});
			
			it('choose 30day plan and add to cart', function(done) {
				//shop.choosePlan("PAYASGO");
				var planType		= sessionData.totalwireless.shopPlan;
				var planName		= sessionData.totalwireless.planName;
				var isAutoRefill	= sessionData.totalwireless.autoRefill;
				var phoneType	    = sessionData.totalwireless.phoneType;
				shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
				shop.doAutoReUpPopupSelection(isAutoRefill);
				console.log("SELECTED!!!");
				//expect(myAccount.checkoutPageLoaded()).toBe(true);
				expect(activation.activationAccountPageLoaded()).toBe(true);
				done();
			});
			
			/*Click on the new account creation 
			 *Expected result - New Account creation form will be shown
			 */
			it('should load the account creation form', function(done) {
				activation.clickonAccountCreationContinueBtn();
				expect(activation.emailTextBoxLoaded()).toBe(true);
				done();		
			});
			
			/* Enter email,password,DOB and securitypin 
			 * Expected result - Account created successfully popup page
			 */
			it('should create a new account', function(done) {
				var emailval = DataUtil.getEmail();
				var password = "tracfone"; 
		       	console.log('returns', emailval);
				activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
				sessionData.totalwireless.username = emailval;
				sessionData.totalwireless.password = password;
				expect(activation.accountCreationDone()).toBe(true);
				done();		
			});
			
			/*Click on continue button in the popup page 
			 *Expected result - Checkout Page will be shown
			 */
			it('should load the checkout page', function(done) {
				activation.clickOnAccountCreatedPopupBtn();
				expect(myAccount.checkoutPageLoaded()).toBe(true);
				done();		
			}); 
			
			/*Enter the credit card and billing details in the payment form 
			 *Expected result - billing zipcode changes popup shown
			 */	
			it('should show the billing zipcode changes popup', function(done) {
				//myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType), true);
				
				//enter cc details
				myAccount.enterCcNumbers(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
				
				//enter expiry date details
				var moreServicesLabel = element(by.css('[aria-label="more services"]')).element(by.className('selectize-input'));
				moreServicesLabel.isPresent().then(function(isVisible){
					if(isVisible)
					{
						//console.log("visible");
						myAccount.enterCcExpiryDateIndexOne();
					}
					else
					{
						//console.log("not visible");
						myAccount.enterCcExpiryDateIndexTwo();
					}
				});
				
				//enter billing details	
				myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
				myAccount.placeMyOrder();
				//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
				expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
				done();		
			}); 
			
			/*Agree billing zipcode changes popup
			 *Expected result - checkout page will be loaded
			 */	
			it('should load back to the same check out page', function(done) {
				myAccount.agreeBillingZipcodeChanges();
				expect(myAccount.checkoutPageLoaded()).toBe(true);
				done();		
			});
			
			/*Click place my order button
			 *Expected result - Summary Page will be shown
			 */	
			it('should load the summary page', function(done) {
				myAccount.placeMyOrder();
				expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
				done();		
			});
			
			/*Click on continue button in the Final Instruction Page 
			 *Expected result - Summary Page will be shown
			 */
			it('should load the summary page', function(done) {
				activation.finalInstructionProceedPurchase();
				expect(activation.summaryPageLoaded()).toBe(true);
				done();		
			});

			/*Click on continue button in the Summary Page 
			 *Expected result - Survey Page will be loaded
			 */
			it('should load the survey page', function(done) {
				activation.clickOnSummaryBtn();
				expect(activation.surveyPageLoaded()).toBe(true);
				done();		
			});
			
			/*Click on 'No,Thanks' button in the Survey Page 
			 *Expected result - My account dashboard will be loaded
			 */
			it('should redirect to the account dashboard page', function(done) {
				activation.clickOnThankYouBtn();
				expect(myAccount.isLoaded()).toBe(true);
				done();		
			});
		});
	}
	//});
	
	describe('Total Wireless Mixed Lines Activation - Activate Secondary Lines', function() {
		for(var lineNumber = 2;lineNumber <= sessionData.totalwireless.noOfLines;lineNumber++)
		{
			if(lineNumber == 2)
			{
				console.log("line two"+lineTypeTwo);
				activationLineType = lineTypeTwo;
				esnPartNumber = sessionData.totalwireless.esnPartNumberTwo;
				simPartNumber = sessionData.totalwireless.simPartNumberTwo;
			}
			else if(lineNumber == 3)
			{
				activationLineType = lineTypeThree;
				esnPartNumber = sessionData.totalwireless.esnPartNumberThree;
				simPartNumber = sessionData.totalwireless.simPartNumberThree;
			}
			else if(lineNumber == 4)
			{
				activationLineType = lineTypeFour;
				esnPartNumber = sessionData.totalwireless.esnPartNumberFour;
				simPartNumber = sessionData.totalwireless.simPartNumberFour;
			}
		}
	});
});