//This spec file is for Simple Mobile BYOP activation. 
//User has to choose device type "Bring your own phone"  
//Provide BYOP SIM, security PIN, zipcode, AT Purchase to activate the BYOP.

'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var ElementsUtil = require("../../util/element.util");
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");

var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};


describe('BYOP activation with Purchase', function() {
	//to click activate link in the menu and check whether the activation page is loaded 
	it('Go to BYOP activation page', function(done) {
		homePage.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});
	
	//to choose the device type as "Bring your own phone" and check whether the ESN page is loaded
	it('Choose device type BYOP',  function(done) {
		activation.selectByopDeviceType();
		activation.acceptTermsConditions();
		expect(activation.isByopSIMPage()).toBe(true);	
		done();
	});
	
	//to provide the ESN
	it('Enter ESN',  function(done) {
		var esnVal = DataUtil.getCdmaByopESN(); //sessionData.totalwireless.esnPartNumber, sessionData.totalwireless.simPartNumber
		var simVal = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
		console.log("esnVal::: "+esnVal);
		console.log("simVal::: "+simVal);
		
		sessionData.totalwireless.esn = esnVal;	
		activation.enterByopESN(esnVal);
		sessionData.totalwireless.sim = simVal;	
		
		var transactionPendingMessage = element(by.css('[title="Transaction is pending for this device"]'));
		//ElementsUtil.waitForElement(transactionPendingMessage,200000);
		
		var EC = protractor.ExpectedConditions;
		browser.wait(EC.elementToBeClickable(transactionPendingMessage), 200000);
		
		transactionPendingMessage.isPresent().then(function(isVisible){
			if(isVisible)
			{
				DataUtil.updateCdmaByopESN(sessionData.totalwireless.esn,simVal,sessionData.totalwireless.isTrio,sessionData.totalwireless.isLTE,
						sessionData.totalwireless.carrier,sessionData.totalwireless.phoneType,sessionData.totalwireless.phoneModel,sessionData.totalwireless.isHD);
			}
		});
		
		activation.enterByopESN(sessionData.totalwireless.esn); 
		generatedEsn['esn'] = sessionData.totalwireless.esn;		
		expect(activation.isCdmaByopSimLoaded()).toBe(true);
		//expect(activation.isActiveWithCarrierLoaded()).toBe(true);
		done();
	},200000).result.data = generatedEsn;
	
	it('Enter Sim',  function(done) {
		console.log("sim::: "+sessionData.totalwireless.sim);
		activation.enterCdmaByopSim(sessionData.totalwireless.sim);		
		generatedSim['sim'] = sessionData.totalwireless.sim;
		expect(activation.isActiveWithCarrierLoaded()).toBe(true);
		done();
	}).result.data = generatedSim;
	
	
	it('select not active',  function(done) {
		activation.selectNotActive();
		expect(activation.zipcodeDivLoaded()).toBe(true);
		done();
	});
	
	it('enter the zipcode  navigate to airtimeserviceplan page', function(done) {
		//activation.chooseDontKeepMyNumber();
		activation.enterZipCodeTabView(sessionData.totalwireless.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.totalwireless.shopPlan;
		var planName		= sessionData.totalwireless.planName;
		var isAutoRefill	= sessionData.totalwireless.autoRefill;
		var phoneType	    = sessionData.totalwireless.phoneType;
		console.log("planType,planName,isAutoRefill,phoneType :: "+planType+planName+isAutoRefill+phoneType)
		shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
		shop.doAutoReUpPopupSelection(isAutoRefill);
		console.log("SELECTED!!!");
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		//expect(shop.isAddNowWarningPageLoaded()).toBe(true);
		done();
	});
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Checkout Page will be shown
	 */
	it('should load the checkout page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		//myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType), true);
		
		//enter cc details
		myAccount.enterCcNumbers(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
		
		//enter expiry date details
		var moreServicesLabel = element(by.css('[aria-label="more services"]')).element(by.className('selectize-input'));
		moreServicesLabel.isPresent().then(function(isVisible){
			if(isVisible)
			{
				//console.log("visible");
				myAccount.enterCcExpiryDateIndexOne();
			}
			else
			{
				//console.log("not visible");
				myAccount.enterCcExpiryDateIndexTwo();
			}
		});
		
		//enter billing details	
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click place my order button
	 *Expected result - Summary Page will be shown
	 */	
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	
});
	

