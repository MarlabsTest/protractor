//This spec file is for Simple Mobile BYOP activation. 
//User has to choose device type "Bring your own phone"  
//Provide BYOP SIM, security PIN, zipcode, AT PIN to activate the BYOP.

'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var ElementsUtil = require("../../util/element.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};
var generatedPin ={};


describe('BYOP activation', function() {
	//to click activate link in the menu and check whether the activation page is loaded 
	it('Go to BYOP activation page', function(done) {
		homePage.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});
	
	//to choose the device type as "Bring your own phone" and check whether the ESN page is loaded
	it('Choose device type BYOP',  function(done) {
		activation.selectByopDeviceType();
		activation.acceptTermsConditions();
		expect(activation.isByopSIMPage()).toBe(true);	
		done();
	});
	
	//to provide the ESN
	it('Enter ESN',  function(done) {
		var esnVal = DataUtil.getCdmaByopESN(); //sessionData.totalwireless.esnPartNumber, sessionData.totalwireless.simPartNumber
		var simVal = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
		console.log("esnVal::: "+esnVal);
		console.log("simVal::: "+simVal);
		
		sessionData.totalwireless.esn = esnVal;	
		activation.enterByopESN(esnVal);
		sessionData.totalwireless.sim = simVal;	
		
		var transactionPendingMessage = element(by.css('[title="Transaction is pending for this device"]'));
		//ElementsUtil.waitForElement(transactionPendingMessage,200000);
		
		var EC = protractor.ExpectedConditions;
		browser.wait(EC.elementToBeClickable(transactionPendingMessage), 200000);
		
		transactionPendingMessage.isPresent().then(function(isVisible){
			if(isVisible)
			{
				DataUtil.updateCdmaByopESN(sessionData.totalwireless.esn,simVal,sessionData.totalwireless.isTrio,sessionData.totalwireless.isLTE,
						sessionData.totalwireless.carrier,sessionData.totalwireless.phoneType,sessionData.totalwireless.phoneModel,sessionData.totalwireless.isHD);
			}
		});
		
		activation.enterByopESN(sessionData.totalwireless.esn); 
		generatedEsn['esn'] = sessionData.totalwireless.esn;		
		expect(activation.isCdmaByopSimLoaded()).toBe(true);
		//expect(activation.isActiveWithCarrierLoaded()).toBe(true);
		done();
	},200000).result.data = generatedEsn;
	
	it('Enter Sim',  function(done) {
		console.log("sim::: "+sessionData.totalwireless.sim);
		activation.enterCdmaByopSim(sessionData.totalwireless.sim);		
		generatedSim['sim'] = sessionData.totalwireless.sim;
		expect(activation.isActiveWithCarrierLoaded()).toBe(true);
		done();
	}).result.data = generatedSim;
	
	
	it('select not active',  function(done) {
		activation.selectNotActive();
		expect(activation.zipcodeDivLoaded()).toBe(true);
		done();
	});
	
	it('enter the zipcode  navigate to airtimeserviceplan page', function(done) {
		//activation.chooseDontKeepMyNumber();
		activation.enterZipCodeTabView(sessionData.totalwireless.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it(' enter airtime pin navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
		activation.enterAirTimePin(pinval);
		sessionData.totalwireless.pin = pinval;
		generatedPin['pin'] = sessionData.totalwireless.pin;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	it('select create account option', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.fbButtonLoaded()).toBe(true);
		done();		
	}); 
	
	it('enter account creation details and create the account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load the final instruction page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.pinCheckoutPageLoaded()).toBe(true);
		done();		
	});
	
	it('load the pin checkout page', function(done) {
		activation.pinCheckoutProceed();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('continue from final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});	
	
	
	//click "No thanks" button and check whether the dashboard page loaded
	it('Click "No thanks" button in survey page', function(done) {
		activation.clickOnSummaryBtn();
		DataUtil.activateESN(sessionData.totalwireless.esn);
		sessionData.totalwireless.upgradeEsn = sessionData.totalwireless.esn; //for upgrade
		var min = DataUtil.getMinOfESN(sessionData.totalwireless.esn);
		DataUtil.checkActivation(sessionData.totalwireless.esn,sessionData.totalwireless.pin,'1','TOTAL_WIRELESS_ACTIVATION_WITH_PIN');
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
});
	

