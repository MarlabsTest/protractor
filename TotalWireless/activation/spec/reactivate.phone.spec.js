/*
 * **********Spec file is for REACTIVATING A DEVICE ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.Once the device get deactivated,it will be listed under inactive devices in myaccount dashboard.
 * 4.Activate the device by clicking on the activate button in dashboard (Reactivation)
 * 5.we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 6.After doing all these,we are done with the reactivation process. 
 * 
 */

'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};
var generatedPin ={};

describe('Total Wireless Phone ReActivation', function() {	
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should deactivate the device', function(done) {	
        var min = DataUtil.getMinOfESN(sessionData.totalwireless.esn);
        console.log('minVal ,    ::'+min);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.totalwireless.esn,min,'PASTDUE','TOTAL_WEB');	
        console.log('Deactivated    ::');
		generatedMin['min'] = min;
		//home.homePageLoad();
		 console.log('Home pageloaded    ::');
		//home.myAccount();
		 browser.refresh();
		console.log('Myaccount loaded ::');
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}).result.data = generatedMin;	  
	
	/*click on the activate button in the dashboard for the deactivated device
	 *Expected result - redirected to the page where we need to enter the zipcode.  
	 */
	
	it('should navigate to refillpage', function(done) {	
		myAccount.clickRefillNow();
		expect(activation.servicePlanPageLoaded()).toBe(true);	
		done();
	});
	it('should enter pin and navigate to final instruction page', function(done) {	
		var pinval = DataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
       	console.log('returns Pin value', pinval);
		generatedPin['pin'] = sessionData.totalwireless.pin;
		activation.enterAirTimePin(pinval);
		expect(activation.finalInstructionPageLoaded()).toBe(true);	
		done();
	}).result.data = generatedPin;
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be loaded
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the MyAccount dashboard', function(done) {
		activation.clickOnSummaryBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
