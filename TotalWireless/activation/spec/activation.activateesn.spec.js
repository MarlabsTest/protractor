'use strict';

var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};

describe('Total Wireless Activation ESN', function() {

	it('MIN Updation', function(done) {
		//console.log("before wait");
		browser.sleep(30000);
		//console.log("after wait");
		console.log("result from cleartnumber==="+DataUtil.activateESN(sessionData.totalwireless.esn));
		console.log("update table with esn :"+sessionData.totalwireless.esn+":");
		browser.sleep(30000);
		var min = DataUtil.getMinOfESN(sessionData.totalwireless.esn);
		generatedMin['min'] = min;
		console.log("MIN is login :::::" +min);
		//check_activation		
		DataUtil.checkActivation(sessionData.totalwireless.esn,sessionData.totalwireless.pin,'1','TOTAL_WIRELESS_ACTIVATION_WITH_PIN');
		console.log("ITQ table updated");
		console.log("MIN2 is login :::::" +min);
		done();
	}).result.data = generatedMin;
	
});
