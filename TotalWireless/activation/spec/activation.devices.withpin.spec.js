/*
 * **********Scenario is the consolidation of Upgrade  flows in TracFone ******** 
 * 
 * 1.Read the input data for activatePin from CSV
 * 2.Based on the deviceType ,decide which flow to proceed with.
 * 3. ESN gets upgraded with the new ESN with the same MIN number.
 */

'use strict';

var sessionData = require("../../common/sessiondata.do");
var CommonUtil =  require('../../util/common.functions.util');
var FlowUtil = require('../../util/flow.util');

describe('Total Wireless activation' , function() {
	if(sessionData.totalwireless.deviceType == "phone"){
		FlowUtil.run('TW_ACTIVATION_WITHPIN_WITHACCOUNT');				
	}else if(sessionData.totalwireless.deviceType == "homePhone"){
		FlowUtil.run('TW_ACTIVATION_HOMEPHONE_WITHPIN_WITHACCOUNT');		
	}else if(sessionData.totalwireless.deviceType == "byopPhone"){
		FlowUtil.run('TW_BYOP_ACTIVATION_WITHPIN_WITHACCOUNT');
	}else{
		FlowUtil.run('TW_ACTIVATION_HOTSPOT_WITHPIN_WITHACCOUNT');
	}
});
