/*
 * **********Scenario is for ADD SERVICE PLAN INSIDE ACCOUNT ******** 
 * 
 * 1.Do a normal activation flow with PIN using new account
 * 2.In the my account dashboard,select the option add service plan
 * 3.Enter a new service pin 
 * 4.After that we will be redirected to a confirmation page and finally to the my account dashboard
 */
'use strict';
var myAccount = require("../../MyAccount/myAccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedPin ={};

describe('Total Wireless add service pin through account', function() {
 

	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	/*Click on the service plan link inside the All Options
	 *Expected result - Redirected to the page to enter a new service pin  
	 */
	it('should navigate to renew service page', function(done) {
		myAccount.clickOnAllOptions();
		myAccount.clickOnservicePlanLink();
		expect(myAccount.renewServicePageLoaded()).toBe(true);
		done();		
	});
	
	/*Enter service pin
	 *Expected result - Redirected to confirmation page  
	 */
	it('should enter navigate to redemption confirmation page', function(done) {
		var pinval = dataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
       	console.log('returns', pinval);		
		sessionData.totalwireless.pin = pinval;
		generatedPin['pin'] = pinval;
		myAccount.enterAirtimePinRenew(pinval);
		expect(myAccount.redemptionSuccessPage()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	/*Continue from the confiramtion page
	 *Expected result - Redirected to my account dashboard page  
	 */
	it('should enter click on the proceed button and will be navigated to dashoard page', function(done) {
		myAccount.clickOnContinueBtnFromConfirmation();
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(sessionData.totalwireless.esn,sessionData.totalwireless.pin,'401','TOTAL_WIRELESS_Redeem_Pin','false','true');
		console.log('ITQ_DQ_CHECK table updated!!');
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
