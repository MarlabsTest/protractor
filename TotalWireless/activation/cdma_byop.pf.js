//this represents a modal for the ESN page

'use strict';
var ElementsUtil = require("../util/element.util");


var cdmaByop = function() {

	this.verizon = element.all(by.id('btn_selectvzw')).get(1);
	this.notActive = element(by.id('currently-not-active'));
	this.carrierText = element.all(by.css("span[translate='ACRE_6743']")).get(0);
	
    this.isCarrierLoaded = function(){
    	return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /coveragecheck/.test(url);
		});
    }
    
    this.selectVerizon = function(){
    	this.verizon.click();
    }
    
    this.isByopEsnLoaded = function(){
    	return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /VZW/.test(url);
    	});
    }
    
    this.selectNotActive = function(){
    	ElementsUtil.waitForElement(this.notActive);
    	this.notActive.click();
    }
    
   this.isActiveWithCarrierLoaded = function(){
	  // ElementsUtil.waitForElement(this.carrierText);
	  // return this.carrierText.isPresent();
	   ElementsUtil.waitForElement(this.notActive);
	   return this.notActive.isPresent();
   }
    
   
};
module.exports = new cdmaByop;