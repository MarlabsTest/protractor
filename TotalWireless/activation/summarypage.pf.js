//this represnts a modal for the summary page

'use strict';
var ElementsUtil = require("../util/element.util");

var summaryPge = function() {
	
	
	this.confirmationMessageText = element(by.xpath('/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div[1]/div/div/div[2]'));
	this.summaryBtn = element(by.id('done_confirmation'));//element.all(by.id('btn_done')).get(1);
	//this.summaryBtnPurchase =  element(by.id('done_confirmation'));//.get(0);//element.all(by.css('[ng-click="action()"]')).get(1);
	this.summaryBtnPurchase =  element.all(by.css('[ng-click="action()"]')).get(1);
	this.doneBtn = element.all(by.css("button[id='btn_done']")).get(1);
 
 
	//** this method is to check whether the summary page is loaded or not
	this.summaryPageLoaded  = function(){		
		ElementsUtil.waitForElement(this.summaryBtn);
		return this.summaryBtn.isPresent();		
	};
  
	//** this method is to proceed from the summary page
	this.clickOnSummaryBtn = function(){		
		//ElementsUtil.waitForElement(this.summaryBtn);
		this.summaryBtn.click();		
	};
	
	this.summaryPageLoadedPurchase  = function(){
	//	browser.wait(expectedConditions.visibilityOf(this.summaryBtnPurchase),4000);
		ElementsUtil.waitForElement(this.summaryBtnPurchase);
		return this.summaryBtnPurchase.isPresent();
	};
  
	this.clickOnSummaryBtnPurchase = function(){
		//browser.wait(expectedConditions.visibilityOf(this.summaryBtnPurchase),4000);
		browser.executeScript("arguments[0].click();", this.summaryBtnPurchase.getWebElement());
		//this.summaryBtnPurchase.click();
	};
	
	this.clickOnRedemptionDoneBtn = function(){		
		//ElementsUtil.waitForElement(this.summaryBtn);
		this.doneBtn.click();		
	};
	
	this.reactivationSummaryPageLoaded  = function(){		
		//$$//ElementsUtil.waitForElement(this.doneBtn);
		return this.doneBtn.isPresent();		
	};
	
};
module.exports = new summaryPge;