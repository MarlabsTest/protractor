//this represnts a modal for the survey page

'use strict';
var ElementsUtil = require("../util/element.util");

var surveyPge = function() {

	this.thankYouBtn =  element(by.css('[ng-click="action()"]'));//.get(0);//1

	//** this method is to check whether the survey page is loaded or not
	this.surveyPageLoaded = function(){		
		ElementsUtil.waitForElement(this.thankYouBtn);
		return this.thankYouBtn.isPresent();
//		return element.all(by.partialButtonText('NO, THANKS'))
//        .filter(element => {
//          return element.isDisplayed();
//        })
//        .isPresent();
//		
	};
  
	//** this method is to proceed from the survey page using the THANK YOU option
	this.clickOnThankYouBtn = function(){
		return element.all(by.partialButtonText('no, thanks'))
        .filter(element => {
          return element.isDisplayed();
        })
        .click();


	};

 };
module.exports = new surveyPge;