//this represents a modal for servicePlan page
'use strict';

var ElementsUtil = require("../util/element.util");

var purchasePlan = function() {
	
	this.plan =  element.all(by.css('[ng-click="action()"]')).get(1);
	
	//** method to check whether the service plan page is loaded or not
	this.selectServicePlanPageLoaded = function(){
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /serviceplan/.test(url);
		});
	};
  
	//** this method is to select a plan
	this.pickPlan = function(planName){
		//browser.wait(expectedConditions.visibilityOf(this.plan),10000);
		//ElementsUtil.waitForElement(this.plan);
		var chosenPlanBtn = element(by.css("button[title*='"+planName+"']"));
		chosenPlanBtn.click();
	};
  
};

module.exports = new purchasePlan;

