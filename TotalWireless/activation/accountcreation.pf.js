//**this represents a modal for account creation

'use strict';

var ElementUtil = require("../util/element.util");

var CreateAccount = function() {

	this.fbButtonText = element.all(by.id('btn_fbsignup')).get(0);
	this.emailTextBox= element.all(by.name('createacfm.user_email')).get(1);
	this.passwordBox=element.all(by.name('createacfm-user_pwd')).get(1);
	this.DOBBox=element.all(by.name('createacfm-DOB')).get(1);
	this.confirmPswdBox=element.all(by.name('createacfm-confirmuser_pwd')).get(1);
	this.secPinBox=element.all(by.id('createacfm-user_pin')).get(1);
	this.accountCreatedPopUp = element(by.xpath('//*[@id="modal-body"]/div[2]/form/div/div[1]/div[2]'));
	this.accountCreatedPopupContinue = element.all(by.id('btn_continue11'));//.get(2);
	this.actualAccoutnCreatedPopup = this.accountCreatedPopupContinue.get(0);//1
	this.actCreationBtn = element.all(by.id('btn_createaccount'));//.get(3)
	this.accountCreationContinueBtn = element.all(by.css('[ng-click="action()"]'));//.get(3);
	this.actualAccountCreationContinueBtn = this.actCreationBtn.get(0);
	this.newAccountContinueBtn = element.all(by.id('btn_createnew'));
	this.updateAccountContinueBtn = element.all(by.id('btn_createnew'));
	this.actualAccountCreated = element.all(by.id('btn_createnew')).get(0);//this.newAccountContinueBtn.get(0);
	this.skipAccountLink = element(by.css("[ng-click='skipLogin()']"));
	
	this.updateEmailTextBox = element.all(by.id("updateacfm-user_email")).get(1);
	this.updatePasswordBox = element.all(by.id("updateacfm-user_pwd")).get(1);
	this.updateConfirmPswdBox = element.all(by.id("updateacfm-confirmuser_pwd")).get(1);
	this.updateDOBBox = element.all(by.id("updateacfm-DOB")).get(1);
	this.updateSecPinBox = element.all(by.id("updateacfm-user_pin")).get(1);
	
	this.accountUpdatedPopupContinue = element.all(by.id('btn_myaccount'));//.get(2);
	this.actualAccountUpdatedPopupBtn = this.accountUpdatedPopupContinue.get(0);//1
	
	this.skipAccountLogin = function() {
	return this.skipAccountLink.click();
	};
	
	//** this method checks whether the account creation page is loaded or not
	this.activationAccountPageLoaded = function(){  
		return ElementUtil.waitForUrlToChangeTo(/activation!airtimeserviceplan$/);
	};
  
	//** this method is to proceed with account creation flow
	this.clickonAccountCreationContinueBtn = function(){
//		browser.executeScript("arguments[0].click();", this.actualAccountCreationContinueBtn.getWebElement());			
		this.actualAccountCreationContinueBtn.click();
		
	};
	
	this.isUpdateAccountPageLoaded = function() {
		//ElementUtil.waitForElement(this.updateAccountContinueBtn);
		return this.updateAccountContinueBtn.isPresent();
	};
	
	this.updateAccountDetails = function(email,password,DOB,pin) {
		this.updateEmailTextBox.clear().sendKeys(email);
		this.updatePasswordBox.clear().sendKeys(password);
		this.updateConfirmPswdBox.clear().sendKeys(password);
		this.updateDOBBox.clear().sendKeys(DOB);
		this.updateSecPinBox.clear().sendKeys(pin);
		
		this.updateAccountContinueBtn.click();
	};
	
	//** this method checks whether the account details fields to be enetered  are loaded or not
	this.emailTextBoxLoaded = function(){
		return this.emailTextBox.isPresent();
	};
	
	//** this method checks whether the account details fields to be enetered  are loaded or not
	this.fbButtonLoaded = function(){
		return this.fbButtonText.isPresent();
	};
	
	//** this method is to enter the required fields to create new account
	this.enterAccountDetails= function(email,password,DOB,pin){
		//ElementUtil.waitForElement(this.emailTextBox);
		this.emailTextBox.clear().sendKeys(email);
		//ElementUtil.waitForElement(this.passwordBox);
		this.passwordBox.clear().sendKeys(password);
		//ElementUtil.waitForElement(this.confirmPswdBox);
		this.confirmPswdBox.clear().sendKeys(password);
		//ElementUtil.waitForElement(this.DOBBox);
		this.DOBBox.clear().sendKeys(DOB);
		//ElementUtil.waitForElement(this.secPinBox);
		this.secPinBox.clear().sendKeys(pin);
		//this.actualAccountCreated.click();
		
		browser.executeScript("arguments[0].click();", this.actualAccountCreated.getWebElement());
		
	};
	
	//** this method is to check whether the popup page is loaded after successful account creation
	this.accountCreationDone = function(){
		ElementUtil.elementHasCome(this.accountCreatedPopupContinue, 0);
		return this.actualAccoutnCreatedPopup.isPresent();
	};
	
	//** this method is to check whether the popup page is loaded after successful account updation
	this.accountUpdationDone = function(){
		ElementUtil.elementHasCome(this.accountUpdatedPopupContinue, 0);
		return this.actualAccountUpdatedPopupBtn.isPresent();
	};
	
	//** this method is to proceed from the popup page
	this.clickOnAccountCreatedPopupBtn = function(){
	//browser.wait(Waitutil.elementHasCome(this.accountCreatedPopupContinue, 2), 20000);
		ElementUtil.elementHasCome(this.accountCreatedPopupContinue, 0);
		this.actualAccoutnCreatedPopup.click();
		
	};
	
	//** this method is to proceed from the account updated popup page
	this.clickOnAccountUpdatedPopupBtn = function(){
		this.actualAccountUpdatedPopupBtn.click();
	};
};
module.exports = new CreateAccount;