/*
 * **********Scenario is the consolidation of Upgrade  flows in Total Wireless ******** 
 * 
 * 1.Read the input data for upgrade from CSV
 * 2.Based on the partnumber decide which flow to proceed with.
 * 3.If the partnumber starts with 'PH' ,it will be a  BYOP upgrade.In that we are initially doing a BYOP activation process
 * 4.The third upgrade flow is the normal Total Wireless phone upgrade.  We will be doing activation with PIN flow.
 * 5.In all the above cases,After activating the device, will get the MIN number.
 * 6.After getting MIN, It's executes an phone upgrade spec with new part numbers and the same MIN.Finally, the older ESN gets upgraded with the new ESN with the same MIN number.
 */
'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var phoneupgradeUtil = require('../../util/phoneupgrade.util');
var sessionData = require("../../common/sessiondata.do");

describe('TW Upgrade Flow', function() {
	var phoneupgradeData = phoneupgradeUtil.getTestData();
	console.log('phoneupgradeData:', phoneupgradeData);
	console.log('protractor.basePath ', protractor.basePath);
	
	drive(phoneupgradeData, function(inputPhoneUpgradeData) {
		sessionData.totalwireless.esnPartNumber = inputPhoneUpgradeData.FromPartNumber;
		console.log('sessionData.totalwireless.esnPartNumber: ',sessionData.totalwireless.esnPartNumber );
		sessionData.totalwireless.arFlag = inputPhoneUpgradeData.AR;
		console.log('sessionData.totalwireless.arFlag::',sessionData.totalwireless.arFlag);
		sessionData.totalwireless.phoneStatus = inputPhoneUpgradeData.PhoneStatus;
		describe('Drive Spec', function() {
			it('Copying phone upgrade test data to session', function(done) {
				sessionData.totalwireless.simPartNumber = inputPhoneUpgradeData.FromSIM;
				sessionData.totalwireless.zip = inputPhoneUpgradeData.FromZipCode;
				sessionData.totalwireless.pinPartNumber = inputPhoneUpgradeData.FromPIN;
				/*sessionData.totalwireless.toEsnPartNumber = inputPhoneUpgradeData.ToPartNumber;
				sessionData.totalwireless.toSimPartNumber = inputPhoneUpgradeData.ToSIM;
				sessionData.totalwireless.toPinPartNumber = inputPhoneUpgradeData.ToPIN;*/
				console.log('Copying upgrade', inputPhoneUpgradeData);
				done();
			
		});
			FlowUtil.run('TW_UPGRADE_FLOWS');
		}).result.data = inputPhoneUpgradeData;
	});
});