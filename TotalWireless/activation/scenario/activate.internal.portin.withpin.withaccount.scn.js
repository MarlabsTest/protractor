'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/porting.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Internal Port In with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		sessionData.totalwireless.noOfLines = inputActivationData.noOfLines;
		sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
		sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
		sessionData.totalwireless.oldPartNumber = inputActivationData.OldPart;
		
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('TW_INTERNAL_PORTIN_WITHPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});