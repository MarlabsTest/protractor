'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activationdevicespin.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Home Phone Activation with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		sessionData.totalwireless.deviceType = inputActivationData.deviceType;
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				done();
			});
			FlowUtil.run('NT_ACTIVATION_DEVICES_WITHPIN_FLOWS');
		}).result.data = inputActivationData;
	});
});