'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/byop.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Byop Activation with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				//sessionData.totalwireless.isActive = inputActivationData.isActive;
				sessionData.totalwireless.isLTE = inputActivationData.isLTE;
				sessionData.totalwireless.carrier = inputActivationData.carrier;
				//sessionData.totalwireless.isIPhone = inputActivationData.isIphone;
				sessionData.totalwireless.isHD = inputActivationData.isHD;
				sessionData.totalwireless.isTrio = inputActivationData.isTrio;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.phoneType = inputActivationData.PhoneType;
				sessionData.totalwireless.phoneModel = inputActivationData.PhoneModel;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TW_BYOP_ACTIVATION_WITHPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});