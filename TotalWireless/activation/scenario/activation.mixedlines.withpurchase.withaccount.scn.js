'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.mixedlines.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Mixed Lines Activation with Purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		sessionData.totalwireless.noOfLines = inputActivationData.noOfLines;
		sessionData.totalwireless.esnPartNumber = inputActivationData.EsnPartNumberOne;
		sessionData.totalwireless.simPartNumber = inputActivationData.SimPartNumberOne;
		sessionData.totalwireless.lineTypeOne = inputActivationData.LineTypeOne;
		sessionData.totalwireless.esnPartNumberTwo = inputActivationData.EsnPartNumberTwo;
		sessionData.totalwireless.simPartNumberTwo = inputActivationData.SimPartNumberTwo;
		sessionData.totalwireless.lineTypeTwo = inputActivationData.LineTypeTwo;
		sessionData.totalwireless.esnPartNumberThree = inputActivationData.EsnPartNumberThree;
		sessionData.totalwireless.simPartNumberThree = inputActivationData.SimPartNumberThree;
		sessionData.totalwireless.lineTypeThree = inputActivationData.LineTypeThree;
		sessionData.totalwireless.esnPartNumberFour = inputActivationData.EsnPartNumberFour;
		sessionData.totalwireless.simPartNumberFour = inputActivationData.SimPartNumberFour;
		sessionData.totalwireless.lineTypeFour = inputActivationData.LineTypeFour;
		
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.cardType = inputActivationData.cardType;
				sessionData.totalwireless.autoRefill = inputActivationData.AutoRefill;
				sessionData.totalwireless.planName = inputActivationData.PlanName;
				sessionData.totalwireless.shopPlan = inputActivationData.shopplan;
				done();
			});
			FlowUtil.run('TW_ACTIVATION_MIXEDLINES_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});