/*
 * **********Scenario is for ADD SERVICE PLAN INSIDE ACCOUNT ******** 
 * 
 * 1.Do a normal activation flow with PIN using new account
 * 2.In the my account dashboard,select the option add service plan
 * 3.Enter a new service pin 
 * 4.After that we will be redirected to a confirmation page and finally to the my account dashboard
 */
'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Add Service Plan', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TW_ACTIVATION_ADD_SERVICE_PLAN');
		}).result.data = inputActivationData;
	});
});