'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.hotspot.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Hotspot One Line Activation with Purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumberOne;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.cardType = inputActivationData.cardType;
				sessionData.totalwireless.autoRefill = inputActivationData.AutoRefill;
				sessionData.totalwireless.planName = inputActivationData.PlanName;
				sessionData.totalwireless.shopPlan = inputActivationData.shopplan;
				sessionData.totalwireless.phoneType = inputActivationData.phoneType;
				done();
			});
			FlowUtil.run('TW_ACTIVATION_HOTSPOT_ONELINE_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});