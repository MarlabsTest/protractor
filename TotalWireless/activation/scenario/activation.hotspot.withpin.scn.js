'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.hotspot.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Hotspot Activation with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumberOne;
			sessionData.totalwireless.esnPartNumberTwo = inputActivationData.PartNumberTwo;
			sessionData.totalwireless.esnPartNumberThree = inputActivationData.PartNumberThree;
			sessionData.totalwireless.esnPartNumberFour = inputActivationData.PartNumberFour;
			sessionData.totalwireless.noOfLines = inputActivationData.noOfLines;
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('TW_ACTIVATION_HOTSPOT_WITHPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});