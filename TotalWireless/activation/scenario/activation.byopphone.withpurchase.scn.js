'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/byop.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Byop Activation with purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.planName = inputActivationData.PlanName;
				sessionData.totalwireless.autoRefill = inputActivationData.AutoRefill;
				sessionData.totalwireless.cardType = inputActivationData.cardType;
				sessionData.totalwireless.shopPlan = inputActivationData.shopplan;
				sessionData.totalwireless.phoneType = inputActivationData.PhoneType;
				sessionData.totalwireless.isLTE = inputActivationData.isLTE;
				sessionData.totalwireless.carrier = inputActivationData.carrier;
				sessionData.totalwireless.isHD = inputActivationData.isHD;
				sessionData.totalwireless.isTrio = inputActivationData.isTrio;
				sessionData.totalwireless.phoneModel = inputActivationData.PhoneModel;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TW_BYOP_ACTIVATION_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});