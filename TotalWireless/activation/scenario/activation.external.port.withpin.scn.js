'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/porting.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless External Port Activation with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		sessionData.totalwireless.noOfLines = inputActivationData.noOfLines;
		sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
		sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('TW_EXTERNAL_PORT_WITHPIN');
		}).result.data = inputActivationData;
	});
});