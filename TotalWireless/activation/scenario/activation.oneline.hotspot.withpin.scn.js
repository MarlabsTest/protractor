'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.hotspot.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Hotspot One Line Activation with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumberOne;
				done();
			});
			FlowUtil.run('TW_ACTIVATION_HOTSPOT_ONELINE_WITHPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});