//this represents a modal for the ESN page

'use strict';
var ElementsUtil = require("../util/element.util");

var Esn = function() {

	this.esn = element.all(by.id('tfvalesn-esn')).get(1);
	this.cdmaEsn = element.all(by.id('number')).get(1);
	this.cdmaEsnContinueBtn = element.all(by.css('[ng-click="action()"]')).get(0);
	this.errorText = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/span/span"));
	this.randomEsnError = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div/span"));
	this.esnTextLink = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[2]/subheading/div/div/div[1]/table/tbody/tr[1]/td/h3"));
	this.term = element.all(by.id('term')).get(0);
	this.pinErrorText = element(by.xpath('//*[@id="updateSecPin-pin"]/div/p'));
	
	this.esnContinueBtn = element(by.id('btn_imeicontinue'));
	this.securityPinBox=element.all(by.name('updateSecPin-pin')).get(1);// element.all(By.xpath('//*[@id="updateSecPin-pin"]')).get(1);
    this.pinContinueButton =  element.all(by.css('[ng-click="action()"]')).get(0);//element(by.xpath('//*[@id="modal-body"]/div[2]/form/div[2]/wfm-button-primary/span[1]/button'));

	//**checks whether the esn page is loaded or not
	this.esnPageLoaded=function(){
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /collectesn/.test(url);
		});
	};
	// Change in FLOW	
	//**method to enter the esn
	this.enterEsn=function(esnVal){
		ElementsUtil.waitForElement(this.esn);
		this.esn.clear().sendKeys(esnVal);		
	}; 
	
  
	//** method to click on terms and condition checkbox
	this.checkBoxCheck=function(){
		browser.executeScript("arguments[0].click();", this.term.getWebElement());
	};
	
	//method to proceed from the esn page
	this.continueESNClick=function(){
		this.esnContinueBtn.click();
	};
	
	//method to display error message after entering already activated ESN
	this.errorMessage=function(){
		
		//$$//ElementsUtil.waitForElement(this.errorText);
		return this.errorText.getText();
	};
	
	//method to display error message after entering a random number
	this.randomEsnErrorMsg = function(){
		//browser.wait(expectedConditions.visibilityOf(this.randomEsnError), 10000);
		//$$//ElementsUtil.waitForElement(this.randomEsnError);
		return this.randomEsnError.getText();
	};
  
	//** method to check whether the security pin popup page is loaded
	this.securityPinPopUp=function(){
		//browser.wait(expectedConditions.visibilityOf(this.securityPinBox), 10000);
		//$$//ElementsUtil.waitForElement(this.securityPinBox);
		return this.securityPinBox.isPresent();
	};
  
	//** method to display error message after entering invalid pin
	this.displayPinError = function(){
		//browser.wait(expectedConditions.visibilityOf(this.pinErrorText), 10000);
		//$$//ElementsUtil.waitForElement(this.pinErrorText);
		return this.pinErrorText.getText();
	};
  
	//** method to enter pin 
	this.enterPin = function(pinVal){
		this.securityPinBox.clear().sendKeys(pinVal);
	};
  
	//** method to proceed from security pin popup page
	this.clickOnPinContinue = function(){
		this.pinContinueButton.click();	
	};
	
	this.enterCdmaByopEsn =  function(esnVal){
		this.cdmaEsn.clear().sendKeys(esnVal);
		this.cdmaEsnContinueBtn.click();
		
	};
	
	this.isCdmaByopEsn =  function(esnVal){
		ElementsUtil.waitForElement(this.cdmaEsnContinueBtn);
		this.cdmaEsnContinueBtn.isPresent();
		
	};
  
};
module.exports = new Esn;