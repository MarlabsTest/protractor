//this represnts a modal for the SIM page

'use strict';
var ElementsUtil = require("../util/element.util");

var sim = function() {
	
	this.identifySIMPage = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[2]/subheading/div/div/div[1]/table/tbody/tr[1]/td"));
	this.simErrorField = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div"));
	this.simTextBox = element.all(by.name('sim')).get(1);
	this.cdmaByopSimTextBox = element.all(by.id('simbyop')).get(1);
	this.simContinueBtn = element(by.id('btn_continuesimcardnumber'));
	//this.byopSimTextBox = element.all(by.id('number')).get(1);
	this.byopSimContinueBtn = element.all(by.id('btn_collectsim')).get(0);
	this.byopPurchasePinBtn = element.all(by.css('[ng-click="action()"]')).get(3);
	this.byopSimTextBox = element.all(by.name('sim')).get(1);
	this.popupMessage = element(by.css('modal-content'));
	this.continueFromPopUp = element(by.css("span[translate='BAT_24130']"));
	this.byopPopupMessage = element(by.css('modal-body'));
	this.continueFromByopPopUp = element(by.css("span[translate='ACRE_6936']"));
    this.cdmaByopText = element.all(by.css("span[translate='ACRE_6736']")).get(0);
    this.cdmabyopSimContinueBtn = element.all(by.id('btn_entervzwsim')).get(0);
    this.transactionPendingMessage = element(by.css('[title="Transaction is pending for this device"]'));
	
	this.isCdmaByopSimLoaded = function(){
		//ElementsUtil.waitForElement(this.cdmaByopText);
		//return this.cdmaByopText.isPresent();
		ElementsUtil.waitForElement(this.cdmabyopSimContinueBtn);
		return this.cdmabyopSimContinueBtn.isPresent();
	}
	
	this.isTransactionPendingMessageShown = function(){
		ElementsUtil.waitForElement(this.transactionPendingMessage,200000);
		return this.transactionPendingMessage.isPresent();
	}
	
	this.enterByopSIM = function(simNum){
		this.byopSimTextBox.clear().sendKeys(simNum);
		this.byopSimContinueBtn.click();
	};
	
	this.enterCdmaByopSim = function(sim){
		this.cdmaByopSimTextBox.clear().sendKeys(sim);
		this.cdmabyopSimContinueBtn.click();
		
	}
	
	this.selectByopPurchaseAirTime = function() {
		this.byopPurchasePinBtn.click();
	};
  
	this.isByopSIMPage = function(){
        return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /collectesn/.test(url);
		});
	};
	
	//** method to check whether the SIM page is loaded
	this.isSIMPage = function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /simnumber/.test(url);
		});
	};
	
	//** method to enter the SIM
	this.enterSIM = function(simNum){
		ElementsUtil.waitForElement(this.simTextBox);//for add device scenario
		this.simTextBox.clear().sendKeys(simNum);
		this.simContinueBtn.click();
	};
  
	//** method to display error after entering invalid SIM 
	this.isSIMError = function(){
		//browser.wait(expectedConditions.visibilityOf(this.simErrorField),10000);
		//ElementsUtil.waitForElement(this.simErrorField);
		return this.simErrorField.getText();
	};
	
	//BYOP activation starts -- Aswathy
	this.popUpBox = function(){
		return this.continueFromPopUp.isPresent();
	};
	
	this.continueFromPopUpBox = function(){
		this.continueFromPopUp.click();
	};
	
	this.byopPopUpBox = function(){
		return this.continueFromByopPopUp.isPresent();
	};
	
	this.continueFromByopPopUpBox = function(){
		this.continueFromByopPopUp.click();
	};
	//BYOP activation ends
};
module.exports = new sim;