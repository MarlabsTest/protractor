//this modal represents a modal for whether to go with activate new phone or activate a BYOP

'use strict';

var ElementsUtil = require("../util/element.util");

var Activate = function() {
		
	//this.continueBtn =element.all(by.css('[ng-click="action()"]'));
	//this.actualContinue = this.continueBtn.get(3);
	this.continueBtn = element(by.id("btn_havephone"));
	this.byopBtn = element.all(by.id('btn_havetablet')).get(0);
	this.homePhoneContinueBtn = element.all(by.id("btn_havetablet")).get(1);
	this.hotspotCntButtn = element.all(by.id('btn_havetablet')).get(1);
	this.termsConditionsModal = element(by.id('modal-title'));
	this.termsConditionsAcceptBtn = element.all(by.id('btn_acceptterms&conditions')).get(0);
	this.airtimeBenefitsWarningModal = element(by.css('.modal-dialog.modal-md'));
	this.continueAirtimeBenefitsWarningBtn = element.all(by.id('btn_continue')).get(1);
	//this.continuePhoneUpgradeRequestBtn = element.all(by.id('btn_summaryviewpincontinue')).get(1);
	this.continuePhoneUpgradeRequestBtn = element(by.id('btn_summaryviewpincontinue'));
	this.keepCurrentPhnNumNo= element.all(by.buttonText('NO')).get(1);
	this.homePhoneThankingPopUp = element(by.css('.modal-dialog.modal-md'));
	this.homePhoneThankingPopUpContinueBtn = element.all(by.css('[type="submit"]')).get(0);
	this.enterEsnForHotSpot = element(by.id('tfvalesn-esn'));
	this.skipButton = element.all(by.css('[ng-click="action()"]')).get(2);
	this.selDevOrGroup	= element.all(by.css('[ng-click="$select.toggle($event)"]')).get(0);	
	this.minValue	= element.all(by.className('ng-binding ng-scope')).get(0);
	this.minContinueBtn =  element.all(by.id('btn_continuephonenumber')).get(0);
  
	this.maximiseBrowser = function() {
		browser.driver.manage().window().maximize();
	};
    
	//** this method checks whether the page is loaded or not after the click on activate link from homepage
	this.isActivateLoaded = function() {
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /selectdevice/.test(url);
		});
	};
	
	this.keepCurrentPhnNum =  function() {
		this.keepCurrentPhnNumNo.click();
	
	};
	
	//** this method is to skip the purchase either through PIN or Plan (activation with no pin) 
	this.clickSkipButton =  function() {
		this.skipButton.click();
	
	};
	
	this.isHomePhoneThankingPopUpShown =  function() {
		ElementsUtil.waitForElement(this.homePhoneThankingPopUp);
		return this.homePhoneThankingPopUp.isPresent();
	};
	
	this.continueHomePhoneThankingPopUp =  function() {
		this.homePhoneThankingPopUpContinueBtn.click();
	};
	
	this.isPhoneUpgradeRequestPageLoaded = function() {
		ElementsUtil.waitForElement(this.continuePhoneUpgradeRequestBtn);
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /PIN_FLOW/.test(url);
		});
	};
	
	//check whether the terms and conditions popup shown
	this.isTermsAndConditionsPopUpShown = function() {
		//$$//ElementsUtil.waitForElement(this.termsConditionsModal);
		return this.termsConditionsModal.isPresent();
	}
	
	this.isAirtimeBenefitsWarningShown = function() {
		//$$//ElementsUtil.waitForElement(this.airtimeBenefitsWarningModal);
		return this.airtimeBenefitsWarningModal.isPresent();
	}

	this.selectDeviceTypeBYOP = function() {
		//console.log("clicking byop button");
		this.byopBtn.click();
	};
	
	this.acceptTermsConditions = function() {
		if(this.checkTermsConditionsModalExist())
		{
			this.termsConditionsAcceptBtn.click();
		}
	}
	
	this.continueAirtimeBenefitsWarning = function() {
		this.continueAirtimeBenefitsWarningBtn.click();
	}
	
	this.continuePhoneUpgradeRequestPage = function() {
		this.continuePhoneUpgradeRequestBtn.click();
	}
	
	this.checkTermsConditionsModalExist = function() {
		return this.termsConditionsModal.isPresent();
	}

	//**this method is to proceed with new phone activation flow
	this.gotToEsnPage= function() {
		//ElementsUtil.elementHasCome(this.continueBtn, 3);
		ElementsUtil.waitForElement(this.continueBtn);
		this.continueBtn.click();
	};
	
	//**this method is to proceed with hotspot activation flow
	this.gotToHotSpotEsnPage= function() {
		ElementsUtil.waitForElement(this.hotspotCntButtn);
		this.hotspotCntButtn.click();
	};
	
   
	//**this method is to proceed with new home phone activation flow
	this.goToHomePhoneEsnPage= function() {
		//ElementsUtil.elementHasCome(this.continueBtn, 3);
		ElementsUtil.waitForElement(this.homePhoneContinueBtn);
		this.homePhoneContinueBtn.click();
	};
	this.selectMin= function() {
		//ElementsUtil.waitForElement(this.homePhoneContinueBtn);
		this.selDevOrGroup.click();
		ElementsUtil.waitForElement(this.minValue);
		this.minValue.click();
	};
	this.clickOnMinContinue= function() {		
		this.minContinueBtn.click();
	}; 
	
	//this.waitForElementLoad  = function(min) {
	//	browser.sleep(min);
	//};
  
  };
module.exports = new Activate;