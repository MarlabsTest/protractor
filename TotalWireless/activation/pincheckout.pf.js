//this represents a modal for pin checkout page

'use strict';

var ElementsUtil = require("../util/element.util");

var PinCheckout = function() {

	this.pinCheckoutContinueBtn =  element(by.id('btn_actcontinue'));
	this.pinCheckoutAddLineBtn =  element(by.id('btn_adddevicestage'));
	
	//** this method is to check whether the pin checkout page is loaded or not
	this.pinCheckoutPageLoaded = function(){
		ElementsUtil.waitForElement(this.pinCheckoutContinueBtn);
		return this.pinCheckoutContinueBtn.isPresent();
	};
	
	//** this method is to proceed from the pin checkout page
	this.pinCheckoutProceed = function(){
		this.pinCheckoutContinueBtn.click();
	};
	
	//** this method is to add device from the pin checkout page
	this.pinCheckoutAddDevice = function(){
		this.pinCheckoutAddLineBtn.click();
	};
};
module.exports = new PinCheckout;
