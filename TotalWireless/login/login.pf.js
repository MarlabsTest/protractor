'use strict';
var ElementsUtil = require("../util/element.util"); 

var Login = function() {
	
 	//this.login = element.all(by.id("createac-device")).get(1);
	this.login = element(by.css('input[id="createac-device"]'));
	//this.validateLoginBtn = element.all(by.css("[ng-click='action()']")).get(2);
	//this.validateLoginBtn = element.all(by.id("btn_continue25")).get(0);
 	this.validateLoginBtn = element.all(by.id("btn_login")).get(0);
	//this.password = element.all(by.id("loginpinpwd-device")).get(1);
 	this.password = element(by.css('input[id="loginpinpwd-device"]'));
	this.passwordForMin = element.all(by.model('modelValue')).get(0);
	this.validatePassswordBtnForMin = element(by.css("button[id='btn_login']"));
	this.validatePassswordBtn = element.all(by.css("[ng-click='action()']")).get(12);
	this.forgotPasswordLnk = element(by.id('login_forgotpassword'));
	this.resetMessage = element(by.id("modal-title")); 
	
	this.userNameInputElement = element.all(by.id('loginForm-email')).get(1);
	this.passwordInputElement = element.all(by.id('loginForm-password')).get(1);
	
	this.userNameInput = element.all(by.id('login-email')).get(3);
	this.passwordInput = element.all(by.id('login-password')).get(3);
	this.loginBtn = element.all(by.id('btn_login_existuser')).get(2);
	this.securityPinTextBox = element.all(by.id('secpin-security_pin')).get(1);
	this.securityPinSubmitBtn = element(by.buttonText('submit'));
	
	this.provideUserLogin = function(userName, password) {
		this.userNameInput.clear().sendKeys(userName);
		this.passwordInput.clear().sendKeys(password);
		this.loginBtn.click();
	};
	
	this.validateUserLogin = function(){
		return this.loginBtn.isPresent();
	}
	
	this.resetPassword = function() {
		//$$//ElementsUtil.waitForElement(this.forgotPasswordLnk); 
		this.forgotPasswordLnk.click();
	};
	
	this.isPasswordReset = function() {
		//$$//ElementsUtil.waitForElement(this.resetMessage); 
		return this.resetMessage.getText();
	};

	this.provideLogin = function(login) {
		//$$//ElementsUtil.waitForElement(this.login); 
		this.login.clear().sendKeys(login);
	};
    
	this.validateLogin = function(login) {
		this.provideLogin(login);
		//$$//ElementsUtil.waitForElement(this.validateLoginBtn); 
		this.validateLoginBtn.click();
	};
  
	this.providePassword = function(password) {
		//$$//ElementsUtil.waitForElement(this.password); 
		this.password.clear().sendKeys(password);
	};
    
	this.validatePassswordForMin = function(password) {
		//$$//ElementsUtil.waitForElement(this.passwordForMin); 
		this.passwordForMin.clear().sendKeys(password);  
		//$$//ElementsUtil.waitForElement(this.validatePassswordBtnForMin); 
		this.validatePassswordBtnForMin.click();
	};
	
	this.validatePasssword = function(password) {
		//$$//ElementsUtil.waitForElement(this.password); 
		this.password.clear().sendKeys(password);
		//$$//ElementsUtil.waitForElement(this.validatePassswordBtn); 
		this.validateLoginBtn.click();
	};
	
	this.isSignOut = function() {
		return ElementsUtil.waitForUrlToChangeTo(/$/);
	};
	
	this.isValidLogin = function() {
		return ElementsUtil.waitForUrlToChangeTo(/collectpassword$/);
		//return this.validateLoginBtn.isPresent();
	};
	
	this.isCollectSecurityPinPage = function() {
		ElementsUtil.waitForElement(this.securityPinTextBox);
		return this.securityPinTextBox.isPresent();
	};
	
	this.enterSecurityPin = function(securityPin) {
		this.securityPinTextBox.clear().sendKeys(securityPin);
		this.securityPinSubmitBtn.click();
	};
	
	//provides the password and the username to login
	this.provideUserLoginInfo = function(userName, password) {
		this.userNameInputElement.clear().sendKeys(userName);
		this.passwordInputElement.clear().sendKeys(password);
		this.validateLoginBtn.click();
	};
	//navigates to the dashboard
	this.isValidPassword = function() {
		return ElementsUtil.waitForUrlToChangeTo(/dashboard$/);
	};
};

module.exports = new Login;
