'use strict';

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('../TotalWireless/properties/Protractor.properties');
var homePage = require("../../common/homepage.po");
var Constants = require("../../util/constants.util");

/*
 * This Spec is to Load a Total Wireless Home page.
 */

describe('Total Wireless HomePage', function() {

	it('should load the Total Wireless home page', function(done) {
		//homePage.load(Constants.ENV[browser.params.environment]);
		homePage.load(properties.get('weburl.'+browser.params.environment));
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();
	});

});
