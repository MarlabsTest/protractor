'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

/*
 * Scenario : Total Wireless Login with Email
 * Details	: This scenario first read an test data from JSON file.
 * 			  Using part numbers mentioned in JSON it's execute an activation spec file and 
 * 			  then activate that ESN to get MIN for the same.Also,username password stored on session object.
 * 			  Then it executes an myaccount login spec with the same username/password to test login functionality.
 * */
describe('Total Wireless Login With Email', function() {
	
	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TW_LOGIN_WITH_EMAIL');
		}).result.data = inputActivationData;
	});
		
});
	