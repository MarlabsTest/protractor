'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

/*
 * Scenario : NT Login with MIN
 * Details	: This scenario first read an test data from JSON file.
 * 			  Using part numbers mentioned in JSON it's execute an activation spec file and 
 * 			  then activate that ESN to get MIN for the same.Also,MIN, password stored on session object.
 * 			  Then it executes an myaccount login with MIN spec with the same MIN/password to test login functionality.
 * */
describe('NT Login With Min', function() {
	
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				done();
			});
			FlowUtil.run('TW_LOGIN_WITH_MIN');
		}).result.data = inputActivationData;
	});
		
});
	
