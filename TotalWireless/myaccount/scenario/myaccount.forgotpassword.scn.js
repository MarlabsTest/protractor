'use strict';
//forgot password scenario, the link will be sent to the given mail for resetting the password
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Total Wireless Forgot Password', function() {
	
	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TW_FORGOT_PASSWORD');
		}).result.data = inputActivationData;
	});
		
});
	
