'use strict';
var ElementsUtil = require("../util/element.util");

//=============Serves all functions inside MyAccount============

var MyAccount = function() {
	

	this.manageProfile = element.all(by.id('Manageprofile')).get(1);
	this.paymentMethod = element.all(by.id('paymentmethod')).get(1);
	this.signOut = element.all(by.css("a[id='Signoutnav']")).get(2);
	this.addDevice = element.all(by.id('Adddevicenav')).get(0);
	this.addDevicePopupBtn = element.all(by.id('btn_adddevice1')).get(1);
	this.activateButton = element(by.css("span[translate='MYAC_99867']"));
	
	this.selectDevice 				= element.all(by.css('[ng-model="$select.search"]')).get(0);
	this.selectActiveMin			= element.all(by.className('dropdown-content-item ng-scope')).get(0);
	this.btnContinuePhonenumber		= element.all(by.id('btn_continuephonenumber')).get(0);
	this.addPlan                    = element.all(by.id('refill')).get(0);
	this.addPlanButton = element(by.id('AddAirtime_btn'));	
	this.eneterPin = element.all(by.id('formPlan.pin')).get(1);
	this.gotoMyAccountLink = element.all(by.id('lnk_tfhome_myaccount')).get(1);
	this.loginInput = element.all(by.css("input[id='createac-device']")).get(2);
	this.loginButton = element.all(by.css("button[id='btn_login_home']")).get(2);
	
	this.passwordInput = element.all(by.id('loginpinpwd-device')).get(1);
	this.loginButton2 = element(by.id('btn_login'));	
	this.buyAddOnBttn = element(by.css("span[translate='MYACC_5015']"));
	
	this.ILD10$Tab = element(by.id('accordiongroup-4855-9560-tab'));
	
	this.addOn10ILD = element(by.id('monthly_500'));
	//TW Phone upgrade
	this.showLink	= element.all(by.css("span[translate='MYACC_5397']")).get(1); 
	this.optionsLink 	= element(by.css("span[translate='MYACC_5405']"));
	this.upgradeDeviceLink 	=  element(by.id('upgrade_device'));
	this.refillNow = element(by.id('activate'));
	this.ILDLearnMore = element(by.id('lnk_shop_globalcalling'));
	this.ILDBuyNow = element(by.id('lnk_ild_buy_now'));
	this.ILDEnroll = element(by.css("span[translate='BAT_1074']"));
	this.deviceInfo = element.all(by.id('device-info')).get(1); 
	this.deviceContinue = element(by.id('saveDeviceSubmit_btn'));
	this.mangeILDEnrollment = element(by.id('manage10global'));
	
	
	this.ManageILDEnrollment = function(){
		return this.mangeILDEnrollment.click();
		
	}
	
	this.clickbuyILD = function(min){
		this.ILDLearnMore.click();
		browser.getAllWindowHandles().then(function (handles) {
		    var secondWindowHandle = handles[1];
		    browser.switchTo().window(secondWindowHandle);
		    });
		this.ILDBuyNow.click();
		this.deviceInfo.sendKeys(min)
		this.deviceContinue.click();
		return this.ILDEnroll.click();
	}
	
	this.select10ILD = function() {
		ElementsUtil.waitForElement(this.addOn10ILD);
		return this.addOn10ILD.click();
	};
	
	this.clickOnILD10$Tab = function() {
		ElementsUtil.waitForElement(this.buyAddOnBttn);
		return this.ILD10$Tab.click();
	};
	
	this.clickbuyAddOnBttn = function() {
		ElementsUtil.waitForElement(this.buyAddOnBttn);
		return this.buyAddOnBttn.click();
	};
	
	
	this.isBuyAddOnPageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/addons/);	
	};
	
	this.isPasswordPageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/collectpassword/);	
	};
	
	
	this.isPlanAdded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/redemptionconfirmationSuccess/);	
	};
	
	this.isDashBoardPageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/dashboard/);	
	};
	
	this.accountLogin = function(password) {
		ElementsUtil.waitForElement(this.passwordInput);
		this.passwordInput.sendKeys(password);
		return this.loginButton2.click();
	};

	
	this.enterEmailId = function(email) {
		ElementsUtil.waitForElement(this.loginInput);
		this.loginInput.sendKeys(email);
		return this.loginButton.click();
	};
	
	this.goToMyAccount = function() {
		ElementsUtil.waitForElement(this.gotoMyAccountLink);
		return this.gotoMyAccountLink.click();
	};
	
	this.enterPinToAddPlan = function(pin){
		ElementsUtil.waitForElement(this.eneterPin);
		 this.eneterPin.sendKeys(pin);
		 this.addPlanButton.click();
	};

	
	this.addPlanWithPin = function() {
		ElementsUtil.waitForElement(this.addPlan);
		return this.addPlan.click();
	};
	
	this.clickRefillNow = function(){
		//ElementsUtil.waitForElement(this.refillNow,2000);
		this.refillNow.click();
	};
	
	
	
	
	// this.serviceRenewBtn = element.all(by.buttonText('Pay Service')).get(1);
	/*this.enrolBtn = element.all(by.css("button[id='noEnrollToAutoPay_btn']")).get(1);
	this.serviceRenewBtn = element.all(by.css("span[translate='MYACC_5001']")).get(0);
	this.activateBtn = element.all(by.buttonText('Activate')).get(1);
	this.AutoPayBtn = element.all(by.css("span[translate='MYACC_5000']")).get(1);	
	

	this.continueBtn = element.all(by.buttonText('CONTINUE')).get(1);
	this.buyPlan = element.all(by.buttonText('BUY SERVICE PLAN')).get(1); 	
	this.close = element(by.css("button[title='close']"));
	this.clickPlan = element(by.linkText('SERVICE ADD-ONS')); 
	this.deviceAdded = element.all(by.css("button[id='btn_']")).get(3);
	this.deviceToRemove = element.all(by.css("a[ng-click='editAction()']")).get(2); 
	this.removeDeviceOption = element.all(by.css("a[ng-click='removeDevice()']")).get(1); 
	this.removeConfirm = element.all(by.buttonText('YES')).get(1);
	this.closeBtnDevice = element(by.css("button[title='CLOSE']"));
	// this.popupClose = element(by.css("button[title='close']"));
	this.selectPlan = element.all(by.css("button[id='btn_plancardaddonaddtocart']")).get(5);	
	this.continueToCheckout = element.all(by.buttonText('Continue to Checkout')).get(1);
	this.newpopupwindow = element.all(by.buttonText('Not this time')).get(1);
	this.serviceRenewBtn = element.all(by.css("span[translate='MYACC_5001']")).get(1); //payservice
	//newly added for tracfone
	this.addAirTimePinBtn = element.all(by.buttonText("ADD AIRTIME")).get(1);
	//this.addAirtimePin =  element.all(by.css("button[id='btn_addairtime']")).get(1);
	this.addAirtimePin =  element.all(by.css("[ng-click='action()']")).get(1);
	
	this.goToMultilineAutopay = function() {
		//$$//ElementsUtil.waitForElement(this.AutoPayBtn);
		return this.AutoPayBtn.click();
	};
	
	
	
	
	addPlan
	
	this.clickAddAirtime = function() {
		//$$//ElementsUtil.waitForElement(this.addAirtimePin);
		return this.addAirtimePin.click();
	};
	
	this.payService = function(){  //payservice
		//$$//ElementsUtil.waitForElement(this.serviceRenewBtn);
		return this.serviceRenewBtn.click();
	};
	
	this.enroll = function(){  
		return this.enrolBtn.click();
	};
	
	this.clickContinue = function() {
		//$$//ElementsUtil.waitForElement(this.continueBtn);
		return this.continueBtn.click();
	};
	
	this.goToAllOptions = function() {
		//$$//ElementsUtil.waitForElement(this.allOptions);
		return this.allOptions.click();
	};
	
	this.chooseBuyPlan = function() {
		ElementsUtil.waitForElement(this.buyPlan);
		return this.buyPlan.click();
	}; 
	
	this.selectAPlan = function() {
		ElementsUtil.waitForElement(this.clickPlan);
		this.close.click();
		this.clickPlan.click();
		// this.popupClose.click();
		return this.selectPlan.click();	
	}; 
	
	this.goToSelectPlan = function() {
		//$$//ElementsUtil.waitForElement(this.selectPlan);
		this.selectPlan.click();	
		return this.continueToCheckout.click();
	}; 
	
	this.goToMyDevices = function() {
		//$$//ElementsUtil.waitForElement(this.myDevices);
		return this.myDevices.click();
	};
	*/
	
	this.selectActivatedMin = function(){
		ElementsUtil.waitForElement(this.selectDevice);
		this.selectDevice.click();
		this.selectActiveMin.click();
		this.btnContinuePhonenumber.click();
	};
	
	this.goToManageProfile = function() {
		ElementsUtil.waitForElement(this.manageProfile);
		return this.manageProfile.click();
	};
	this.goToPaymentMethod = function() {
		ElementsUtil.waitForElement(this.paymentMethod);
		return this.paymentMethod.click();
	};
	/*
	this.goToPaymentMethod = function() {
		ElementsUtil.waitForElement(this.paymentMethod);
		return this.paymentMethod.click();
	};
	
	this.goToPaymentHistory = function() {
		//$$//ElementsUtil.waitForElement(this.paymentHistory);
		return this.paymentHistory.click();
	};*/
	
	this.goToAddDevice = function() {
		//ElementsUtil.waitForElement(this.addDevice);
	    this.addDevice.click();
	    ElementsUtil.waitForElement(this.addDevicePopupBtn);
	    this.addDevicePopupBtn.click();
	    
	};
	
	this.goToSignOut = function() {
//		ElementsUtil.waitForElement(this.signOut);//needed for byop activation scenario
		return this.signOut.click();
	};
	/*
	this.removeDevice = function() {
		//$$//ElementsUtil.waitForElement(this.deviceToRemove);
		this.deviceToRemove.click();
		// ElementsUtil.waitForElement(this.removeDeviceOption);
		this.removeDeviceOption.click();
		//$$//ElementsUtil.waitForElement(this.removeConfirm);
		this.removeConfirm.click();
		ElementsUtil.waitForElement(this.closeBtnDevice);
		return this.closeBtnDevice.click(); 
	};
	
	this.isDeviceRemoved = function() {
		return browser.wait(function() {
			return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /dashboard$/.test(url);
		});
		}, 10000, "URL hasn't changed");
	}; 
	*/

	//** method checks whether the account dashboard is loaded or not
	this.isLoaded = function(){
		//ElementsUtil.waitForElement(this.deviceAdded);
		return browser.wait(function() {
			return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /dashboard$/.test(url);
		});
		}, 10000, "URL hasn't changed");
	};
	
	//** method to click on the Activate button in dashboard under inactive devices -- Reactivation scenario
	this.clickOnActivateBtn = function(){
		this.activateButton.click();
	};
	
	this.refresh = function() {
		return browser.getCurrentUrl().then(function (url) {
					console.log('Currenturl: ', url);
                    return browser.get(url);
                });
        };

      //** method to check whether the checkout page is loaded or not
    	this.checkoutPageLoaded = function(){
    		return ElementsUtil.waitForUrlToChangeTo(/checkout$/);	
    	};
    	//TW Phone upgrade- to click show device link in Myaccount page
    	this.clickOnShowDevice = function(){
    		this.showLink.click();
    	};
    	this.clickOnOptionLink = function(){
    		this.optionsLink.click();
    	}; 
    	this.clickOnUpgrdaeDevice = function(){
    		this.upgradeDeviceLink.click();
    	}; 
    /*	this.checkoutPageLoaded = function(){
    		return ElementsUtil.waitForUrlToChangeTo(/collectpassword/);	
    	};*/
    	
    	
	/*
	//** method to proceed with the pay service flow
	this.payService = function(){
		//$$//ElementsUtil.waitForElement(this.serviceRenewBtn);
		this.serviceRenewBtn.click();

	
	//** method to click on the activate button, after adding a device through the dashboard
	this.clickOnActivate = function() {
		this.activateBtn.click();
	};
	
	//** method to check whether  a popup is dispalyed after clicking the pay service button in the account dashboard
	this.newPopupLoaded = function(){
		//$$//ElementsUtil.waitForElement(this.newpopupwindow);
		return this.newpopupwindow.isPresent();
	};
	
	//** method to proceed from the popup page
	this.continueFromPopUp = function(){
		//$$//ElementsUtil.waitForElement(this.newpopupwindow);
		this.newpopupwindow.click();
	};
	
	//** method to click on the Add Airtime Pin button in dashboard
	this.clickOnAddAirtimePin = function(){
		//$$//ElementsUtil.waitForElement(this.addAirTimePinBtn);
		this.addAirTimePinBtn.click();
	};
	*/
};

module.exports = new MyAccount;
