'use strict';
var myAccount = require("../myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");

describe('Add plan with pin', function() {	
//	
//	it('enteremail id', function(done) {
//		myAccount.enterEmailId(sessionData.totalwireless.username);
//		expect(myAccount.isPasswordPageLoaded()).toBe(true);
//		done();		
//	});
//	
//	
//	it('login', function(done) {
//		myAccount.accountLogin(sessionData.totalwireless.password);
//		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
//		done();		
//	});
	
	it('click on refill plan in the dashboard', function(done) {
		console.log("refill done")
		myAccount.refillPlanWithPin();
		expect(myAccount.renewServicePageLoaded()).toBe(true);
		done();		
	});
	
	it('enterpin to add plan', function(done) {
		myAccount.enterAirtimePinRenew(dataUtil.getPIN(sessionData.totalwireless.pinPartNumber));
		expect(myAccount.isPlanAdded()).toBe(true);
		done();		
	});
	
	it('loads dashboard page', function(done) {
		myAccount.clickOnContinueBtnFromConfirmation();
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	});
});
