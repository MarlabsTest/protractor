'use strict';
var myAccount = require("../myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");

describe('Apply Now your plan with pin', function() {	

	
	it('click on refill plan in the dashboard', function(done) {
		console.log("refill done")
		myAccount.refillPlanWithPin();
		expect(myAccount.renewServicePageLoaded()).toBe(true);
		done();		
	});
	
	it('enterpin to add plan', function(done) {
		myAccount.enterAirtimePinRenew(dataUtil.getPIN(sessionData.totalwireless.pinPartNumber));
		expect(myAccount.isPlanAdded()).toBe(true);
		done();		
	});
	
	it('click on myaccount for add now', function(done) {
		console.log("click on my account");
		myAccount.clickOnAddFromMyAccount();
		console.log("url page loaded 1");
		expect(myAccount.selectPlanForAddNowPage()).toBe(true);
		done();		
	});
	
	it('select plan and click on add now', function(done) {
		myAccount.selectPlanForAddNow();
		expect(myAccount.isPlanAdded()).toBe(true);
		done();		
	});
	
	it('loads dashboard page', function(done) {
		myAccount.clickOnContinueBtnFromConfirmation();
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	});
});
