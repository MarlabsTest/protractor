'use strict';

var activation = require("../../activation/activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};
var generatedEsnSim = {};

describe('Total Wireless Add device to myaccount', function() {
	it('click on add line menu from My Account page', function(done) {		
		myAccount.goToAddline();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on the continue button and it will navigates to Sim page',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the esn and click continue button', function(done) {
	 	//console.log('returns'+ sessionData.totalwireless.esnPartNumber);
	 	//console.log('returns'+ sessionData.totalwireless.simPartNumber);
		var esnval = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
       	console.log('returns', esnval);
		
		
		sessionData.totalwireless.esn = esnval;
		sessionData.totalwireless.esnsToReactivate.push(esnval);
		generatedEsn['esn'] = sessionData.totalwireless.esn;
		
		activation.enterEsn(esnval);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter the sim number and click the continue button', function(done) {
		
		var simval = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
       	console.log('returns', simval);
       	sessionData.totalwireless.sim = simval;		
       	generatedSim['sim'] = sessionData.totalwireless.sim;
		activation.enterSIM(simval);		
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.totalwireless.zip);		
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it(' enter airtime pin and navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.totalwireless.pinPartNumber);
       	console.log('returns Pin value', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.totalwireless.pin = pinval;
		generatedPin['pin'] = sessionData.totalwireless.pin;
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	
	
	it('click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnSummaryBtn();
//		DataUtil.activateESN(sessionData.totalwireless.esn);
//		console.log("update table with esn :"+sessionData.totalwireless.esn);
//		DataUtil.updateMin(sessionData.totalwireless.esn);
//		var min = DataUtil.getMinOfESN(sessionData.totalwireless.esn);
//		console.log("MIN is  :::::" +min);
		sessionData.totalwireless.upgradeEsn = sessionData.totalwireless.esn;//specific to phone upgrade
		//check_activation		
//		DataUtil.checkActivation(sessionData.totalwireless.esn,sessionData.totalwireless.pin,'1','TOTAL_WIRELESS_ACTIVATION_WITH_PIN');
//		generatedMin['min'] = min;
//		console.log("ITQ table updated");			
		expect(myAccount.isLoaded()).toBe(true);		
		done();	
	}).result.data = generatedMin;

	// });
	
});
