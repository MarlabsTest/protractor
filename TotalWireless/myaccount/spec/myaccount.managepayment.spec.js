'use strict';

var myAccount = require("../myaccount.po");

//================Manage Payment Scenario=====================

describe('TW Manage Payment', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	// require("./myaccount.login.spec.js");

	it('navigate to Manage Payment page', function(done) {
		console.log("navigate to Manage Payment page");
		myAccount.paymentMethod();
		expect(myAccount.isManagePaymentLoaded()).toBe(true);
		done();
	});	
		
	it('add payment details', function(done) {
		console.log("add payment details");
		myAccount.editPaymentDetails();
		expect(myAccount.isPaymentAdded()).toEqual(1);
		//expect(myAccount.isPaymentAdded());
		done();
	});	
});
	