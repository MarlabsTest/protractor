'use strict';

var login = require("../../login/login.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var homePage = require("../../common/homePage.po");
var myAccount = require("../myaccount.po");
var activation = require("../../activation/activation.po");

/*
 * Spec		: TW First Time Login with MIN
 * Details	: This spec is for testing the login functionality using an valid MIN/password.which is already stored on 
 * 			  session by activation spec.
 * */
describe('TW First Time Login with MIN', function() {

	/*
	 * Should navigate to My Account page for entering MIN of existing account. 
	 * */	
	it('Should navigate to My Account page', function(done) {
		homePage.myAccount();
		expect(homePage.isMyAccountPageLoaded()).toBe(true);
		done();
	});
	
	/*
	 * Should navigate to access account page for entering MIN of dummy account. 
	 * */
	it('Should navigate to access account page for entering min', function(done) {
		var esn = DataUtil.getESN(sessionData.totalwireless.esnPartNumber);
		var sim = DataUtil.getSIM(sessionData.totalwireless.simPartNumber);
		console.log("esn :: "+esn);
		var planName = sessionData.totalwireless.planName;
		var zip = sessionData.totalwireless.zip;
		var planPartNumber = sessionData.totalwireless.pinPartNumber;
		var brand = "TOTAL_WIRELESS";
		
		DataUtil.activateUserDummyAccount(esn, sim, zip, brand, planPartNumber, planName);
		
		console.log("esn:::: ",esn);
		var min = DataUtil.getMinOfESN(esn);
		console.log("min:::: :"+min+":");
		login.validateLogin(min); 
		expect(login.isCollectSecurityPinPage()).toBe(true);
		done();
	});
	
	/*
	 * Should enter security PIN and navigate to update user account page 
	 * */	
	it('Should navigate to My Account page', function(done) {
		login.enterSecurityPin('1234');
		expect(activation.isUpdateAccountPageLoaded()).toBe(true);
		done();
	});
	
	/*
	 * Should update user account details and check account updation success pop up shown
	 * */	
	it('Should update user account details', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.updateAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.totalwireless.username = emailval;
		sessionData.totalwireless.password = password;
		expect(activation.accountUpdationDone()).toBe(true);
		done();
	});
	
	/*should click on continue button in the popup page 
	 *
	 */
	it('should click on continue button in popup page', function(done) {
		activation.clickOnAccountUpdatedPopupBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
	