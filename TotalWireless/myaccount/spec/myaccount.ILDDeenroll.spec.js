'use strict';
var myAccount = require("../myaccount.po");

describe('DeEnroll ILD', function() {	
	
	it('Go to Manage ILD Enrollment', function(done) {
		console.log("Inside Deenroll Spec")
		myAccount.clickOnShowDevice();
		myAccount.clickOnOptionLink();
		myAccount.ManageILDEnrollment();
		expect(myAccount.IsManageILDEnrollmentLoaded()).toBe(true);
		done();		
	});
	
	it('cancel Enrollment and goto Dashboard', function(done) {
		myAccount.cancelEnrollment();
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();			
	});
	
	});
