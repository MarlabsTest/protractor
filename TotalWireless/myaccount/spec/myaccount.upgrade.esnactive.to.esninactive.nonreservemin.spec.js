
/* Spec file for  Upgrade from ESN Active with service end date in future to 
 * ESN Inactive with Min not reserved - Belongs to Same account
 * */

'use strict';

var sessionData 	= require("../../common/sessiondata.do");
var activationPo	= require("../../activation/activation.po");
var myAccountPo 	= require("../../myaccount/myaccount.po");
var homePagePo 		= require("../../common/homepage.po");
var dataUtil		= require("../../util/datautils.util");
var generatedMin 	= {};
var generatedEsnSim = {};

describe('Total Wireless Upgrade activated ESN to inactive ESN with MIN not reserved: ', function() {
	//should first deactivate the device and then reload the page for to activate the device,upon clicking activate link
	it('Deactivate the device', function(done) {	
        var min = dataUtil.getMinOfESN(sessionData.totalwireless.esn);
        var varPhoneStstus	= sessionData.totalwireless.phoneStatus;
        if(varPhoneStstus == "INACTIVE_NONRESERVE"){
        	dataUtil.deactivatePhone('DEACTIVATE',sessionData.totalwireless.esn,min,'PAST_DUE','TOTALWIRELESS');
        }else{
        	dataUtil.deactivatePhone('DEACTIVATE',sessionData.totalwireless.esn,min,'CUSTOMER REQUESTED','TOTALWIRELESS');
        }
       	
        homePagePo.homePageLoad();
		homePagePo.myAccount();
		expect(myAccountPo.isLoaded()).toBe(true);		
		done();
	}); 
	
	
	//Click on activate button of deactivated device.Opt for 'Keep My PhoneNumber' and select active device's MIN.
	it('Proceed for activation,', function(done) {
		myAccountPo.clickOnActivateBtn();
		//expect(myAccountPo.selectDevicePageLoaded()).toBe(true);
		expect(activationPo.finalInstructionPageLoaded()).toBe(true);
		done();
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be loaded
	 */
	it('should load the summary page', function(done) {
		activationPo.finalInstructionProceed();
		expect(activationPo.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the summary page', function(done) {
		activationPo.clickOnSummaryBtn();
		expect(myAccountPo.isLoaded()).toBe(true);
		done();		
	});
});
