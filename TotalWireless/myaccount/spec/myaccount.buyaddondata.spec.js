'use strict';
var myAccount = require("../myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");

describe('purchase  add on data', function() {	
	
	it('enteremail id', function(done) {
		myAccount.enterEmailId(sessionData.totalwireless.username);
		expect(myAccount.isPasswordPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('login', function(done) {
		myAccount.accountLogin(sessionData.totalwireless.password);
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('click on buy addon data button', function(done) {
		myAccount.clickbuyAddOnBttn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('should show the billing zipcode changes popup', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.confirmationPageLoaded()).toBe(true);
		done();		
	}); 
	
	it('should load confirmation page', function(done) {
		myAccount.clickDoneConfirmationBtn();
		expect(myAccount.isSurveyPageLoaded()).toBe(true);
		done();		
	}); 
	
	
	it('should load dashboard page', function(done) {
		myAccount.clickOnSurvey();
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	}); 
	
	
	});
