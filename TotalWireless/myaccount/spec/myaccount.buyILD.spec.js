'use strict';
var myAccount = require("../myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var menu = require("../../common/homepage.po");
var shop = require("../../shop/shop.po");

describe('purchase  add on data', function() {	
	
	it('click on shop link', function(done) {
		menu.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on buy ILD button', function(done) {
		var min = dataUtil.getMinOfESN(sessionData.totalwireless.esn);
		myAccount.clickbuyILD(min);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('should show the billing zipcode changes popup and proceed to confirmation', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		myAccount.agreeBillingZipcodeChanges();
		myAccount.placeMyOrder();
		expect(myAccount.confirmationPageLoaded()).toBe(true);
		done();		
	}); 
	
	it('should load Dashboard after confirmation', function(done) {
		myAccount.clickDoneConfirmationBtn();
		expect(myAccount.isDashBoardPageLoaded()).toBe(true);
		done();		
	}); 
	
	});
