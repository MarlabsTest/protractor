'use strict';

var login = require("../../login/login.po");
var loginData = require("../../common/sessiondata.do");
var homePage = require("../../common/homePage.po");

/*
 * Spec		: Total Wireless Login with Email
 * Details	: This spec is for testing the login functionality using an valid username/password.which is already stored on 
 * 			  session by activation spec.
 * */
describe('Total Wireless Login', function() {
	
	/*
	 * Should navigate to My Account page for entering username of existing account. 
	 * */
	it('Should navigate to My Account page', function(done) {
		homePage.myAccount();
		expect(homePage.isMyAccountPageLoaded()).toBe(true);
		done();
	});
	
	/*
	 * Should navigate to access account page for entering password of existing account. 
	 * */
	it('Should navigate to access account page', function(done) {	
		login.validateLogin(loginData.totalwireless.username); 
		expect(login.isValidLogin()).toBe(true);
		done();
	});
	
	/*
	 * Should navigate to my account dashboard page. 
	 * */
	it('Should navigate to myaccount dashboard page', function(done) {
		login.validatePassword(loginData.totalwireless.password);
		expect(login.isValidPassword()).toBe(true);
		done();
	});  
	
});
	