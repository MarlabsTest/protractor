'use strict';
var ElementsUtil = require("../util/element.util");
var generator = require('creditcard-generator');
var CCUtil = require("../util/creditCard.utils");
var CommonUtil= require("../util/common.functions.util");
var sessionData = require("../common/sessiondata.do");
//===================Enter Card Details For Manage Payment in MyAccount=====================

var ManageProfile = function() {
		
	//this.editBtn = element(by.css("a[title='MYAC_99744']")); 
	//this.editBtn = element(by.css("tf-edit-hide-inline"));
	this.editBtn = element.all(by.css("[ng-click='toggleOpen($event); isOpen?hideAction():editAction()']")).get(0); //element(by.id('accordiongroup-1726-6989-tab'));
	this.saveChangesBtn = element.all(by.id('btn_card')).get(0);
	this.enterCardInfo = element.all(by.css("input[id='formName.number_mask']")).get(0);
	this.month = element.all(by.className('selectize-input')).get(0); 
	this.enterMonth = element.all(by.className('dropdown-content-item ng-scope')).get(6);	
	this.year = element.all(by.className('selectize-input')).get(1);	
	this.enterYear = element.all(by.className('dropdown-content-item ng-scope')).get(2);
	this.enterFirstName = element.all(by.name('fname')).get(1);
	this.enterLastName = element.all(by.name('lname')).get(1);
	this.enterAddress = element.all(by.name('address1')).get(1);
	this.houseNo = element.all(by.name('address2')).get(1);
	this.enterCity = element.all(by.id('city')).get(1);
	this.enterZip = element.all(by.id('zipcode')).get(1);
	this.state = element.all(by.className('selectize-input')).get(2);	
	this.enterState = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	
	/*
	this.enterFirstName = element(by.css("input[id='fname']"));
	this.enterLastName = element(by.css("input[id='lname']"));
	this.enterAddress = element(by.css("input[id='address1']"));
	this.enterCity = element(by.css("input[id='city']"));
	this.state = element.all(by.model('$select.search')).get(2);
	this.enterState = element(by.xpath("//div[@id='state']/div[2]/div/div/div[6]/div/div/p"));
	this.enterZip = element(by.css("input[id='zipcode']"));*/
	//this.addCreditCardBtn = element.all(by.css("button[id='btn_card']")).get(0);
	this.addCreditCardBtn = element.all(by.id('btn_card')).get(0);
	this.deleteOption = element(by.id('btn_deletethiscard'));
	
	this.ccTabEdit = element.all(by.css('[ng-click="toggleOpen()"]')).get(0);
	this.btn_DeleteCard=element(by.id('btn_deletethiscard'));
	
	this.goToEditPaymentInfo = function() {
		this.editBtn.click();
		this.enterCardInfo.clear().sendKeys(""+generator.GenCC(sessionData.totalwireless.cardType));
		this.month.click();
		this.enterMonth.click();
		this.year.click();
		this.enterYear.click();
		this.enterFirstName.sendKeys(CCUtil.firstName);
		this.enterLastName.sendKeys(CCUtil.lastName);		
		this.enterAddress.sendKeys(CCUtil.addOne);	
		this.state.click();
		this.enterState.click();
		this.enterCity.clear().sendKeys(CCUtil.city);
		this.enterZip.clear().sendKeys(CCUtil.pin); 
		this.saveChangesBtn.click();
	};
	
	//---------------Check if Payment Added--------------
	this.isPaymentAdded = function() { 
		var paymentAddedPanel = element.all(by.css("div[class='panel-secondary ng-isolate-scope panel']"));
		return paymentAddedPanel.count().then(function (num) {
        console.log("count======================",num);
		return num;
		//return this.editBtn.isPresent();
		 });
	};
	
	//--------------Check if manage Payment page loaded----------------------
	this.isLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/paymentmethod$/);		
	};
	
	this.goTodeleteOption = function() {
		this.editBtn.click();	
	};
	
	//--------------Check if delete option is present-------------------------
	this.deleteOptionIsPresent = function(){
		return this.deleteOption.isPresent();
	};
	
	//-------------- Delete the saved cards from the manage payment ---------------
	this.clickOnDeleteOption = function(){
		this.deleteOption.click();
	};
	
	//-------------- Delete the saved cards from the manage payment ---------------
	this.deleteCardDetails = function(){
		ElementsUtil.waitForElement(this.ccTabEdit);
		this.ccTabEdit.click();
		this.btn_DeleteCard.click();
	};
	
};

module.exports = new ManageProfile;
