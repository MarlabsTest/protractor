//this represents a modal for add service plan option

'use strict';
var ElementsUtil = require("../util/element.util");

var ILDDeEnroll = function() {
	this.cancelEnrollButton = element(by.id('deenroll')); 
	this.selectReason = element(by.id('deenroll-selectedreason')); 
	this.cancelYes = element.all(by.css('[ng-click="action()"]')).get(1);
	
	
	this.IsManageILDEnrollmentLoaded = function(){
		//return ElementsUtil.waitForUrlToChangeTo(/manageenroll?basePlan=false&ild=true/);
		return this.cancelEnrollButton.isPresent();
	}
	
	this.cancelEnrollment = function(){
		this.cancelEnrollButton.click();
		element(by.model('data.reason')).$('[value="not needed"]').click();
		this.cancelYes.click();
	}

};

module.exports = new ILDDeEnroll;