//this represents a modal for add service plan option

'use strict';
var ElementsUtil = require("../util/element.util");

var AddServicePin = function() {
	this.refillPlanLink = element.all(by.css('[ng-click="data.method()"]')).get(0);
	//this.addServicePlanLink = element(by.css('[ng-click="addServicePlan()"]'));
	this.renewPin = element.all(by.name('form_redeem.pin')).get(1);
	this.renewContinueBtn = element(by.id('AddServicePlan'));
	this.proceedFromCOnfirmationPage = element.all(by.id('done_confirmation')).get(0);
	this.addFromMyAccountButton = element.all(by.id('managestash')).get(0);
	this.selectPlan = element.all(by.id('select_plan')).get(0);
	this.addNow =element.all(by.css('[ng-click="action()"]')).get(1); // element.all(by.linkText('apply now')).get(0);
	
	//** method is to proceed with the add service plan flow
	this.clickOnservicePlanLink = function(){
		ElementsUtil.waitForElement(this.addServicePlanLink);
		this.addServicePlanLink.click();
	};
  
	//** method checks whether the renew service plan page is loaded or not
	this.renewServicePageLoaded = function(){
		ElementsUtil.waitForElement(this.renewPin);
		return this.renewPin.isPresent();
	}
  
  //** method to enter new pin 
	this.enterAirtimePinRenew = function(pin){
		ElementsUtil.waitForElement(this.renewPin);
		this.renewPin.clear().sendKeys(pin);
		ElementsUtil.waitForElement(this.renewContinueBtn);
		this.renewContinueBtn.click();
	};
  
	//** method to check the redemption confirmation page is loaded after entering the new pin
	this.redemptionSuccessPage = function(){	
		ElementsUtil.waitForElement(this.proceedFromCOnfirmationPage);
		return this.proceedFromCOnfirmationPage.isPresent();
	};
  
	//** method to proceed from the confirmation page
	this.clickOnContinueBtnFromConfirmation = function(){
		ElementsUtil.waitForElement(this.proceedFromCOnfirmationPage);
		this.proceedFromCOnfirmationPage.click();
	};
	
	this.refillPlanWithPin = function(){
//		ElementsUtil.waitForElement(this.refillPlanLink);
		this.refillPlanLink.click();
	};
	
	//method to click add from my account button
	this.clickOnAddFromMyAccount = function(){
		ElementsUtil.waitForElement(this.addFromMyAccountButton);
		this.addFromMyAccountButton.click();
	};
	
	//method to select plan page is loaded
	this.selectPlanForAddNowPage = function(){
		ElementsUtil.waitForElement(this.selectPlan);
		return this.selectPlan.isPresent();
//		return ElementsUtil.waitForUrlToChangeTo(/managereserve/);
	};
	
	//method to select the plan and click on the apply now button
	this.selectPlanForAddNow = function(){
		this.selectPlan.click();
		this.addNow.click();
	};

};

module.exports = new AddServicePin;