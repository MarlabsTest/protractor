'use strict';
var ElementsUtil = require("../util/element.util");
var sessionData = require("../common/sessiondata.do");

//=============To Edit your Account Profile============

var ManageProfile = function() {

//----------------Enter Details--------------------------
	
	this.editContactInfoLink = element.all(by.css('[ng-click="editAction()"]')).get(0);
	this.changePasswordInfo = element.all(by.css('[ng-click="editAction()"]')).get(2);
	this.enterFirstName = element.all(by.id('contact_form-first_name')).get(1);
	this.enterLastName = element.all(by.id('contact_form-last_name')).get(1);
	this.addressLine1 = element.all(by.id('contact_form-address1')).get(1);
	this.addressLine2 = element.all(by.id('contact_form-address2')).get(1);
	this.city = element(by.css('input[id="contact_form-city"]'));
	this.enterPhoneNum = element.all(by.id('contact_form-phone')).get(1);
	// this.state = element(by.model('form.contact.state')).$('[value="FL"]');
	this.state = element(by.model('$select.search')); 
	this.enterState = element(by.xpath("//div[@id='state']/div[2]/div/div/div[6]/div/div/p")); 
	this.zipcode = element(by.css('input[id="contact_form-zipcode"]'));
	this.saveButton = element.all(by.css('[ng-click="action()"]')).get(0);	
	this.confirmPassword = element.all(by.id('loginForm-confirm_new_password')).get(1);
	this.newPassword = element.all(by.id('loginForm-new_password')).get(1);
	this.confirmSave = element.all(by.id('btn_save')).get(0);
	this.editedFirstName = $('.col-xs-12.col-sm-6.col-md-6.col-lg-6').all(by.tagName('p'));
	this.securityQuestion = element(by.css('[ng-click="$select.toggle($event)"]'));
	this.securityClick = element.all(by.css('[ng-click="$select.select(option,$select.skipFocusser,$event)"]')).get(1);
	this.securityAnswer = element.all(by.id('loginForm-security_answer')).get(1);
	this.confirmPopupPassword = element.all(by.id('pswpopup-password')).get(1);
	
	
	this.goToEditContactInfo = function() {
		ElementsUtil.waitForElement(this.editContactInfoLink);
		this.editContactInfoLink.click();
		this.enterFirstName.clear().sendKeys('test');
		this.enterLastName.clear().sendKeys('lastname');  // 1
		this.addressLine1.clear().sendKeys('Address line one'); 
		this.addressLine2.clear().sendKeys('Address line two');
		this.enterPhoneNum.sendKeys('9098765432');		// 1	
		this.saveButton.click();
		this.confirmPopupPassword.sendKeys(sessionData.totalwireless.password);
		this.confirmSave.click();
		this.editLoginInfo();
		/*
		this.saveButton.click();
		this.changePasswordInfo.click();
		this.newPassword.sendKeys(sessionData.simplemobile.password);
		this.confirmPassword.sendKeys(sessionData.simplemobile.password);
		this.securityQuestion.click();
		this.securityAnswer.sendKeys('dog');
		this.confirmSave.click();
		this.confirmPopupPassword.sendKeys(sessionData.simplemobile.password);
		this.confirmSave.click();
		*/
	};
	
	this.editLoginInfo = function() {
		ElementsUtil.waitForElement(this.changePasswordInfo);
		this.changePasswordInfo.click();
		this.newPassword.sendKeys(sessionData.totalwireless.password);
		this.confirmPassword.sendKeys(sessionData.totalwireless.password);
		this.securityQuestion.click();
		this.securityClick.click();
		this.securityAnswer.sendKeys('dog');
		this.confirmSave.click();
		this.confirmPopupPassword.sendKeys(sessionData.totalwireless.password);
		this.confirmSave.click();
	};
	
	//---------------Check if profile edited--------------
	this.isContactEdited = function() {
		var editDone = element.all(by.css("div[class='border-dashed border-top-0 border-right-0 border-bottom-0 col-xs-12 col-sm-6 col-md-6 col-lg-6']")).get(0).element(by.tagName('p'));
		console.log("edit",editDone.getText());		
		return editDone.getText().then(function (text) { 
			console.log("EditedName========================:",text.trim()+":");
			return text.trim();
			}); 
				
	};
	
	//----------------Check if MyProfile page loaded----------
	this.isLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/myprofile$/);	
	};
	
};

module.exports = new ManageProfile;