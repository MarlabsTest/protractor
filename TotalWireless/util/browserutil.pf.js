'use strict';

var Browserutil = function() {
	
	this.switchTab = function(index) {
		browser.getAllWindowHandles().then(function (handles) {
			browser.switchTo().window(handles[index]);
		});
	};
	
	
	/* this.switchTab = function(index) {
		browser.getAllWindowHandles().then(function (handles) {
			var totalHandle = handles.count();
			console.log("Total window : "+totalHandle);
			if(totalHandle<index){
				browser.switchTo().window(handles[index]);
			}else{
				browser.switchTo().window(handles[totalHandle-1]);
			}
		 });
	}; */
	
};
module.exports = Browserutil;