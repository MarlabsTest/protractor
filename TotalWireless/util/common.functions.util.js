'use strict';
var CreditCardUtil = require("./creditCard.utils");

var CommonUtil = {
	
		getCvv:function(cardType){
		console.log("inside util cvv *********");
			var cvv;
			if (cardType == "Amex"){
				cvv = CreditCardUtil.amexCvv;
				}
			else{
				cvv = CreditCardUtil.cvvNumber;
				}
			return cvv;
			},
			
	getLastDigits: function(inputValue, noOfDigits) {
		var lastDigits = "";
		var len = inputValue.length;
		
		if(noOfDigits > 0)
		{
			if(noOfDigits <= len)
			{
				lastDigits = inputValue.toString().substr( len - noOfDigits);
			}
			else if(noOfDigits > len)
			{
				lastDigits = inputValue.toString();
			}
		}
		
		return lastDigits;
	},
	
	reverseString: function(str){
        return str.split('').reverse().join('');
	},
	
	isByopEsn: function(inputEsn){
		if(inputEsn.startsWith('PH')){
			return true;
		}
		else{
			return false;
		}
	},
	
	isARFlow: function(arValue){		
		if(arValue.startsWith('Y')){
			return true;
		}else{
			return false;
		}
	},
	
	isBYOPPhone: function(phoneType){		
		if(phoneType.startsWith('PH') || phoneType == 'Byop'){
		
			return true;
		}else{
			return false;
		}
	},
	
	
	isBrandedPhone: function(phoneType){
		if(phoneType == 'Branded'){
			return true;
		}
		else{
			return false;
		}
	},
	
	isHOTSPOTPhone: function(phoneType){
		if(phoneType == 'Hotspot'){
			return true;
		}
		else{
			return false;
		}
	},
	/*Upgrade ESN active to ESN inactive- MIN reserve and non reserve scenario.
	 * This method check the PhoneStatus field in input file and return the result*/
	isInactive: function(varPhoneStatus){		
		if(varPhoneStatus == "INACTIVE_NONRESERVE" || varPhoneStatus == "INACTIVE_RESERVE"){
			return true;
		}else{
			return false;
		}
	},
	isEqualString: function(strOne, strTwo){
		var toRet;
		if(strOne != null && strTwo != null && typeof strOne != undefined && typeof strTwo != undefined)
		{
			var trimmedStrOne = strOne.trim();
			var trimmedStrTwo = strTwo.trim();
			
			var lowerCaseStrOne = trimmedStrOne.toLowerCase();
			var lowerCaseStrTwo = trimmedStrTwo.toLowerCase();
			
			if(lowerCaseStrOne == lowerCaseStrTwo)
			{
				toRet = true;
			}
			else
			{
				toRet = false;
			}
		}
		else
		{
			toRet = false;
		}
		return toRet;
	},
	//trim a character on both ends
	trimCharacter: function(str, charToTrim){
		var toRet = str;
		
		if(toRet.startsWith(charToTrim))
		{
			toRet  = toRet.substr(1);
		}
		
		if(toRet.endsWith(charToTrim))
		{
			toRet  = toRet.substr(0, toRet.length-1);
		}
		
		return toRet;
	}
	
};

module.exports = CommonUtil;