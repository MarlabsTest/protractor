'use strict';

var loginUtil = require('./login.util');
var activationUtil = require('./activation.util');
var activationMixedLinesUtil = require('./activation.mixedlines.util');
var activationWithNoPinUtil = require('./activation.withnopin.util');
var activationHotSpotPhoneUtil = require('./activation.hotspot.util');
//var activationCdmaUtil = require('./cdmaactivation.util');
var phoneUpgradeUtil = require('./phoneupgrade.util');
var byopActivationUtil = require('./byop.activation.util');
var shopUtil = require('./shopplans.util');
var refillUtil = require('./refill.util');
var portingUtil = require('./porting.util');
//var activateDevicesPinUtil = require('./activationdevicespin.util');
var TestDataGenerator = {
	
	generateTestData: function() {
		loginUtil.prepareLoginData();
		activationUtil.prepareActivationData();
		activationWithNoPinUtil.prepareActivationData();
		phoneUpgradeUtil.prepareActivationData();
		byopActivationUtil.prepareActivationData();
//		activationCdmaUtil.prepareCdmaActivationData();
		portingUtil.prepareActivationData();
		shopUtil.prepareShopplansData();
		refillUtil.prepareRefillData();
		activationHotSpotPhoneUtil.prepareActivationData();
//		activateDevicesPinUtil.prepareActivationData();
		activationMixedLinesUtil.prepareActivationData();
	}
};

module.exports = TestDataGenerator;