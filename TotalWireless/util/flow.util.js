'use strict';

var runUtil = require('./run.util');
const path = require('path');

var FLOWS = {
		TW_ACTIVATION_ONELINE_WITHPIN_WITHACCOUNT: ['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_ACTIVATION_ONELINE_WITHPURCHASE_WITHACCOUNT: ['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.withpurchase.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.multiple.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_INTERNAL_PORTIN_WITHPIN_WITHACCOUNT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activate.internal.portin.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_INTERNAL_PORTIN_WITHPURCHASE_WITHACCOUNT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activate.internal.portin.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_EXTERNAL_PORT_WITHPIN:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.external.port.withpin.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_EXTERNAL_PORT_WITHPURCHASE:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.external.port.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_ACTIVATION_MIXEDLINES_WITHPIN_WITHACCOUNT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.mixedlines.withpin.withaccount.spec.js','activation/spec/activation.multiple.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_ACTIVATION_MIXEDLINES_WITHPURCHASE_WITHACCOUNT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.mixedlines.withpurchase.withaccount.spec.js','activation/spec/activation.multiple.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_ACTIVATION_HOTSPOT_WITHPIN_WITHACCOUNT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.hotspot.withpin.spec.js','activation/spec/activation.multiple.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_ACTIVATION_HOTSPOT_ONELINE_WITHPURCHASE_WITHACCOUNT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.hotspot.withpurchase.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_ACTIVATION_HOTSPOT_ONELINE_WITHPIN_WITHACCOUNT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.hotspot.withpin.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_MYACCOUNT_ADDSERVICEPIN_INSIDE:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.addServicePin.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_REFILL_WITHPIN_ALL:['reup/spec/reup.all.withpin.outside.spec.js'],
		TW_REFILL_WITHPIN_BRANDED:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.spec.js'],
		TW_REFILL_WITHPIN_BYOP:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.byopphone.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.spec.js'],
		TW_REFILL_WITHPIN_HOTSPOT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.hotspot.withpin.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.spec.js'],
		TW_BYOP_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.byopphone.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_BYOP_ACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.byopphone.withpurchase.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_LOGIN_WITH_EMAIL : ['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.Email.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_LOGIN_WITH_MIN : ['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.loginwithmin.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_LOGIN_FIRSTTIME_WITH_MIN : ['common/spec/totalwireless.loadhomepage.spec.js','myaccount/spec/myaccount.firsttime.login.withmin.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_MYACCOUNT_ADDSERVICEPIN_INSIDE:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.login.Email.spec.js','myaccount/spec/myaccount.addServicePin.inside.spec.js'],
		TW_MYACCOUNT_BUYADDON:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.buyaddondata.spec.js'],
		TW_FORGOT_PASSWORD : ['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.forgotpassword.spec.js'],
		TW_SHOP_ADDONS:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','shop/spec/shop.addon.spec.js'],
		TW_ENROLLMENT_OF_AUTOREFILL:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.Email.spec.js','autoreup/spec/autoreup.specs.js'],
		TW_DEENROLLMENT_OF_AUTOREFILL:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.Email.spec.js','autoreup/spec/autoreup.specs.js','autoreup/spec/autoreup.deenroll.specs.js'],
		TW_MYACCOUNT_ADDSERVICEPIN_ADDNOW:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.login.Email.spec.js','myaccount/spec/myaccount.addServicePin.addNow.spec.js'],
		TW_MANAGE_PAYMENT:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_DELETE_MANAGE_PAYMENT:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.Email.spec.js','myaccount/spec/myaccount.delete.payment.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_EDIT_CONTACT_INFO:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','myaccount/spec/myaccount.editcontactinfo.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_REFILL_WITHPURCHASE_ALL:['reup/spec/reup.all.withpurchase.outside.spec.js'],
		TW_REFILL_WITHPURCHASE_BRANDED:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpurchase.outside.spec.js'],
		TW_REFILL_WITHPURCHASE_BYOP:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.byopphone.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpurchase.outside.spec.js'],
		TW_REFILL_WITHPURCHASE_HOTSPOT:['common/spec/totalwireless.loadhomepage.spec.js', 'activation/spec/activation.oneline.hotspot.withpin.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpurchase.outside.spec.js'],
		TW_UPGRADE_FLOWS:['activation/spec/activation.upgrade.flows.spec.js'],
		TW_PHONE_UPGRADE:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','activation/spec/activation.phoneupgrade.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_REFILL_WITHPURCHASE_ALL_INSIDE:['reup/spec/reup.all.withpurchase.inside.spec.js'],
		TW_REFILL_WITHPURCHASE_INSIDE:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.Email.spec.js','reup/spec/reup.withpurchase.inside.spec.js'],
		TW_REACTIVATION_PHONE:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','activation/spec/reactivate.phone.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_REACTIVATION_PHONE_WITHPURCHASE:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','activation/spec/reactivate.phone.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		TW_MYACCOUNT_BUYILD:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.buyILD.spec.js'],
		TW_MYACCOUNT_ILDDeEnrollment:['common/spec/totalwireless.loadhomepage.spec.js','activation/spec/activation.oneline.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.buyILD.spec.js','myaccount/spec/myaccount.ILDDeenroll.spec.js']
};
	
var FlowUtil = {
	
	run: function(flow) {		
		FLOWS[flow].forEach(function (spec) {
			//console.log("running spec..",path.join(protractor.basePath, spec));
			runUtil.requireSuite(path.join(protractor.basePath, spec));
		});
	}
};

module.exports = FlowUtil;
