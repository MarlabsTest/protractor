'use strict';
var CommonUtil = require('./common.functions.util.js');

var CreditCardUtil = {
    //Method to get the credit card number 
    //prefix value (e.g 4539, 4556, 4916, 6011)
    //cclength is the card number length (eg. 15, 16)
    ccNumber: function(prefix,cclength){
        var ccnumber = prefix;
        
        //generate a (cclength-1) digit number with the given prefix
        while (ccnumber.length < (cclength - 1)) {
                        ccnumber = ccnumber.concat(Math.floor(Math.random() * 10));
        }
        
        // reverse number and store it in the array
        var reversedCCnumberString = CommonUtil.reverseString(ccnumber);
        var reversedCCnumberList = new Array();
        for (var i = 0; i < reversedCCnumberString.length; i++) {
                        reversedCCnumberList.push(parseInt(reversedCCnumberString.charAt(i)));
        }
        
        // calculate sum using the array
        var sum = 0;
        var pos = 0;
        while (pos < cclength - 1) {
                        var odd = reversedCCnumberList[pos] * 2;
                        if (odd > 9) {
                                        odd = odd - 9;
                        }

                        sum = sum + odd;

                        if (pos != (cclength - 2)) {
                                        sum = sum + reversedCCnumberList[pos + 1];
                        }
                        pos = pos + 2;
        }
        
        // calculate check digit(will be 1 to 9)
        var checkdigit = ((Math.floor(sum / 10) + 1) * 10 - sum) % 10;
        
        ccnumber = ccnumber + checkdigit;
        
        return ccnumber;
    }
};

module.exports = CreditCardUtil;
