'use strict';

var drive = require('jasmine-data-provider');
var sessionData = require("../../common/sessiondata.do");
var FlowUtil = require('../../util/flow.util');
var refillUtil = require('../../util/refill.util');

describe('Total Wireless ReUp Service with Purchase', function() {

	var activationData = refillUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		sessionData.totalwireless.type = inputActivationData.Type;
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.totalwireless.esnPartNumber = inputActivationData.PartNumber;
				sessionData.totalwireless.simPartNumber = inputActivationData.SIM;
				sessionData.totalwireless.zip = inputActivationData.ZipCode;
				sessionData.totalwireless.pinPartNumber = inputActivationData.PIN;
				sessionData.totalwireless.cardType = inputActivationData.cardType;
				sessionData.totalwireless.autoRefill = inputActivationData.AutoRefill;
				sessionData.totalwireless.planName = inputActivationData.PlanName;
				sessionData.totalwireless.shopPlan = inputActivationData.shopplan;
				
				//specific to byop flow
				sessionData.totalwireless.isLTE = inputActivationData.isLTE;
				sessionData.totalwireless.carrier = inputActivationData.carrier;
				sessionData.totalwireless.isHD = inputActivationData.isHD;
				sessionData.totalwireless.isTrio = inputActivationData.isTrio;
				sessionData.totalwireless.phoneType = inputActivationData.PhoneType;
				sessionData.totalwireless.phoneModel = inputActivationData.PhoneModel;
				done();
			});
			FlowUtil.run('TW_REFILL_WITHPURCHASE_ALL');	
		}).result.data = inputActivationData;
	});
});