'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");

var reUpPage = require("../reup.po");
var shop = require("../../shop/shop.po");

describe('Total Wireless ReUp with Purchase inside Account', function() {
	
	it('should navigate to ReUp page', function(done) {
		reUpPage.goToReUpLink();		
		expect(reUpPage.isReUpPagePageLoaded()).toBe(true);		
		done();
	});
	
	
	it('should enter min', function(done) {
		reUpPage.enterMinInside(dataUtil.getMinOfESN(sessionData.totalwireless.esn));
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();
	});
	
	
	it('should select plan and listed with available plans', function(done){
		var planType		= sessionData.totalwireless.shopPlan;
		var planName		= sessionData.totalwireless.planName;
		var isAutoRefill	= sessionData.totalwireless.autoRefill;
		var phoneType	    = sessionData.totalwireless.phoneType;
		console.log("planType planName="+planType+"="+planName);
		shop.choosePlanByName(planType,planName,isAutoRefill,phoneType);
		expect(shop.isAddNowWarningPageLoaded()).toBe(true);
		done();		
	});

	it('should do add service plan now', function(done) {
		shop.doAddNow();
		shop.doAutoReUpPopupSelection(sessionData.totalwireless.autoRefill);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});

	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.totalwireless.cardType),CommonUtil.getCvv(sessionData.totalwireless.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	});
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click place my order button
	 *Expected result - Summary Page will be shown
	 */	
	it('should load the summary page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnSummaryBtn();
		sessionData.totalwireless.upgradeEsn = sessionData.totalwireless.esn;//specific to phone upgrade					
		expect(homePage.isHomePageLoaded()).toBe(true);		
		done();	
	});
});
	