'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");

var reUpPage = require("../reup.po");

describe('Total Wireless ReUp with PIN Outside Account', function() {
	
	it('navigates to ReUp page', function(done) {
		console.log("should navigate to My Refill page");
		reUpPage.goToReUpLink();		
		expect(reUpPage.isReUpPagePageLoaded()).toBe(true);		
		done();
	});
	
	
	it('enter min and pin', function(done) {
		reUpPage.addAirTime(dataUtil.getMinOfESN(sessionData.totalwireless.esn),dataUtil.getPIN(sessionData.totalwireless.redemptionPin));
		expect(reUpPage.goToConfirmOrderPage()).toBe(true);
		done();
	});
	
	it('Confirmation', function(done) {
		reUpPage.clickOnDone();
		expect(reUpPage.goToHomePage()).toBe(true);
		done();
	});
	

});
	