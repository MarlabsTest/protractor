'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var CommonUtil =  require('../../util/common.functions.util');
var FlowUtil = require('../../util/flow.util');

var reUpPage = require("../reup.po");

describe('REUP with Purchase', function() {
	
	var type = sessionData.totalwireless.type;
		
	if(CommonUtil.isBrandedPhone(type)){
		console.log('Refill : Branded phone');
			FlowUtil.run('TW_REFILL_WITHPURCHASE_BRANDED');	
	}else if(CommonUtil.isBYOPPhone(type)){
		console.log('Refill : BYOP phone');
		FlowUtil.run('TW_REFILL_WITHPURCHASE_BYOP');
	}else if(CommonUtil.isHOTSPOTPhone(type)){
		console.log('Refill : Hotspot phone');
		FlowUtil.run('TW_REFILL_WITHPURCHASE_HOTSPOT');		
	}/*else{
		console.log('Refill : Home phone');
		FlowUtil.run('NT_REFILL_WITHPIN_HOME');	
	}
	 */
});
	