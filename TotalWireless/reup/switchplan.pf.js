'use strict';
var ElementsUtil = require("../util/element.util");
var sessionData = require("../common/sessiondata.do");
var dataUtil= require("../util/datautils.util");
var ConstantsUtil = require("../util/constants.util");


var ReUpPf = function() {
	
	this.manageReserveButton = element(by.id('managestash'));
	this.applyNowButton= element(by.id('addnow'));
	this.addNowButton = element(by.id('add'));
	this.planRadioButton =element(by.name('selstashplan'));
	
	this.minTextField = element(by.xpath('/html/body/tf-update-lang/div[2]/div[2]/div/div/div[2]/div/div/tf-page-body/div/div[1]/div[2]/div[2]/ng-include/tf-split-vertical-or/div/div[3]/div/tf-split-vertical-or-right/form/div[1]/div[2]/div[1]/div/tf-phone/div/div/input'));
	this.pinTextField = element.all(by.id('form_redeem.pin')).get(1);
	this.addAirTimeButton = element(by.id('managestash'));
	this.doneConfirmButton = element(by.id('done_confirmation'));
	this.buyPlanButton = element(by.id('RenewService'));
	this.continueBttn = element.all(by.css("span[translate='BAT_24130']")).get(0);
	
	/*This  method will navigates to manageReserve page*/
	this.clickOnManageReserve = function() {
		ElementsUtil.waitForElement(this.manageReserveButton,2000);
		return this.manageReserveButton.click();
	};
	
	//*** method to check whether the Manage reserve page is loaded or not
	this.isManageReserveLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/managereserve/);
	};
	
	/*This method will enter min and pin and clicks on add air time button*/
	this.clickOnApplyNow = function() {
		ElementsUtil.waitForElement(this.planRadioButton,2000);
		this.planRadioButton.click();
		this.applyNowButton.click();
	};
	
	 /*This method will check whether the popup is loaded or not*/
	this.isPopUpLoaded = function() {
		ElementsUtil.waitForElement(this.addNowButton);
		return this.addNowButton.isPresent();
	};
	
	this.clickOnAddnow = function() {
		//ElementsUtil.waitForElement(this.phoneNumber,2000);
		this.addNowButton.click();
	};
	
	/*This method will click on done button*/
	this.clickOnDone = function() {
		ElementsUtil.waitForElement(this.doneConfirmButton);
		return this.doneConfirmButton.click();
	};
	
	 /*This method will check whether the home page loaded or not*/
	this.goToHomePage = function() {
		return ElementsUtil.waitForUrlToChangeTo(/siteng.totalwireless.com/);
	};
	
	
	};

module.exports = new ReUpPf;