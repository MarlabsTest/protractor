'use strict';
var reUpPf = require("./reup.pf");
var switchplanpf = require("./switchplan.pf");

var ReUpPO = function() {

	this.goToReUpLink = function() {
		return reUpPf.goToReUpLink();
	};
	
	//*** method to check the refill page loaded
	this.isReUpPagePageLoaded = function() {	
		return reUpPf.isReUpPagePageLoaded();
	};
	
	this.addAirTime = function(min,pin) {
		return reUpPf.addAirTime(min,pin);
	};
	
	this.clickOnContinue = function() {
		return reUpPf.clickOnContinue();
	};
	
	this.goToConfirmOrderPage = function() {	
		return reUpPf.goToConfirmOrderPage();
	};
	
	this.clickOnDone = function() {
		return reUpPf.clickOnDone();
	};
	
	this.goToHomePage = function() {
		return reUpPf.goToHomePage();
	};
	
	this.enterMin = function(min) {
		return reUpPf.enterMin(min);
	};
	
	this.enterMinInside = function(min) {
		return reUpPf.enterMinInside(min);
	};
	
	this.goToRefillWithPin = function(){
		return reUpPf.goToRefillWithPin();
	};
	this.isRefillPagePageLoaded = function(){
		return reUpPf.isRefillPagePageLoaded();
	};
	this.enterPin = function(pin){
		return reUpPf.enterPin(pin);
	}
	
	this.clickOnManageReserve = function(){
		return switchplanpf.clickOnManageReserve();
	}
	
	this.isManageReserveLoaded = function(){
		return switchplanpf.isManageReserveLoaded();
	}
	
	this.clickOnApplyNow = function(){
		return switchplanpf.clickOnApplyNow();
	}
	this.isPopUpLoaded = function(){
		return switchplanpf.isPopUpLoaded();
	};
	this.clickOnAddnow = function(){
		return switchplanpf.clickOnAddnow();
	};

	
	};

module.exports = new ReUpPO;