'use strict';

var menu = require("./menu.pf");
var ElementUtil = require("../util/element.util");

var HomePage = function() {

	this.load = function(url) {
		browser.get(url);
	};

	this.isHomePageLoaded = function() {
		return menu.isHomePageLoaded();
	};

	this.goToShop = function() {
		return menu.goToShop();
	};

	this.goToActivate = function() {
		return menu.goToActivate();
	};

	this.isActivateLoaded = function() {
		return ElementUtil.waitForUrlToChangeTo(/selectdevice$/);
	};
	
	this.goToRefill = function() {
		return menu.goToRefill();
	};

	this.goToMyAccount = function() {
		menu.goToMyAccount();
	};
	
	this.isMyAccountPageLoaded = function() {	
		return ElementUtil.waitForUrlToChangeTo(/collectusername$/);
	};
		//reactivation scenario-aswathy
	this.myAccount = function() {
		return menu.goToMyAccount();
	};
	//reactivation scenario-aswathy
	this.homePageLoad = function() {
		return menu.goToHomePage();
	};
};

module.exports = new HomePage;
