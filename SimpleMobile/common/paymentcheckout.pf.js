'use strict';
var ElementUtil = require("../util/element.util");

var PaymentCheckout = function() {

	this.creditCardNumberTextbox = element.all(By.id('formName.number_mask')).get(1);
	this.nickNameTextbox = element.all(By.id('nickname')).get(1);	
	this.cvvTextbox = element.all(By.id('cvv')).get(1);
	//this.monthDropdown = element.all(By.id('exp_month')).get(0).$('[label="Jul"]');;
	//this.yearDropdown = element.all(By.id('exp_year')).get(0).$('[label="2024"]');;
	this.firstNameTextbox = element.all(By.id('fname')).get(1);
	this.lastNameTextbox = element.all(By.id('lname')).get(1);
	this.addressOneTextbox = element.all(By.id('address1')).get(1);
	this.cityTextbox = element.all(By.id('city')).get(1);
	this.stateDropdown = element.all(By.id('state')).get(0).$('[label="CA"]');;
	this.countryDropdown = element.all(By.id('country')).get(0).$('[label="USA"]');;
	this.zipcodeTextbox = element.all(By.id('zipcode')).get(1);
	this.placeorderBtn = element.all(By.id('btn_placeyourorder')).get(0);
	//this.btncontinuetopayment = element.all(by.id('btn_continuetopayment')).get(2);
	this.purchaseSummaryDoneBtn = element.all(By.id('btn_done')).get(1);
	this.serveyNoThanksBtn = element.all(By.css("[ng-click='action()']")).get(1);
	this.creditCardTab =element.all(By.css("[ng-click='selectTab(1)']")).get(0);
	this.monthDropDown = element.all(by.className('selectize-input')).get(0); //ui-select-match ng-scope
	this.monthDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(6);
	this.yearDropDown = element.all(by.model('$select.search')).get(1);
	this.yearDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(7);
	this.state = element.all(by.model('$select.search')).get(2);
	this.stateDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	this.completeCheckout = function() {
		console.log("IAM HERE..");
		//this.creditCardTab.click();
		this.creditCardNumberTextbox.clear().sendKeys("372523281441007");
		//this.nickNameTextbox.clear().sendKeys("TracFone");
		this.cvvTextbox.clear().sendKeys("1234");
		//this.monthDropdown.click();
		//this.yearDropdown.click();
		this.monthDropDown.click();
		this.monthDropDownSelect.click();
		this.yearDropDown.click();
		this.yearDropDownSelect.click();
		this.firstNameTextbox.clear().sendKeys("Test");
		this.lastNameTextbox.clear().sendKeys("Test");
		this.addressOneTextbox.clear().sendKeys("1295 Charleston Road");
		//this.stateDropdown.click();
		
		//this.countryDropdown.click();
		
		this.cityTextbox.clear().sendKeys("Mountain View");
		this.state.click();//clear().sendKeys("CA");//click();
		this.stateDropDownSelect.click();
		this.zipcodeTextbox.clear().sendKeys(94043);
		this.placeorderBtn.click();
	};
	
	this.isCheckoutSuccess = function(){
		return ElementUtil.waitForUrlToChangeTo(/purchasesummary$/);
	};
	
	this.completeSummaryProcess = function() {
		ElementUtil.waitForElement(this.purchaseSummaryDoneBtn);
		this.purchaseSummaryDoneBtn.click();
	};
	
	this.isServeyCompleted = function() {
		ElementUtil.waitForElement(this.serveyNoThanksBtn);
		this.serveyNoThanksBtn.click();
		return ElementUtil.waitForUrlToChangeTo(/$/);
	};
};

module.exports = new PaymentCheckout;