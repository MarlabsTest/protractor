'use strict';

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('../SimpleMobile/properties/Protractor.properties');
var homePage = require("../../common/homepage.po");
var Constants = require("../../util/constants.util");

/*
 * This Spec is to Load a SimpleMobile Home page.
 */

describe('SimpleMobile HomePage', function() {

	it('should load the SimpleMobile home page', function(done) {
		//homePage.load(Constants.ENV[browser.params.environment]);
		homePage.load(properties.get('weburl.'+browser.params.environment));
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();
	});

});
