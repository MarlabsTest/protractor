module.exports = {

	// selenium address
	// seleniumAddress: (process.env.SELENIUM_URL || 'http://localhost:5012/wd/hub'),

	// A base URL for your application under test.
	baseUrl: '/',
  
	//Timeouts
	syncScriptsTimeout: (process.env.ASYNC_TIMEOUT || 25000),
	pageTimeout: (process.env.PAGE_TIMEOUT || 5000),
	
	//other options
	untrackOutstandingTimeouts: true
	
	//directConnect: true
};