
module.exports = {

	params: {
		environment: 'sita',
		reportDir: './reports/',
		loginData: '1', // comma separated csv line numbers / All
		activationData: '1,2,3' // comma separated csv line numbers / All
	}
};