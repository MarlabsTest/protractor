module.exports = {
	suites: {
		Activation: [
			'activation/scenario/activation.withpin.withaccount.scn.js',
			// 'activation/scenario/activation.withpurchase.withaccount.scn.js',
			// 'activation/scenario/activation.byopphone.scn.js',
			// 'activation/scenario/activation.byopphone.withpurchase.scn.js',
			// 'activation/scenario/activate.internal.portin.withpin.withaccount.scn.js',
			// 'activation/scenario/activate.internal.portin.withpurchase.scn.js',
			// 'activation/scenario/activation.withport.withpin.scn.js',
			// 'activation/scenario/activation.withport.withpurchase.scn.js',
			// 'activation/scenario/activation.phoneupgrade.scn.js',
			// 'activation/scenario/reactivate.phone.with.pin.scn.js',
			// 'activation/scenario/reactivate.phone.with.purchase.scn.js',
			// 'activation/scenario/multiline.reactivation.scn.js',
		]
		,
		Myaccount: [
			// 'myaccount/scenario/myaccount.login.withmin.scn.js',
			// 'myaccount/scenario/myaccount.login.withemail.scn.js',
			// 'myaccount/scenario/myaccount.forgotpassword.scn.js',
			// 'myaccount/scenario/myaccount.editcontactinfo.scn.js',
			// 'myaccount/scenario/myaccount.managepayment.scn.js',
			// 'myaccount/scenario/myaccount.delete.manageepayment.scn.js',
			// 'myaccount/scenario/myaccount.buyaddon.withinaccount.scn.js',
			// 'myaccount/scenario/myaccount.adddevice.scn.js',
			// 'myaccount/scenario/myaccount.reup.payservice.scn.js',
			// 'myaccount/scenario/myaccount.buy.ild.withenrollment.scn.js',
		]
		,
		AutoReUp: [
			// 'autoreup/scenario/autoreup.scn.js',
			// 'autoreup/scenario/deenroll.autoreup.scn.js'
		]
		,
		ReUp: [
			// 'reup/scenario/reup.withpin.outside.scn.js',
			// 'reup/scenario/reup.withpurchase.outside.scn.js'
		]
		,
		Shop: [
			// 'shop/scenario/shop.buyphones.scn.js',
			// 'shop/scenario/shop.buysimcard.scn.js',
			// 'shop/scenario/shop.buyplans.smcustomer.scn.js',
			// 'shop/scenario/shop.buyserviceplan.outsideaccount.newuser.scn.js'
		]
	}
};