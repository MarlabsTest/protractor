var jasmineReporter = require('jasmine-reporters');
var HTMLReporter = require('protractor-html-reporter'); 
var fs = require('fs');
var path = require('path');
var ProtractorReporter = require('protractor_datadriven_reporter');

var Reporter = (function () {
	
	this.initXmlReporter = function(browserName, reportDir) {
		var xmlReporter = new jasmineReporter.JUnitXmlReporter({
			consolidate: true,
			consolidateAll: true,
			savePath: reportDir,
			filePrefix: '',
			modifySuiteName: function(generatedSuiteName, suite) {
				//console.log('generatedSuiteName:', generatedSuiteName);
				return browserName + '-' + generatedSuiteName;
			},
			modifyReportFileName: function(generatedFileName, suite) {
				//console.log('generatedFileName:', generatedFileName);
				return browserName + '-' + generatedFileName;
			}
		});
		
		jasmine.getEnv().addReporter(xmlReporter);
	};
	
	this.createHtmlReport = function(browserName, reportDir) {	
	
		var files = fs.readdirSync(reportDir)
		.map(function(fileName) { 
			return { name: fileName,
				time: fs.statSync(reportDir + '/' + fileName).mtime.getTime()
			}; 
		})
		.sort(function(file1, file2) { return file1.time - file2.time; })
		.map(function(file) { return file.name; });
		
		files.forEach(function(file) {
				if(path.extname(file) === '.xml') {
					console.log('File: ' + file);
					new HTMLReporter().from(reportDir + '/' + file, {
						reportTitle: 'Report',
						outputPath: reportDir,
						screenshotPath: './screenshots',
						testBrowser: browserName,
						modifiedSuiteName: true,
						screenshotsOnlyOnFailure: true
					});
				}
			});	
	};

	this.initHtmlReporter = function(browserName, reportDest) {
		jasmine.getEnv().addReporter(new ProtractorReporter.TestResultsGenerator({
			reportDir : reportDest,
			takeScreenshots : true,
			takeScreenshotsOnlyOnFailures : false,
			cleanReportDir: false
		}));  
	};
	
	this.generateReport = function(baseReportDir, reportDir) {
		var brand = 'SimpleMobile';
		console.log('### GENERATING REPORTS ###');
		new ProtractorReporter.TestReportGenerator(baseReportDir, reportDir,brand).generateReport();		
		console.log('### REPORTS GENERATED  ###');
	};
	
	return this;
})();

module.exports = Reporter;