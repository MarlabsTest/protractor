'use strict';

var Constants = {
	ENV : {site:'https://siteng.simplemobile.com/',test:'https://test2.simplemobile.com/'},
	LOGIN_CSV_FILE: './testdata/login.td.csv',
	LOGIN_JSON_FILE: './testdata/login.td.json',
	SHOP_PLANS_CSV_FILE: './testdata/shopplans.td.csv',
	SHOP_PLANS_JSON_FILE: './testdata/shopplans.td.json',
	ACTVE_PIN_CSV_FILE: './testdata/activatepin.td.csv',
	ACTVE_PIN_JSON_FILE: './testdata/activatepin.td.json',
	BYOP_ACTVE_PIN_CSV_FILE: './testdata/activatebyoppin.td.csv',
	BYOP_ACTVE_PIN_JSON_FILE: './testdata/activatebyoppin.td.json',
	PORTING_CSV_FILE: './testdata/porting.td.csv',
	PORTING_JSON_FILE: './testdata/porting.td.json',
	UTIL_JAR_PATH: './javautils/DataGen.jar',
	ALL: 'All',
	PHONE_UPGRADE_CSV_FILE: './testdata/phoneupgrade.td.csv',
	PHONE_UPGRADE_JSON_FILE: './testdata/phoneupgrade.td.json',
	ACTVE_CDMA_PIN_CSV_FILE: './testdata/activatecdmapin.td.csv',
	ACTVE_CDMA_PIN_JSON_FILE: './testdata/activatecdmapin.td.json',
	REFILL_CSV_FILE: './testdata/refill.td.csv',
	REFILL_JSON_FILE: './testdata/refill.td.json',
	SHOP_PLANS:['30dayplan','dataonlyplan','addon'],
    DAY_PLANS_30 : {70:'monthly_360',25:'monthly_235',30:'monthly_461',40:'monthly_238',
    	            50:'monthly_240',60:'monthly_416'},
    DATAONLY_PLANS : {20:'monthly_285',40:'monthly_286'},
    ADDON_PLANS : {5:'monthly_507',10:'monthly_508'}

};

module.exports = Constants;