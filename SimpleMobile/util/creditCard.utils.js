'use strict';

var CreditCardUtil = { VISA_PREFIX_LIST : ["4539","4556", "4916", "4532", "4929", "40240071", "4485", "4716", "4" ],
		 MASTERCARD_PREFIX_LIST : [ "51","52", "53", "54", "55" ],
		 AMEX_PREFIX_LIST : [ "34", "37" ],
		 DISCOVER_PREFIX_LIST : [ "6011" ],
		 DINERS_PREFIX_LIST : [ "300","301", "302", "303", "36", "38" ],
		 ENROUTE_PREFIX_LIST : [ "2014","2149" ],
		 JCB_16_PREFIX_LIST : [ "3088","3096", "3112", "3158", "3337", "3528" ],
		 JCB_15_PREFIX_LIST : [ "2100","1800" ],
		 VOYAGER_PREFIX_LIST : [ "8699" ],
		 cvvNumber : "671",
		 amexCvv : "6715",
		 firstName : "Test",
		 lastName : "Test",
		 addOne : "1295 Charleston Road",
		 city : "Mountain View",
		 pin : "94043"
};

module.exports = CreditCardUtil;
