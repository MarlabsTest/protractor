'use strict';

var runUtil = require('./run.util');
const path = require('path');

var FLOWS = {
		SM_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_ACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpurchase.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_BYOP_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.byopphone.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_BYOP_ACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.byopphone.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_ENROLLMENT_OF_AUTOREFILL : ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','autoreup/spec/autoreup.specs.js'/*,'common/spec/myaccount.logout.spec.js'*/],
		SM_DEENROLLMENT_OF_AUTOREFILL : ['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.activateesn.spec.js','autoreup/spec/autoreup.specs.js','myaccount/spec/myaccount.login.spec.js','autoreup/spec/autoreup.deenroll.specs.js','common/spec/myaccount.logout.spec.js'],
		SM_REFILL_WITHPIN : ['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpin.outside.spec.js'],
		SM_LOGIN_WITH_EMAIL: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_LOGIN_WITH_MIN: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.loginwithmin.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_MANAGE_PAYMENT:['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_INTERNAL_PORTIN_WITHPIN_WITHACCOUNT:['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activate.internal.portin.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_INTERNAL_PORTIN_WITHPURCHASE_WITHACCOUNT:['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activate.internal.portin.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_FORGOT_PASSWORD : ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.forgotpassword.spec.js'],
		SM_DELETE_MANAGE_PAYMENT :['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.spec.js','myaccount/spec/myaccount.delete.managepayment.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_UPGRADE_FLOWS: ['activation/spec/activation.upgrade.flows.spec.js'],
		SM_PHONE_UPGRADE: ['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.phoneupgrade.spec.js'],
		SM_BYOP_UPGRADE: ['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.byop.upgrade.spec.js'],
		SM_REFILL_WITHPIN_PURCHASE : ['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','reup/spec/reup.withpurchase.outside.spec.js'],
		SM_SHOP_SELECT_PHONES : ['common/spec/simplemobile.loadhomepage.spec.js','shop/spec/shop.selectphones.spec.js'],
		SM_SHOP_BUY_PLANS:['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','shop/spec/shop.buyplans.forsmcustomer.spec.js'],
		SM_SHOP_SERVICEPLAN_INSIDE_ACC:['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.spec.js','shop/spec/shop.buyserviceplan.insideaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_ACTIVATION_WITH_PORT_PIN: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activate_with_port.js','common/spec/myaccount.logout.spec.js'],
		SM_ACTIVATION_WITH_PORT_PURCHASE: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activate_with_port_purchase.js','common/spec/myaccount.logout.spec.js'],
		SM_SHOP_BUY_SIMCARD : ['common/spec/simplemobile.loadhomepage.spec.js','shop/spec/shop.selectsimcard.spec.js'],
		SM_MYACCOUNT_ADDDEVICE: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.adddevice.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_BUY_ADDON_PLAN_WITHIN_ACC: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.buyaddon.withinaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_BUY_ILD_WITH_ENROLLMENT: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.buy.ild.withenrollment.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_EDIT_CONTACT_INFO:['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.editcontactinfo.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_SHOP_SERVICEPLAN_OUTSIDE_ACC_NEWUSER: ['common/spec/simplemobile.loadhomepage.spec.js','shop/spec/shop.buyserviceplan.outsideaccount.newuser.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_UPGRADE_ACTIVE_ESN_TO_INACTV_ESN: ['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.adddevice.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.upgrade.esnactive.to.esninactive.nonreservemin.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_MYACCOUNT_ADD_NEW_ESN:['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.addnewesn.spec.js','common/spec/myaccount.logout.spec.js'],
	    SM_REACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.login.spec.js','activation/spec/reactivate.phone.with.pin.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_REACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.spec.js','activation/spec/reactivate.phone.with.purchase.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_MYACCOUNT_MULTILINE_SERVICE_RENEWAL:['common/spec/simplemobile.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/activation.activateesn.spec.js','myaccount/spec/myaccount.reup.payservice.spec.js','common/spec/myaccount.logout.spec.js'],
		SM_MULTILINE_REACTIVATION:['common/spec/simplemobile.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.adddevice.spec.js','activation/spec/activation.activateesn.spec.js','activation/spec/multiline.deactivation.spec.js','myaccount/spec/myaccount.reup.payservice.spec.js','common/spec/myaccount.logout.spec.js']
};
	
var FlowUtil = {
	
	run: function(flow) {		
		FLOWS[flow].forEach(function (spec) {
			//console.log("running spec..",path.join(protractor.basePath, spec));
			runUtil.requireSuite(path.join(protractor.basePath, spec));
		});
	}
};

module.exports = FlowUtil;
