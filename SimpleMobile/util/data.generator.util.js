'use strict';

var loginUtil = require('./login.util');
var activationUtil = require('./activation.util');
var activationCdmaUtil = require('./cdmaactivation.util');
var phoneupgradeUtil = require('./phoneupgrade.util');
var byopActivationUtil = require('./byop.activation.util');
var portingUtil = require('./porting.util');
var shopUtil = require('./shopplans.util');
var refillUtil = require('./refill.util');

var TestDataGenerator = {
	
	generateTestData: function() {
		//loginUtil.prepareLoginData();
		activationUtil.prepareActivationData();
		phoneupgradeUtil.prepareActivationData();
		byopActivationUtil.prepareActivationData();
		//activationCdmaUtil.prepareCdmaActivationData();
		portingUtil.prepareActivationData();
		shopUtil.prepareShopplansData();
		refillUtil.prepareRefillData();
	}
};

module.exports = TestDataGenerator;