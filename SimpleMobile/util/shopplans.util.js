var jasmineReporter = require('jasmine-reporters');
var HTMLReporter = require('protractor-html-reporter');
var csvJsonUtil = require('./csvjson.util.js');
var Constants = require('./constants.util.js');
var CompareCsvJsonUtil = require('./comparecsvjson.util.js');

var fs = require('fs');
var path = require('path');
var util = require('util');

module.exports = {

	prepareShopplansData : function() {

		if (!CompareCsvJsonUtil.isJsonFileExist(Constants.SHOP_PLANS_JSON_FILE)
				|| CompareCsvJsonUtil.isCsvModifyed(
						Constants.SHOP_PLANS_CSV_FILE,
						Constants.SHOP_PLANS_JSON_FILE)) {
			console.log("Updating New data from CSV to JSON file..");
			csvJsonUtil.convertCsvToJson(Constants.SHOP_PLANS_CSV_FILE,
					Constants.SHOP_PLANS_JSON_FILE);
		}
	},

	getTestData : function() {

		var activationDataIndex = browser.params.activationData;
		var jsonObj = csvJsonUtil.getJsonData(Constants.SHOP_PLANS_JSON_FILE);
		console.log('jsonObj', jsonObj);
		console.log('Selected data:', activationDataIndex);

		if (Constants.ALL === activationDataIndex) {
			return jsonObj;
		}
		return this.getSelectedData(jsonObj, activationDataIndex.toString()
				.split(','));
	},

	getSelectedData : function(json, datas) {
		var selectedData = [];

		datas.forEach(function(data, index) {
			selectedData.push(json[data - 1]);
		});

		return selectedData;
	}
};