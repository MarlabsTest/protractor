'use strict';
var CreditCardUtil = require("./creditCard.utils");

var CommonUtil = {
	
		getCvv:function(cardType){
		console.log("inside util cvv *********");
			var cvv;
			if (cardType == "Amex"){
				cvv = CreditCardUtil.amexCvv;
				}
			else{
				cvv = CreditCardUtil.cvvNumber;
				}
			return cvv;
			},
			
	getLastDigits: function(inputValue, noOfDigits) {
		var lastDigits = "";
		var len = inputValue.length;
		
		if(noOfDigits > 0)
		{
			if(noOfDigits <= len)
			{
				lastDigits = inputValue.toString().substr( len - noOfDigits);
			}
			else if(noOfDigits > len)
			{
				lastDigits = inputValue.toString();
			}
		}
		
		return lastDigits;
	},
	
	reverseString: function(str){
        return str.split('').reverse().join('');
	},
	
	isByopEsn: function(inputEsn){
		if(inputEsn.startsWith('PH')){
			return true;
		}
		else{
			return false;
		}
	},
	
	isARFlow: function(arValue){		
		if(arValue.startsWith('Y')){
			return true;
		}else{
			return false;
		}
	},
	/*Upgrade ESN active to ESN inactive- MIN reserve and non reserve scenario.
	 * This method check the PhoneStatus field in input file and return the result*/
	isInactive: function(varPhoneStatus){		
		if(varPhoneStatus == "INACTIVE_NONRESERVE" || varPhoneStatus == "INACTIVE_RESERVE"){
			return true;
		}else{
			return false;
		}
	}
};

module.exports = CommonUtil;