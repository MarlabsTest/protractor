'use strict';

var shopPf = require("./shop.pf");
var shopServicePlanPf = require("./shopserviceplan.pf");
var paymentCheckoutPf = require("../common/paymentcheckout.pf");
var selectphonespf = require("./selectphones.pf");

var Shop = function() {
	

	this.goToShopServicePlan = function() {
		return shopPf.goToShopServicePlan();
	};
	
	this.buyServicePlan = function() {
		return shopServicePlanPf.buyServicePlan();
	};

	this.isShopPageLoaded = function() {
		return shopPf.isShopPageLoaded();
	};
	//this method will navigate to enter the phone number
	this.isShopServicePlanPageLoaded = function() {
		return shopServicePlanPf.isShopServicePlanPageLoaded();
	};
	this.shopServicePlanAsNewCustomer = function(planName,planType){
		console.log('monthlyPlan=' ,planName);
		console.log('planType=' ,planType);
		shopServicePlanPf.ServicePlanForNewCustomer(planName,planType);
	};	
    //Newly added by
    this.choosePlanByName = function(planType,planName) {
          shopServicePlanPf.choosePlanByName(planType,planName);
    };
    
    this.doAutoRefill = function() {
        shopServicePlanPf.doAutoRefill();
    };
  
    this.doOneTimePurchase = function() {
    	shopServicePlanPf.doOneTimePurchase();
  	};

    this.providePhoneNumber = function(phoneNumber){
    	shopServicePlanPf.providePhoneNumber(phoneNumber);
    }
     this.isActivatePageLoaded = function(){
		return shopServicePlanPf.isActivatePageLoaded();
	};
	//for SM Multiline add
	 this.choosePlanByNameForNewLine = function(planType,planName) {
          shopServicePlanPf.choosePlanByNameForNewLine(planType,planName);
    };

	
	//this method checks whether the service plan page is loaded 
	this.isShopServicePlansListed = function() {
		return shopServicePlanPf.isShopServicePlansListed();
	};
	
	//this method is to select auto refill option
	this.registerAutoRefill = function() {
		shopServicePlanPf.registerAutoRefill();
	};
	
	//this method clicks the shop link
	this.goToShopPhones = function() {
		return selectphonespf.goToShopPhones();
	};
	
	//this method clicks the simcard link
	this.goToShopSimCard = function() {
		return selectphonespf.goToShopSimCard();
	};
	
	//this method checks whether the page is navigated to enter the byop
	this.isShopByopPageLoaded = function() {
		return selectphonespf.isShopByopPageLoaded();
	};
	
	//this method checks whether  itis navigated to enter the carrier page
	this.isShopCarrierPageLoaded = function() {
		return selectphonespf.isShopCarrierPageLoaded();
	};
	
	
	//this this method checks whether the page is navigated to enter the simcard shop page
	this.isShopSimCardPageLoaded = function() {
		return selectphonespf.isShopSimCardPageLoaded();
	};
	
	//this method checks whether the page is navigated to enter the zipcode 
	this.isShopPhonePageLoaded = function() {
		return selectphonespf.isShopPhonePageLoaded();
	};
	
	//this method enters the zipcode and confirm the same.
	this.enterZipcode = function(zipcode) {
		return selectphonespf.enterZipcode(zipcode);
	};
	
	//this method navigates to buy the phones 
	this.isSelectPhonesPageLoaded = function() {
		return selectphonespf.isSelectPhonesPageLoaded();
	};
	
};

module.exports = new Shop;