'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");

//Firstly go to the shop page 
//click on the select phones
//enter the ZIP Code of the area where you will be using your simple mobile
//confirm the zip code by clicking yes button
//it redirects to the page where we can purchase the phones

describe('Simple Mobile buy phones', function() {

	//click on the shop link which is shown in the homepage
	//should check whether the shop phone page is loaded or not
	it('should open shopping page', function(done) {
		homePage.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);
		done();
	});
	
	//click on the bring a phone now and will be redirected byop page
	it('should open shop byop page,', function(done) {
		shop.goToShopSimCard();		 
		expect(shop.isShopByopPageLoaded()).toBe(true);
		done();
	});	
	
	
	//click on the check compatability button and it will be redirected to carrier page
	it('should open Carrier page,', function(done) {				 
		expect(shop.isShopCarrierPageLoaded()).toBe(true);
		done();
	});	
	
	//click on the check compatability button and it will be redirected to carrier page
	it('should open simcard page,', function(done) {				 
		expect(shop.isShopSimCardPageLoaded()).toBe(true);
		done();
	});	
	
});
