'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var myAccount = require("../../myaccount/myaccount.po");
var activation = require("../../activation/activation.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
//Firstly go to the shop page 
//click on the select service plans
//enter the MIN which is activated in the simple mobile
//choose the plans for your smartphone and add to the cart
//make the payment and place an order
//finally it redirects to the confirmation page

describe('Simple Mobile buy service plans', function() {

	//click on the shop link which is shown in the homepage
	//should check whether the shop service plan page is loaded or not
	it('should open shopping page', function(done) {
		homePage.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);
		done();
	});
	
	//click on the select service plans now and will be redirected to enter the MIN page
	it('should open shop phone page', function(done) {
		shop.goToShopServicePlan();
		//expect(shop.isShopServicePlanPageLoaded()).toBe(true);
		expect(shop.isShopServicePlansListed()).toBe(true);
		done();
	});	
	
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.simplemobile.shopPlan;
		var planName		= sessionData.simplemobile.planName;
		//var isAutoRefill	= 'N';//sessionData.simplemobile.autoRefill;
		var min = dataUtil.getMinOfESN(sessionData.simplemobile.esn);
		console.log("shop plan for MIN  :::::" +min);
		shop.selectPlan(planType,planName);
		shop.providePhoneNumber(min);
		shop.oneTimePurchaseForShop();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
	
	it(' click on continue to payment button and should be asked to fill cc details', function(done) {
		//myAccount.continueToPayment();
		//	myAccount.goToPaymentTab();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	it(' enter cc and billing address details,click on proceed and navigate to final indtructions page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	it(' click on the continue button  and  navigate to Succcess page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it(' click on the done button  navigate to survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it(' click on the thank you button and navigate to the account dashboard ', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 
	
});
