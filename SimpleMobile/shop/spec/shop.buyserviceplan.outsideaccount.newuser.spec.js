'use strict';

var homePagePo = require("../../common/homepage.po");
var shopPo = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");
var activationPo = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsnSim ={};
var generatedMin ={};

describe('SM Buy Service plan Outside Account- New User', function() {

	it('Select SHOP option from Home page', function(done) {
		homePagePo.goToShop();	
		expect(shopPo.isShopPageLoaded()).toBe(true);	
		done();  
	});
	
	it('select Shop Plans and load the service plan page', function(done) {
		shopPo.goToShopServicePlan();
		expect(shopPo.isShopServicePlansListed()).toBe(true);	
		done();  
	});
	
	it('Shop service plan as a new customer', function(done) {
		console.log('monthlyPlan=' ,sessionData.simplemobile.planName);
		console.log('planType =' ,sessionData.simplemobile.planType);
		shopPo.shopServicePlanAsNewCustomer(sessionData.simplemobile.planName,sessionData.simplemobile.planType);		
		expect(shopPo.isActivatePageLoaded()).toBe(true);	
		done();  
	});
	it('navigate to activation flow',function(done){		
		activationPo.gotToEsnPage();
		//expect(activationPo.esnPageLoaded()).toBe(true);
		expect(activationPo.isSIMPage()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter married SIM number', function(done) {
		var esnval = DataUtil.getESN(sessionData.simplemobile.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.simplemobile.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.simplemobile.esn = esnval;
		activationPo.enterMarriedSim(simval);
		sessionData.simplemobile.sim = simval;
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		activationPo.checkBoxCheck();
		activationPo.continueESNClick();
		expect(activationPo.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	/*Enter a 4 digit security pin 
	 *Expected result - Page to opt for porting or new number will be displayed
	 */
	/*it(' enter Security pin', function(done) {
		activationPo.enterPin("1234");
		activationPo.clickOnPinContinue();
		//expect(activation.isSIMPage()).toBe(true);
		expect(activationPo.keepMyPhonePageLoaded()).toBe(true);
		done();		
	});*/
	
	/*Enter a valid zipcode 
	 *Expected result - Page to proceed with plan purchase will be displayed
	 */	
	it('enter Zip code and continue', function(done) {
		activationPo.enterZipCode(sessionData.simplemobile.zip);		
		expect(activationPo.activationAccountPageLoaded()).toBe(true);
		done();		
	});
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	
	it('load the account creation page', function(done) {
		activationPo.clickonAccountCreationContinueBtn();
		expect(activationPo.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activationPo.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.simplemobile.username = emailval;
		sessionData.simplemobile.password = password;
		expect(activationPo.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Checkout Page will be shown
	 */
	it('load the checkout page', function(done) {
		activationPo.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 */	
	/*it('load the payment form', function(done) {
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	*/
	/*Enter the credit card and billing details in the payment form 
	 *
	 */	
	it('enter payment details', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();	
	}); 
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	 */	
	it('load the final instruction page', function(done) {
		myAccount.placeMyOrder();
		expect(activationPo.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('load the summary page', function(done) {
		activationPo.finalInstructionProceedPurchase();
		expect(activationPo.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('load the survey page', function(done) {
		activationPo.clickOnSummaryBtnPurchase();
		expect(activationPo.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('redirect to the account dashboard page', function(done) {
		activationPo.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.simplemobile.esn);	
		DataUtil.updateMin(sessionData.simplemobile.esn);
		var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
		console.log("MIN is  :::::" +min);
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	
});
	
