'use strict';

var homePagePo = require("../../common/homepage.po");
var shopPo = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");
var activationPo = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsnSim ={};
var generatedMin ={};

describe('SM Buy Service plan Outside Account- New User', function() {

	it('select Shop Plans and load the service plan page', function(done) {
		myAccount.chooseBuyPlan();	
		expect(shopPo.isShopServicePlansListed()).toBe(true);	
		done();  	
	});	
	
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.simplemobile.shopPlan;
		var planName		= sessionData.simplemobile.planName;
		shopPo.selectPlan(planType,planName);
		shopPo.oneTimePurchaseForShop();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
		
	it(' enter cc and billing address details,click on proceed and navigate to final indtructions page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activationPo.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	it(' click on the continue button  and  navigate to Succcess page', function(done) {
		activationPo.finalInstructionProceedPurchase();
		expect(activationPo.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it(' click on the done button  navigate to survey page', function(done) {
		activationPo.clickOnSummaryBtn();
		expect(activationPo.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it(' click on the thank you button and navigate to the account dashboard ', function(done) {
		activationPo.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 
			
});
	
