'use strict';
var ElementUtil = require("../util/element.util");
var ConstantsUtil = require("../util/constants.util");

var ShopServicePlan = function() {


    //Newly added $$
    this.smartPhonePlansTab = element(By.xpath("//span[@translate='BAT_24184']"));
    this.payAsGoPlansTab = element(By.xpath("//span[@translate='ACRE_6801']"));
    this.addOnPlansTab = element(By.xpath("//span[@translate='BAT_24571']"));
    
    this.thirtydayPlans = element.all(By.id('monthly_238'));
    this.dataonlyPlans = element.all(By.id('monthly_285'));
    this.addOnPlans = element.all(By.id('monthly_102'));

	
	this.oneTimePurchaseBtn = element.all(By.id('btn_onetimepurchase'));
	//this.autoRefillBtn = element.all(By.id('btn_enrollinautorefill')).get(1);
	this.autoRefillBtn = element(By.id('addToCartEnroll_popup_btn'));
	//this.actualOneTimePurchase = element.all(By.id('btn_onetimepurchase')).get(1);
	this.actualOneTimePurchase = element(By.id('addToCartOneTimePurchpopup_btn'));
	this.phoneNumberTextbox = element(By.css('input[id="device-info"]'));
	this.phoneNumberContinueBtn = element(By.css('button[id="saveDeviceSubmit_btn"]'));
	this.addPopUpOne = element(By.css("[ng-click='cancel()']"));
	
	this.thirtydayPlanTab = element(By.xpath("//span[@translate='BAT_24531']"));	
	
	//this.oneTimePurchaseBtn = element.all(By.id('btn_onetimepurchase')).get(1);
	
	this.dataonlyplanTab = element(By.xpath("//span[@translate='BAT_24913']"));
	this.newCustomerLink = element(By.css("[ng-click='newCustomer()']"));
	this.buyServicePlanBtn = element.all(by.css('[ng-click="action()"]')).get(2);
	
	this.autoReupPopup = element(by.className('modal-dialog'));
	this.servicePlanLoaded = element(by.css("h1[translate='BAT_24565']"));
	
	
	this.choosePlanByName = function(planType,planName) {
      
		console.log("plan type:::: ",planType);
		
		console.log("plan type0:::: ",ConstantsUtil.SHOP_PLANS[0]);
		console.log("plan type1:::: ",ConstantsUtil.SHOP_PLANS[1]);
		console.log("plan type2:::: ",ConstantsUtil.SHOP_PLANS[2]);
		
        if (planType == ConstantsUtil.SHOP_PLANS[0]) {
        	console.log("plan 0 ");
              //element(By.id(ConstantsUtil.DAY_PLANS_30[planName])).click();
			  //element(by.css('button[id="monthly_235"]')).click();
			  element(by.css('button[id="'+ConstantsUtil.DAY_PLANS_30[planName]+'"]')).click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
        	console.log("plan 1 ");
              element(By.id(ConstantsUtil.DATAONLY_PLANS[planName])).click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
        	console.log("plan 2 ");
              element(By.id(ConstantsUtil.ADDON_PLANS[planName])).click();
        }
        
        /*
        //check whether autoreup popup shown (wait for 2 secs) and select option if popup shown
        ElementUtil.waitForElement(this.autoReupPopup, 2000);
        if(this.autoReupPopup.isPresent())
        {
        	if(isAutoRefill == 'YES' || isAutoRefill == 'Y'){
                this.autoRefillBtn.click();
            }else{
                this.actualOneTimePurchase.click();
            }
        }
        
        */
        
  
  };
	
  	this.doAutoRefill = function() {
		this.autoRefillBtn.click();
	};
	
	this.doOneTimePurchase = function() {
		this.actualOneTimePurchase.click();
	};
	
	this.providePhoneNumber = function(phoneNumber){
	//ElementUtil.waitForElement(this.phoneNumberTextbox);
	this.phoneNumberTextbox.sendKeys(phoneNumber);
	//ElementUtil.waitForElement(this.phoneNumberContinueBtn);
	this.phoneNumberContinueBtn.click();
	};
	
	//this will navigate to enter the phone number
	this.isShopServicePlanPageLoaded = function() {
		return ElementUtil.waitForUrlToChangeTo(/shop\/plans$/);
	};
	
	this.buyServicePlan = function() {
		this.buyServicePlanBtn.click();
	};
	
	
	//check whether the page is navigated to the service plan page
	this.isShopServicePlansListed = function() {
	//	ElementUtil.waitForElement(this.addPopUpOne);
	//	this.addPopUpOne.click();
	//	return ElementUtil.waitForUrlToChangeTo(/serviceplan$/);
		return this.servicePlanLoaded.isPresent();
		
	};
	this.ServicePlanForNewCustomer = function(planName,planType){
		this.selectPlan(planName,planType);
		this.newCustomerLink.click();
	};
	this.selectPlan = function(planName,planType){		
		if (planType == ConstantsUtil.SHOP_PLANS[0]){
			element(by.css('button[id="'+ConstantsUtil.DAY_PLANS_30[planName]+'"]')).click();			
      }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
		element(by.css('button[id="'+ConstantsUtil.DATAONLY_PLANS[planName]+'"]')).click();
      }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
		element(by.css('button[id="'+ConstantsUtil.ADDON_PLANS[planName]+'"]')).click();    	
      }     
				
	}
	
	this.isActivatePageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/selectdevice$/);
	};
	
	//SM MLD New Line
	this.choosePlanByNameForNewLine = function(planType,planName) {
        if (planType == ConstantsUtil.SHOP_PLANS[0]) {
        	console.log("plan 0 ");
              element(by.css('button[id="'+ConstantsUtil.DAY_PLANS_30[planName]+'"]')).click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
              element(by.css('button[id="'+ConstantsUtil.DATAONLY_PLANS[planName]+'"]')).click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
        	element(by.css('button[id="'+ConstantsUtil.ADDON_PLANS[planName]+'"]')).click(); 
        }
  };
  
  this.oneTimePurchaseForShop = function(){
	    this.actualOneTimePurchase.click();
   };
	

};

module.exports = new ShopServicePlan;