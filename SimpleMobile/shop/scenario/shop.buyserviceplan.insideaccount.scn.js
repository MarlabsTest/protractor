'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var shopplansUtil = require('../../util/shopplans.util');
var sessionData = require("../../common/sessiondata.do");

describe('Buy Service Plan Inside Account', function() {

	var shopplansData = shopplansUtil.getTestData();
	drive(shopplansData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.planType = inputActivationData.shopplan;
				sessionData.simplemobile.planName = inputActivationData.PlanName;
				sessionData.simplemobile.cardType = inputActivationData.cardType;
				sessionData.simplemobile.esnsToReactivate = [];
				done();
			});
			FlowUtil.run('SM_SHOP_SERVICEPLAN_INSIDE_ACC');
		}).result.data = inputActivationData;
	});
});
