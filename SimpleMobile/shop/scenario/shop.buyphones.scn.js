'use strict';
//Firstly go to the shop page 
//click on the select phones
//enter the ZIP Code of the area where you will be using your simple mobile
//confirm the zip code by clicking yes button
//it redirects to the page where we can purchase the phones

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('SM buy phones', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('SM_SHOP_SELECT_PHONES');
		}).result.data = inputActivationData;
	});
});