'use strict';
//Firstly go to the shop page 
//click on the select service plans
//enter the MIN which is activated in the Simple Mobile
//choose the plans for your smartphone and add to the cart
//make the payment and place an order
//finally it redirects to the confirmation page

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var shopplansUtil = require('../../util/shopplans.util');
var sessionData = require("../../common/sessiondata.do");

describe('Simple Mobile buy service plans', function() {

	var shopplansData = shopplansUtil.getTestData();
	console.log('activationData:', shopplansData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(shopplansData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.shopPlan = inputActivationData.shopplan;
				sessionData.simplemobile.planName = inputActivationData.PlanName;
				sessionData.simplemobile.autoRefill = inputActivationData.AutoRefill;
				sessionData.simplemobile.cardType = inputActivationData.cardType;
				sessionData.simplemobile.esnsToReactivate = [];
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('SM_SHOP_BUY_PLANS');
		}).result.data = inputActivationData;
	});
});