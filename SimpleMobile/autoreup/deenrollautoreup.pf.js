'use strict';
var ElementsUtil = require("../util/element.util");

	/*	
	This PF will De Enroll user from Auto Pay service	
	*/

var DeEnrollAutoPay = function() {
	this.autoReupBtn =  element.all(by.css('[ng-click="action()"]')).get(3);
	this.reasonForDeEnrollDropDown = element(by.id('deenroll-selectedreason')).$('[value="Not needed"]');
	//this.clickOnYesBtnForDeEnroll = element(by.xpath("//*[@id='modal-body']/div[2]/form/div[5]/div/div[3]/tf-button-primary/span[1]/button"));
	this.clickOnYesBtnForDeEnroll = element.all(by.css('[ng-click="action()"]')).get(1);
	//this.autoPayButtn = element.all(by.buttonText('Auto Pay')).get(1);
	this.autoPayButtn = element.all(by.css('[ng-click="action()"]')).get(0);
	this.cancelAutoReUpBtn = element.all(by.css('[ng-click="action()"]')).get(3);
	this.cancelAutoReUpPopup = element(by.css('.modal-dialog.modal-md'));
                                                        
	/*
		This function will click on "Cancel Auto Pay Enrollment" link .
	*/
	this.clickOnAutoReupBtn = function(){
		ElementsUtil.waitForElement(this.autoReupBtn);
		 this.autoReupBtn.click();	
	};
	
	/*
		This function will select reason for De Enrollment of Auto pay from drop down 
	*/
	
	this.selectReasonForCancelAutorefil = function(){	
		ElementsUtil.waitForElement(this.reasonForDeEnrollDropDown,10000);
		 this.reasonForDeEnrollDropDown.click();
		 this.clickOnYesBtnForDeEnroll.click();
	};
	
	/*
	To cancel the autoreup service
	*/
	 this.cancelAutoReUp = function(){
		 this.cancelAutoReUpBtn.click();
	 };
	 
	 /*
	To cancel the autoreup service
	*/
	 this.isCancelAutoReUpPopupShown = function(){
		 ElementsUtil.waitForElement(this.cancelAutoReUpPopup);
		 return this.cancelAutoReUpPopup.isPresent();
	 };
	
	/*
	This function will check whether the manage enroll page is loaded or not 
*/
	this.isManageEnrollPageLoaded = function(){		
		return ElementsUtil.waitForUrlToChangeTo(/manageenroll$/);
	};
};

module.exports = new DeEnrollAutoPay;