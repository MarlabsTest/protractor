'use strict';

var deEnrollAutoPay = require("../deenrollautoreup.po");
var myAccount = require("../../myaccount/myaccount.po");

describe('De-Enrolling for Auto ReUp service', function() {
	
	//To click the autoreup button in dashboard page
	it('should click the auto reup button in dashboard page', function(done) {
		deEnrollAutoPay.clickOnAutoReupBtn();
		expect(deEnrollAutoPay.isManageEnrollPageLoaded()).toBe(true);
		done();
	});
	
	//To click the cancel auto reup from manage enroll page
	it('should click the cancel auto reup button', function(done) {
		deEnrollAutoPay.cancelAutoReUp();
		expect(deEnrollAutoPay.isCancelAutoReUpPopupShown()).toBe(true);
		done();
	});
	
	//To cancel the auto reup service
	it('should select reason and cancel the auto reup', function(done) {
		deEnrollAutoPay.selectReasonForCancelAutorefil();
		expect(myAccount.isLoaded()).toBe(true);
		done();
	});
});
