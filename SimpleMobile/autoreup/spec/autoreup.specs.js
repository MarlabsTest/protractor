'use strict';

var autoReUp = require("../autoreup.po");
var sessionData = require("../../common/sessiondata.do");
var activation = require("../../activation/activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var home = require("../../common/homepage.po");
var dataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil = require("../../util/common.functions.util");

describe('Enrolling for Auto ReUp service : ', function() {

	/*
	*This scenario will click on Auto Pay link in home page and checks whether
	*page is redirected to expected url
	*/

	it('should click on auto reup',function(done){		
		autoReUp.goToPayServiceAndAutoPay();
		expect(autoReUp.isPayServiceAndAutoPayLoaded()).toBe(true);
		done();
	});
	
	/*
	This scenario will send  mailId  or phone number and click on continue button
	checks whether page is redirected to expected url
	*/
	it('should enter mailId  or phone number and click on continue button',function(done){
		console.log("min of esn",dataUtil.getMinOfESN(sessionData.simplemobile.esn));
		autoReUp.enterPhoneNumberOrMailId(dataUtil.getMinOfESN(sessionData.simplemobile.esn));
		//expect(autoReUp.isPhoneNumberOrMailIdLoaded()).toBe(true);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
	/*
	This scenario will send  password and click on continue button
		checks whether page is redirected to expected url
	* /
	it('should enter password and login to account',function(done){		
		console.log("pwd ::",sessionData.simplemobile.password,":");
		autoReUp.enterPasswordForAutoPay(sessionData.simplemobile.password);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	*/
	
	/*
	 * Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 *
	 */	
	it('should load the payment form', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		//expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 * /	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	 * /	
	it('should load the final instruction page', function(done) {
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	*/
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(home.isHomePageLoaded()).toBe(true);
		done();		
	});
	
	/*
	it('should click on the thank you button and will be redirected to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();	
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(sessionData.simplemobile.esn,'0','6','SIMPLE_MOBILE_ACTIVATION_WITH_PURCHASE','true','false');
		console.log('ITQ_DQ_CHECK table updated!!');	
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	*/
});
