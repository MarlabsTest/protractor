'use strict';

var homePage = require("../../common/homepage.po");
var drive = require('jasmine-data-provider');
var refillUtil = require('../../util/refill.util');
var sessionData = require("../../common/sessiondata.do");
var FlowUtil = require('../../util/flow.util');

describe('SM ReUp Service with PIN', function() {

	var activationData = refillUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.redemptionPinPartNumber = inputActivationData.redemptionPin;
				sessionData.simplemobile.esnsToReactivate = [];
				done();
			});
			FlowUtil.run('SM_REFILL_WITHPIN');	
		}).result.data = inputActivationData;
	});
});