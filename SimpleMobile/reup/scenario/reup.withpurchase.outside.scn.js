'use strict';

var homePage = require("../../common/homepage.po");
var drive = require('jasmine-data-provider');
var sessionData = require("../../common/sessiondata.do");
var FlowUtil = require('../../util/flow.util');
var refillUtil = require('../../util/refill.util');

describe('SM ReUp Service with PIN purchase', function() {
	
	var activationData = refillUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	console.log('activationData ', activationData);
//	var allActivationdata
	drive(activationData, function(inputActivationData) {
		sessionData.simplemobile.shopPlan = inputActivationData.shopPlan;
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.planName = inputActivationData.planName;
				console.log('refillData =', sessionData.simplemobile.shopPlan+"==="+sessionData.simplemobile.planName);
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.esnsToReactivate = [];
				done();
			});
			FlowUtil.run('SM_REFILL_WITHPIN_PURCHASE');	
		}).result.data = inputActivationData;
	});
});