'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var myAccount = require("../../myaccount/myaccount.po");
var generator = require('creditcard-generator');
var CommonUtil= require("../../util/common.functions.util");
var CCUtil = require("../../util/creditCard.utils");
var reUpPage = require("../reup.po");

describe('Simple Mobile ReUp with PIN purchase', function() {
	var planType = sessionData.simplemobile.shopPlan;
	it('should navigate to ReUp page', function(done) {
		reUpPage.goToreUpLink();		
		expect(reUpPage.isReUpPagePageLoaded()).toBe(true);		
		done();
	});
	
	
	it('should enter min', function(done) {
		reUpPage.enterMin(dataUtil.getMinOfESN(sessionData.simplemobile.esn));		
		expect(reUpPage.isServicePlansPageLoaded()).toBe(true);
		done();
	});
	
	
	 it('should select plan and listed with available plans', function(done){
			 var planName		= sessionData.simplemobile.planName;
			 console.log("planType planName="+planType+"="+planName);
			 reUpPage.choosePlanByName(planType,planName);
			expect(myAccount.checkoutPageLoaded()).toBe(true);
			done();		
		});


	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		
		if(planType == 'addon')
		{
			//console.log("refill add on plan");
			expect(activation.summaryPageLoadedPurchase()).toBe(true);
		}
		else
		{
			expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		}
		
		done();		
	});
	
	if(planType != 'addon')
	{
		/*Agree billing zipcode changes popup
		 *Expected result - checkout page will be loaded
		 */	
		it('should load back to the same check out page', function(done) {
			myAccount.agreeBillingZipcodeChanges();
			expect(myAccount.checkoutPageLoaded()).toBe(true);
			myAccount.placeMyOrder();
			done();		
		}); 
	}
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.summaryPageLoadedPurchase();
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();		
	});
	
	
	
	
});
	