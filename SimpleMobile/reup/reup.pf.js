'use strict';
var ElementsUtil = require("../util/element.util");
var sessionData = require("../common/sessiondata.do");
var dataUtil= require("../util/datautils.util");
var ConstantsUtil = require("../util/constants.util");


var ReUpPf = function() {
	
	this.reUpLink = element.all(by.id('lnk_REFILL')).get(0);
	//this.minTextField = element(by.xpath('/html/body/tf-update-lang/div[2]/div[2]/div/div/div[2]/div/div/tf-page-body/div/div[1]/div[2]/div[2]/ng-include/tf-split-vertical-or/div/div[3]/div/tf-split-vertical-or-right/form/div[1]/div[2]/div[1]/div/tf-phone/div/div/input'));
	this.minTextField = element.all(by.css("input[name='deviceinfo']")).get(1);
	this.pinTextField = element.all(by.id('form_redeem.pin')).get(1);
	this.addAirTimeButton = element(by.id('AddAirtime'));
	this.doneConfirmButton = element(by.id('done_confirmation'));
	this.minTextFieldForPurchase = element.all(by.css("input[id='formreup']")).get(1);//element.all(by.id('formphone-min')).get(1);
	this.buyPlanButton = element(by.css("button[id='RenewService']"));
	this.selcectPlan;
	/*This  method will navigates to reup page*/
	this.goToreUpLink = function() {
		return this.reUpLink.click();
	};
	
	
	
	//*** method to check whether the reup page loaded or not
	this.isReUpPagePageLoaded = function() {
		//return ElementsUtil.waitForUrlToChangeTo(/collectminpinpromo$/);
		ElementsUtil.waitForElement(this.minTextFieldForPurchase,2000);
		return this.minTextFieldForPurchase.isPresent(); 
	};
	
	/*This method will enter min and pin and clicks on add air time button*/
	this.addAirTime = function(min,pin) {
//		ElementsUtil.waitForElement(this.minTextField,2000);
		this.minTextField.sendKeys(min);
		this.pinTextField.sendKeys(pin);
		this.addAirTimeButton.click();
	};
	
	 /*This method will check whether the confirm page loaded or not*/
	this.goToConfirmOrderPage = function() {
		return ElementsUtil.waitForUrlToChangeTo(/redemptionconfirmationSuccess$/);
	};
	
	/*This method will click on done button*/
	this.clickOnDone = function() {
		ElementsUtil.waitForElement(this.doneConfirmButton);
		return this.doneConfirmButton.click();
	};
	
	 /*This method will check whether the home page loaded or not* /
	this.goToHomePage = function() {
		return ElementsUtil.waitForUrlToChangeTo(/siteng.simplemobile.com/);
	};
	*/
	
	this.enterMin = function(min) {
		//ElementsUtil.waitForElement(this.minTextFieldForPurchase,2000);
		this.minTextFieldForPurchase.sendKeys(dataUtil.getMinOfESN(sessionData.simplemobile.esn));
		return this.buyPlanButton.click();
	};
	
	this.selectPlan = function() {
		ElementsUtil.waitForElement(this.planButton);
		return this.planButton.click();
	};
	
	this.isServicePlansPageLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/serviceplan$/);
	};
	
	this.selectAddOnPlan = function() {
		ElementsUtil.waitForElement(this.addOnPlanButton);
		return this.selectAddOnPlan.click();
	};
	
	this.choosePlanByName = function(planType,planName) {
        if (planType == ConstantsUtil.SHOP_PLANS[0]) {
        	//this.selcectPlan = element(by.id(ConstantsUtil.DAY_PLANS_30[planName]));
        	//ElementsUtil.waitForElement(this.selcectPlan);
        	//this.selcectPlan.click();
			//element.all(by.css('button[id="monthly_235"]')).get(0).click();	
			element(by.css('button[id="'+ConstantsUtil.DAY_PLANS_30[planName]+'"]')).click();
			element(by.css('button[id="addToCartOneTimePurchpopup_btn"]')).click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[1]){
        	this.selcectPlan = element(by.id(ConstantsUtil.DATAONLY_PLANS[planName]));
        	ElementsUtil.waitForElement(this.selcectPlan);
        	this.selcectPlan.click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
        	this.selcectPlan = element.all(by.css("button[id='"+ConstantsUtil.ADDON_PLANS[planName]+"']")).get(0);
        	ElementsUtil.waitForElement(this.selcectPlan);
        	this.selcectPlan.click();
        }
 
  }
	};

module.exports = new ReUpPf;