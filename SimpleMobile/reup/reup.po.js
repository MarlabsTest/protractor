'use strict';
var reUpPf = require("./reup.pf");


var ReUpPO = function() {

	this.goToreUpLink = function() {
		return reUpPf.goToreUpLink();
	};
	
	//*** method to check the refill page loaded
	this.isReUpPagePageLoaded = function() {	
		return reUpPf.isReUpPagePageLoaded();
	};
	
	this.addAirTime = function(min,pin) {
		return reUpPf.addAirTime(min,pin);
	};
	
	this.goToConfirmOrderPage = function() {	
		return reUpPf.goToConfirmOrderPage();
	};
	
	this.clickOnDone = function() {
		return reUpPf.clickOnDone();
	};
	/*
	this.goToHomePage = function() {
		return reUpPf.goToHomePage();
	};
	*/
	this.enterMin = function(min) {
		return reUpPf.enterMin(min);
	};
	
	this.selectPlan = function() {
		return reUpPf.selectPlan();
	};
	
	this.isServicePlansPageLoaded = function() {
		return reUpPf.isServicePlansPageLoaded();
	};
	
	this.selectAddOnPlan = function() {
		return reUpPf.selectAddOnPlan();
	};
	
	
	this.choosePlanByName = function(planType,planName) {
		return reUpPf.choosePlanByName(planType,planName);
	};
};

module.exports = new ReUpPO;