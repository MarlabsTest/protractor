//this is the page fragment that validate the current device esn

'use strict';
var ElementsUtil = require("../util/element.util");
var validateEsn = function() {
		
	//this.esnTextBox = element.all(by.id('esnsimmin')).get(1);
	this.esnTextBox = element.all(by.id('lastfour_serial')).get(1);
	//this.esnTextBox = element.all(by.id('sim')).get(1)
	//this.esnContinueBtn =  element.all(by.id('btn_continue')).get(1);//.get(2);
	this.esnContinueBtn =  element.all(by.css('[type="submit"]')).get(1);//.get(2);
	this.fourDigitCodeTextBox = element.all(by.id('lastfour_serial')).get(1);
	//this.fourDigitCodeContinueBtn =  element.all(by.css('[type="submit"]')).get(1);//Removed get(0)
	this.fourDigitCodeContinueBtn =  element(by.css('[type="submit"]'));
	
	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.validateEsnLastNumbersPageLoaded = function(){
		ElementsUtil.waitForElement(this.fourDigitCodeTextBox);
		return this.fourDigitCodeTextBox.isPresent();
		/*
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /activation!collectlastfourvalidacct$/.test(url);
		});
		*/
	};
  
	//specific to phone upgrade
	//** this method is to check whether the validate last four esn number of the current device
	this.enterCurrentEsnLastFourDigits = function(number) {
		this.esnTextBox.clear().sendKeys(number);
		this.esnContinueBtn.click();
	};
	
	//** this method is to check whether the validate four digit code after message
	this.enterFourDigitCodeFromMsg = function(number) {
		console.log("number :: "+number);
		this.fourDigitCodeTextBox.clear().sendKeys(number);
		this.fourDigitCodeContinueBtn.click();
	};
};
module.exports = new validateEsn;
