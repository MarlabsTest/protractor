/*
 * **********Spec file is for REACTIVATING A DEVICE WITH PIN ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.Once the device get deactivated,it will be listed under inactive devices in myaccount dashboard.
 * 4.Activate the device by clicking on the activate button in dashboard (Reactivation)
 * 5.It will be redirected to the page where we need to enter zipcode.
 * 6.Enter the airtime pin and continue from the page,we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 7.After doing all these,we are done with the reactivation process. 
 * 
 */

'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};
var generatedPin ={};

describe('SM ReActivation  with PIN', function() {	
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should deactivate the device', function(done) {	
        var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
        console.log('minVal ,    ::'+min);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.simplemobile.esn,min,'PASTDUE','SIMPLEMOBILE');	
		generatedMin['min'] = min;
		home.homePageLoad();
		home.myAccount();
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}).result.data = generatedMin;	  
	
	/*click on the activate button in the dashboard for the deactivated device
	 *Expected result - redirected to the page where we need to enter the zipcode.  
	 */
	it('should navigate to page for entering ESN', function(done) {	
		myAccount.clickOnActivateBtn();
		expect(activation.isSIMPage()).toBe(true);	
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to page for entering Airtime PIN', function(done) {		
		activation.enterMarriedSim(sessionData.simplemobile.sim);
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();	
	});
	
	/*Enter an airtime PIN 
	 *Expected result - Final Instruction Page will be loaded
	 */
	it('should navigate to final instruction page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.simplemobile.pinPartNumber);
       	console.log('returns', pinval);
		activation.enterAirTimePin(pinval);
		generatedPin['pin'] = pinval;
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be loaded
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
