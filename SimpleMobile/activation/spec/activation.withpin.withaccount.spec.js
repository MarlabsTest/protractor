'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};
var generatedEsnSim = {};

describe('SimpleMobile Activation GSM', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on a simple mobile phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.isSIMPage()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
	 	//console.log('returns'+ sessionData.simplemobile.esnPartNumber);
	 	//console.log('returns'+ sessionData.simplemobile.simPartNumber);
		var esnval = DataUtil.getESN(sessionData.simplemobile.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.simplemobile.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.simplemobile.esn = esnval;
		sessionData.simplemobile.esnsToReactivate.push(esnval);
		activation.enterMarriedSim(simval);
		sessionData.simplemobile.sim = simval;
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.simplemobile.zip);		
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it(' enter airtime pin and navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.simplemobile.pinPartNumber);
       	console.log('returns Pin value', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.simplemobile.pin = pinval;
		generatedPin['pin'] = sessionData.simplemobile.pin;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	
	it('select create account  and navigate to account creation page', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	it('enter the account details and navigate to account creation successful popup', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "simplemobile";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.simplemobile.cardpin = "12345";
		sessionData.simplemobile.username = emailval;
		sessionData.simplemobile.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the popup ', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnSummaryBtn();
//		DataUtil.activateESN(sessionData.simplemobile.esn);
//		console.log("update table with esn :"+sessionData.simplemobile.esn);
		//DataUtil.updateMin(sessionData.simplemobile.esn);
//		var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
//		console.log("MIN is  :::::" +min);
		sessionData.simplemobile.upgradeEsn = sessionData.simplemobile.esn;//specific to phone upgrade
		//check_activation		
//		DataUtil.checkActivation(sessionData.simplemobile.esn,sessionData.simplemobile.pin,'1','SIMPLE_MOBILE_ACTIVATION_WITH_PIN');
//		generatedMin['min'] = min;
//		console.log("ITQ table updated");			
		expect(myAccount.isLoaded()).toBe(true);		
		done();	
	}).result.data = generatedMin;

	// });
	
});
