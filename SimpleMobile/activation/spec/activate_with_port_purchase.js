/*
 * **********Spec file is for ACTIVATING A GSM WITH PORTING WITH PURCHASE- NEW ACCOUNT ******** 
 * 
 * 1.Initially we will be reading partnumber from csv file & storing it in session.(This is for handling multiple testdata)
 * 2.Generate ESN,SIM & PIN from DB using the partnumbers
 * 3.DO MARRY the ESN with the SIM,and provide the SIM number in the flow.
 * 4.Provide external number
 * 5.Provide the service provider details & account details
 * 6.Enter the address details
 * 7.Select a service plan
 * 8.Create a new account
 * 9.Do the payment after successful account creation.
 * 10.After purchasing plan,we will be redirected to final Instruction page,summary page and survey page
 * 11.Final page will be the MyAccount dashboard, where the device will the displayed under Active devices section. 
 * 
 */
'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var shop = require("../../shop/shop.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsnSim ={};
var generatedMin ={};

describe('SM External Porting with Purchase', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	/*Click on the activate link in the home page
	 *Expected result - Select Device Page(Either activate a family Phone Or a BYOP device )  
	 */
	it('should navigate to the Select Device Page', function(done) {	
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	/*Proceed with the Activate Family Phone flow
	 *Expected result - Page to enter SIM number will be displayed  
	 */
	it('should navigate to the page to enter SIM number',function(done){
		activation.gotToEsnPage();
		expect(activation.isSIMPage()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
		var esnval = DataUtil.getESN(sessionData.simplemobile.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.simplemobile.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.simplemobile.esn = esnval;
		sessionData.simplemobile.esnsToReactivate.push(esnval);
		activation.enterMarriedSim(simval);
		sessionData.simplemobile.sim = simval;
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	/*Enter existing mobile number 
	 *Expected result - Page to enter the current mobile number details will be loaded
	 */	
	it('should navigate to enter the existing number details', function(done) {
		var min = Math.floor((Math.random() * (9999999999-9000000000)) + 90000000000);
//		var min = Math.floor((Math.random() * 10000000000) + 1);
		console.log("Random MIN ::",min);
		sessionData.simplemobile.min = min;
		activation.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activation.ServiceProviderPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;	
	
	/*Select the phone type 
	 *Expected result - Previous phone account details form will be loaded
	 */	
	it('should load the previous phone account details form', function(done) {
		activation.selectPhoneType();
		expect(activation.accountDetailsLoaded()).toBe(true);
		done();		
	});
	
	/*Enter the phone account details
	 *Expected result - Select Address popup page will be loaded
	 */	
	it('should navigate to the address popup page', function(done) {
		activation.enterPhoneAccountDetails("012345900","1234","3559","Sirius","Black","9809806751","Rocky Hill","Miami","Florida","33178");
		expect(activation.selectAddressPopUpLoaded()).toBe(true);
		done();		
	});
	
	/*Select the Keep this address option
	 *Expected result - Page to enter address details  will be loaded
	 */	
	it('should load the address details page', function(done) {
		activation.keepThisAddress();
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
//	/*Enter the address details like street number etc
//	 *Expected result - Page to proceed with purchase plan page will be loaded
//	 */	
//	it('should navigate to airtimeserviceplan page', function(done) {
//		activation.addressDetails("40","75th","12533");
//		expect(activation.servicePlanPageLoaded()).toBe(true);
//		done();		
//	});
	
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*it('should navigate to account creation page', function(done) {
		console.log('name',sessionData.simplemobile.planName);
		activation.pickPlan(sessionData.simplemobile.planName);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	});*/
	
	it('choose service plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.simplemobile.shopPlan;
		var planName		= sessionData.simplemobile.planName;
		var isAutoRefill	= sessionData.simplemobile.autoRefill;
		shop.choosePlanByName(planType,planName);
		console.log("SELECTED!!!");
		var autoReupPopup = element(by.className('modal-dialog'));
		
		autoReupPopup.isPresent().then(function(isVisible)
		{
			if(isVisible)
			{
				if(isAutoRefill == 'YES' || isAutoRefill == 'Y')
				{
					shop.doAutoRefill();
	            }
				else
				{
					shop.doOneTimePurchase();
	            }
			}
		});
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();
	});
	
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.simplemobile.username = emailval;
		sessionData.simplemobile.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Checkout Page will be shown
	 */
	it('should load the checkout page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*
	/*Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 * /	
	it('should load the payment form', function(done) {
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	*/
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	 */	
	it('should load the final instruction page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.simplemobile.esn);		
		var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
		console.log("MIN is  :::::" +min);
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
});
