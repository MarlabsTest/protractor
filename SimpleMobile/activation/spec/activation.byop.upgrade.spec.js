'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
// newly added for data integration

//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
//var activationUtil = require('../../util/activation.util');

// Newly added by gopi
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};
var generatedEsnSim = {};

describe('SM BYOP Upgrade', function() {
	
	it('Select Activation', function(done) {	
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on a simple mobile phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.isSIMPage()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
		var esnval = DataUtil.getESN(sessionData.simplemobile.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.simplemobile.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.simplemobile.esn = esnval;
		sessionData.simplemobile.esnsToReactivate.push(esnval);
		activation.enterMarriedSim(simval);
		sessionData.simplemobile.sim = simval;
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
		var min = DataUtil.getMinOfESN(sessionData.simplemobile.upgradeEsn);
		activation.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activation.validateEsnLastNumbersPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	//To provide four digit authentication code and check whether the service plan page loaded
	it('Provide four digit authentication code', function(done) {
		//console.log("CommonUtil.getLastDigits(esnToBePort, 4) :: "+CommonUtil.getLastDigits(esnToBePort, 4));
		activation.enterCurrentEsnLastFourDigits(CommonUtil.getLastDigits(sessionData.simplemobile.upgradeEsn, 4));//last four digits of the current esn
		expect(activation.isPhoneUpgradeRequestPageLoaded()).toBe(true);
		done();		
	});

	it('Continue phone upgrade request page', function(done) {
		activation.continuePhoneUpgradeRequestPage();
		//expect(activation.isPhoneUpgradeRequestPageLoaded()).toBe(true);
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('proceed from summary page and load the home page', function(done) {
		activation.clickOnSummaryBtn();
		expect(home.isHomePageLoaded()).toBe(true);
		done();		
	});
});
