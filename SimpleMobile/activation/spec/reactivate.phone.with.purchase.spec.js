/*
 * **********Spec file is for REACTIVATING A DEVICE WITH PLAN PURCHASE ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.The deactivated device will be listed under inactive devices in myaccount dashboard.
 * 4.Activate the device by clicking on the activate button in dashboard (Reactivation)
 * 5.It will be redirected to the page where we need to enter zipcode.
 * 6.Select an airtime plan and do the payment.
 * 7.After successful purchase,we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 8.After doing all these,we are done with the reactivation process. 
 * 
 */
'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedMin ={};
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");

describe('SM ReActivation with purchase', function() {
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	it('should deactivate the device', function(done) {	
        var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
        console.log('minVal ,    ::'+min);
        DataUtil.deactivatePhone('DEACTIVATE',sessionData.simplemobile.esn,min,'PASTDUE','SIMPLEMOBILE');	
		generatedMin['min'] = min;
		DataUtil.changeServiceEndDate(sessionData.simplemobile.esn);
		home.homePageLoad();
		home.myAccount();
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	}).result.data = generatedMin;	
	
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 * /
	it('should set due date to past', function(done) {	
        DataUtil.changeServiceEndDate(sessionData.simplemobile.esn);	
		home.homePageLoad();
		home.myAccount();
		expect(myAccount.isLoaded()).toBe(true);		
		done();
	});	
	*/
	
	/*click on the activate button in the dashboard for the deactivated device
	 *Expected result - redirected to the page where we need to enter the zipcode.  
	 */
	it('should navigate to SIM page', function(done) {		
		myAccount.clickOnActivateBtn();
		expect(activation.servicePlanPageLoaded()).toBe(true);	
		done();
	});
	
	/*
	it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.simplemobile.zip);		
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	*/
	
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.simplemobile.shopPlan;
		var planName		= sessionData.simplemobile.planName;
		var isAutoRefill	= sessionData.simplemobile.autoRefill;
		shop.choosePlanByName(planType,planName);
		console.log("SELECTED!!!");
		
		var autoReupPopup = element(by.className('modal-dialog'));
		
		autoReupPopup.isPresent().then(function(isVisible)
		{
			if(isVisible)
			{
				if(isAutoRefill == 'YES' || isAutoRefill == 'Y')
				{
					shop.doAutoRefill();
	            }
				else
				{
					shop.doOneTimePurchase();
	            }
			}
		});
		
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - final instruction page will be loaded
	 */	
	it('should load the final instruction page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	 */	
	it('should load the final instruction page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the summary page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
});
