'use strict';

var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var myAccount = require("../../myaccount/myaccount.po");
var home = require("../../common/homepage.po");
var generatedMin ={};

describe('SimpleMobile Activation ESN', function() {

	it('MIN Updation', function(done) {
		
		for(var line=0;line<sessionData.simplemobile.esnsToReactivate.length;line++){
			//browser.sleep(30000);
			console.log("result from cleartnumber==="+DataUtil.activateESN(sessionData.simplemobile.esnsToReactivate[line]));
			console.log("update table with esn :"+sessionData.simplemobile.esnsToReactivate[line]+":");
			browser.sleep(30000);
			var min = DataUtil.getMinOfESN(sessionData.simplemobile.esnsToReactivate[line]);
			generatedMin['min'] = min;
			console.log("MIN is login :::::" +min);
			//check_activation		
			DataUtil.checkActivation(sessionData.simplemobile.esnsToReactivate[line],sessionData.simplemobile.pin,'1','SIMPLE_MOBILE_ACTIVATION_WITH_PIN');
			console.log("ITQ table updated");
			home.homePageLoad();
			home.myAccount();
			expect(myAccount.isLoaded()).toBe(true);
			done();
		}
		
	}).result.data = generatedMin;
	
});
