/*
 * **********Spec file is for ACTIVATING A GSM WITH PORTING - NEW ACCOUNT ******** 
 * 
 * 1.Initially we will be reading partnumber from csv file & storing it in session.(This is for handling multiple testdata)
 * 2.Generate ESN,SIM & PIN from DB using the partnumbers
 * 3.DO MARRY the ESN with the SIM,and provide the SIM number in the flow.
 * 4.Provide external number
 * 5.Provide the service provider details & account details
 * 6.Enter the address details
 * 7.Enter airtime PIN
 * 8.Create a new account
 * 9.After successful account creation,we will be redirected to final Instruction page,summary page and survey page
 * 10.Final page will be the MyAccount dashboard, where the device will the displayed under Active devices section. 
 * 
 */
'use strict';
var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};
var generatedPin ={};


describe('SM External Porting with PIN', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	/*Click on the activate link in the home page
	 *Expected result - Select Device Page(Either activate a family Phone Or a BYOP device )  
	 */
	it('should navigate to the Select Device Page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});
	
	/*Proceed with the Activate Family Phone flow
	 *Expected result - Page to enter SIM number will be displayed  
	 */
	it(' click on a simple mobile phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.isSIMPage()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
		var esnval = DataUtil.getESN(sessionData.simplemobile.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.simplemobile.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.simplemobile.esn = esnval;
		sessionData.simplemobile.esnsToReactivate.push(esnval);
		activation.enterMarriedSim(simval);
		sessionData.simplemobile.sim = simval;
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
//	/*Enter a 4 digit security pin 
//	 *Expected result - Page to opt for porting or new number will be displayed
//	 */
//	it('should navigate to page for entering mobile number', function(done) {		
//		activation.enterPin("1234");
//		activation.clickOnPinContinue();
//		expect(activation.keepMyPhonePageLoaded()).toBe(true);
//		done();		
//	});
//	
	
	//To provide MIN number and check whether the validate ESN page loaded
	it('Provide mobile number', function(done) {
//		var min = "9002345678";
		var min = Math.floor((Math.random() * (9999999999-9000000000)) + 90000000000);
//		var min = Math.floor((Math.random() * 10000000000) + 1);
		console.log("Random MIN ::",min);
		sessionData.simplemobile.min = min;
		activation.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activation.ServiceProviderPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin; 
	
	/*Select the phone type 
	 *Expected result - Previous phone account details form will be loaded
	 */	
	it('should load the previous phone account details form', function(done) {
		activation.selectPhoneType();
		expect(activation.accountDetailsLoaded()).toBe(true);
		done();		
	});
	
	/*Enter the phone account details
	 *Expected result - Select Address popup page will be loaded
	 */	
	it('should navigate to the address popup page', function(done) {
		activation.enterPhoneAccountDetails("012345900","1234","3559","Ginny","Potter","9809806751","Rocky Hill","Miami","Florida","33178");
		expect(activation.selectAddressPopUpLoaded()).toBe(true);
		done();		
	});
	
	/*Select the Keep this address option
	 *Expected result - Page to enter address details  will be loaded
	 */	
	it('should load the address details page', function(done) {
		activation.keepThisAddress();
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
//	/*Enter the address details like street number etc
//	 *Expected result - Page to enter the airtime PIN will be loaded
//	 */	
//	it('should navigate to airtimeserviceplan page', function(done) {
//		activation.addressDetails("40","76th","14532");
//		expect(activation.servicePlanPageLoaded()).toBe(true);
//		done();		
//	});
	
	/*Enter an airtime PIN 
	 *Expected result - Account creation Page will be loaded
	 */
	it('should navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.simplemobile.pinPartNumber);
       	console.log('returns', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.simplemobile.pin = pinval;
		generatedPin['pin'] = pinval;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,"tracfone","02/02/1990","12345");
		sessionData.simplemobile.username = emailval;
		sessionData.simplemobile.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Final Instruction Page will be shown
	 */
	it('should load the final instruction page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoadedPortInPin()).toBe(true);
		done();		
	}); 
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPortInPin();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtn();

//Below commented :: Flow directly moving to dashboard without going to survey page 
/*
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 * /
 	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn(); 
*/
		DataUtil.activateESN(sessionData.simplemobile.esn);
		console.log("update table with esn :"+sessionData.simplemobile.esn);
		var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
		console.log("MIN is  :::::" +min);
		//check activation
		DataUtil.checkActivation(sessionData.simplemobile.esn,sessionData.simplemobile.pin,'1','WFM_Activation_with_PIN');
		console.log("DB call");	
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 
		
});
