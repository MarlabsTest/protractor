//This spec file is for Simple Mobile BYOP activation. 
//User has to choose device type "Bring your own phone"  
//Provide BYOP SIM, security PIN, zipcode, AT PIN to activate the BYOP.

'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsnSim ={};
var generatedMin ={};
var generatedPin ={};


describe('BYOP activation', function() {
	//to click activate link in the menu and check whether the activation page is loaded 
	it('Go to BYOP activation page', function(done) {
		homePage.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});
	
	//to choose the device type as "Bring your own phone" and check whether the SIM page is loaded
	it('Choose device type BYOP',  function(done) {
		activation.selectByopDeviceType();
		activation.acceptTermsConditions();
		expect(activation.isByopSIMPage()).toBe(true);	
		done();
	});
	
	//to provide the SIM number and check whether the security popup window shown
	it('Provide SIM number',  function(done) {
		var simval = DataUtil.getByopSim(sessionData.simplemobile.esnPartNumber, sessionData.simplemobile.simPartNumber);
		activation.enterByopSim(simval);
		sessionData.simplemobile.sim = simval;	
		var esnVal = CommonUtil.getLastDigits(simval, 15);//to get dummy esn from sim number
		sessionData.simplemobile.esn = esnVal;
		sessionData.simplemobile.upgradeEsn = esnVal; 
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();
	}).result.data = generatedEsnSim;
	
	//to provide the zipcode and check whether the service plan page loaded
	it('Provide zipcode',  function(done) {
		activation.enterByopZipCode(sessionData.simplemobile.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();
	});
	
	//to provide the airtime pin and check whether the account creation page loaded
	it('Provide airtime pin', function(done) {
		var pinval = DataUtil.getPIN(sessionData.simplemobile.pinPartNumber);
		activation.enterAirTimePin(pinval);
		sessionData.simplemobile.pin = pinval;
		generatedPin['pin'] = pinval;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	//to switch to create account option and check whether the email text box loaded to create account
	it('Switch to create account option', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	//create the account and check whether the account creation success popup shown
	it('Provide the account details', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.simplemobile.username = emailval;
		sessionData.simplemobile.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	//click continue on account creation success popup and check whether the instruction page loaded
	it('Click continue button in account creation success popup', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	//click continue in final instruction page and check whether the summary page loaded
	it('Click continue button in final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	//click "No thanks" button and check whether the dashboard page loaded
	it('Click "No thanks" button in survey page', function(done) {
		activation.clickOnSummaryBtn();
		DataUtil.activateESN(sessionData.simplemobile.esn);
		sessionData.simplemobile.upgradeEsn = sessionData.simplemobile.esn; //for upgrade
		var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
		DataUtil.checkActivation(sessionData.simplemobile.esn,sessionData.simplemobile.pin,'1','SIMPLE_MOBILE_ACTIVATION_WITH_PIN');
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
});
	

