//This spec file is for WFM BYOP activation. 
//User has to choose device type "Bring your own phone"  
//Provide BYOP SIM, security PIN, zipcode, AT PIN to activate the BYOP.

'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../../shop/shop.po");
var activation = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var generatedEsnSim ={};
var generatedMin ={};
var generatedPin ={};


describe('BYOP activation', function() {
	//to click activate link in the menu and check whether the activation page is loaded 
	it('Go to BYOP activation page', function(done) {
		homePage.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});
	
	//to choose the device type as "Bring your own phone" and check whether the SIM page is loaded
	it('Choose device type BYOP',  function(done) {
		activation.selectByopDeviceType();
		activation.acceptTermsConditions();
		expect(activation.isByopSIMPage()).toBe(true);	
		done();
	});
	
	//to provide the SIM number and check whether the security popup window shown
	it('Provide SIM number',  function(done) {
		var simval = DataUtil.getByopSim(sessionData.simplemobile.esnPartNumber, sessionData.simplemobile.simPartNumber);
		activation.enterByopSim(simval);
		//activation.enterByopSimPurchase(simval);
		sessionData.simplemobile.sim = simval;	
		var esnVal = CommonUtil.getLastDigits(simval, 15);//to get dummy esn from sim number
		sessionData.simplemobile.esn = esnVal;
		sessionData.simplemobile.upgradeEsn = esnVal; 
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();
	}).result.data = generatedEsnSim;
	
	//to provide the zipcode and check whether the service plan page loaded
	it('Provide zipcode',  function(done) {
		activation.enterByopZipCode(sessionData.simplemobile.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();
	});
	
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*Buy an airtime Plan 
	 *Expected result - Account creation page will be loaded
	 */
	/*it('should navigate to account creation page', function(done) {
		activation.pickPlan(sessionData.simplemobile.planName);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	});*/
	it('choose service plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.simplemobile.shopPlan;
		var planName		= sessionData.simplemobile.planName;
		var isAutoRefill	= sessionData.simplemobile.autoRefill;
		shop.choosePlanByName(planType,planName);
		console.log("SELECTED!!!");
		var autoReupPopup = element(by.className('modal-dialog'));
		
		autoReupPopup.isPresent().then(function(isVisible)
		{
			if(isVisible)
			{
				if(isAutoRefill == 'YES' || isAutoRefill == 'Y')
				{
					shop.doAutoRefill();
	            }
				else
				{
					shop.doOneTimePurchase();
	            }
			}
		});
		//expect(myAccount.checkoutPageLoaded()).toBe(true);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();
	});
	
	/*Click on the new account creation 
	 *Expected result - New Account creation form will be shown
	 */
	it('should load the account creation form', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.emailTextBoxLoaded()).toBe(true);
		done();		
	});
	
	/* Enter email,password,DOB and securitypin 
	 * Expected result - Account created successfully popup page
	 */
	it('should create a new account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone"; 
       	console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.simplemobile.username = emailval;
		sessionData.simplemobile.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the popup page 
	 *Expected result - Checkout Page will be shown
	 */
	it('should load the checkout page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*
	/*Click on continue to payment button in the checkout page 
	 *Expected result - payment form will be loaded
	 * /	
	it('should load the payment form', function(done) {
		myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	*/
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */	
	it('should show the billing zipcode changes popup', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 */	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	 */	
	it('should load the final instruction page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 */
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.simplemobile.esn);		
		var min = DataUtil.getMinOfESN(sessionData.simplemobile.esn);
		console.log("MIN is  :::::" +min);
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
});
	

