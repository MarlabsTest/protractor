//this represnts the modal for zipcode(new number selection) flow

'use strict';
var ElementsUtil = require("../util/element.util");
var zipCode = function() {
		
	this.zipCodeTextBox = element.all(by.name('zip')).get(1);
	this.zipCodeTextBoxAddDevice = element.all(by.xpath("//input[@id='tfvaldevice-zip']")).get(0);
	this.zipCodeContinueBtn =  element.all(by.id('btn_continuezipcode')).get(0);	
	this.zipCodeByopTextBox = element.all(by.name('zip')).get(1);
	this.zipCodeByopContinueBtn =  element.all(by.id("btn_continuezipcode")).get(0);
	
	this.zipCodeTabViewTextBox = element.all(by.name('tfvaldevice-zip')).get(1);
	this.zipCodeTabViewContinueBtn =  element.all(by.id('btn_continuezipcode')).get(0);
	this.keepMyPhoneNoBtn = element.all(by.id('btn_changenumber')).get(1);

	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.keepMyPhonePageLoaded = function(){
		ElementsUtil.waitForElement(this.zipCodeTextBox);
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			console.log('expected url: /keepcollectphone$/ :: '+/keepcollectphone$/.test(url));
			return /keepcollectphone$/.test(url);
		});
	};
	
	//** method to choose "no" in "Keep my phone number" 
	this.chooseDontKeepMyNumber = function(){
		ElementsUtil.waitForElement(this.keepMyPhoneNoBtn);
		this.keepMyPhoneNoBtn.click();
	};
	
	//** method to load the Zipcode text box" 
	this.zipcodeDivLoaded = function(){
		//$$//ElementsUtil.waitForElement(this.zipCodeTabViewTextBox);
		return this.zipCodeTabViewTextBox.isPresent();
	};
  
	//** method to enter zipcode for tab style UI 
	this.enterZipCodeTabView = function(zipCode){
		//ElementsUtil.waitForElement(this.zipCodeTabViewTextBox);
		this.zipCodeTabViewTextBox.clear().sendKeys(zipCode);
		this.zipCodeTabViewContinueBtn.click();
	};
	
	//** method to enter zipcode
	this.enterZipCode = function(zipCode){
		//browser.wait(expectedConditions.visibilityOf(this.zipCodeTextBox),20000);
		//ElementsUtil.waitForElement(this.zipCodeTextBox);
		this.zipCodeTextBox.clear().sendKeys(zipCode);
		//browser.wait(waitutil.elementHasCome(this.zipCodeContinueBtn, 2), 20000);
		//ElementsUtil.elementHasCome(this.zipCodeContinueBtn,2);
		this.zipCodeContinueBtn.click();
		
		//browser.wait(expectedConditions.visibilityOf(this.zipCodeContinueBtn),40000);
		//this.zipCodeContinueBtn.click();
	};

	this.enterZipCodeAddDevice = function(zipCode){
		this.zipCodeTextBoxAddDevice.clear().sendKeys(zipCode);
		this.zipCodeContinueBtn.click();
	};

	
	
	
	this.enterByopZipCode = function(zipCode){
		//browser.wait(expectedConditions.visibilityOf(this.zipCodeByopTextBox),20000);
		//$$//ElementsUtil.waitForElement(this.zipCodeByopTextBox);
		this.zipCodeByopTextBox.clear().sendKeys(zipCode);
		//browser.wait(expectedConditions.visibilityOf(this.zipCodeByopContinueBtn),40000);
		//$$//ElementsUtil.waitForElement(this.zipCodeByopContinueBtn);
		this.zipCodeByopContinueBtn.click();
	};

};
module.exports = new zipCode;