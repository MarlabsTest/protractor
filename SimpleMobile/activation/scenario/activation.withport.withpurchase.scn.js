/*
 * **********Scenario is for ACTIVATING A Device WITH PORTING WITH PURCHASE PLAN- NEW ACCOUNT ******** 
 * 
 * 1.Initially we will be reading partnumber from csv file & storing it in session.(This is for handling multiple testdata)
 * 2.Generate ESN,SIM & PIN from DB using the partnumbers
 * 3.DO MARRY the ESN with the SIM,and provide the SIM number in the flow.
 * 4.Provide external number
 * 5.Provide the service provider details & account details
 * 6.Enter the address details
 * 7.Select an airtime Plan
 * 8.Create a new account
 * 9.Do the payment after successful account creation.
 * 10.After purchasing plan,we will be redirected to final Instruction page,summary page and survey page
 * 11.Final page will be the MyAccount dashboard, where the device will the displayed under Active devices section. 
 * 
 */
'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Activation Porting With Plan Purchase', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.cardType = inputActivationData.cardType;
				sessionData.simplemobile.planName = inputActivationData.PlanName;
				sessionData.simplemobile.autoRefill = inputActivationData.AutoRefill;
				sessionData.simplemobile.shopPlan = inputActivationData.shopplan;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('SM_ACTIVATION_WITH_PORT_PURCHASE');
		}).result.data = inputActivationData;
	});
});