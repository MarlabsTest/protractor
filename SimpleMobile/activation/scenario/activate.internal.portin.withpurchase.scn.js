'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/porting.util');
var sessionData = require("../../common/sessiondata.do");

describe('SM Internal Port In with purchase', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.cardType = inputActivationData.cardType;
				sessionData.simplemobile.oldPartNumber = inputActivationData.OldPart;
				sessionData.simplemobile.planName = inputActivationData.PlanName;
				sessionData.simplemobile.autoRefill = inputActivationData.AutoRefill;
				sessionData.simplemobile.shopPlan = inputActivationData.shopplan;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('SM_INTERNAL_PORTIN_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});