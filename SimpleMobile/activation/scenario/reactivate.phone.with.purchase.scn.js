/*
 * **********Scenario is for REACTIVATING A DEVICE WITH PLAN PURCHASE ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,we will run the deactivation procedure.
 * 3.The deactivated device will be listed under inactive devices in myaccount dashboard.
 * 4.Activate the device by clicking on the activate button in dashboard (Reactivation)
 * 5.It will be redirected to the page where we need to enter zipcode.
 * 6.Select an airtime plan with enrollment and do the payment.
 * 7.After successful purchase,we will be redirected to finalinstruction,summary,survey and finally to my account dashboard page 
 * 8.After doing all these,we are done with the reactivation process. 
 * 
 */
'use strict';


var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Reactivation Device With Plan Purchase', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.cardType = inputActivationData.cardType;
				sessionData.simplemobile.autoRefill = inputActivationData.AutoRefill;
				sessionData.simplemobile.planName = inputActivationData.PlanName;
				sessionData.simplemobile.shopPlan = inputActivationData.shopplan;
				sessionData.simplemobile.esnsToReactivate = [];
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('SM_REACTIVATION_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});