'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('SM Activation GSM with PIN with Account', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:gowtham', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				//console.log('Copying activation jojen'+ inputActivationData.PartNumber);
				//console.log('Copying activation jojen2'+ inputActivationData.SIM);
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.esnsToReactivate = [];
				done();
			});
			FlowUtil.run('SM_ACTIVATION_WITHPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});