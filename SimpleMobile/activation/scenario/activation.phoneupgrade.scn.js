'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var phoneupgradeUtil = require('../../util/phoneupgrade.util');
var sessionData = require("../../common/sessiondata.do");

describe('SM Phone Upgrade', function() {
	var phoneupgradeData = phoneupgradeUtil.getTestData();
	console.log('phoneupgradeData:', phoneupgradeData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(phoneupgradeData, function(inputPhoneUpgradeData) {
		sessionData.simplemobile.esnPartNumber = inputPhoneUpgradeData.FromPartNumber;
		//console.log('sessionData.tracfone.esnPartNumber: ',sessionData.tracfone.esnPartNumber );
		sessionData.simplemobile.arFlag = inputPhoneUpgradeData.AR;
		sessionData.simplemobile.phoneStatus = inputPhoneUpgradeData.PhoneStatus;
		//console.log('inputPhoneUpgradeData.PhoneStatus:',inputPhoneUpgradeData.PhoneStatus);
		describe('Drive Spec', function() {
			
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.simPartNumber = inputPhoneUpgradeData.FromSIM;
				sessionData.simplemobile.zip = inputPhoneUpgradeData.FromZipCode;
				sessionData.simplemobile.pinPartNumber = inputPhoneUpgradeData.FromPIN;
				sessionData.simplemobile.toEsnPartNumber = inputPhoneUpgradeData.ToPartNumber;
				sessionData.simplemobile.toSimPartNumber = inputPhoneUpgradeData.ToSIM;
				sessionData.simplemobile.toPinPartNumber = inputPhoneUpgradeData.ToPIN;
				sessionData.simplemobile.esnsToReactivate = [];
				//console.log('inputPhoneUpgradeData.PhoneStatus:',inputPhoneUpgradeData.PhoneStatus);
				//console.log('Copying upgrade', sessionData.simplemobile.phoneStatus);
				done();
			});
			FlowUtil.run('SM_UPGRADE_FLOWS');
		}).result.data = inputPhoneUpgradeData;
	});
});