//** This represents a  common page object for all the activation flows.

'use strict';
var activationPF = require("./activation.pf");
var esnObj = require("./esn.pf");
var simObj = require("./sim.pf");
var zipCodeObj = require("./zipcode.pf");
var purchasePlan = require("./purchaseplan.pf");
var airtimePinObj = require("./airtimepin.pf");
var newAccountObj = require("./accountcreation.pf");
var summaryObj = require("./summarypage.pf");
var surveyPageObj = require("./survey.pf");
var finalInstructionPage = require("./finalinstruction.pf");
var existingAccnt = require("./existingaccount.pf");
var portIn = require("./port.pf");
var phoneAccountDetails  = require("./phoneaccountdetails.pf");
var selectPlan = require("./selectserviceplan.pf");
//var internalPortInPf =  require("./internalportin.pf");
var checkoutpf	= require("../myaccount/checkout.pf");//remya
var validateCurrentEsn = require("./validatecurrentesn.pf");

var Activate = function() {

this.skipAccountLogin = function() {		
		
	return 	newAccountObj.skipAccountLogin();
	
	};
	
	//** this method is to check whether the validate last four esn number of the current device - page loaded
	this.validateEsnLastNumbersPageLoaded = function(){
		return validateCurrentEsn.validateEsnLastNumbersPageLoaded();
	};
	
	this.enterCurrentEsnLastFourDigits = function(number){
		return validateCurrentEsn.enterCurrentEsnLastFourDigits(number);
	};
	
	this.isPhoneUpgradeRequestPageLoaded = function(){
		return activationPF.isPhoneUpgradeRequestPageLoaded();
	};
	
	this.isAirtimeBenefitsWarningShown = function(){
		return activationPF.isAirtimeBenefitsWarningShown();
	};
	
	//** method to enter airtime pin
	this.enterReactivationAirTimePin = function(pin){
		airtimePinObj.enterReactivationAirTimePin(pin);      
	};
	
	this.continueAirtimeBenefitsWarning = function(){
		activationPF.continueAirtimeBenefitsWarning();
	};
	
	this.continuePhoneUpgradeRequestPage = function(){
		activationPF.continuePhoneUpgradeRequestPage();
	};

	this.selectByopDeviceType = function() {
		activationPF.selectDeviceTypeBYOP();
	};
	
	this.acceptTermsConditions = function() {
		activationPF.acceptTermsConditions();
	};
  
	this.enterByopSim = function(simNum){
		simObj.enterByopSIM(simNum);
	};
	
	this.enterByopSimPurchase = function(simNum){
		simObj.enterByopSIM(simNum);
	};
  
	this.enterByopZipCode = function(zipCode){
		zipCodeObj.enterByopZipCode(zipCode);
	};
  
	this.enterByopAirTimePin = function(pin){
		airtimePinObj.enterByopAirTimePin(pin);      
	};
  
	this.selectByopPurchaseAirTime = function() {
		simObj.selectByopPurchaseAirTime();
	};
	
	this.isByopSIMPage = function(){
		return simObj.isByopSIMPage();
	};

	//this.waitForElementLoad  = function(min) {
		//browser.sleep(min);
	//};
  
	this.maximiseBrowser = function() {
		browser.driver.manage().window().maximize();
	};
	
	//** this method checks whether the page is loaded or not after the click on activate link from homepage	
	this.isActivateLoaded = function() {
		return activationPF.isActivateLoaded();
	};
	
	//**this method is to proceed with new phone activation flow
	this.gotToEsnPage= function() {
		return activationPF.gotToEsnPage();
	};
	
	//**checks whether the esn page is loaded or not
	this.esnPageLoaded=function(){
		return esnObj.esnPageLoaded();
	};
	
   //**method to enter the esn 
	this.enterEsn=function(esnVal){
		esnObj.enterEsn(esnVal);		
	};
	
	
	//**method to enter the married SIM 
	this.enterMarriedSim=function(simVal){
		esnObj.enterMarriedSim(simVal);		
	};
	
	//** method to click on terms and condition checkbox
	this.checkBoxCheck=function(){
		esnObj.checkBoxCheck();
	};
	
	//check whether the terms and conditions popup shown
	this.isTermsAndConditionsPopUpShown = function() {
		return activationPF.isTermsAndConditionsPopUpShown();
	};
	
	//method to proceed from the esn page
	this.continueESNClick=function(){
		esnObj.continueESNClick();
	};
	
	//method to display error message after entering already activated ESN
	this.errorMessage=function(){
		return esnObj.errorMessage();
	};
  
	//method to display error message after entering a random number
	this.randomEsnErrorMsg = function(){
		return esnObj.randomEsnErrorMsg();
	};
	
	//** method to check whether the security pin popup page is loaded
	this.securityPinPopUp=function(){
		return esnObj.securityPinPopUp();
	};
	
	//** method to display error message after entering invalid pin
	this.displayPinError = function(){
		return esnObj.displayPinError();
	};
	
	//** method to enter pin 
	this.enterPin = function(pinVal){
		esnObj.enterPin(pinVal);
	};
  
	//** method to proceed from security pin popup page
	this.clickOnPinContinue = function(){
		esnObj.clickOnPinContinue();
	};
  
	//** method to check whether the SIM page is loaded
	this.isSIMPage = function(){
		return simObj.isSIMPage();
	};
  
	//** method to enter the SIM
	this.enterSIM = function(simNum){
		simObj.enterSIM(simNum);
	};
  
	//** method to display error after entering invalid SIM 
	this.isSIMError = function(){
		return simObj.isSIMError();
	}; 
  
	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.keepMyPhonePageLoaded = function(){
		return zipCodeObj.keepMyPhonePageLoaded();
	};
  
	//** method to enter zipcode
	this.enterZipCode = function(zipCode){
		zipCodeObj.enterZipCode(zipCode);
	};
	
	this.enterZipCodeAddDevice = function(zipCode){
		zipCodeObj.enterZipCodeAddDevice(zipCode);
	};
	
	
	//** method to enter zipcode for tab style UI 
	this.enterZipCodeTabView = function(zipCode){
		zipCodeObj.enterZipCodeTabView(zipCode);
	};
	
	//** method to choose "no" in "Keep my phone number"
	this.chooseDontKeepMyNumber = function(){
		zipCodeObj.chooseDontKeepMyNumber();
	};
	
	//** method to check whether the plan(purchase plan/already have a pin -flows) page is loaded
	this.servicePlanPageLoaded = function(){
		return purchasePlan.servicePlanPageLoaded();
	};
	//remya
	//To check whether Base plan page is listed on clicking the purchase  button
	this.baseSevicePlanPageLoaded = function(){
		return purchasePlan.baseServicePlanPageLoaded();
	};
	//mrthod for selecting Pay As you Go plan
	this.selectPayAsYouGoPlan =function(){
		purchasePlan.selectPayAsYouGoPlan();
	}
	//method to check whether buy button loaded on click of Pay As You Go option
	this.buyPlanBtnLoaded =function(){
		return purchasePlan.buyPlanBtnLoaded();
	}
	//method for clicking buy button 
	this.buyServicePlan = function(){
		purchasePlan.buyServicePlan();
	};
	//isPopUpLoaded checks whether pop-up loaded on clicking buy button
	this.isPurchasePopUpLoaded = function(){
		return purchasePlan.isPurchasePopUpLoaded();
	};
	//Checks whether the pop-up is displayed with a checkout button
	this.isChkoutPopUpLoaded = function(){
		return purchasePlan.isChkoutPopUpLoaded();
	};	
	//method to click on One Time Purchase button
	this.purchaseOneTimePlan = function(){
		purchasePlan.purchaseOneTimePlan();
	};
	//method to click on Checkoout button in purchase plan
	this.clickOnCheckout = function(){ 
		purchasePlan.clickOnCheckout();
	};
	
	//method to click on 'credit' tab for payment
	this.clickCreditTab = function(){
		checkoutpf.clickCreditTab();
	};
	//method to check whether credit details listed for payment
	this.paymentOptionLoaded = function(){
		return checkoutpf.paymentOptionLoaded();		
	};
	//method to click on cc number field
	this.clickNewPayment = function(){
		checkoutpf.clickNewPayment();
	};
	// method to enter cc details
	this.enterCcDetails = function(ccNum,cvv){
		checkoutpf.enterCcDetails(ccNum,cvv);
	};
	// method to enter billing details
	this.enterBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		checkoutpf.enterBillingDetails(fname,lname,address1,houseNum,city,zipcode);
	};
	//method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function(){
		return checkoutpf.confirmationPageLoaded();
	};
	//method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function(){
		checkoutpf.proceedFromConfirmationPage();
	};
	
	//** this method checks whether the account details fields to be enetered are loaded or not
	this.emailTextBoxLoaded = function(){
		return newAccountObj.emailTextBoxLoaded();
	};
	
	//** method to enter airtime pin
	this.enterAirTimePin = function(pin){
		airtimePinObj.enterAirTimePin(pin);      
	};
  
	this.airtimePurchase = function(){
		purchasePlan.airtimePurchase();
	};
  
	//** this method checks whether the account creation/existing account flows page is loaded or not
	this.activationAccountPageLoaded = function(){ 
		return newAccountObj.activationAccountPageLoaded(); 
	};
  
	//** this method is to proceed with account creation flow
	this.clickonAccountCreationContinueBtn = function(){
		newAccountObj.clickonAccountCreationContinueBtn();
	};
  
	//** this method checks whether the account details fields to be enetered are loaded or not
	this.fbButtonLoaded = function(){
		return newAccountObj.fbButtonLoaded();
	};
  
	//** this method is to enter the required fields to create new account
	this.enterAccountDetails= function(email,password,DOB,pin){
		newAccountObj.enterAccountDetails(email,password,DOB,pin);
	};
	
	//** this method is to check whether the popup page is loaded after successful account creation
	this.accountCreationDone = function(){
		return newAccountObj.accountCreationDone();
	};
  
	//** this method is to proceed from the popup page
	this.clickOnAccountCreatedPopupBtn = function(){
		newAccountObj.clickOnAccountCreatedPopupBtn();
	};
    
	//** this method is to check whether the final instruction page is loaded or not
	this.finalInstructionPageLoaded = function(){
		return finalInstructionPage.finalInstructionPageLoaded();
	};
	
	//** this method is to check whether the final instruction page is loaded or not for port in scenario
	this.finalInstructionPageLoadedPortInPurchase = function(){
		return finalInstructionPage.finalInstructionPageLoadedPortInPurchase();
	};
	
	this.finalInstructionPageLoadedPortInPin = function(){
		return finalInstructionPage.finalInstructionPageLoadedPortInPin();
	};
  
	//** this method is to proceed from the final instruction page
	this.finalInstructionProceed = function(){
		finalInstructionPage.finalInstructionProceed();
	};
	
	//** this method is to proceed from the final instruction page
	this.finalInstructionProceedPortInPurchase = function(){
		finalInstructionPage.finalInstructionProceedPortInPurchase();
	};
	
	this.finalInstructionProceedPortInPin = function(){
		finalInstructionPage.finalInstructionProceedPortInPin();
	};
	
	//** this method is to check whether the summary page is loaded or not
	this.summaryPageLoaded  = function(){
		return summaryObj.summaryPageLoaded();
	};
	
	//** this method is to check whether the summary page is loaded or not
	this.reactivationSummaryPageLoaded  = function(){
		return summaryObj.reactivationSummaryPageLoaded();
	};
  
	//** this method is to proceed from the summary page
	this.clickOnSummaryBtn = function(){
		summaryObj.clickOnSummaryBtn();
	};
	
	//** this method is to proceed from the summary page
	this.clickOnRedemptionDoneBtn = function(){
		summaryObj.clickOnRedemptionDoneBtn();
	};
	
  
	//** this method is to check whether the survey page is loaded or not
	this.surveyPageLoaded = function(){
		return surveyPageObj.surveyPageLoaded();
	};
  
	//** this method is to proceed from the survey page using the THANK YOU option
	this.clickOnThankYouBtn = function(){
		return surveyPageObj.clickOnThankYouBtn();
	}; 
  
	//** this method is to proceed with existing account flow
	this.clickonExistingAccountContinueBtn = function(){
		existingAccnt.clickonExistingAccountContinueBtn();
	};
	
	//** this method is to check whether the field to enter existing userid is loaded or not
	this.existingUserIdBoxLoaded = function(){
		return existingAccnt.existingUserIdBoxLoaded();
	};
  
	//** this method is to enter the userid/sim(field identifier is SIM hence using SIM in the methods)
	this.enterSimInExistingflow = function(sim){
		existingAccnt.enterSimInExistingflow(sim);
	};
  
	//** this method is to check whether field to enter the password is loaded or not
	this.enterPasswordBoxLoaded = function(){
		return existingAccnt.enterPasswordBoxLoaded();
	};
  
	//** this method is to enter the password
	this.enterPassword = function(pwd){
		existingAccnt.enterPassword(pwd);
	};
   
	//** this method is to check whether the page to select service plan is loaded or not
	this.selectServicePlanPageLoaded = function(){
		return selectPlan.selectServicePlanPageLoaded();
	};
  
	//** this method is to select a plan
	this.pickPlan = function(planName){
		selectPlan.pickPlan(planName);
	};
  
	//** this method is check whether the page to enter service provider details is loaded or not
	this.ServiceProviderPageLoaded = function(){
		return phoneAccountDetails.ServiceProviderPageLoaded();
	};
  
	//** this method is to enter the phone type
	this.selectPhoneType = function(){
		phoneAccountDetails.selectPhoneType();
	};
	
	//** this method is to check whether the fields to enter phone account details is loaded or not
	this.accountDetailsLoaded = function(){
		return phoneAccountDetails.accountDetailsLoaded();
	};
  
   //** this method is to enter the phone account details
	this.enterPhoneAccountDetails = function(accountNum,pswd,socialSec,fname,lname,phnNo,addOne,addtwo,city,zip) {
		phoneAccountDetails.enterPhoneAccountDetails(accountNum,pswd,socialSec,fname,lname,phnNo,addOne,addtwo,city,zip);
	};
  
	//** this method is to check whether the pop up for selecting address is loaded or not
	this.selectAddressPopUpLoaded = function(){
		return phoneAccountDetails.selectAddressPopUpLoaded();
	};
  
	//** this method is to click on the keep this address button  in the pop up
	this.keepThisAddress = function(){
		phoneAccountDetails.keepThisAddress();
	};
  
	//** this method is to check whether the page to enter address details is loaded or not
	this.addressDetailsPageLoaded = function(){
		return phoneAccountDetails.addressDetailsPageLoaded();
	};
  
	//** this method is to enter the address details
	this.addressDetails = function(unitNo,street,houseNo){
		phoneAccountDetails.addressDetails(unitNo,street,houseNo);
	};
  
	//** this method is to enter the mobile number 
	this.enterMobNumber = function(number){
		portIn.enterMobNumber(number);
	};
	
	//** this method is to  check whether the final instruction page is loaded or not
	this.finalInstructionPageLoadedPurchase = function(){
		return finalInstructionPage.finalInstructionPageLoadedPurchase();
	};
  
	//** this method is to proceed from the final instruction page for purchase scenarios
	this.finalInstructionProceedPurchase = function(){
		finalInstructionPage.finalInstructionProceedPurchase();
	};
	
	//** this method is to check whether the summary page is loaded or not for purchase scenarios
	this.summaryPageLoadedPurchase  = function(){
	return summaryObj.summaryPageLoadedPurchase ();
	
	};
  
	//** this method is to proceed from the summary page for purchase scenarios
	this.clickOnSummaryBtnPurchase = function(){
	summaryObj.clickOnSummaryBtnPurchase();
	};
	
	/*
	this.toEnterSimNumber = function(sim){
		return internalPortInPf.toEnterSimNumber(sim);
	};
	
	this.toEnterLastFourPinPageLoad = function(){
		return internalPortInPf.toEnterLastFourPinPageLoad();
	};
		
	this.toEnterLastFourPin = function(esn){
		return internalPortInPf.toEnterLastFourPin(esn);
	};
	
	this.airTimePinPageLoad = function(){
		return internalPortInPf.airTimePinPageLoad();
	};*/
	
	this.enterFourDigitCodeFromMsg = function(number){
		validateCurrentEsn.enterFourDigitCodeFromMsg(number);
	};
	
	//method to check whether the error message is displayed after giving the invalid pin
	this.errorMessageIsDisplayed = function(){
		return airtimePinObj.errorMessageIsDisplayed();
	};
	
	this.keepCurrentPhnNum = function(){
		return zipCodeObj.chooseDontKeepMyNumber();
	};
	
	this.zipcodeDivLoaded = function(){
		return zipCodeObj.zipcodeDivLoaded();
	};
  
  };
module.exports = new Activate;