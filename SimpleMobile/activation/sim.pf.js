//this represnts a modal for the SIM page

'use strict';
var ElementsUtil = require("../util/element.util");

var sim = function() {
	
	this.identifySIMPage = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[2]/subheading/div/div/div[1]/table/tbody/tr[1]/td"));
	this.simErrorField = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div"));
	this.simTextBox = element.all(by.name('sim')).get(1);//element.all(by.xpath('//*[@id="sim"]')).get(1);//*[@id="sim"]
	this.simContinueBtn = element.all(by.id('btn_continuesimcardnumber')).get(0);
	//this.byopSimTextBox = element.all(by.id('number')).get(1);
	this.byopSimContinueBtn = element(by.css('button[id="btn_collectsim"]'));
	this.byopPurchasePinBtn = element.all(by.css('[ng-click="action()"]')).get(3);
	this.byopSimTextBox = element(by.css('input[id="simbyop"]'));
	this.byopSimPurchaseTextBox = element.all(by.css('input[id="simbyop"]')).get(1);

	this.enterByopSIM = function(simNum){
		this.byopSimTextBox.clear().sendKeys(simNum);
		this.byopSimContinueBtn.click();
	};
	
	this.enterByopSimPurchase = function(simNum){
		this.byopSimPurchaseTextBox.clear().sendKeys(simNum);
		this.byopSimContinueBtn.click();
	};
	
	this.selectByopPurchaseAirTime = function() {
		this.byopPurchasePinBtn.click();
	};
  
	this.isByopSIMPage = function(){
        return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /byopcollectsim/.test(url);
		});
	};
	
	//** method to check whether the SIM page is loaded
	this.isSIMPage = function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			//return /collectsim/.test(url);
			return /collectesn/.test(url);
		});
	};
	
	//** method to enter the SIM
	this.enterSIM = function(simNum){
		ElementsUtil.waitForElement(this.simTextBox);//for add device scenario
		this.simTextBox.clear().sendKeys(simNum);
		this.simContinueBtn.click();
	};
  
	//** method to display error after entering invalid SIM 
	this.isSIMError = function(){
		//browser.wait(expectedConditions.visibilityOf(this.simErrorField),10000);
		//ElementsUtil.waitForElement(this.simErrorField);
		return this.simErrorField.getText();
	};
	
};
module.exports = new sim;