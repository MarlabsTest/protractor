//this modal represents a modal for whether to go with activate new phone or activate a BYOP

'use strict';

var ElementsUtil = require("../util/element.util");

var Activate = function() {
		
	//this.continueBtn =element.all(by.css('[ng-click="action()"]'));
	//this.actualContinue = this.continueBtn.get(3);
	//this.continueBtn = element(by.id("btn_continuetracfonephone"));
	this.continueBtn = element(by.id("btn_havephone"));
 
	//this.byopBtn = element.all(by.css('[ng-click="action()"]')).get(5);
	//this.byopBtn = element(by.id('btn_continuesmartphone'));
	this.byopBtn = element.all(by.id('btn_havetablet')).get(0);
	this.termsConditionsModal = element(by.id('modal-body'));
	this.termsConditionsAcceptBtn = element(by.id('btn_acceptterms&conditions'));
	this.airtimeBenefitsWarningModal = element(by.css('.modal-dialog.modal-md'));
	this.continueAirtimeBenefitsWarningBtn = element.all(by.id('btn_continue')).get(1);
	//this.continuePhoneUpgradeRequestBtn = element.all(by.id('btn_summaryviewpincontinue')).get(1);
	this.continuePhoneUpgradeRequestBtn = element(by.id('btn_summaryviewpincontinue'));
	this.keepCurrentPhnNumNo= element.all(by.buttonText('NO')).get(1);
  
	this.maximiseBrowser = function() {
		browser.driver.manage().window().maximize();
	};
    
	//** this method checks whether the page is loaded or not after the click on activate link from homepage
	this.isActivateLoaded = function() {
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /selectdevice/.test(url);
		});
	};
	
	this.keepCurrentPhnNum =  function() {
		this.keepCurrentPhnNumNo.click();
	
	};
	
	this.isPhoneUpgradeRequestPageLoaded = function() {
		ElementsUtil.waitForElement(this.continuePhoneUpgradeRequestBtn);
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /PIN_FLOW/.test(url);
		});
	};
	
	//check whether the terms and conditions popup shown
	this.isTermsAndConditionsPopUpShown = function() {
		//$$//ElementsUtil.waitForElement(this.termsConditionsModal);
		return this.termsConditionsModal.isPresent();
	}
	
	this.isAirtimeBenefitsWarningShown = function() {
		//$$//ElementsUtil.waitForElement(this.airtimeBenefitsWarningModal);
		return this.airtimeBenefitsWarningModal.isPresent();
	}

	this.selectDeviceTypeBYOP = function() {
		//console.log("clicking byop button");
		this.byopBtn.click();
	};
	
	this.acceptTermsConditions = function() {
		if(this.checkTermsConditionsModalExist())
		{
			this.termsConditionsAcceptBtn.click();
		}
	}
	
	this.continueAirtimeBenefitsWarning = function() {
		this.continueAirtimeBenefitsWarningBtn.click();
	}
	
	this.continuePhoneUpgradeRequestPage = function() {
		this.continuePhoneUpgradeRequestBtn.click();
	}
	
	this.checkTermsConditionsModalExist = function() {
		return this.termsConditionsModal.isPresent();
	}

	//**this method is to proceed with new phone activation flow
	this.gotToEsnPage= function() {
		//ElementsUtil.elementHasCome(this.continueBtn, 3);
		ElementsUtil.waitForElement(this.continueBtn);
		this.continueBtn.click();
	};
   
	
	
	//this.waitForElementLoad  = function(min) {
	//	browser.sleep(min);
	//};
  
  };
module.exports = new Activate;