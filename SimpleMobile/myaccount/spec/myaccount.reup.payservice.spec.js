/*
 * **********Spec file is for MULTILINE SERVICE RENEWAL ******** 
 * 
 * 1.Initially we will do the normal activation flow using airtime pin using new account 
 * 2.After successful activation,click on the payservice button in the my account dashboard
 * 3.Redirected to the cehckout page
 * 4.Do the payment and will be redirected to the confirmation page,survey page and finally to my account dashboard  
 * 
 */
'use strict';

var myAccount = require("../myaccount.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var activation = require("../../activation/activation.po");

describe('SM Multiline Service Renewal', function() {
	var expectedConditions = protractor.ExpectedConditions;
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	/*Click on the pay service button in myaccount dashboard
	 *Expected result - AutoEnrollment POPup page will be displayed  
	 */
	it('should display autoenrollment popup page', function(done) {
//		myAccount.goToManageProfile();
//		myAccount.goToMyDevices();
		//myAccount.reload();
		myAccount.payService();		
		expect(myAccount.payServiceButtonPresent()).toBe(true);
		done();		
	});
	
	/*Click on the no thanks button in popup page
	 *Expected result - Checkout page will be displayed  
	 */
	it('should navigate to the checkout page', function(done) {
		myAccount.proceedFromPayService();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
//		expect(myAccount.discountTextVisible()).toBe(true);
		done();		
	});
	
	
	it('should enter billing and cc details', function(done) {
		myAccount.smMldDiscountTextVisible();
		//myAccount.discountTextVisible()
		var savedCardTab = element.all(by.css("a[translate='BAT_24506']")).get(0);		
		savedCardTab.isPresent().then(function(isVisible) {
		if(isVisible){
			myAccount.savedCardCVV(CommonUtil.getCvv(sessionData.simplemobile.cardType));
			
		}else {
			myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType));
			myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		}
		});	
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	*/	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	*/
	it('should load the final instruction page', function(done) {
		myAccount.placeMyOrder();		
		expect(myAccount.confirmationPageLoaded()).toBe(true);
		done();		
	});
	
	/*Continue from the confirmation page ,will be redirected to the survey page
	 *Continue from survey page 
	 *Expected result - myaccount dashboard will be loaded
	 */
	it('should navigate to the account dashboard page', function(done) {
		myAccount.proceedFromConfirmationPage();
		activation.clickOnThankYouBtn();					
		
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(sessionData.simplemobile.esn,'0','6','SM_Purchase','true','false');
		console.log('ITQ_DQ_CHECK table updated!!');
		//myAccount.dashboard();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	
});
