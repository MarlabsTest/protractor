'use strict';

//Firstly click on the forgot password 
//then we have to enter the email through which we reset the password
//then the link will be sent to our email to reset the password 

var resetPassword = require("../myaccount.po");
var login = require("../../login/login.po");
var loginData = require("../../common/sessiondata.do");
var home = require("../../common/homepage.po");

describe('Tracfone Forgot Password', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
		
	//In My Account home page provide the username which is already have an account in simple mobile.
	//Thn it will navigate to enter the password page
	it('provide username and navigates to enter password',function(done){
		home.myAccount();
		resetPassword.enterTheUserName(loginData.simplemobile.username); 
		expect(resetPassword.isForgotPasswordPageLoaded()).toBe(true); 
		done();
	});
	
	//Here click on the forgot password link so that you will get a temporary password to retrieve your account
	//Pop window appears saying that "temporary password will be sent to your email id you provided"
	it('Click on the forgot password',function(done){
		resetPassword.clickOnForgotPassword(); 
		expect(resetPassword.popUpWindow()).toBe(true); 
		done();
	});
	
	
//	it('the password resetting mail has been sent when Login button is clicked ',function(done){
//		resetPassword.PopUpWindowClick();
//		expect(resetPassword.isForgotPasswordPageLoaded()).toBe(true); 
//	});

});
	