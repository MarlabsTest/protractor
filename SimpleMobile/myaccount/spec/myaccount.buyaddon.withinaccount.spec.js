'use strict';

var activation = require("../../activation/activation.po");
var myAccount = require("../myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
//===============Buy service plan with in account Scenario========================

describe('SM buy ILD with enrollment inside Account', function() {
	it('should navigate to Active Devices Buy Add Ons', function(done) {
		console.log("should navigate to Active Devices All Options");
		myAccount.chooseBuyAddOnPlan();  
		expect(myAccount.isBuyAddOnPageLoaded()).toBe(true);
		done();
	});
		
	it('should choose Add On plan and complete payment', function(done) {
		console.log("should choose Add On plan and complete payment");
		myAccount.selectAPlan();     //--------Continue with the plan-----------
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});	
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - final instruction page will be loaded
	 */	
	it('should display the billing zipcode popup', function(done) {
		
		var moreServicesLabel = element.all(by.css('[name="addon"]')).get(0).element(by.className('selectize-input'));
		moreServicesLabel.isPresent().then(function(isVisible){
			if(!isVisible)
			{
				//console.log("not visible");
				myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
				myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
			}
			else
			{
				//console.log("visible");
				myAccount.enterCcDetailsDynamicIndexOne(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType), true);
				myAccount.enterBillingDetailsDynamicIndexOne(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
			}
		});
		
		myAccount.placeMyOrder();
		//expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	 * /	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	 * /	
	it('should load the final instruction page', function(done) {
		myAccount.placeMyOrder();
		//expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	*/
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 * /
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	*/
	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the summary page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		//expect(activation.surveyPageLoaded()).toBe(true);
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	
	/*Click on 'No,Thanks' button in the Survey Page 
	 *Expected result - My account dashboard will be loaded
	 * /
	it('should redirect to the account dashboard page', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	*/
});
	