'use strict';

var activation = require("../../activation/activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};
var generatedEsnSim = {};

describe('SimpleMobile Add device to myaccount', function() {

	//add line configurable
	console.log('Spec sessionData.simplemobile.noOfLines======',sessionData.simplemobile.noOfLines);	
	if (sessionData.simplemobile.noOfLines == ''){
		sessionData.simplemobile.noOfLines = 1;
	}	
	
	for(var line = 0;line < sessionData.simplemobile.noOfLines;line++)
	{
	
	
	it('click on add line menu from My Account page', function(done) {		
		myAccount.goToAddline();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it(' click on the continue button and it will navigates to Sim page',function(done){		
		activation.gotToEsnPage();
		expect(activation.isSIMPage()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('enter the married sim and click the continue button', function(done) {
		var esnval = DataUtil.getESN(sessionData.simplemobile.esnPartNumber);
       	console.log('returns', esnval);
		var simval = DataUtil.getSIM(sessionData.simplemobile.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
		DataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.simplemobile.esn = esnval;
		sessionData.simplemobile.esnsToReactivate.push(esnval);
		activation.enterMarriedSim(simval);
		sessionData.simplemobile.sim = simval;
		generatedEsnSim['esn'] = sessionData.simplemobile.esn;
		generatedEsnSim['sim'] = sessionData.simplemobile.sim;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	it('enter the zipcode and navigate to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.simplemobile.zip);		
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*Select an airtime Plan 
	 *Expected result - Selected Plan details form will be loaded
	 */
	it('should load the selected plan details and buy it', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('choose 30day plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.simplemobile.shopPlan;
		var planName		= sessionData.simplemobile.planName;
		
		console.log('from spec plan type',planType);
		console.log('from spec plan name',planName);
		shop.choosePlanByNameForNewLine(planType,planName);
		console.log("SELECTED!!!");
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
	/*Enter the credit card and billing details in the payment form 
	 *Expected result - billing zipcode changes popup shown
	 */
	it('should show the billing zipcode changes popup', function(done) {
		myAccount.smMldDiscountTextVisible();		
		var savedCardTab = element.all(by.css("a[translate='BAT_24506']")).get(0);		
		savedCardTab.isPresent().then(function(isVisible) {
		if(isVisible){
			myAccount.savedCardCVV(CommonUtil.getCvv(sessionData.simplemobile.cardType));
			
		}else {
			myAccount.enterCcDetails(""+generator.GenCC(sessionData.simplemobile.cardType),CommonUtil.getCvv(sessionData.simplemobile.cardType));
			myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		}
		});
		myAccount.placeMyOrder();
		expect(myAccount.isBillingZipcodeChangesPopupShown()).toBe(true);
		done();		
	}); 
	
	/*Agree billing zipcode changes popup
	 *Expected result - checkout page will be loaded
	*/	
	it('should load back to the same check out page', function(done) {
		myAccount.agreeBillingZipcodeChanges();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	}); 
	
	/*Click place my order button
	 *Expected result - final instruction page will be loaded
	*/
	it('should load the final instruction page', function(done) {
		myAccount.placeMyOrder();
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	});
	
	/*Click on continue button in the Final Instruction Page 
	 *Expected result - Summary Page will be shown
	 */
	it('should load the summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoadedPurchase()).toBe(true);
		done();		
	});
	

	/*Click on continue button in the Summary Page 
	 *Expected result - Survey Page will be loaded
	 */
	it('should load the survey page', function(done) {
		activation.clickOnSummaryBtnPurchase();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	/*Click on No Thanks button in the Survey Page 
	 *Expected result - MyAccount Dashboard will be loaded
	 */
	it('click on the done button in the summary page and navigate to the account dashboard', function(done) {
		activation.clickOnThankYouBtn();					
		expect(myAccount.isLoaded()).toBe(true);		
		done();	
	});
	 
	}
	
});
