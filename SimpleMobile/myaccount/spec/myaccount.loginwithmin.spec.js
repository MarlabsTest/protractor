'use strict';

var login = require("../../login/login.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var homePage = require("../../common/homePage.po");

/*
 * Spec		: SM Login with MIN
 * Details	: This spec is for testing the login functionality using an valid MIN/password.which is already stored on 
 * 			  session by activation spec.
 * */
describe('SM Login with MIN', function() {

	/*
	 * Should navigate to My Account page for entering MIN of existing account. 
	 * */	
	it('Should navigate to My Account page', function(done) {
		homePage.myAccount();
		expect(homePage.isMyAccountPageLoaded()).toBe(true);
		done();
	});
	
	/*
	 * Should navigate to access account page for entering password of existing account. 
	 * */
	it('Should navigate to access account page', function(done) {
		console.log("esn:::: ",sessionData.simplemobile.esn);
		var min = dataUtil.getMinOfESN(sessionData.simplemobile.esn);
		console.log("min:::: ",min);
		login.validateLogin(min); 
		expect(login.isValidLogin()).toBe(true);
		done();
	});
	 
	/*
	 * Should navigate to my account dashboard page. 
	 * */
	it('Should navigate to myaccount dashboard page', function(done) {
		console.log("password=====",sessionData.simplemobile.password);
		login.validatePassswordForMin(sessionData.simplemobile.password);	
		expect(login.isValidPassword()).toBe(true);
		done();
	});  
	
});
	