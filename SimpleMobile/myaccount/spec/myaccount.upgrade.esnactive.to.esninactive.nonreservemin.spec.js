
/* Spec file for  Upgrade from ESN Active with service end date in future to 
 * ESN Inactive with Min not reserved - Belongs to Same account
 * author:rpillai
 * */

'use strict';

var sessionData 	= require("../../common/sessiondata.do");
var activationPo	= require("../../activation/activation.po");
var myAccountPo 	= require("../../myaccount/myaccount.po");
var homePagePo 		= require("../../common/homepage.po");
var dataUtil		= require("../../util/datautils.util");
var generatedMin 	= {};
var generatedEsnSim = {};

describe('Net10 Upgrade activated ESN to inactive ESN with MIN not reserved: ', function() {
	//should first deactivate the device and then reload the page for to activate the device,upon clicking activate link
	it('Deactivate the device', function(done) {	
        var min = dataUtil.getMinOfESN(sessionData.netten.esn);
        var varPhoneStstus	= sessionData.netten.phoneStatus;
        if(varPhoneStstus == "INACTIVE_NONRESERVE"){
        	dataUtil.deactivatePhone('DEACTIVATE',sessionData.netten.esn,min,'PAST_DUE','NET10');
        }else{
        	dataUtil.deactivatePhone('DEACTIVATE',sessionData.netten.esn,min,'CUSTOMER REQUESTED','NET10');
        }
       	
		homePagePo.myAccount();
		expect(myAccountPo.isLoaded()).toBe(true);		
		done();
	}); 
	//Click on activate button of deactivated device.Opt for 'Keep My PhoneNumber' and select active device's MIN.
	it('Proceed for activation,', function(done) {
		myAccountPo.clickOnActivate();
		//expect(myAccountPo.selectDevicePageLoaded()).toBe(true);
		expect(activationPo.isSIMPage()).toBe(true);
		done();
	});
	
	/*Enter the married SIM 
	 *Expected result - Security Popup page will be displayed  
	 */
	it('should navigate to pop up for providing the security PIN number', function(done) {
		var esnval = dataUtil.getESN(sessionData.netten.esnPartNumber);
       	console.log('returns', esnval);
		var simval = dataUtil.getSIM(sessionData.netten.simPartNumber);
       	console.log('returns', simval);
		//db call for EN-SIM marry
       	dataUtil.addSimToEsn(esnval,simval);
		console.log("DB updated !!");
		sessionData.netten.esn = esnval;
		sessionData.netten.esnsToReactivate.push(esnval);
		activationPo.enterMarriedSim(simval);
		sessionData.netten.sim = simval;
		generatedEsnSim['esn'] = sessionData.netten.esn;
		generatedEsnSim['sim'] = sessionData.netten.sim;
		activationPo.checkBoxCheck();
		activationPo.continueESNClick();
		expect(activationPo.keepMyPhonePageLoaded()).toBe(true);
		done();	
	}).result.data = generatedEsnSim;
	
	//For upgrading inactive device, select MIN of active device from dropdown
	it('Select MIN of Active device', function(done) {	
		myAccountPo.selectActivatedMin();
		expect(activationPo.finalInstructionContinueBtnPortInPin()).toBe(true);
		done();
	});
	//should click on the continue button in the final instruction page
	it('Load Final instruction page', function(done) {
		activationPo.finalInstructionProceedPortInPin();		
		expect(activationPo.summaryPageLoaded()).toBe(true);		
		done();	
	});
	//should click on the done button in the summary page
	it('Load survey page', function(done) {
		activationPo.clickOnSummaryBtn();
		expect(activationPo.surveyPageLoaded()).toBe(true);
		done();	
	});
	//should click on the thank you button and will be redirected to the account dashboard page
	it('Take activation survey and activate device', function(done) {
		activationPo.clickOnThankYouBtn();
		expect(myAccountPo.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
});
