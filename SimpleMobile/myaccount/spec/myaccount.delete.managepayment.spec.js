'use strict';
//go to the manage payment page
//click on the edit payment details
//the card details which we saved during our payment will be deleted

var myAccount = require("../myaccount.po");
//================Manage Payment Scenario=====================

describe(' delete manage payment', function() {

	
	it('should navigate to Manage Payment page', function(done) {
		console.log("should navigate to Manage Payment page");
		myAccount.paymentMethod();
		expect(myAccount.isManagePaymentLoaded()).toBe(true);
		done();
	});
	
	
	
	//click on the edit payment 
	//click ont the delete option which is displayed
	//the saved card details will be deleted and it navigate to the homepage.
	it('Delete the saved card details', function(done) {
		console.log("Should navigate to manage payment page");
		myAccount.deleteCardDetails();
		expect(true).toBe(true);
		done();
	});	
});
	