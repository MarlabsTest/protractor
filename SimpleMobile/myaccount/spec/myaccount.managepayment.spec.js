'use strict';

var myAccount = require("../myaccount.po");

//================Manage Payment Scenario=====================

describe('SM Manage Payment', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	// require("./myaccount.login.spec.js");

	it('should navigate to Manage Payment page', function(done) {
		console.log("should navigate to Manage Payment page");
		myAccount.paymentMethod();
		expect(myAccount.isManagePaymentLoaded()).toBe(true);
		done();
	});	
		
	it('should add payment details', function(done) {
		console.log("should add payment details");
		myAccount.editPaymentDetails();
		expect(myAccount.isPaymentAdded()).toEqual(1);	//myAccount.isPaymentAdded()
		done();
	});	
});
	