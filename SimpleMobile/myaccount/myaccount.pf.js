'use strict';
var ElementsUtil = require("../util/element.util");

//=============Serves all functions inside MyAccount============

var MyAccount = function() {
	
	this.myDevices = element(by.linkText('MY DEVICES'));  //Add Device
	this.manageProfile = element.all(by.linkText('MANAGE PROFILE')).get(0);	
	this.paymentMethod = element.all(by.id('paymentmethod')).get(0);
	this.signOut = element(by.linkText('SIGN OUT'));
	this.addDevice = element.all(by.id('Adddevicenav')).get(0);
	this.addDevicePopupBtn = element.all(by.id('btn_adddevice1')).get(1);
	this.serviceRenewBtn = element(by.buttonText('ReUp My Service'));
	this.enrolBtn = element.all(by.css("button[id='noEnrollToAutoPay_btn']")).get(1);
//	this.serviceRenewBtn = element.all(by.css("span[translate='MYACC_5001']")).get(0);
	//this.activateBtn = element.all(by.buttonText('ACTIVATE')).get(1);
	this.activateBtn = element.all(by.css('[type="submit"]')).get(4);
	this.AutoPayBtn = element.all(by.css("span[translate='MYACC_5000']")).get(1);
	this.allOptions = element.all(by.css("a[title='EDIT']")).get(0);
	this.myAccount = element.all(by.id('lnk_tfhome_myaccount')).get(1);
	this.discountText = element(by.css("p[translate='MYACC_5382']"));
	
	this.continueBtn = element.all(by.buttonText('CONTINUE')).get(1);
	this.buyPlan = element(by.css("span[translate='MYACC_5238']"));	
	this.buyAddOnPlan = element(by.css("span[translate='MYACC_5015']"));
	this.close = element(by.css("button[title='close']"));
	this.clickPlan = element(by.css('button[id="monthly_507"]'));	                 
	this.deviceAdded = element.all(by.css("button[id='btn_']")).get(3);
	this.deviceToRemove = element.all(by.css("a[ng-click='editAction()']")).get(2); 
	this.removeDeviceOption = element.all(by.css("a[ng-click='removeDevice()']")).get(1); 
	this.removeConfirm = element.all(by.buttonText('YES')).get(1);
	this.closeBtnDevice = element(by.css("button[title='CLOSE']"));
	this.ildPlanBtn = element.all(by.css("button[id='monthly_102']")).get(0);
	// this.popupClose = element(by.css("button[title='close']"));
	this.selectPlan = element.all(by.css("button[id='btn_plancardaddonaddtocart']")).get(5);	
	this.continueToCheckout = element.all(by.buttonText('Continue to Checkout')).get(1);
	this.newpopupwindow = element.all(by.buttonText('Yes')).get(0);
//	this.serviceRenewBtn = element.all(by.css("span[translate='MYACC_5001']")).get(1); //payservice
	//newly added for tracfone
	this.addAirTimePinBtn = element.all(by.buttonText("ADD AIRTIME")).get(1);
	//reactivation by aswathy
	//this.activateButton = element.all(by.id('btn_activate')).get(1);
	//this.activateButton = element(by.css('[type="submit"]'));
	this.activateButton = element.all(by.css('[type="submit"]')).get(1);
	//this.addAirtimePin =  element.all(by.css("button[id='btn_addairtime']")).get(1);
	this.addAirtimePin =  element.all(by.css("[ng-click='action()']")).get(1);
	this.selectDevice = element.all(by.css('[ng-model="$select.search"]')).get(0);
	this.selectActiveMin = element.all(by.className('dropdown-content-item ng-scope')).get(0);
	this.btnContinuePhonenumber	= element.all(by.id('btn_continuephonenumber')).get(0);
	this.payServiceBtn = element.all(by.id('PayService')).get(0);
	
	this.goToMultilineAutopay = function() {
		//$$//ElementsUtil.waitForElement(this.AutoPayBtn);
		return this.AutoPayBtn.click();
	};
	
	this.clickAddAirtime = function() {
		//$$//ElementsUtil.waitForElement(this.addAirtimePin);
		return this.addAirtimePin.click();
	};
	
//	this.payService = function(){  //payservice
//		//$$//ElementsUtil.waitForElement(this.serviceRenewBtn);
//		return this.serviceRenewBtn.click();
//	};
	
	this.enroll = function(){  
		return this.enrolBtn.click();
	};
	
	this.selectActivatedMin = function(){
		ElementsUtil.waitForElement(this.selectDevice);
		this.selectDevice.click();
		this.selectActiveMin.click();
		this.btnContinuePhonenumber.click();
	};
	
	this.isBuyAddOnPageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/addons/);	
	};
	
	this.selectILDPlan = function() {
		this.ildPlanBtn.click();
	};
	
	//Upgrade ESN active to ESN inactive(non reserve) starts :Remya
	this.selectDevicePageLoaded = function(){
		ElementsUtil.waitForElement(this.selectDevice);
		return this.selectDevice.isPresent();
	};
	
	this.clickContinue = function() {
		//$$//ElementsUtil.waitForElement(this.continueBtn);
		return this.continueBtn.click();
	};
	
	this.goToAllOptions = function() {
		//$$//ElementsUtil.waitForElement(this.allOptions);
		return this.allOptions.click();
	};
	
	this.chooseBuyPlan = function() {
		//ElementsUtil.waitForElement(this.buyPlan);
		return this.buyPlan.click();
	};
	
	this.chooseBuyAddOnPlan = function() {
		ElementsUtil.waitForElement(this.buyAddOnPlan);
		return this.buyAddOnPlan.click();
	};
	
	this.selectAPlan = function() {
		//ElementsUtil.waitForElement(this.clickPlan);
		//this.close.click();
		this.clickPlan.click();
		// this.popupClose.click();
		//return this.selectPlan.click();	
	}; 
	
	this.goToSelectPlan = function() {
		//$$//ElementsUtil.waitForElement(this.selectPlan);
		this.selectPlan.click();	
		return this.continueToCheckout.click();
	}; 
	
	this.goToMyDevices = function() {
		//$$//ElementsUtil.waitForElement(this.myDevices);
		return this.myDevices.click();
	};
	
	this.goToManageProfile = function() {
		ElementsUtil.waitForElement(this.manageProfile);
		return this.manageProfile.click();
	};
	
	this.reload = function() {
		this.paymentMethod.click();
		browser.sleep(1000);
		return this.myAccount.click();
	};
	
	this.discountTextVisible = function() {
		return this.discountText.isPresent();
	};
	
	
	
	this.goToPaymentMethod = function() {
		ElementsUtil.waitForElement(this.paymentMethod);
		return this.paymentMethod.click();
	};
	
	this.goToPaymentHistory = function() {
		//$$//ElementsUtil.waitForElement(this.paymentHistory);
		return this.paymentHistory.click();
	};
	
	this.goToAddDevice = function() {
		//ElementsUtil.waitForElement(this.addDevice);
	    this.addDevice.click();
	    ElementsUtil.waitForElement(this.addDevicePopupBtn);
	    this.addDevicePopupBtn.click();
	    
	};
	
	this.goToSignOut = function() {
		ElementsUtil.waitForElement(this.signOut);//needed for byop activation scenario
		return this.signOut.click();
	};
	
	this.removeDevice = function() {
		//$$//ElementsUtil.waitForElement(this.deviceToRemove);
		this.deviceToRemove.click();
		// ElementsUtil.waitForElement(this.removeDeviceOption);
		this.removeDeviceOption.click();
		//$$//ElementsUtil.waitForElement(this.removeConfirm);
		this.removeConfirm.click();
		ElementsUtil.waitForElement(this.closeBtnDevice);
		return this.closeBtnDevice.click(); 
	};
	
	this.isDeviceRemoved = function() {
		return browser.wait(function() {
			return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /dashboard$/.test(url);
		});
		}, 10000, "URL hasn't changed");
	};
	//** method checks whether the account dashboard is loaded or not
	this.isLoaded = function(){
		//ElementsUtil.waitForElement(this.deviceAdded);
		return browser.wait(function() {
			return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /dashboard$/.test(url);
		});
		}, 10000, "URL hasn't changed");
	};
	
	this.refresh = function() {
		return browser.getCurrentUrl().then(function (url) {
					console.log('Currenturl: ', url);
                    return browser.get(url);
                });
        };
	
	//** method to proceed with the pay service flow
	this.payService = function(){
		//$$//ElementsUtil.waitForElement(this.serviceRenewBtn);
		this.serviceRenewBtn.click();
	};
	
	//** method to check whether the checkout page is loaded or not
	this.checkoutPageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/checkout$/);	
	};
	
	//** method to click on the activate button, after adding a device through the dashboard
	this.clickOnActivate = function() {
		this.activateBtn.click();
	};
	
	//** method to check whether  a popup is dispalyed after clicking the pay service button in the account dashboard
	this.newPopupLoaded = function(){
		//$$//ElementsUtil.waitForElement(this.newpopupwindow);
		return this.newpopupwindow.isPresent();
	};
	
	//** method to proceed from the popup page
	this.continueFromPopUp = function(){
		//$$//ElementsUtil.waitForElement(this.newpopupwindow);
		this.newpopupwindow.click();
	};
	
	//** method to click on the Add Airtime Pin button in dashboard
	this.clickOnAddAirtimePin = function(){
		//$$//ElementsUtil.waitForElement(this.addAirTimePinBtn);
		this.addAirTimePinBtn.click();
	};
	
	//** method to click on the Activate button in dashboard under inactive devices -- Reactivation scenario
	this.clickOnActivateBtn = function(){
		this.activateButton.click();
	};
	
	this.payServiceButtonPresent = function(){
		return this.payServiceBtn.isPresent();
	};
		
	this.proceedFromPayService = function(){
		this.payServiceBtn.click();
	};
};

module.exports = new MyAccount;
