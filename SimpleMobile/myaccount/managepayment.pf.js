'use strict';
var ElementsUtil = require("../util/element.util");
var generator = require('creditcard-generator');
var CCUtil = require("../util/creditCard.utils");
var CommonUtil= require("../util/common.functions.util");
var sessionData = require("../common/sessiondata.do");
//===================Enter Card Details For Manage Payment in MyAccount=====================

var ManageProfile = function() {
		
	//this.editBtn = element(by.css("a[title='MYAC_99744']")); 
	this.editBtn = element(by.css("tf-edit-hide-inline"));	
	this.enterCardInfo = element.all(by.css("input[id='formName.number_mask']")).get(0);
	this.month = element.all(by.model('$select.search')).get(0); 
	this.year =  element.all(by.model('$select.search')).get(1);
	this.enterMonth = element(by.xpath("//div[@id='exp_month']/div[2]/div/div/div[8]/div/div/p"));
	this.enterYear =  element(by.xpath("//div[@id='exp_year']/div[2]/div/div/div[4]/div/div/p")); 
	this.enterFirstName = element(by.css("input[id='fname']"));
	this.enterLastName = element(by.css("input[id='lname']"));
	this.enterAddress = element(by.css("input[id='address1']"));
	this.enterCity = element(by.css("input[id='city']"));
	this.state = element.all(by.model('$select.search')).get(2);
	this.enterState = element(by.xpath("//div[@id='state']/div[2]/div/div/div[6]/div/div/p"));
	this.enterZip = element(by.css("input[id='zipcode']"));
	//this.addCreditCardBtn = element.all(by.css("button[id='btn_card']")).get(0);
	this.addCreditCardBtn = element.all(by.id('btn_card')).get(0);
	this.deleteOption = element(by.id('btn_deletethiscard'));
	
	this.ccTabEdit = element.all(by.css('[ng-click="toggleOpen()"]')).get(0);
	this.btn_DeleteCard=element(by.id('btn_deletethiscard'));
	
	this.goToEditPaymentInfo = function() {
		this.editBtn.click();
		this.enterCardInfo.clear().sendKeys(""+generator.GenCC(sessionData.simplemobile.cardType));
		this.month.click();
		this.enterMonth.click();
		this.year.click();
		this.enterYear.click();
		this.enterFirstName.sendKeys(CCUtil.firstName);
		this.enterLastName.sendKeys(CCUtil.lastName);		
		this.enterAddress.sendKeys(CCUtil.addOne);	
		this.state.click();
		this.enterState.click();
		this.enterCity.clear().sendKeys(CCUtil.city);
		this.enterZip.clear().sendKeys(CCUtil.pin); 
		this.addCreditCardBtn.click();

	};
	
	//---------------Check if Payment Added--------------
	this.isPaymentAdded = function() { 
		var paymentAddedPanel = element.all(by.css("div[class='panel-secondary ng-isolate-scope panel']"));
		return paymentAddedPanel.count().then(function (num) {
        console.log("count======================",num);
		return num;
    });
	};
	
	//--------------Check if manage Payment page loaded----------------------
	this.isLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/paymentmethod$/);		
	};
	
	this.goTodeleteOption = function() {
		this.editBtn.click();	
	};
	
	//--------------Check if delete option is present-------------------------
	this.deleteOptionIsPresent = function(){
		return this.deleteOption.isPresent();
	};
	
	//-------------- Delete the saved cards from the manage payment ---------------
	this.clickOnDeleteOption = function(){
		this.deleteOption.click();
	};
	
	//-------------- Delete the saved cards from the manage payment ---------------
	this.deleteCardDetails = function(){
		ElementsUtil.waitForElement(this.ccTabEdit);
		this.ccTabEdit.click();
		this.btn_DeleteCard.click();
	};
	
};

module.exports = new ManageProfile;
