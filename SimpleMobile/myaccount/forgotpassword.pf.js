'use strict';
var ElementsUtil = require("../util/element.util");

var forgotPassword = function(){
	
	this.enterUserName = element.all(by.id('createac-device')).get(1);
	this.loginBtn = element(by.id('btn_login'));
	this.forgotPassword = element.all(by.linkText('FORGOT PASSWORD?')).get(0);
	this.popup = element(by.id('temporarypassword'));
	
	//checks wheather the forgot password link is present in the new page.
	this.isForgotPasswordPageLoaded = function(){
		ElementsUtil.waitForElement(this.forgotPassword);
		return this.forgotPassword.isPresent();
	};
	
	//method to enter the username
	this.enterTheUserName = function(userName) {
		ElementsUtil.waitForElement(this.enterUserName);
		this.enterUserName.clear().sendKeys(userName);	
		return this.loginBtn.click();
	};
	
	//the forgotpassword link is clicked
	this.clickOnForgotPassword = function() {
		return this.forgotPassword.click();
	};
	
	//finally the pop up window is displayed with login button which takes to the homepage.
	this.popUpWindow = function(){
		ElementsUtil.waitForElement(this.popup);
		return this.popup.isPresent();
	};
	
	this.PopUpWindowClick = function(){
		this.popup.click();
	};
	
};
module.exports = new forgotPassword;
