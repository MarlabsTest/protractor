'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('SM Buy ILD plan with enrollment within account', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.simplemobile.esnPartNumber = inputActivationData.PartNumber;
				sessionData.simplemobile.simPartNumber = inputActivationData.SIM;
				sessionData.simplemobile.zip = inputActivationData.ZipCode;
				sessionData.simplemobile.pinPartNumber = inputActivationData.PIN;
				sessionData.simplemobile.cardType = inputActivationData.cardType;
				sessionData.simplemobile.autoRefill = inputActivationData.AutoRefill;
				sessionData.simplemobile.planName = inputActivationData.PlanName;
				sessionData.simplemobile.shopPlan = inputActivationData.shopplan;
				sessionData.simplemobile.esnsToReactivate = [];
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('SM_BUY_ILD_WITH_ENROLLMENT');
		}).result.data = inputActivationData;
	});
});