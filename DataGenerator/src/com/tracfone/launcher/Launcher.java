package com.tracfone.launcher;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.commons.cli.ParseException;

import com.tracfone.cbo.impl.CboUtility;
import com.tracfone.phone.impl.PhoneDAOImpl;
import com.tracfone.util.Constants.enumCodeValues;
import com.tracfone.util.ConstructOptions;
import com.tracfone.util.DBConnectionUtil;
import com.tracfone.util.TestUtil;
import com.tracfone.vo.InputOptions;

/**
 * This class is starting point of application
 * */
public class Launcher {
	
	/**
	 * This is starting of application where execution starts. 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			//System.out.print("executing !!!! ");
			InputOptions parsedCommandLineOptions = ConstructOptions.parseCommandLineOptions(args);
			System.setProperty("db.env",parsedCommandLineOptions.getEnv());
			callMethodWithSwitch(parsedCommandLineOptions);
			
		} catch (ParseException | NullPointerException e) {
			e.printStackTrace();
			System.err.println("Encountered " + e.getClass().getSimpleName() + " in main() method  due to : " + e.getCause());
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Exception occurred in main() due to : " + e);
		}finally {
			try {
				
				Connection con= DBConnectionUtil.getConnection();				
				if(con !=null){
					con.close();
				}
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
				System.err.println("Encountered " + e.getClass().getSimpleName() + " in main() method due to : " + e.getCause());
			} catch (Exception e) {
				e.printStackTrace();
				System.err.println("Exception occurred in main() due to : " + e);
			}
		}
	}
	
	/**
	 * This method will call PhoneDAOImpl {@link PhoneDAOImpl} methods depending on code values by using switch case.
	 * 
	 * @param inputOptions {@link InputOptions}
	 * @throws Exception {@link Exception}
	 * 
	 * @author pradeep.viramalla
	 */
	
	public static void callMethodWithSwitch(InputOptions inputOptions) throws Exception {
		PhoneDAOImpl phoneDAOImpl = new PhoneDAOImpl();
		    enumCodeValues codeValue= enumCodeValues.valueOf(inputOptions.getOperation().toUpperCase());
		    
			switch (codeValue) {
			
				case ESN : System.out.print(phoneDAOImpl.getNewEsnByPartNumber(inputOptions.getEsnPartNumber()));;
					       break;
					
				case SIM : System.out.print(phoneDAOImpl.getNewSimCardByPartNumber(inputOptions.getSimPartNumber()));;
					       break;
					
				case PIN : System.out.print(phoneDAOImpl.getNewPinByPartNumber(inputOptions.getPinPartNumber()));;
					       break;
					
				case MIN : System.out.print(phoneDAOImpl.getMinOfActiveEsn(inputOptions.getEsn()));
				           break;
				
				case EMAIL : System.out.print(new TestUtil().createRandomEmail());
					        break;
					
				case ACTIVATE : System.out.println(phoneDAOImpl.clearTNumber(inputOptions.getEsn()));
					            break;
					
				case BYOPSIM : System.out.print(phoneDAOImpl.getTestSIMBYOP(inputOptions.getEsnPartNumber(),
						       inputOptions.getSimPartNumber()));
					           break;
					
				case CHECK_ACTIVATION : phoneDAOImpl.checkActivation(inputOptions.getEsn(), inputOptions.getPin(),
						                inputOptions.getActionType(), inputOptions.getTransactionType());
										break;
					
				case GET_ACTIVE_ESN : System.out.print(phoneDAOImpl.getActiveEsnByPartNumber(inputOptions.getEsnPartNumber(), inputOptions.getNoOfRecords()));;
									  break;
					
				case CHECK_REDEMPTION : phoneDAOImpl.checkRedemption(inputOptions.getEsn(), inputOptions.getPin(), inputOptions.getActionType(), inputOptions.getTransactionType()
		                                                             ,inputOptions.getBuyFlag(), inputOptions.getRedeemType());
					 					break;
					
				case JOINSIM : System.out.print(phoneDAOImpl.addSimToEsn(inputOptions.getEsn(), inputOptions.getSim()));;
					           break;					           

				case CLEAR_OTA : System.out.print(phoneDAOImpl.clearOTAForEsn(inputOptions.getEsn()));;
					           break;
					           
				case UPDATE_MIN : System.out.print(phoneDAOImpl.updateMinOfActiveEsn(inputOptions.getEsn()));;
		           break;
		           
				case CDMA_BYOP_ESN : System.out.print(phoneDAOImpl.getCdmaByopEsn());
		           break;
				case UPDATE_CDMA_BYOP_ESN : System.out.print(phoneDAOImpl.updateCdmaByopEsn(inputOptions.getEsn(),inputOptions.getIsActive(),inputOptions.getZip(),inputOptions.getIsLTE(),inputOptions.getCarrier(),inputOptions.getIsIphone(),inputOptions.getIsHD()));//("100000000013481377","Active","33178","YES","Verizon","YES","YES"));
					break;
					
				case CDMA_BYOP_ESN_TW : System.out.print(phoneDAOImpl.getCdmaByopEsnTW());
		             break;
				case UPDATE_CDMA_BYOP_ESN_TW : System.out.print(phoneDAOImpl.updateCdmaByopEsnTW(inputOptions.getEsn(),inputOptions.getSim(), inputOptions.getIsTrio(), 
						inputOptions.getIsLTE(), inputOptions.getCarrier(), inputOptions.getPhoneType(), inputOptions.getPhoneModel(), inputOptions.getIsHD()));
					 break;	
					
				case SET_LEASED_ESN_STATUS:phoneDAOImpl.setLeaseStatus(inputOptions.getStatus(), inputOptions.getEsn());
				  	break;
				case REDEEM_LEASED_ESN:String pin = phoneDAOImpl.getNewPinByPartNumber(inputOptions.getPinPartNumber());
				  						phoneDAOImpl.redeemLeasedEsn(pin, inputOptions.getEsn());
				  	break;	
					case CLEAR_CARRIER_PENDING : System.out.print(phoneDAOImpl.clearCarrierPending(inputOptions.getEsn(),inputOptions.getMin()));;
		           break;          
				  
					case CLEAR_CARRIER_ON_REACTIVATION : System.out.print(phoneDAOImpl.clearCarrierPendingOnReactivation(inputOptions.getEsn()));;
		           break; 
		           
					case CHECK_PHONE_STATUS : System.out.print(phoneDAOImpl.checkPhoneStatus(inputOptions.getEsn()));
			           break;  
					   case RTR_ACTIVATION : System.out.print(phoneDAOImpl.doRtrActivation(inputOptions.getEsn(),
							inputOptions.getSim(),inputOptions.getPinPartNumber(),inputOptions.getZip(),
							inputOptions.getBrand(),inputOptions.getPlanName()));;
			           break;
					//by default DEACTIVATE
				default:System.out.print(new CboUtility().callDeactivatePhone(inputOptions.getEsn(), inputOptions.getMin(),
						                  inputOptions.getDeactivationReason(), inputOptions.getBrand()));;
			}
		
	}

		
}
