package com.tracfone.cbo.impl;

import static com.tracfone.util.Utilities.getProperties;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/*import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.axis.message.SOAPEnvelope;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger; */
import org.w3c.dom.Document;

import com.tracfone.util.Constants;

public class ServiceUtil {
	//private static PhoneUtil phoneUtil;
	//private static Logger logger = LogManager.getLogger(ServiceUtil.class.getName());
	//private static final ResourceBundle rb = ResourceBundle.getBundle("tfcommon");
	//String url = getProperties().getProperty(Constants.DB_ENIRONMENT + "_cbourl");
	String url = getProperties().getProperty("cbourl_"+Constants.DB_ENIRONMENT);
	public String access_token;
	public String output;
	//public Json ValidateInput;
	//private static ESNUtil esnUtil;
	private long lStartTime;
	private long lEndTime;
	private long difference;
	
	
	public StringBuffer callCboMethodWithRequest(String request) throws Exception {
		BufferedReader in = null;
		//String url = props.getString("cbourl_"+System.getProperty("db.env"));
		String payload = request;
		String finalUrl = url + payload;
		try {
			lStartTime = System.currentTimeMillis();
			
			URL site = new URL(finalUrl);
			URLConnection st = site.openConnection();
			InputStream is = st.getInputStream();
			
			lEndTime = System.currentTimeMillis();
            difference = lEndTime - lStartTime;
            System.out.println("Elapsed milliseconds: " + difference);
			 
			in = new BufferedReader(new InputStreamReader(is));
			

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Unable to get response for URL = " + url);
		}
		StringBuffer out = new StringBuffer();
		String line;
		while ((line = in.readLine()) != null) {
			out.append(line);
		}
		
		System.out.println("out value....."+out.toString()); // Prints the string content read
											// from input stream
		byte[] bytes = out.toString().getBytes();
		 InputStream inputStream = new ByteArrayInputStream(bytes);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(inputStream);
		//phoneUtil.insertIntoServiceResultsTable(document.getDocumentElement().getNodeName(),URLDecoder.decode(payload,"UTF-8"),out,String.valueOf(difference));
		//logger.info("Request:" + payload);
		//logger.info("Respnse:" + out.toString());
		//logger.info("\n");
		in.close();
		return out;
	}

	
	
}
