package com.tracfone.cbo.impl;

//import static junit.framework.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLEncoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class CboUtility {

		//Deactivate Phone  REQUIRES  #ESN, #MIN, #REASON, #BRAND
	/**
	 * 
	 * @param esn {@link String}
	 * @param min {@link String}
	 * @param reason {@link String}
	 * @param brand {@link String}
	 * 
	 * @return responseMsg {@link String}
	 */
	public String callDeactivatePhone(String esn, String min, String reason, String brand) throws Exception {
		String responseMsg = null;
		InputStream istream = null;
		StringWriter writer = null;
		try {
			istream = Thread.currentThread().getContextClassLoader().getResourceAsStream("deactivatePhone.xml");

			writer = new StringWriter();

			IOUtils.copy(istream, writer, "UTF-8");
			String content = writer.toString();

			// String content = FileUtils.readFileToString(file, "UTF-8");
			content = content.replaceAll("@esn", esn);
			content = content.replaceAll("@min", min);
			content = content.replaceAll("@reason", reason);
			content = content.replaceAll("@brand", brand);

			System.out.println("content..." + content);

			StringBuffer response = new ServiceUtil().callCboMethodWithRequest(URLEncoder.encode(content, "UTF-8"));
			responseMsg = parseResponse(response, "DeactivatePhone");

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Generating file failed", e);
		} finally {
			if (writer != null) {
				writer.close();
			}

			if (istream != null) {
				istream.close();
			}
		}

		return responseMsg;
	}
		
	/**
	 * 
	 * @param sbf {@link StringBuffer}
	 * @param serviceType {@link String}
	 * 
	 * @return message {@link String}
	 */
	public String parseResponse(StringBuffer sbf, String serviceType) {
		String message = null;
		String result = null;
		String errorNum = null;
		try {
			byte[] bytes = sbf.toString().getBytes();
			InputStream inputStream = new ByteArrayInputStream(bytes);
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(inputStream);
			String rootNode = document.getDocumentElement().getNodeName();
			NodeList nlist = document.getElementsByTagName(rootNode);
			try {
				message = (String) ((Element) nlist.item(0)).getElementsByTagName("ERROR_STRING").item(0)
						.getChildNodes().item(0).getNodeValue();
				System.out.println("message..." + message);
				/*
				 * if(message.contains("Success") || message.contains("Sucess")
				 * || message.contains("SUCCESS") ||
				 * message.equalsIgnoreCase("")){ result = "success"; }else{
				 * result = "unsuccessful"; }
				 */
			} catch (NullPointerException e) {
				result = "success";
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return message;
	}
	
}