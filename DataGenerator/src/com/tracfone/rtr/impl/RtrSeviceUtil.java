package com.tracfone.rtr.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

public class RtrSeviceUtil 
{
	public SOAPMessage getActivationSoapMessage(String endPointUrl, String esn, String sim, String pinPartNumber, 
												String zip, String brand, String planName)
	{
		InputStream istream = null;
		StringWriter writer = null;
		SOAPMessage soapMessage = null;
		try {
			istream = Thread.currentThread().getContextClassLoader().getResourceAsStream("rtrActivation.xml");

			writer = new StringWriter();

			IOUtils.copy(istream, writer, "UTF-8");
			String request = writer.toString();
			// String content = FileUtils.readFileToString(file, "UTF-8");
			request = request.replaceAll("@RefNbr", generateRandomRefNbr());
			request = request.replaceAll("@brand", brand);
			request = request.replaceAll("@zip", zip);
			request = request.replaceAll("@esn", esn);
			request = request.replaceAll("@sim", sim);
			request = request.replaceAll("@amount", planName);
			request = request.replaceAll("@planCode", pinPartNumber);

			istream = new ByteArrayInputStream(request.getBytes());
			MessageFactory messageFactory = MessageFactory.newInstance();
			MimeHeaders headers = new MimeHeaders();
			
			soapMessage = messageFactory.createMessage(headers, istream);

			headers = soapMessage.getMimeHeaders();
			headers.addHeader("SOAPAction", endPointUrl);// + serviceName);
			soapMessage.saveChanges();

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException("Generating file failed", e);
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (istream != null) {
				try {
					istream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return soapMessage;
	}
	
	public String parseSoaResponse(String response){
		String message = null;
		try {
			byte[] bytes = response.toString().getBytes(); 
			InputStream inputStream = new ByteArrayInputStream(bytes); 
			DocumentBuilderFactory factory =DocumentBuilderFactory.newInstance(); 
			DocumentBuilder builder =factory.newDocumentBuilder();
			Document doc = builder.parse(inputStream);
			doc.getDocumentElement().normalize();
			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			String rootNode = doc.getDocumentElement().getNodeName();
			NodeList nlist = doc.getElementsByTagName(rootNode);
			if (doc.hasChildNodes()) {
				//System.out.println("Response:");
				printNote(doc.getChildNodes());
			}
			message = (String) ((Element) nlist.item(0)).getElementsByTagName("resultCode").item(0).getChildNodes().item(0).getNodeValue();
			//System.out.println("\n\n SOA response : "+ message);
		} 
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return message;
	}
	
	private static void printNote(NodeList nodeList) {

		for (int count = 0; count < nodeList.getLength(); count++) {
			Node tempNode = nodeList.item(count);

			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
				//System.out.print("\n<" + tempNode.getNodeName());
				//System.out.print(" " + tempNode.getTextContent()+ ">");

				if (tempNode.hasAttributes()) {
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i = 0; i < nodeMap.getLength(); i++) {
						Node node = nodeMap.item(i);
						//System.out.print("\n<" + node.getNodeName() + ">");
						//System.out.print(" " + node.getNodeValue());
					}
				}
				if (tempNode.hasChildNodes()) {
					printNote(tempNode.getChildNodes());
				}
				//System.out.print("</" + tempNode.getNodeName() + ">");
			}
		}
	}
	
	public String generateRandomRefNbr() {
		Random random = new Random();
		int area = 200 + random.nextInt(789);
		int central = 200 + random.nextInt(799);
		int station = random.nextInt(99);
		return String.valueOf(area) + String.valueOf(central) + String.format("%04d", station);
	}
}
