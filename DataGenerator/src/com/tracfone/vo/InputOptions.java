package com.tracfone.vo;


/**
 * This class is value object for storing command line options
 * */
public class InputOptions {

	private String operation;
	private int actionType;
	private String transactionType;
	private String esnPartNumber;
	private String simPartNumber;
	private String pinPartNumber;
	private String esn;
	private String sim;
	private String pin;
	private String env;
	private String min;
	private String brand;
	private String deactivationReason;
	private String buyFlag;
	private String redeemType;
	private String baseDirectory;
	private String isActive;
	private String zip;
	private String isLTE;
	private String carrier;
	private String isIphone;
	private String isHD;
	private String isTrio;
	private String phoneType;
	private String phoneModel;
	private String status;
	private String planName;
	private int noOfRecords = 1;

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getIsLTE() {
		return isLTE;
	}

	public void setIsLTE(String isLTE) {
		this.isLTE = isLTE;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getIsIphone() {
		return isIphone;
	}

	public void setIsIphone(String isIphone) {
		this.isIphone = isIphone;
	}

	public String getIsHD() {
		return isHD;
	}

	public void setIsHD(String isHD) {
		this.isHD = isHD;
	}

	public int getActionType() {
		return actionType;
	}

	public void setActionType(int actionType) {
		this.actionType = actionType;
	}
	

	public String getEsnPartNumber() {
		return esnPartNumber;
	}

	public void setEsnPartNumber(String esnPartNumber) {
		this.esnPartNumber = esnPartNumber;
	}

	public String getSimPartNumber() {
		return simPartNumber;
	}

	public void setSimPartNumber(String simPartNumber) {
		this.simPartNumber = simPartNumber;
	}

	public String getPinPartNumber() {
		return pinPartNumber;
	}

	public void setPinPartNumber(String pinPartNumber) {
		this.pinPartNumber = pinPartNumber;
	}

	public String getEsn() {
		return esn;
	}

	public void setEsn(String esn) {
		this.esn = esn;
	}

	public String getSim() {
		return sim;
	}

	public void setSim(String sim) {
		this.sim = sim;
	}

	
	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}


	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getDeactivationReason() {
		return deactivationReason;
	}

	public void setDeactivationReason(String deactivationReason) {
		this.deactivationReason = deactivationReason;
	}

	public String getBuyFlag() {
		return buyFlag;
	}

	public void setBuyFlag(String buyFlag) {
		this.buyFlag = buyFlag;
	}

	public String getRedeemType() {
		return redeemType;
	}

	public void setRedeemType(String redeemType) {
		this.redeemType = redeemType;
	}

	
	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getBaseDirectory() {
		return baseDirectory;
	}

	public void setBaseDirectory(String baseDirectory) {
		this.baseDirectory = baseDirectory;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getNoOfRecords() {
		return noOfRecords;
	}

	public void setNoOfRecords(int noOfRecords) {
		this.noOfRecords = noOfRecords;
	}

	public String getIsTrio() {
		return isTrio;
	}

	public void setIsTrio(String isTrio) {
		this.isTrio = isTrio;
	}

	public String getPhoneType() {
		return phoneType;
	}

	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}

	public String getPhoneModel() {
		return phoneModel;
	}

	public void setPhoneModel(String phoneModel) {
		this.phoneModel = phoneModel;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}
}
