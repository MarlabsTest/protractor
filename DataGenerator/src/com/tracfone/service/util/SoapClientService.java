package com.tracfone.service.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

public class SoapClientService 
{
	public static String callSoapWebService(String soapEndpointUrl, String soapAction, SOAPMessage soapMessage) 
	{
		SOAPConnection soapConnection = null;
		SOAPMessage soapResponse = null;
		String soapResponseStr = "";
        try 
        {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);
            
            soapResponseStr = printSOAPResponse(soapResponse);
            // Print the SOAP Response
            //soapResponse.writeTo(System.out);
            System.out.println();
        } 
        catch (Exception e) 
        {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        finally
        {
        	try 
        	{
        		if(soapConnection != null)
        		{
        			soapConnection.close();
        		}
			} 
        	catch (SOAPException e) 
        	{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return soapResponseStr;
    }
	
	private static String printSOAPResponse(SOAPMessage soapResponse) {
		String str = null;
		ByteArrayOutputStream os = null;
		try {
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			Source sourceContent = soapResponse.getSOAPPart().getContent();

			os = new ByteArrayOutputStream();
			StreamResult result = new StreamResult(os);
			transformer.transform(sourceContent, result);
			str = os.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (os != null) {
				try {
					os.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return str;
	}
	
	
}
