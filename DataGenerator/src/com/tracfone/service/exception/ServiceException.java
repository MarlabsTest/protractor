package com.tracfone.service.exception;

import javax.xml.ws.WebFault;

@WebFault(name="FaultBean")
public class ServiceException extends Exception {

private static final long serialVersionUID = -5307754615848423430L;
private FaultBean faultBean;

public ServiceException(String message) {
        super(message);
        this.faultBean = new FaultBean();
        this.faultBean.setMessage(message);
}

public FaultBean getFaultInfo() {
    return faultBean;
}
}
