package com.tracfone.phone.impl;

import static com.tracfone.util.DBConnectionUtil.*;
import static com.tracfone.util.DBQueryConstants.*;

import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Node;
import com.sun.xml.internal.ws.api.message.Headers;
import com.sun.xml.internal.ws.developer.WSBindingProvider;
import com.tracfone.b2b.phoneservices.PhoneInformationRequestType;
import com.tracfone.b2b.phoneservices.PhoneInformationResponseType;
import com.tracfone.b2b.phoneservices.PhoneServices;
import com.tracfone.b2b.phoneservices.PhoneServices_Service;
import com.tracfone.b2b.redemptionservices.PINCardRedemptionRequestType;
import com.tracfone.b2b.redemptionservices.PINCardRedemptionResponseType;
import com.tracfone.b2b.redemptionservices.RedemptionServices;
import com.tracfone.b2b.redemptionservices.RedemptionServices_Service;
import com.tracfone.commontypes.BaseRequestType;
import com.tracfone.commontypes.RequestorIdType;
import com.tracfone.commontypes.TracfoneBrandType;
import com.tracfone.commontypes.LanguageType;
import com.tracfone.phonecommontypes.DeviceIdType;
import com.tracfone.service.exception.ServiceException;

import org.w3c.dom.NodeList;

import com.tracfone.phone.api.PhoneDAO;
import com.tracfone.rtr.impl.RtrSeviceUtil;
import com.tracfone.service.util.SoapClientService;
import com.tracfone.util.Constants;
import com.tracfone.util.DBConnectionUtil;
/*import com.tracfone.phone.lease.*;
import com.tracfone.util.TracfoneBrandType;
import com.tracfone.util.LanguageType;*/
import com.tracfone.util.Utilities;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.ws.BindingProvider;
import javax.xml.namespace.QName;
import static com.tracfone.util.Utilities.getProperties;
import org.junit.Assert;
import javax.xml.ws.*;

/*import org.junit.Assert;
import com.sun.xml.ws.api.message.Headers;
import com.sun.xml.ws.developer.WSBindingProvider;
*/
public class PhoneDAOImpl implements PhoneDAO {
	
	/**
	 * This method will return ESN value with given partnumber.
	 * 
	 * @param partNumber {@link String}
	 * @return ESN {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public String getNewEsnByPartNumber(String partNumber) throws SQLException, Exception {
		
		if ( partNumber != null && partNumber.startsWith("STAPI4")) {
			throw new IllegalArgumentException("Can not find new ESN for ACME part number");
		}
		
		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", partNumber);		
		return executeStoredFunction(GET_TEST_NEW_ESN_BY_PART_NUMBER, params,1);
	}
	
	/**
	 * This method will return SIM value with given partnumber.
	 * 
	 * @param partNumber {@link String}
	 * @return SIM {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public String getNewSimCardByPartNumber(String partNumber) throws SQLException,Exception {
		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", partNumber);
		return executeStoredFunction(GET_TEST_SIM_BY_PART_NUMBER, params,1);
	}

	/**
	 * This method will return PIN value with given partnumber.
	 * 
	 * @param partNumber {@link String}
	 * @return PIN {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public String getNewPinByPartNumber(String partNumber)  throws Exception, SQLException {
		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", partNumber);
		return executeStoredFunction(GET_TEST_PIN_BY_PART_NUMBER, params,1);
	}
	
	/**
	 * This method will marry ESN to SIM.
	 * 
	 * @param esn {@link String}
	 * @param newSim {@link String}
	 * @return SIM {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	
	public boolean addSimToEsn(String esn, String newSim) throws Exception, SQLException {
		try {
			HashMap<String, Object> params =new HashMap<>();
			params.put("input1", newSim);
			params.put("input2", esn);			
			executePreparedStatement(params,ADD_SIM_TO_ESN,"update",2);
			//System.out.println("true");
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * 
	 * @param esn {@link String}
	 * @return SIM {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public  String clearTNumber(String esn) throws Exception, SQLException {
		String result = null;
		String status = checkPhoneStatus(esn);
		int statusInt = Integer.parseInt(status);
		while(statusInt != 52)
		{
			Thread.sleep(2000);
			status = checkPhoneStatus(esn);
			statusInt = Integer.parseInt(status);
		};
		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", esn);
		params.put("input2", "33178");
		
		result = executeStoredFunction(CLEAR_TNUMBER, params,2);
		return result;
	}
	
	public String doRtrActivation(String esn, String sim, String pinPartNumber, String zip, String brand, 
									String planName)
	{
		String toRet = null;
		
		try
		{
			String endPointUrl = getProperties().getProperty("rtr_url_"+Constants.DB_ENIRONMENT);
			String soapAction = endPointUrl;
			RtrSeviceUtil rtrSeviceUtil = new RtrSeviceUtil();
			SOAPMessage rtrActivationSoapMsg = rtrSeviceUtil.getActivationSoapMessage(endPointUrl, esn, 
															sim, pinPartNumber, zip, brand, planName);
			
			String soapResponse = SoapClientService.callSoapWebService(endPointUrl, soapAction, rtrActivationSoapMsg);
			String respMessage = rtrSeviceUtil.parseSoaResponse(soapResponse);
			
			if(respMessage.equalsIgnoreCase("success") || respMessage.equalsIgnoreCase("success."))
			{
				toRet = clearTNumber(esn);
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception while doing rtr activation");
		}
		return toRet;
	}
	
	/**
	 * This method will return active MIN with given ESN
	 * 
	 * @param esn {@link String}
	 * @return MIN {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public String getMinOfActiveEsn(String esn) throws Exception, SQLException {
		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", esn);
		params.put("output1", "X_MIN");
		
		return executePreparedStatement(params,SQL_MIN_OF_ACTIVE_ESN,"select",1);
	}
	

	/**
	 * 
	 * @param byopESNPartnumber {@link String}
	 * @param byopSIMPartnumber {@link String}
	 * @return  {@link String}
	 *
	 * @author pradeep.viramalla
	 * 
	 */
	public String getTestSIMBYOP(String byopESNPartnumber, String byopSIMPartnumber) throws Exception, SQLException {

		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", byopESNPartnumber);
		params.put("input2", byopSIMPartnumber);
		
		String newEsn = executeStoredFunction(GET_TEST_SIM_BYOP, params,2);

		if (newEsn == null || newEsn.isEmpty()) {
			throw new IllegalArgumentException("No new esns with part number: " + byopESNPartnumber);
		} else {
			return newEsn;
		}
	}
	
	/**
	 * This method  will return active ESN with given partnumber
	 * @param partNumber {@link String}
	 * @return  ESN {@link String}
	 *
	 * @author pradeep.viramalla
	 * 
	 */
	public  String getActiveEsnByPartNumber(String partNumber, int noOfRecords) throws Exception, SQLException {
		
		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", partNumber);
		params.put("input2", noOfRecords);
		params.put("output1", "part_serial_no");
		String partSerialNo = DBConnectionUtil.getPipelineSeparatedQueryResult(params,GET_TEST_ACTIVE_ESN_BY_PART_NUMBER,"select",2);
		
		if (partSerialNo==null) {
			throw new IllegalArgumentException("No active esns with part number: " + partNumber);
		} else {
			resetILDCounter(partSerialNo);
			return partSerialNo;
		}
	}
	
	/**
	 * 
	 * @param esn  {@link String}
	 * @return  {@link Boolean}
	 * 
	 * @author pradeep.viramalla
	 */
	public  boolean resetILDCounter(String esn) {
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("input1", esn);
			executePreparedStatement(params,RESET_ILD_COUNT,"update",1);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * This method will invoke CHECK_ACTIVATION stored procedure from database.
	 * 
	 * @param esn {@link String}
	 * @param pin  {@link String}
	 * @param actionType  {@link Integer}
	 * @param transType  {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public void checkActivation(String esn, String pin, int actionType, String transType)throws Exception, SQLException {
		CallableStatement callableStatement = getConnection().prepareCall("{call " + CHECK_ACTIVATION + Utilities.buildplaceHolders(8) + "}");
		callableStatement.setString("V_TRANSTYPE", transType);
		callableStatement.setString("INESN", esn);
		callableStatement.setString("ACTIONTYPE", actionType+"");
		callableStatement.setString("REDEEMFLAG", "");
		callableStatement.setString("PIN", pin);
		callableStatement.setString("V_IPADDRESS", getIpAddress());
		callableStatement.registerOutParameter("RESULTSTR", Types.VARCHAR);
		callableStatement.registerOutParameter("RETURNCODE", Types.INTEGER);
		callableStatement.executeUpdate();
	}

	
	/**
	 * This method will invoke CHECK_REDEMPTION stored procedure from database.
	 * 
	 * @param esn {@link String}
	 * @param pin  {@link String}
	 * @param actionType  {@link Integer}
	 * @param transType  {@link String}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public void checkRedemption(String esn, String pin, int actionType, String transType ,String buyFlag,String redeemType) throws Exception, SQLException {
		CallableStatement callableStatement =getConnection().prepareCall("{call " + CHECK_REDEMPTION + Utilities.buildplaceHolders(10)+"}");
		callableStatement.setString("V_TRANSTYPE", transType + " Add Now");
		callableStatement.setString("INESN", esn);
		callableStatement.setString("ACTIONTYPE", actionType+"");
		callableStatement.setString("REDEEMTYPE", redeemType);
		callableStatement.setString("BUYFLAG", buyFlag);
		
		if(pin == "0"){
			callableStatement.setString("PIN", "");
		}else{
			callableStatement.setString("PIN", pin);
		}
		System.out.print(pin);
		callableStatement.setString("V_IPADDRESS", getIpAddress());
		callableStatement.setString("ENROLLED_DAYS", "0");
		callableStatement.registerOutParameter("RESULTSTR", Types.VARCHAR);
		callableStatement.registerOutParameter("RETURNCODE", Types.INTEGER);
		callableStatement.executeUpdate();
	}
	
	/**
	 * This will return Ip address
	 * 
	 * @return ipAddress {@link String}
	 * 
	 * @author pradeep.viramalla
	 */
	public String getIpAddress() {
		String ipAddress=null;
			try {
				ipAddress = InetAddress.getLocalHost().getHostAddress();
			} catch (UnknownHostException e) {
				ipAddress = "Could not get IP";
			}
		return ipAddress;
	}


	@Override
	public boolean setDateInPast(String esn) throws SQLException, Exception {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean deactivateEsn(String esn) throws SQLException, Exception {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * 
	 * @param esn  {@link String}
	 * @return  {@link Boolean}
	 * 
	 * @author aswathy.nair
	 */
	public  boolean clearOTAForEsn(String esn) throws SQLException, Exception{
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("input1", esn);
			executePreparedStatement(params,CLEAR_OTA,"update",1);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * 
	 * @param esn  {@link String}
	 * @return  {@link Boolean}
	 * 
	 * @author aswathy.nair
	 */
	public  boolean updateMinOfActiveEsn(String esn) throws SQLException, Exception{
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("input1", esn);
			params.put("input2", esn);
			executePreparedStatement(params,UPDATE_MIN,"update",1);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean clearCarrierPending(String esn, String min) throws Exception, SQLException {
		
			HashMap<String, Object> params =new HashMap<>();
			params.put("input1", min);
			params.put("input2", min);
			params.put("input3", esn);	
			executePreparedStatement(params,CLEAR_CARRIER_PENDING,"update",3);
			return true;
	}
	
	public String getCdmaByopEsnTW()throws SQLException, Exception{
		HashMap<String, Object> params = new HashMap<>();
		params.put("output1", "ESN");
		String ESN = executePreparedStatement(params,SQL_GET_NEXT_CDMA,"select",0);		
		return ESN;
		
     }
	 
	 	/**
	 * 
	 * @param esn  {@link String}
	 * @param sim  {@link String}
	 * @return  {@link Boolean}
	 * 
	 * 
	 */
	public  boolean updateCdmaByopEsnTW(String ESN,String SIM, String isTrio, 
			String isLTE, String carrier, String phoneType, String phoneModel, String isHd) throws SQLException, Exception{
		try {
			/*HashMap<String, Object> params = new HashMap<>();
			params.put("input1", sim);
			params.put("input2", esn);
			executePreparedStatement(params,UPDATE_CDMA_BYOP_TW,"update",2);*/
			
			System.out.println("inside update method  jar::"+ESN+" "+SIM+" "+isTrio+" "+isLTE+" "+carrier+" "+phoneType+" "+phoneModel+" "+isHd);
			HashMap<String,Object> finalUpdateParams = new HashMap<>();
			
			
				
			String result = "";
			
				
				
				 
				if (isHd.equalsIgnoreCase("Yes") || isTrio.equalsIgnoreCase("Yes")) {
					finalUpdateParams.put("input1", "isPIBLock=N,isLostorStolen=N,isBYOPEligible=Y,returnCode=S0050,returnMessage=DISCOUNT_2_ACCESS,validWithCarrier=Y,DEVICETYPE=U,HDVoice=Y");
					
				}
				else{
					finalUpdateParams.put("input1", "isPIBLock=N,isLostorStolen=N,isBYOPEligible=Y,returnCode=S0050,returnMessage=DISCOUNT_2_ACCESS,validWithCarrier=Y,DEVICETYPE=U,HDVoice=N");
					
				}
				
				if (isLTE.equalsIgnoreCase("YES") && phoneType.equalsIgnoreCase("NO")) {
					isLTE = "4G";
					phoneType = "TESTPHONE";
				} else if (isLTE.equalsIgnoreCase("NO") && phoneType.equalsIgnoreCase("NO")) {
					isLTE = "3G";
					phoneType = "TESTPHONE";
				} else if (isLTE.equalsIgnoreCase("YES")) {
					isLTE = "4G";
					
				} else if (isLTE.equalsIgnoreCase("NO") ) {
					isLTE = "3G";					
				} else {
					isLTE = "";
					phoneType = "";
				}
				finalUpdateParams.put("input2",phoneType );
				if(isLTE.equalsIgnoreCase("4G")){
					finalUpdateParams.put("input3", SIM);	
					finalUpdateParams.put("input4", phoneModel);
					finalUpdateParams.put("input5", isLTE);
					finalUpdateParams.put("input6", ESN);
					finalUpdateParams.put("input7", carrier);
				} else {
				
				finalUpdateParams.put("input3", phoneModel);
				finalUpdateParams.put("input4", isLTE);
				finalUpdateParams.put("input5", ESN);
				finalUpdateParams.put("input6", carrier);
				}
				
				if(isLTE.equalsIgnoreCase("4G")){
				executePreparedStatement(finalUpdateParams, UPDATE_CDMA_BYOP_TW_4G, "update",7);
				} else {
				executePreparedStatement(finalUpdateParams, UPDATE_CDMA_BYOP_TW_3G, "update",6);	
				}
				
			
			
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
		
		public boolean clearCarrierPendingOnReactivation(String esn) throws Exception, SQLException {
			
			HashMap<String, Object> params =new HashMap<>();
			params.put("input1", esn);	
			executePreparedStatement(params,CLEAR_CARRIER_ON_REACTIVATION,"update",1);
			return true;
	}
	
	public 	String updateCdmaByopEsn(String ESN,String isActive, String zip, 
			String isLTE, String carrier, String isiPhone, String isHd)throws SQLException, Exception{
		System.out.println("inside update method  jar::"+ESN+" "+isActive+" "+zip+" "+isLTE+" "+carrier+" "+isiPhone+" "+isHd);
		HashMap<String,Object> finishUpdateParams = new HashMap<>();
		HashMap<String,Object> completeUpdateParams = new HashMap<>();
		boolean Active = "Active".equalsIgnoreCase(isActive);	
		String result = "";
		if (isLTE.equalsIgnoreCase("YES") && isiPhone.equalsIgnoreCase("YES")) {
				isLTE = "4G";
				isiPhone = "APL";
			} else if (isLTE.equalsIgnoreCase("YES") && isiPhone.equalsIgnoreCase("NO")) {
				isLTE = "4G";
				isiPhone = "TESTPHONE";
			} else if (isLTE.equalsIgnoreCase("NO") && isiPhone.equalsIgnoreCase("YES")) {
				isLTE = "3G";
				isiPhone = "APL";
			} else if (isLTE.equalsIgnoreCase("NO") && isiPhone.equalsIgnoreCase("NO")) {
				isLTE = "3G";
				isiPhone = "TESTPHONE";
			} else {
				isLTE = "";
				isiPhone = "";
			}
			finishUpdateParams.put("input1",isLTE );
			finishUpdateParams.put("input2", isiPhone);
			finishUpdateParams.put("input4", ESN);
			completeUpdateParams.put("input1", ESN);
			
			if (Active) {
				finishUpdateParams.put("input3", "ESN_IN_USE: Device is in use");
			} 
			if (isHd.equalsIgnoreCase("Yes")) {
				finishUpdateParams.put("input3", "isPIBLock=N,isLostorStolen=N,isBYOPEligible=Y,returnCode=S0048,returnMessage=DISCOUNT_2_ACCESS,validWithCarrier=Y,HDVoice=Y");
			}
			else{
				finishUpdateParams.put("input3", "isPIBLock=N,isLostorStolen=N,isBYOPEligible=Y,returnCode=S0048,returnMessage=DISCOUNT_2_ACCESS,validWithCarrier=Y,HDVoice=N");
			}
			
			
			if ("SPRINT".equalsIgnoreCase(carrier)) {
				finishUpdateParams.put("input5", "SPRINT");
				result = executePreparedStatement(finishUpdateParams,SQL_FINISH_CDMA_BYOP_IGATE,"update", 5);
				completeUpdateParams.put("input2", "RSS");
				executePreparedStatement(completeUpdateParams, SQL_COMPLETE_CDMA_BYOP_IGATE, "update",2);
				
			} else {
				finishUpdateParams.put("input5", "RSS");
				System.out.println("params are::;"+finishUpdateParams);
				result = executePreparedStatement(finishUpdateParams,SQL_FINISH_CDMA_BYOP_IGATE,"update", 5);
				completeUpdateParams.put("input2", "SPRINT");
				executePreparedStatement(completeUpdateParams, SQL_COMPLETE_CDMA_BYOP_IGATE, "update",2);
			}
			
			return result;
		
	}

	public String getCdmaByopEsn()throws SQLException, Exception{
			HashMap<String, Object> params = new HashMap<>();
			params.put("output1", "ESN");
			String ESN = executePreparedStatement(params,SQL_GET_NEXT_CDMA,"select",0);
			params.put("input1", ESN);
			executePreparedStatement(params, SQL_ADD_TO_TEST_OTA_ESN,"update",1);
			params.put("input2", "C");
			executePreparedStatement(params, SQL_ADD_TO_TEST_IGATE_ESN,"update",2);
			return ESN;
			
	}

	@Override
	public String checkPhoneStatus(String esn) throws SQLException, Exception {
		HashMap<String, Object> params =new HashMap<>();
		params.put("input1", esn);
		params.put("output1", "x_part_inst_status");
		
		return executePreparedStatement(params,CHECK_PHONESTATUS,"select",1);
		
	}
	
	/**
	 * This method  will set ESN status to lease
	 * @param status {@link String}
	 * @param esn {@link String}
	 * 
	 * 
	 */
	public  void setLeaseStatus(String status, String esn) throws ServiceException{
		
			try {
				PhoneServices_Service webServiceImpl = new PhoneServices_Service();
				PhoneServices serviceInterface = webServiceImpl.getPhoneServicesSOAP();
				
				addUsernameTokenProfile(serviceInterface);
				// to change the End point during runtime
				BindingProvider bindingProvider = (BindingProvider) serviceInterface;
				bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,"http://dp-site-soa3:9001/B2B/PhoneServices");
				PhoneInformationResponseType oResp;
				PhoneInformationRequestType oRequest = new PhoneInformationRequestType();
//				BaseRequestType bRequest = new BaseRequestType();
				
				RequestorIdType reqToken = new RequestorIdType();
				reqToken.setClientId("SmartPay");
				reqToken.setClientTransactionId("132456");
				oRequest.setRequestToken(reqToken);
				oRequest.setBrandName(TracfoneBrandType.NET_10);
				oRequest.setLanguage(LanguageType.ENG);
				oRequest.setSourceSystem("API");
				oRequest.setApplicationId("546445784");
				oRequest.setEsn(esn);
				oRequest.setLeaseStatus(status);
				
				oResp = serviceInterface.updatePhoneInformation(oRequest);
				Assert.assertEquals(oResp.getResult().getMessage(), "SUCCESS", oResp.getResult().getMessage());
				
			} catch (Exception e) {
//				System.out.println("inside setLease");
				e.printStackTrace();
			}
		
	}
	
	private void addUsernameTokenProfile(PhoneServices serviceInterface) throws ServiceException{
		try {
			SOAPFactory soapFactory = SOAPFactory.newInstance();
			String SECURITY_NAMESPACE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
			QName securityQName = new QName(SECURITY_NAMESPACE, "Security");
			SOAPElement security = soapFactory.createElement(securityQName);
			QName tokenQName = new QName(SECURITY_NAMESPACE, "UsernameToken");
			SOAPElement token = soapFactory.createElement(tokenQName);
			QName userQName = new QName(SECURITY_NAMESPACE, "Username");
			SOAPElement username = soapFactory.createElement(userQName);
			username.addTextNode("webuser");// your username 
			QName passwordQName = new QName(SECURITY_NAMESPACE, "Password");
			SOAPElement password = soapFactory.createElement(passwordQName);
			password.addTextNode("t!n43p2b");// your password
			token.addChildElement(username);
			token.addChildElement(password);
			security.addChildElement(token);
			com.sun.xml.internal.ws.api.message.Header header = Headers.create(security);
			((WSBindingProvider) serviceInterface).setOutboundHeaders(header);
		} catch (SOAPException e) {
			System.out.println("Could not configure Username Token Profile authentication");
			e.printStackTrace();
		}
	}
	
	/**
	 * This method  will redeem the leased ESN 
	 * @param pin {@link String}
	 * @param esn {@link String}
	 * 
	 * 
	 */
	public void redeemLeasedEsn(String pin, String esn) throws ServiceException{
		try {
			RedemptionServices_Service webServiceImpl = new RedemptionServices_Service();
			RedemptionServices serviceInterface = webServiceImpl.getRedemptionServicesSOAP();

			addUsernameTokenProfile(serviceInterface);
			// to change the End point during runtime
			BindingProvider bindingProvider = (BindingProvider) serviceInterface;
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,"http://dp-site-soa3:9001/B2B/RedemptionServices");
			PINCardRedemptionResponseType oResp;
			PINCardRedemptionRequestType oRequest = new PINCardRedemptionRequestType();

			RequestorIdType reqToken = new RequestorIdType();
			reqToken.setClientTransactionId("132456");
			reqToken.setClientId("SmartPay");
			oRequest.setRequestToken(reqToken);
			
			oRequest.setBrandName(TracfoneBrandType.NET_10);
			oRequest.setSourceSystem("API");
			oRequest.setLanguage(LanguageType.ENG);
			oRequest.setPinCardNum(pin);
			oRequest.setRedeemNow(true);
			
			DeviceIdType phone = new DeviceIdType();
			phone.setEsn(esn);
			oRequest.setDeviceId(phone);
			
			oResp = serviceInterface.redeemPinCard(oRequest);
			Assert.assertEquals(oResp.getResult().getMessage(), "SUCCESS", oResp.getResult().getMessage());
		} catch (Exception e) {
//			System.out.println("inside redeemLease");
			e.printStackTrace();
		}
	}
	
	private void addUsernameTokenProfile(RedemptionServices serviceInterface) throws ServiceException {
		try {
			SOAPFactory soapFactory = SOAPFactory.newInstance();
			String SECURITY_NAMESPACE = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";
			QName securityQName = new QName(SECURITY_NAMESPACE, "Security");
			SOAPElement security = soapFactory.createElement(securityQName);
			QName tokenQName = new QName(SECURITY_NAMESPACE, "UsernameToken");
			SOAPElement token = soapFactory.createElement(tokenQName);
			QName userQName = new QName(SECURITY_NAMESPACE, "Username");
			SOAPElement username = soapFactory.createElement(userQName);
			username.addTextNode("webuser");// your username 
			QName passwordQName = new QName(SECURITY_NAMESPACE, "Password");
			SOAPElement password = soapFactory.createElement(passwordQName);
			password.addTextNode("t!n43p2b");// your password
			token.addChildElement(username);
			token.addChildElement(password);
			security.addChildElement(token);
			com.sun.xml.internal.ws.api.message.Header header = Headers.create(security);
			((WSBindingProvider) serviceInterface).setOutboundHeaders(header);
		} catch (Exception e){
			e.getMessage();
		}
	}

}

