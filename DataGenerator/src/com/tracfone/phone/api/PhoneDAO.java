package com.tracfone.phone.api;

import java.sql.SQLException;

public interface PhoneDAO {

	public String getNewEsnByPartNumber(String partNumber)throws SQLException, Exception;

	public String getNewSimCardByPartNumber(String partNumber)throws SQLException, Exception;

	public String getNewPinByPartNumber(String partNumber)throws SQLException, Exception;

	public String clearTNumber(String esn)throws SQLException, Exception;

	public String getMinOfActiveEsn(String esn)throws SQLException, Exception;

	public String getTestSIMBYOP(String byopESNPartnumber, String byopSIMPartnumber)throws SQLException, Exception;

	public boolean addSimToEsn(String byopESNPartnumber, String byopSIMPartnumber)throws SQLException, Exception;

	public void checkActivation(String esn, String pin, int actionType, String transType)throws SQLException, Exception;

	public String getActiveEsnByPartNumber(String partNumber, int noOfRecords)throws SQLException, Exception;

	public boolean setDateInPast(String esn)throws SQLException, Exception;

	public boolean deactivateEsn(String esn)throws SQLException, Exception;

	public void checkRedemption(String esn, String pin, int actionType, String transType, String buyFlag,
			String redeemType)throws SQLException, Exception;
	
	public  boolean clearOTAForEsn(String esn) throws SQLException, Exception;
	
	public boolean updateMinOfActiveEsn(String esn)throws SQLException, Exception;
	
	public boolean clearCarrierPending(String esn,String min) throws SQLException, Exception;
	
	public boolean clearCarrierPendingOnReactivation(String esn) throws SQLException, Exception;
	
	public String checkPhoneStatus(String esn)throws SQLException, Exception;
	
}
