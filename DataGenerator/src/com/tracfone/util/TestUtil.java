package com.tracfone.util;

import java.util.Random;

public class TestUtil
{
  public static final String EMAIL_DOMAIN = "@tracfone.com";
  private static Random random = new Random();
  private static final String alphabet = "abcdefghijklmnopqrstuvwxyz0123456789";
  
  public TestUtil() {}
  
  public String createRandomEmail() {
	String localEmailPart = generateAlphanumericString(9);
    String email = localEmailPart + "@tracfone.com";
    return email;
  }
  
  private static final String nameAlphabet = "abcdefghijklmnopqrstuvwxyz";
  private String generateAlphanumericString(int length) { if (length < 0) {
      throw new IllegalArgumentException("Length of string cannot be negative");
    }
    
    StringBuilder result = new StringBuilder(length);
    for (int i = 0; i < length; i++) {
      result.append("abcdefghijklmnopqrstuvwxyz0123456789".charAt(random.nextInt("abcdefghijklmnopqrstuvwxyz0123456789".length())));
    }
    return result.toString();
  }
}
