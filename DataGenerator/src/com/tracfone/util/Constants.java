package com.tracfone.util;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * This class contains constants of utility Strings and enums.
 * */
public interface Constants {
	
	/**
	 * This enum contains  code values
	 * 
	 * @author pradeep.viramalla
	 */
	enum enumCodeValues {
		ESN,
		SIM, 
		PIN,
		MIN, 
		ACTIVATE, 
		BYOPSIM, 
		CHECK_ACTIVATION,
		EMAIL,
		JOINSIM,
		GET_ACTIVE_ESN,
		CHECK_REDEMPTION,
		DEACTIVATE,
		CLEAR_OTA,
		UPDATE_MIN,
		CDMA_BYOP_ESN,
		UPDATE_CDMA_BYOP_ESN,
		CDMA_BYOP_ESN_TW,
		UPDATE_CDMA_BYOP_ESN_TW,
		SET_LEASED_ESN_STATUS,
		REDEEM_LEASED_ESN,
		CLEAR_CARRIER_PENDING,
		CLEAR_CARRIER_ON_REACTIVATION,
		NUMBER_OF_RECORDS,
		RTR_ACTIVATION,
		CHECK_PHONE_STATUS
	}
		
		
	
	/**
	 * This enum will provide Options for command line arguments and takes 3 values
	 * (option{@link String}, arguments{@link String}, description{@link String}, isOptionRequired{@link Boolean})
	 * 
	 * 
	 * 
	 * @author pradeep.viramalla
	 */
	enum enumOptions {
		/*
		 * ESN_PARTNUMBER("epn","ESN Partnumber","ESN Partnumber",FALSE)
		 * 
		 * epn : option value
		 * ESN Partnumber : argument description
		 * ESN Partnumber : option description
		 * FALSE : True for mandatory and false for non  mandatory options
		 * */
		ESN_PARTNUMBER("epn","ESN Partnumber","ESN Partnumber",FALSE),
		OPERATION("o","OPERATION","Operations ex:SIM, ESN, PIN, EMAIL, MIN etc",TRUE), 
		SIM_PARTNUMBER("spn","SIM Partnumber","SIM Partnumber",FALSE), 
		PIN_PARTNUMBER("ppn","PIN Partnumber","PIN Partnumber",FALSE), 
		SIM("sim","SIM","SIM",FALSE), 
		ESN("esn","ESN","ESN",FALSE),
		TRANSACTION_TYPE("tt","Transaction Type","Transaction Type",FALSE),
		PIN("pin","PIN","PIN",FALSE),
		ENV("env","DB environment","Database Environment",TRUE),
		MIN("min","MIN","MIN",FALSE),
		BRAND("brand","Brand name","Brand name",FALSE),
		DEACTIVATION_REASON("dr","Deactivation Reason","Deactivation Reason",FALSE),
		ACTION_TYPE("ac","Action Type","Action Type",FALSE),
		REDEEM_TYPE("rt","Redeem Type","Redeem Type",FALSE),
		BUY_FLAG("bf","Buy flag","Buy flag",FALSE),
		BASE_DIRECTORY("bd","Base Directory","Base Directory",FALSE),
		ISACTIVE("isActive","IS ACtive","IS Active",FALSE),
		ZIP("zip","zip code","zip code",FALSE),
		ISLTE("isLTE","Is LTE","IS LTE",FALSE),
		CARRIER("carrier","CARRIER","CARRIER",FALSE),
		ISIPHONE("isIphone","IS IPHONE","IS IPHONE",FALSE),
		ISHD("isHD","IS HD","IS HD",FALSE),
		ISTRIO("isTrio","IS TRIO","IS TRIO",FALSE),
		PHONE_TYPE("pt","Phone type","Phone type",FALSE),
		PHONE_MODEL("pm","Phone Model","Phone Model",FALSE),
		STATUS("status","status","status for leased ESN",FALSE),
		NUMBER_OF_RECORDS("noOfRecords","number of records","number of records to be read",FALSE),
		PLAN_NAME("planName", "Name of the plan", "Name of the plan",FALSE);

		private final String option;
		private final String arguments;
		private final String description;
		private Boolean isOptionRequired;
		
		private enumOptions(String option, String arguments, String description,Boolean isOptionRequired) {
			this.option = option;
			this.arguments = arguments;
			this.description = description;
			this.isOptionRequired = isOptionRequired;
		}

		public String getOption() {
			return option;
		}

		public String getArguments() {
			return arguments;
		}

		public String getDescription() {
			return description;
		}

		public Boolean getIsOptionRequired() {
			return isOptionRequired;
		}

		public void setIsOptionRequired(Boolean isOptionRequired) {
			this.isOptionRequired = isOptionRequired;
		}
		

	}

	String DB_ENIRONMENT = System.getProperty("db.env");
	String DB_DRIVER = "oracle.jdbc.OracleDriver";
	String DB_USERNAME = "itquser_data";
	//String DB_DRIVER = "oracle.jdbc.OracleDriver";
	
	
}
