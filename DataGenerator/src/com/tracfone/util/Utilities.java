package com.tracfone.util;


import static java.lang.Boolean.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.tracfone.util.Constants.enumOptions;


/**
 * This class will build Map object for mapping methods with option codes and perfoms reflection call 
 * */
public class Utilities {

	/**
	 * This method will build options {@link Options} object
	 * 
	 *@return commandLineOptions {@link Options}
	 *
	 *@author pradeep.viramalla
	 * 
	 * */
	
	public static Options buildOptionsObject() {
		Options commandLineOptions = new Options();
		
		for (enumOptions enumValue : enumOptions.values()) {
			Option optionObj = new Option(enumValue.getOption(), TRUE, enumValue.getDescription());
			optionObj.setRequired(enumValue.getIsOptionRequired());
			optionObj.setArgName(enumValue.getArguments());
			commandLineOptions.addOption(optionObj);
		}

		return commandLineOptions;
	}
	

	/**
	 * This method will build place holders for stored functions and procedures ex:if we 3 as input 
	 * it will build string as (?,?,?) 
	 * @param inputParams {@link Integer}
	 * 
	 * @return placeholders {@link String}
	 */
	public static String buildplaceHolders(int inputParams) {
		String placeholders = "(";
		for (int i = 1; i <= inputParams; i++) {
			placeholders += "?,";
		}
		return placeholders.substring(0, placeholders.length() - 1) + ")";		
	}
	

	/**
	 * This method will read DB.properties file
	 *  
	 * @return properties {@link Properties}
	 * 
	 * @author pradeep.viramalla
	 */
	public static Properties getProperties() {

		Properties properties = new Properties();
		try {

			File jarPath = new File(DBConnectionUtil.class.getProtectionDomain().getCodeSource().getLocation().getPath());
			String propertiesPath = jarPath.getCanonicalPath();
			properties.load(new FileInputStream(propertiesPath + "\\properties\\Protractor.properties"));
		} catch (IOException e1) {
			e1.printStackTrace();
			System.out.println("Exception : Unable to read data base properties file");
			System.out.println("Expected file : Protractor.properties");
		}
		return properties;
	}

}
