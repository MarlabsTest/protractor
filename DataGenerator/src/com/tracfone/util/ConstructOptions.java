package com.tracfone.util;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.ParseException;

import com.tracfone.util.Constants.enumOptions;
import com.tracfone.vo.InputOptions;


/**
 * This class contains methods to build  and parse the command line options
 * */
@SuppressWarnings("deprecation")
public class ConstructOptions {
	
	/**
	 * This method will parse the given command line arguments and returns the InputOptions {@link ConstructOptions} object
	 * 
	 * @param commandLineArguments {@link String[]}
	 * @return inputOptions {@link InputOptions}
	 * @author pradeep.viramalla
	 * */
	public static InputOptions parseCommandLineOptions(String[] commandLineArguments)throws ParseException,NullPointerException {
		
		InputOptions inputOptions=null;
			final CommandLineParser cmdLineGnuParser = new GnuParser();
			CommandLine commandLine = null;
			try {
				commandLine = cmdLineGnuParser.parse(Utilities.buildOptionsObject(), commandLineArguments);				
				if(commandLine!=null && commandLine.getOptions().length > 0){
					inputOptions=new InputOptions();
					inputOptions.setOperation(commandLine.getOptionValue(enumOptions.OPERATION.getOption()));
					inputOptions.setEsnPartNumber(commandLine.getOptionValue(enumOptions.ESN_PARTNUMBER.getOption()));
					inputOptions.setSimPartNumber(commandLine.getOptionValue(enumOptions.SIM_PARTNUMBER.getOption()));
					inputOptions.setPinPartNumber(commandLine.getOptionValue(enumOptions.PIN_PARTNUMBER.getOption()));
					inputOptions.setEsn(commandLine.getOptionValue(enumOptions.ESN.getOption()));
					inputOptions.setSim(commandLine.getOptionValue(enumOptions.SIM.getOption()));
					inputOptions.setPin(commandLine.getOptionValue(enumOptions.PIN.getOption()));
					inputOptions.setEnv(commandLine.getOptionValue(enumOptions.ENV.getOption()));
					inputOptions.setTransactionType(commandLine.getOptionValue(enumOptions.TRANSACTION_TYPE.getOption()));
					inputOptions.setRedeemType(commandLine.getOptionValue(enumOptions.REDEEM_TYPE.getOption()));
					inputOptions.setBuyFlag(commandLine.getOptionValue(enumOptions.BUY_FLAG.getOption()));
					inputOptions.setBaseDirectory(commandLine.getOptionValue(enumOptions.BASE_DIRECTORY.getOption()));
					inputOptions.setDeactivationReason(commandLine.getOptionValue(enumOptions.DEACTIVATION_REASON.getOption()));
					inputOptions.setMin(commandLine.getOptionValue(enumOptions.MIN.getOption()));
					inputOptions.setBrand(commandLine.getOptionValue(enumOptions.BRAND.getOption()));
					inputOptions.setCarrier(commandLine.getOptionValue(enumOptions.CARRIER.getOption()));
					inputOptions.setIsActive(commandLine.getOptionValue(enumOptions.ISACTIVE.getOption()));
					inputOptions.setZip(commandLine.getOptionValue(enumOptions.ZIP.getOption()));
					inputOptions.setIsIphone(commandLine.getOptionValue(enumOptions.ISIPHONE.getOption()));
					inputOptions.setIsLTE(commandLine.getOptionValue(enumOptions.ISLTE.getOption()));
					inputOptions.setIsHD(commandLine.getOptionValue(enumOptions.ISHD.getOption()));
					inputOptions.setStatus(commandLine.getOptionValue(enumOptions.STATUS.getOption()));
					inputOptions.setIsTrio(commandLine.getOptionValue(enumOptions.ISTRIO.getOption()));
					inputOptions.setPhoneType(commandLine.getOptionValue(enumOptions.PHONE_TYPE.getOption()));
					inputOptions.setPhoneModel(commandLine.getOptionValue(enumOptions.PHONE_MODEL.getOption()));
					inputOptions.setPlanName(commandLine.getOptionValue(enumOptions.PLAN_NAME.getOption()));
					
					String noOfRecords = commandLine.getOptionValue(enumOptions.NUMBER_OF_RECORDS.getOption().toString());
					if(noOfRecords != null)
					{
						try
						{
							inputOptions.setNoOfRecords(Integer.parseInt(noOfRecords));
						}
						catch(Exception ex)
						{
							System.out.println("Exception while parsing noOfRecords argument");
						}
					}
					
					String optionValue = commandLine.getOptionValue(enumOptions.ACTION_TYPE.toString());
					if(optionValue != null){
						inputOptions.setActionType(Integer.parseInt(optionValue));
					}
				}
				
				if(inputOptions!=null && inputOptions.getEsn()==null && inputOptions.getEsnPartNumber()==null 
						              && inputOptions.getPin()==null && inputOptions.getPinPartNumber()==null
						              && inputOptions.getSim()==null && inputOptions.getSimPartNumber()==null
						              && inputOptions.getMin()==null && !(inputOptions.getOperation().equalsIgnoreCase("CDMA_BYOP_ESN"))
						              && !(inputOptions.getOperation().equalsIgnoreCase("UPDATE_CDMA_BYOP_ESN"))
						              && !(inputOptions.getOperation().equalsIgnoreCase("CDMA_BYOP_ESN_TW"))
						              && !(inputOptions.getOperation().equalsIgnoreCase("UPDATE_CDMA_BYOP_ESN_TW"))){
					throw new MissingOptionException("\nOne of option from : -esn,-epn,-pin,-min,-ppn,-sim,-spn is missing.");
				}
			} catch (Exception e) {
				System.out.println("\n"+e.getMessage()+"\n");
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp( "OPTIONS", Utilities.buildOptionsObject() );
				System.out.println("\n-env , -o and one from : -esn,-epn,-pin,-min,-ppn,-sim,-spn options are mandatory.");
				System.exit(0);
			}
			
		return inputOptions;
	}

}
