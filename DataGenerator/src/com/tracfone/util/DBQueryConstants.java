package com.tracfone.util;

/**
 * This class contains constants of database queries.
 * */
public interface DBQueryConstants {

	String GET_TEST_SIM_BY_PART_NUMBER = "get_test_sim_infy";
//	String GET_TEST_SIM_BY_PART_NUMBER = "sa.get_test_sim";
	
	String GET_TEST_ESN_IMSI = "SA.get_test_esn_IMSI";

	String GET_TEST_NEW_ESN_BY_PART_NUMBER = "sa.get_test_esn";

	String GET_TEST_BYOP_ESN = "sa.get_test_esn_byop";

	String CHECK_ACTIVATION = "check_activation";

	String CHECK_REGISTER = "check_register";

	String SM_DEACTIVATION = "SA.SERVICE_DEACTIVATION_CODE.DEACTSERVICE";

	String VERIFY_PHONE_UPGRADE = "sa.verify_phone_upgrade_pkg.verify";

	String CHECK_PHONE_UPGRADE = "CHECK_PHONE_UPGRADE";

	String GET_ACTIVE_ICCID = "select x_zipcode, X_ICCID from table_site_part where part_status='Active' and x_service_id= ?";

	String GET_TEST_ACTIVE_ESN_BY_PART_NUMBER = "SELECT  pi.part_serial_no FROM TABLE_PART_INST PI, TABLE_MOD_LEVEL ML, TABLE_PART_NUM PN, table_part_class pc,"
			+ " TABLE_SITE_PART sp, SA.TEST_IGATE_ESN ig WHERE PI.N_PART_INST2PART_MOD = ML.OBJID AND ML.PART_INFO2PART_NUM = PN.OBJID AND pi.x_part_inst2site_part = sp.objid and pc.objid=pn.part_num2part_class "
			+ "and sp.x_min not like 'T%' and sp.part_status='Active' and pi.x_part_inst_status = '52' and pi.X_DOMAIN||''='PHONES' and pn.part_number = ? and (x_model_number not in ('STAPI4C', 'STAPI4SC', 'STAPI5C') OR "
			+ "pi.x_hex_serial_no is not null) and sp.x_expire_dt > sysdate and ig.ESN=pi.part_serial_no and ig.ESN_TYPE='C' and ROWNUM <= ?";

	String GET_ACTIVE_33178_ESN_BY_PART_NUMBER = "SELECT  pi.part_serial_no FROM TABLE_PART_INST PI, TABLE_MOD_LEVEL ML, TABLE_PART_NUM PN, table_part_class pc,"
			+ " TABLE_SITE_PART sp, SA.TEST_IGATE_ESN ig WHERE PI.N_PART_INST2PART_MOD = ML.OBJID AND ML.PART_INFO2PART_NUM = PN.OBJID AND pi.x_part_inst2site_part = sp.objid and pc.objid=pn.part_num2part_class "
			+ "and sp.x_min not like 'T%' and sp.part_status='Active' and pi.x_part_inst_status = '52' and pi.X_DOMAIN||''='PHONES' and pn.part_number = ? and (x_model_number not in ('STAPI4C', 'STAPI4SC', 'STAPI5C') OR "
			+ "pi.x_hex_serial_no is not null) and sp.x_expire_dt > sysdate and ig.ESN=pi.part_serial_no and ig.ESN_TYPE='C' and sp.x_zipcode = '33178' and ROWNUM < 5";

	String GET_ACTIVE_ESN_BY_PART_NUMBER_NO_ACCOUNT = "SELECT  pi.part_serial_no FROM TABLE_PART_INST PI, TABLE_MOD_LEVEL ML, TABLE_PART_NUM PN, table_part_class pc, "
			+ "TABLE_SITE_PART sp WHERE PI.N_PART_INST2PART_MOD = ML.OBJID AND ML.PART_INFO2PART_NUM = PN.OBJID AND pi.x_part_inst2site_part = sp.objid and pc.objid=pn.part_num2part_class and "
			+ "sp.x_min not like 'T%' and sp.part_status='Active' and pi.x_part_inst_status='52' and pi.X_DOMAIN||''='PHONES' and pn.part_number like ? and (x_model_number not in ('STAPI4C', 'STAPI4SC', 'STAPI5C') OR "
			+ "pi.x_hex_serial_no is not null)  and not exists (select null from TABLE_X_CONTACT_PART_INST cp, table_contact c, table_web_user w WHERE pi.objid = cp.x_contact_part_inst2part_inst "
			+ "and w.web_user2contact =  c.objid and cp.x_contact_part_inst2contact = c.objid) and ROWNUM < 2";

	String GET_ACTIVE_ESN_BY_BRAND = "SELECT pi.part_serial_no FROM table_part_inst pi, table_mod_level ml, table_part_num pn, table_bus_org org, table_part_inst line, TABLE_SITE_PART sp "
			+ "WHERE pi.x_domain || '' = 'PHONES' AND pi.n_part_inst2part_mod = ml.objid AND ml.part_info2part_num = pn.objid and pn.part_num2bus_org=org.objid "
			+ "AND pi.x_part_inst2site_part = sp.objid and line.part_to_esn2part_inst = pi.objid AND line.x_domain || '' = 'LINES'  and sp.x_zipcode = '33178' and "
			+ "sp.x_min not like 'T%' and pi.x_part_inst_status='52' and sp.part_status='Active' and org.s_org_id=? and ROWNUM < 30";

	String GET_NEW_ESN_BY_PART_NUMBER_NO_ACCOUNT = "SELECT  pi.part_serial_no FROM TABLE_PART_INST PI, TABLE_MOD_LEVEL ML, TABLE_PART_NUM PN, "
			+ "table_part_class pc WHERE PI.N_PART_INST2PART_MOD = ML.OBJID AND ML.PART_INFO2PART_NUM = PN.OBJID and pc.objid=pn.part_num2part_class "
			+ "AND pi.x_part_inst_status = '50' and pi.X_DOMAIN||''='PHONES' and pn.part_number = ? and (x_model_number not in ('STAPI4C', 'STAPI4SC', 'STAPI5C') OR "
			+ "pi.x_hex_serial_no is not null) and ROWNUM < 2 and not exists (select null from TABLE_X_CONTACT_PART_INST cp, table_contact c, table_web_user w WHERE pi.objid ="
			+ " cp.x_contact_part_inst2part_inst and w.web_user2contact =  c.objid and cp.x_contact_part_inst2contact = c.objid)";

	String GET_HEX_ESN_BY_DEC_ESN = "SELECT pi.x_hex_serial_no, x_model_number, pi.part_serial_no FROM TABLE_PART_INST pi, TABLE_MOD_LEVEL ml, "
			+ "TABLE_PART_NUM pn, table_part_class pc WHERE pi.N_PART_INST2PART_MOD = ml.OBJID AND ml.PART_INFO2PART_NUM = pn.OBJID and pc.objid=pn.part_num2part_class "
			+ "and X_DOMAIN||''='PHONES' and pi.part_serial_no = ? and pi.x_hex_serial_no is not null and rownum < 2";
	// "and X_DOMAIN||''='PHONES' and x_model_number in ('STAPI4C', 'STAPI4SC',
	// 'STAPI5C') and pi.part_serial_no = ? and pi.x_hex_serial_no is not null
	// and rownum < 2";

	String GET_TEST_PAST_DUE_ESN_BY_PART_NUMBER = "SELECT part_serial_no FROM table_part_inst pi, table_mod_level ml, table_part_num pn WHERE 1=1 AND x_domain || '' = 'PHONES' "
			+ "AND pi.n_part_inst2part_mod = ml.objid AND ml.part_info2part_num = pn.objid AND pn.s_part_number = ? "
			+ "AND x_part_inst_status=54 AND rownum < 2 ";

	String GET_TEST_USED_ESN_BY_PART_NUMBER = "SELECT part_serial_no FROM table_part_inst pi, table_mod_level ml, table_part_num pn WHERE 1=1 AND x_domain || '' = 'PHONES' "
			+ "AND pi.n_part_inst2part_mod = ml.objid AND ml.part_info2part_num = pn.objid AND pn.s_part_number = ? "
			+ "AND x_part_inst_status=51 AND rownum < 2 ";

	String GET_TEST_REFURB_ESN_BY_PART_NUMBER = "SELECT part_serial_no FROM table_part_inst pi, table_mod_level ml, table_part_num pn WHERE 1=1 AND x_domain || '' = 'PHONES' "
			+ "AND pi.n_part_inst2part_mod = ml.objid AND ml.part_info2part_num = pn.objid AND pn.s_part_number = ? "
			+ "AND x_part_inst_status=150 AND rownum < 2 ";

	String GET_TEST_PIN_BY_PART_NUMBER = "sa.get_test_pin";
	String CLEAR_TNUMBER = "SA.clear_tnumber";
	String DEACTIVATE_ESN_BY_PART_NUMBER = "SA.deactivate_past_due_esn";
	String GET_TEST_ACTIVE_ST_ESN_BY_PART_NUMBER = "sa.get_active_esn_pn1";
	String RUN_IGATE_IN = "sa.igate_in3.rta_in";
	String REMOVE_ESN_FROM_ST_ACCOUNT = "remove_myaccount_st";
	String COMMIT_CMD = "commit";

	String SQL_UPDATE_IG_TRANSACTION_CDMA_PHONE_ACTIVATION = "update ig_transaction set STATUS = 'W', min = ?, msid = ?, NEW_MSID_FLAG='Y' "
			+ " where ACTION_ITEM_ID = (select action_item_id from gw1.IG_TRANSACTION where esn = ? and status = 'L' and STATUS <> 'S' and rownum = 1 )";

	String SQL_UPDATE_IG_TRANSACTION_GSM_PHONE_ACTIVATION = "update ig_transaction set STATUS = 'W', msid = ?, NEW_MSID_FLAG='Y' "
			+ " where ACTION_ITEM_ID = (select action_item_id from gw1.IG_TRANSACTION where esn = ? and status = 'L' and STATUS <> 'S' and rownum = 1 )";

	String SQL_UPDATE_PHONE_ACTIVATION_SAME_MIN = "update ig_transaction set STATUS = 'W' where ACTION_ITEM_ID = "
			+ "(select action_item_id from gw1.IG_TRANSACTION where esn = ? and status = 'L' and order_type <> 'S' and rownum = 1 )";

	String SQL_UPDATE_PHONE_ACTIVATION_PORT_MIN = "update ig_transaction set STATUS = 'W' where ACTION_ITEM_ID = "
			+ "(select action_item_id from gw1.IG_TRANSACTION where esn = ? and (order_type = 'PIR' or order_type = 'IPI' or order_type = 'E')  and rownum = 1 )";

	String SQL_FINISH_CDMA_BYOP_IGATE = "update ig_transaction set STATUS = 'S', APPLICATION_SYSTEM='IG_RWK', x_pool_name= ?, X_MPN= ?,  STATUS_MESSAGE= ? where ESN = ? and ORDER_TYPE='VD' and TEMPLATE = ?";
	// "update ig_transaction set STATUS = 'W', APPLICATION_SYSTEM='IG_RWK',
	// X_MPN='APL', X_MPN_CODE='IPHONE 4 BLACK 8GB', STATUS_MESSAGE='TracFone:
	// Validate Device completed succesfully' where ESN = ? and ORDER_TYPE='VD'
	// and TEMPLATE = ?
	// 'ESN_IN_USE: Device is in use'

	String SQL_COMPLETE_CDMA_BYOP_IGATE = "update ig_transaction set STATUS = 'F' where ESN = ? and ORDER_TYPE='VD' and TEMPLATE = ?";

	String SQL_FINISH_REDEEM_IGATE = "update ig_transaction set STATUS = 'W' where ESN = ? and ORDER_TYPE='CR'";

	String SQL_FINISH_ALL_IGATE = "update ig_transaction set STATUS = 'W' where ESN = ? and STATUS='L'";

	String SQL_RESERVED_LINE_BY_ESN_QUERY = "SELECT " + "TABLE_PART_INST.OBJID as id"
			+ ", TABLE_PART_INST.PART_SERIAL_NO as min" + ", TABLE_PART_INST.X_MSID as msid"
			+ ", TABLE_PART_INST.X_NXX as nxx" + ", TABLE_PART_INST.X_NPA as npa" + ", TABLE_PART_INST.X_EXT as ext"
			+ ", TABLE_PART_INST.X_PART_INST_STATUS as status"
			+ ", TABLE_PART_INST.PART_INST2X_PERS as personalityObjid"
			+ ", TABLE_PART_INST.PART_INST2X_NEW_PERS as newPersonalityObjid"
			+ ", TABLE_PART_INST.PART_INST2CARRIER_MKT as marketObjid"
			+ ", TABLE_PART_INST.X_PORT_IN as linePortInStatus"
			+ ", TABLE_PART_INST.PART_TO_ESN2PART_INST as phoneObjid" + " FROM TABLE_X_PARENT pa,"
			+ " TABLE_X_CARRIER_GROUP gr," + " TABLE_X_CARRIER ca," + " TABLE_PART_INST," + " table_part_inst pi2"
			+ " WHERE pa.objid = x_carrier_group2x_parent" + " AND carrier2carrier_group = gr.objid"
			+ " AND TABLE_PART_INST.part_inst2carrier_mkt = ca.objid"
			+ " and TABLE_PART_INST.part_to_esn2part_inst = pi2.objid"
			+ " AND TABLE_PART_INST.x_part_inst_status IN ('37', '39', '73')"
			+ " AND TABLE_PART_INST.part_serial_no IN (SELECT part_serial_no FROM TABLE_PART_INST"
			+ " WHERE part_to_esn2part_inst IN (SELECT objid FROM TABLE_PART_INST" + " where part_serial_no in (?)))";

	String SQL_MIN_OF_ACTIVE_ESN = "SELECT X_MIN FROM TABLE_SITE_PART WHERE X_SERVICE_ID = ? AND PART_STATUS IN ('Active', 'CarrierPending')";

	String SQL_GET_NEXT_CDMA = "select to_char(sa.cdma_serial_no_seq.nextval) as ESN from dual";

	String SQL_UPDATE_OTA_PENDING_BY_ESN = "update table_x_OTA_TRANSACTION "
			+ " set X_STATUS='Completed' " + " WHERE X_STATUS='OTA PENDING' " + " AND X_ESN= ?";

	String SQL_ADD_TO_TEST_OTA_ESN = "insert into GW1.TEST_OTA_ESN values (?)";

	String SQL_ADD_TO_TEST_IGATE_ESN = "insert into SA.TEST_IGATE_ESN values (?, ?)";

	String SQL_CHANGE_TEST_IGATE_ESNTYPE = "update SA.TEST_IGATE_ESN set ESN_TYPE= ? where ESN= ?";

	String ADD_SIM_TO_ESN = "UPDATE table_part_inst SET x_iccid=? WHERE part_serial_no=? AND x_part_inst_status='50'";

	String GET_SIM_FROM_ESN = "select x_iccid from table_part_inst WHERE part_serial_no= ? ";

	String SET_DUE_DATE_IN_FUTURE_SITE = "UPDATE table_site_part "
			+ "SET x_expire_dt = trunc(sysdate+10), " + "warranty_date = trunc(sysdate+10) " + "WHERE x_service_id = ?";

	String SET_DUE_DATE_IN_FUTURE_PART_INST = "UPDATE table_part_inst "
			+ "SET warr_end_date =trunc(sysdate+10) " + "WHERE part_serial_no = ?";

	String SET_DUE_DATE_IN_PAST_SITE = "UPDATE table_site_part "
			+ "SET x_expire_dt = trunc(sysdate-10), " + "warranty_date = trunc(sysdate-10) " + "WHERE x_service_id = ?";

	String SET_DUE_DATE_IN_PAST_PART_INST = "UPDATE table_part_inst "
			+ "SET warr_end_date =trunc(sysdate-10) " + "WHERE part_serial_no = ?";

	String ADD_PIN_TO_QUEUE = "update table_part_inst set x_part_inst_status = '400', "
			+ "x_ext = '1', part_to_esn2part_inst = (select objid from table_part_inst " + "where part_serial_no = ?) "
			+ "where x_red_code = ?";

	String CLEAR_OTA = "update table_x_ota_transaction set x_status = 'Completed' where x_esn = ?";

	String ESNUPDATE_INACTIVE_STATUS = "update table_part_inst set part_status='INACTIVE', x_part_inst_status='59' WHERE part_serial_no= ?";

	String RESET_ILD_COUNT = "update x_vas_event_counter set objectvalue_event_counter=1 "
			+ "where vas_objectvalue = ( select x_min from table_site_part where x_service_id= ? "
			+ "and part_status ='Active' )";

	String GET_OBJID = "SELECT objid FROM TABLE_USER WHERE s_login_name ='SA'";

	String CHECK_REDEMPTION = "CHECK_REDEMPTION";

	String GET_STATE_BY_ZIPCODE = "select x_state from table_x_zip_code where x_zip = ?";

	String MEID_DEC_TO_HEX = "sa.meiddectohex";

	String GET_ESN_BY_BRAND_QUEUE = "select ESN from itq_gen_cases where QUEUE_NAME = ? and BRAND = ?";

	String GET_TEST_SIM_BYOP = "sa.GET_TEST_SIM_BYOP";	
	
	String UPDATE_MIN = "update ig_transaction set MIN = (SELECT X_MIN FROM TABLE_SITE_PART WHERE X_SERVICE_ID = ?) where esn=? ";
	
	String CLEAR_CARRIER_PENDING = "UPDATE ig_transaction SET STATUS = 'W', MSID = ? ,MIN = ? ,NEW_MSID_FLAG ='Y' WHERE esn =? ";
	
	String CLEAR_CARRIER_ON_REACTIVATION = "update ig_transaction set status ='W' where  action_item_id in (select action_item_id from ig_transaction where esn in (?) and order_type ='A' and status ='L')";
	
	String UPDATE_CDMA_BYOP_TW_4G = "UPDATE  GW1.IG_TRANSACTION IG SET IG.STATUS = 'S' , IG.STATUS_MESSAGE = ? "
			                     +" , IG.X_MPN = ? ,IG.ICCID = ? , IG.X_MPN_CODE = ? , IG.X_POOL_NAME = ?  WHERE esn in (?) AND TEMPLATE = ? ";
	
	String UPDATE_CDMA_BYOP_TW_3G = "UPDATE  GW1.IG_TRANSACTION IG SET IG.STATUS = 'S' , IG.STATUS_MESSAGE = ? "
            +" , IG.X_MPN = ? , IG.X_MPN_CODE = ? , IG.X_POOL_NAME = ?  WHERE esn in (?) AND TEMPLATE = ? ";
	
	String CHECK_PHONESTATUS = "select x_part_inst_status from table_part_inst where part_serial_no = ? ";
}