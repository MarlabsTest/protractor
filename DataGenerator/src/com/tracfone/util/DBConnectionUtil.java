package com.tracfone.util;

import static com.tracfone.util.Utilities.getProperties;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DBConnectionUtil {

	 private static Connection connection;  
	 
	 private DBConnectionUtil(){
		 
	 }  
	 
	 /**
	  * This method will return connection object from data base
	  * 
	  * @return connection {@link Connection}
	  * 
	  * @author pradeep.viramalla
	  */
	 
	public static Connection getConnection() throws SQLException, ClassNotFoundException,Exception {
		if (connection == null) {
			Class.forName(Constants.DB_DRIVER);
			connection = DriverManager.getConnection(getProperties().getProperty(Constants.DB_ENIRONMENT + "_url"),
					Constants.DB_USERNAME, getProperties().getProperty(Constants.DB_ENIRONMENT + "_password"));
		}
		return connection;
	} 
	
	/**
	 * This method will prepare callableStatement and will execute stored function.
	 * 
	 * @param functionName {@link String}
	 * @param params {@link HashMap}
	 * @param inputParms {@link Integer}
	 * 
	 * @return result {@link String}
	 * 
	 * @author pradeep.viramalla
	 */
	public static String executeStoredFunction(String functionName,HashMap<String, Object> params,int inputParms) throws SQLException, ClassNotFoundException,Exception {
		CallableStatement callableStatement = getConnection()
				.prepareCall("{? = call " + functionName + Utilities.buildplaceHolders(inputParms) + "}");

		buildCallableForFunction(callableStatement, params);
		callableStatement.executeUpdate();

		String result = callableStatement.getString(1);

		return result;
	}
	
	/**
	 * This method will prepare PreparedStatement and executes query.
	 * 
	 * @param params {@link HashMap}
	 * @param query  {@link String}
	 * @param queryType  {@link String}
	 * @param totaoInputs  {@link Integer}
	 * 
	 *@return outValue {@link String}
	 * 
	 * @author pradeep.viramalla
	 */
	
	public static String executePreparedStatement(HashMap<String, Object> params,String query,String queryType,int totaoInputs)throws Exception {
		PreparedStatement preparedStatement = getConnection().prepareStatement(query);

//		System.out.println("prepared statement "+preparedStatement);
//		System.out.println("params "+params);
//		System.out.println("totaoInputs "+totaoInputs);
		
		buildPreparedStatement(preparedStatement, params, totaoInputs);
//		System.out.println("prepared statement "+preparedStatement);
		ResultSet executeQuery = preparedStatement.executeQuery();

		if ("select".equalsIgnoreCase(queryType)) {
			String outValue = null;
			if (executeQuery.isBeforeFirst()) {
				if(!executeQuery.isFirst()){				
					executeQuery.next();				
				}		
				outValue = executeQuery.getString((String) params.get("output1"));
				
			}

			return outValue;
		}
		return null;
	}
	
	//this method will return pipeline seperated result for multiple records
	public static String getPipelineSeparatedQueryResult(HashMap<String, Object> params,String query,String queryType,int totaoInputs)throws Exception {
		PreparedStatement preparedStatement = getConnection().prepareStatement(query);

		String outValue = null;
		buildPreparedStatement(preparedStatement, params, totaoInputs);
		ResultSet resultSet = preparedStatement.executeQuery();

		int loopCount = 0;
		while(resultSet.next())
		{
			if(loopCount <= 0)
			{
				outValue = resultSet.getString((String) params.get("output1"));
			}
			else
			{
				outValue = outValue +"|"+ resultSet.getString((String) params.get("output1"));
			}
			loopCount = loopCount + 1;
		}
		return outValue;
	}

	/**
	 * This method will prepare CallableStatement {@link CallableStatement} for stored function
	 * 
	 * @param callableStatement {@link CallableStatement}
	 * @param params {@link HashMap}
	 * @param totaoInputs {@link Integer}
	 * 
	 * @return callableStatement  {@link CallableStatement}
	 * 
	 * @author pradeep.viramalla
	 * 
	 */
	public static CallableStatement buildCallableForFunction(CallableStatement callableStatement,HashMap<String, Object> params) throws SQLException {
		int i = 1;

		callableStatement.registerOutParameter(i, java.sql.Types.VARCHAR);
		for (i = 2; i <= params.size() + 1; i++) {
			callableStatement.setObject(i, params.get("input" + (i - 1)));
		}
		
		return callableStatement;
	}
	
	/**
	 * This method will prepare CallableStatement {@link PreparedStatement} for PreparedStatement
	 * 
	 * @param preparedStatement {@link PreparedStatement}
	 * @param params {@link HashMap}
	 * @param totalInputs {@link Integer}
	 * 
	 * @return preparedStatement {@link PreparedStatement}
	 * 
	 * @author pradeep.viramalla
	 */
	public static PreparedStatement buildPreparedStatement(PreparedStatement preparedStatement,
			HashMap<String, Object> params, int totalInputs) throws SQLException {
		for (int i = 1; i <= totalInputs; i++) {
			preparedStatement.setObject(i, params.get("input"+i));
		}
		return preparedStatement;
	}

}
