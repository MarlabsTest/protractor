'use strict';
var expectedConditions = protractor.ExpectedConditions;
var generator = require('creditcard-generator');
var CCUtil = require("../util/creditCard.utils");
var CommonUtil= require("../util/common.functions.util");
var sessionData = require("./sessiondata.do");
//===================CardPayment Page==============================

var cardPayment = function() {

	//-----------Enter Credit Card Details-----------------
	
	this.payment = element.all(by.buttonText('CONTINUE TO PAYMENT')).get(1);	
	this.newPayment = element.all(by.linkText('NEW')).get(0); //credit
	this.enterCardInfo = element.all(by.css("input[id='formName.number_mask']")).get(0);
	this.nickName = element(by.css("input[id='nickname']"));
	this.enterCvv = element.all(by.css("input[id='cvv']")).get(0);
	this.month = element.all(by.model('$select.search')).get(0); 
	this.year = element.all(by.css("input[ng-model='$select.search']")).get(1);  //element.all(by.model('$select.search')).get(1); 
	this.enterMonth = element(by.xpath("//div[@id='exp_month']/div[2]/div/div/div[8]/div/div/p")); 
	this.enterYear =  element(by.xpath("//div[@id='exp_year']/div[2]/div/div/div[4]/div/div/p"));
	this.enterFirstName = element(by.css("input[id='fname']"));
	this.enterLastName = element(by.css("input[id='lname']"));
	this.enterAddress = element(by.css("input[id='address1']"));
	this.city = element.all(by.name('city')).get(1);
	this.enterZip = element(by.css("input[id='zipcode']"));
	this.state = element.all(by.model('$select.search')).get(2); //.$('[label="CA"]');
	this.enterState = element(by.xpath("//div[@id='state']/div[2]/div/div/div[6]/div/div/p"));
	this.addCreditCardBtn = element.all(by.css("button[id='btn_continuetopayment']")).get(1);
		
	this.goToPayCheckout = function() {
		//$$//browser.wait(expectedConditions.visibilityOf(this.payment),40000);
		this.payment.click();
		//$$//browser.wait(expectedConditions.visibilityOf(this.newPayment),40000);
		this.newPayment.click();
		this.enterCardInfo.clear().sendKeys(""+generator.GenCC(sessionData.tracfone.cardType));
		this.nickName.clear().sendKeys('test');
		this.enterCvv.sendKeys(CommonUtil.getCvv(sessionData.tracfone.cardType));
		browser.executeScript("arguments[0].click();", this.month.getWebElement());//this.month.click();
		this.enterMonth.click();
		this.year.click();
		this.enterYear.click();
		this.enterFirstName.sendKeys(CCUtil.firstName);
		this.enterLastName.sendKeys(CCUtil.lastName);
		this.enterAddress.sendKeys(CCUtil.addOne);
		this.city.sendKeys(CCUtil.city);	
		this.state.click();
		this.enterState.click();
		this.enterZip.sendKeys(CCUtil.pin);	
			
		// this.enterCountry.click();
		browser.executeScript("arguments[0].click();", this.addCreditCardBtn.getWebElement()); //this.addCreditCardBtn.click(); 
		// this.addCreditCardBtn.click();
	};
	//-----------------Check if successful checkout is done--------------------------
	
	this.goToSuccessfulCheckout = function() {
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /activationcc\/confirmation_activation/.test(url); 
		});
	};
	
	//-----------------Check if checkout page is loaded--------------------------
	
	this.isLoaded = function() {	
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /checkout$/.test(url);
		});
	};

};

module.exports = new cardPayment;
