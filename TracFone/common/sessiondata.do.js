//toEsnPartNumber, toSimPartNumber, toPinPartNumber are specific to phone upgrade scenario
//oldPartNumber are specific to Port In scenario
//noOfLines is specific to add multi esn scenario
module.exports = {
	"tracfone" : {
		"username" : "",
		"password" : "",
		"esn" : "",
		"pin" : "",
		"zip" : "",
		"sim" : "",
		"cardpin" : "",
		"esnPartNumber" : "",
		"simPartNumber" : "",
		"pinPartNumber" : "",
		"oldPartNumber":"",
		"upgradeEsn":"",
		"toEsnPartNumber" : "",
		"toSimPartNumber" : "",
		"toPinPartNumber" : "",
		"phoneStatus" : "",
		"noOfLines" : "",
		"shopPlan" : "",
		"planName" : "",
		"hppPlan" : "",
		"autoRefill" : "",
		"noOfEsns" :"",
		"esnsToReactivate":[],
		"planType":"",
		"planName":"",
		"cardType":"",
		"isActive":"",
		"carrier":"",
		"isLTE":"",
		"isIPhone":"",
		"isHD":"",
		"isTrio":"",
		"min":"",
		"redemptionPin":""
	}
};
