'use strict';

var myAccount = require("../../myaccount/myaccount.po");
var login = require("../../login/login.po");

//Load HomePage Scenario
describe('Tracfone SignOut', function() {

	beforeEach(function () {

	});
	
	it('should logout Tracfone my account page', function(done) {
		console.log("should logout Tracfone my account page");
		myAccount.signOut();	
		expect(login.isSignOut()).toBe(true);
		done();
	});
	
});
	