'use strict';

var PropertiesReader = require('properties-reader');
var properties = PropertiesReader('../TracFone/properties/Protractor.properties');
var homePage = require("../../common/homepage.po");
var Constants = require("../../util/constants.util");

/*
 * This Spec is to Load a TracFone 4.0 Home page.
 */

describe('TracFone 4.0 HomePage', function() {

	it('should load the TracFone 4.0 home page', function(done) {
		//homePage.load(Constants.ENV[browser.params.environment]);
		homePage.load(properties.get('weburl.'+browser.params.environment));
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();
	});

});
