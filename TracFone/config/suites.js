
module.exports = {
	suites: {
		Activation: [
			'activation/scenario/activation.internalportin.withaccount.withpurchase.scn.js',
			'activation/scenario/activation.internalportin.withaccount.withpin.scn.js',
			'activation/scenario/activation.byopphone.cdma.pin.scn.js',
	    	'activation/scenario/activation.byopphone.scn.js',
			'activation/scenario/activation.withpin.withaccount.scn.js',
			'activation/scenario/activation.with.invalid.pin.scn.js',
			'activation/scenario/activation.phoneupgrade.scn.js',
			'activation/scenario/activation.byopphone.withpurchase.scn.js',
			'activation/scenario/activation.cdma.withpurchase.withaccount.scn.js',
			'activation/scenario/activation.cdma.withpin.withaccount.scn.js',
			'activation/scenario/activation.cdma.withpin.withoutaccount.scn.js',
			'activation/scenario/activation.withpurchase.ach.withaccount.scn.js',
			'activation/scenario/activation.porting.withpin.withaccount.scn.js',
			'activation/scenario/activation.porting.withpurchase.withaccount.scn.js',
			'activation/scenario/activation.withpin.withoutaccount.scn.js',
			'activation/scenario/activation.withpurchase.withaccount.scn.js',
			'activation/scenario/reactivate.phone.with.pin.scn.js',
			'activation/scenario/activation.withpurchase.withaccount.hpp.scn.js',
			'activation/scenario/activation.with.pin.hpp.scn.js',
			'activation/scenario/reactivate.phone.with.purchase.with.enrollment.scn.js'
		],
		Myaccount: [
			'myaccount/scenario/myaccount.login.scn.js',
			'myaccount/scenario/myaccount.forgotpassword.scn.js',
			'myaccount/scenario/myaccount.forgotusername.scn.js',
			'myaccount/scenario/myaccount.add.inactive.esn.scn.js',
			'myaccount/scenario/myaccount.remove.esn.scn.js',
			'myaccount/scenario/myaccount.addnewdevicewithinacc.scn.js',
			'myaccount/scenario/myaccount.buyserviceplanwithinaccount.scn.js',
			'myaccount/scenario/myaccount.editcontactinfo.scn.js',
			'myaccount/scenario/myaccount.managepayment.scn.js',
			'myaccount/scenario/myaccount.removedevicewithinacc.scn.js',
			'myaccount/scenario/myaccount.delete.manageepayment.scn.js',
			'myaccount/scenario/myaccount.buyILD.withenrollment.withinaccount.scn.js',
			'myaccount/scenario/myaccount.ILD.deenrollment.withinaccount.scn.js'
		],
		AutoRefill: [
			'autorefill/scenario/autorefill.deenroll.scn.js',
			'autorefill/scenario/activation.with.autorefil.scn.js'
		],
		Refill: [
			'refill/scenario/refill.withpin.outside.account.scn.js',
			'refill/scenario/refill.withpurchase.outside.account.scn.js',
			'refill/scenario/refill.HPP.plan.purchase.scn.js'
		],
		Shop: [
			'shop/scenario/shop.buyphones.scn.js',
			'shop/scenario/shop.buypalns.tracfonecustomer.scn.js'
		]
	}
};
