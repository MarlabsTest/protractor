//this represents a modal for service provider details page
'use strict';
var ElementsUtil = require("../util/element.util");

var PADetails = function() {
	
	this.phoneTypeDropDown = element.all(by.css('[ng-click="$select.toggle($event)"]')).get(0);
	this.wirelessDropdown = element.all(by.className("dropdown-content-item ng-scope")).get(0);
	this.serviceProviderDropdown = element.all(by.css('[ng-click="$select.toggle($event)"]')).get(1)
	this.ATTDropdown = element.all(by.id("accessible_provider")).get(1);
	this.portzipBox = element.all(by.name('portzip')).get(1);
	this.serviceProviderContinueBtn = element.all(by.css('[ng-click="action()"]')).get(0);
	this.phoneAccountNumberBox = element.all(by.name('actnumber')).get(1);
	this.acountPswdBox = element.all(by.name('actpwd')).get(1);
	this.socialSecBox = element.all(by.name('user_pin')).get(1);
	this.firsNameBox = element.all(by.name('fname')).get(1);
	this.lastnameBox = element.all(by.name('lname')).get(1);
	this.phoneNoBox = element.all(by.name('phone')).get(1);
	this.addressBox = element.all(by.name('address1')).get(1);
	this.addresstwoBox = element.all(by.name('address2')).get(1);
	this.cityBox = element.all(by.name('city')).get(1);
	this.zipBox = element.all(by.name('acctzip')).get(1);
	//this.stateDropdown = element(by.model('data.acctstate')).$('[value="FL"]');
	
	//this.stateDropdown = element.all(by.css('[ng-click="$select.toggle($event)"]')).get(2);  //element(by.model('data.acctstate')).$('[value="FL"]');
	this.FLSelect = element.all(by.className("dropdown-content-item ng-scope")).get(9);
	
	this.stateDropdown = element.all(By.model('data.acctstate')).get(0)
	this.stateDropdown = element.all(by.css('[ng-click="$select.toggle($event)"]')).get(2);
	//this.stateDropdown = element.all(By.model('data.acctstate')).get(0).$('[value="CA"]');;
	//data.acctstate
	
	this.accountDetailsContinueBtn =  element.all(by.id('btn_continuetransfer')).get(2); //btn_continuetransfer
	this.keepThisAddressBtn =  element.all(by.css('[ng-click="action()"]')).get(3);
	this.unitNoBox = element.all(by.name('unitnumber')).get(1);
	this.streetBox = element.all(by.name('streetname')).get(1);
	this.houseNoBox = element.all(by.name('housenumber')).get(1);
	this.streetDirectionDropDown = element(by.model('data.direction')).$('[value="NE"]');
	this.streetTypeDropDown = element(by.model('data.street_type')).$('[value="AVE"]');
	this.addressContinueBtn = element.all(by.css('[ng-click="action()"]')).get(0);
	
	//** this method is check whether the page to enter service provider details is loaded or not
	this.ServiceProviderPageLoaded = function(){
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /collectserviceprovider/.test(url);
		});
	};
  
	//** this method is to enter the phone type
	this.selectPhoneType = function(){
		this.phoneTypeDropDown.click();
		this.wirelessDropdown.click();
		this.serviceProviderDropdown.click();
		this.ATTDropdown.click();
		this.portzipBox.clear().sendKeys("33178");
		//browser.wait(expectedConditions.visibilityOf(this.serviceProviderContinueBtn),40000);
		//ElementsUtil.waitForElement(this.serviceProviderContinueBtn);
		this.serviceProviderContinueBtn.click();
	};
	
	//** this method is to check whether the fields to enter phone account details is loaded or not
	this.accountDetailsLoaded = function(){
		//browser.wait(expectedConditions.visibilityOf(this.phoneAccountNumberBox), 10000);
		//ElementsUtil.waitForElement(this.phoneAccountNumberBox);
		return this.phoneAccountNumberBox.isPresent();
	};
    
	//** this method is to enter the phone account details
	this.enterPhoneAccountDetails = function(accountNum,pswd,socialSec,fname,lname,phnNo,addOne,addtwo,city,zip) {
		this.phoneAccountNumberBox.clear().sendKeys(accountNum);
		this.acountPswdBox.clear().sendKeys(pswd);
		//this.socialSecBox.clear().sendKeys(socialSec);
		this.firsNameBox.clear().sendKeys(fname);
		this.lastnameBox.clear().sendKeys(lname);
		this.phoneNoBox.clear().sendKeys(phnNo);
		this.addressBox.clear().sendKeys(addOne);
		this.addresstwoBox.clear().sendKeys(addtwo);
		this.cityBox.clear().sendKeys(city);
		this.zipBox.clear().sendKeys(zip);
		this.stateDropdown.click();
		//this.FLSelect.click();
		this.accountDetailsContinueBtn.click();
	};
  
	//** this method is to check whether the pop up for selecting address is loaded or not
	this.selectAddressPopUpLoaded = function(){
		//browser.wait(expectedConditions.visibilityOf(this.keepThisAddressBtn), 10000);
		//ElementsUtil.waitForElement(this.keepThisAddressBtn);
		return this.keepThisAddressBtn.isPresent();
	};
  
	//** this method is to check whether the page to enter address details is loaded or not
	this.keepThisAddress = function(){
		this.keepThisAddressBtn.click();
	};
	
	//** this method is to check whether the page to enter address details is loaded or not
	this.addressDetailsPageLoaded = function(){
		//browser.wait(expectedConditions.visibilityOf(this.unitNoBox), 10000);
		//ElementsUtil.waitForElement(this.unitNoBox);
		return this.unitNoBox.isPresent();
	};
  
	//** this method is to enter the address details
	this.addressDetails = function(unitNo,street,houseNo){
		//browser.wait(expectedConditions.visibilityOf(this.unitNoBox), 10000);
		//ElementsUtil.waitForElement(this.unitNoBox);
		this.unitNoBox.clear().sendKeys(unitNo);
		//browser.wait(expectedConditions.visibilityOf(this.streetBox), 10000);
		//ElementsUtil.waitForElement(this.streetBox);
		this.streetBox.clear().sendKeys(street);
		//browser.wait(expectedConditions.visibilityOf(this.houseNoBox), 10000);
		//ElementsUtil.waitForElement(this.houseNoBox);
		this.houseNoBox.clear().sendKeys(houseNo);
		this.streetDirectionDropDown.click();
		this.streetTypeDropDown.click();
		//browser.wait(expectedConditions.visibilityOf(this.addressContinueBtn), 10000);
		//ElementsUtil.waitForElement(this.addressContinueBtn);
		this.addressContinueBtn.click();
	};
  
  };
module.exports = new PADetails;

