//this represnts a modal for the summary page

'use strict';
var ElementsUtil = require("../util/element.util");

var summaryPge = function() {
	
	
	//this.summaryBtn = element.all(by.id('btn_done')).get(1);//element.all(by.id('withpin_donebtn')).get(1);
	this.summaryBtnPurchase =  element.all(by.css('[ng-click="action()"]')).get(1);
	this.doneBtn = element.all(by.css("button[id='btn_done']")).get(1);
 
 
	//** this method is to check whether the summary page is loaded or not
	this.summaryPageLoaded  = function(){		
		//$$//ElementsUtil.waitForElement(this.summaryBtn);
		return this.doneBtn.isPresent();		
	};
  
	//** this method is to proceed from the summary page
	this.clickOnSummaryBtn = function(){		
		//ElementsUtil.waitForElement(this.summaryBtn);
		this.doneBtn.click();		
	};
	
	this.summaryPageLoadedPurchase  = function(){
	//	browser.wait(expectedConditions.visibilityOf(this.summaryBtnPurchase),4000);
		//ElementsUtil.waitForElement(this.summaryBtnPurchase);
		return this.summaryBtnPurchase.isPresent();
	};
  
	this.clickOnSummaryBtnPurchase = function(){
		//browser.wait(expectedConditions.visibilityOf(this.summaryBtnPurchase),4000);
		//ElementsUtil.waitForElement(this.summaryBtnPurchase);
		this.summaryBtnPurchase.click();
	};
	
	this.clickOnRedemptionDoneBtn = function(){		
		//ElementsUtil.waitForElement(this.summaryBtn);
		this.doneBtn.click();		
	};
	
	this.reactivationSummaryPageLoaded  = function(){		
		//$$//ElementsUtil.waitForElement(this.doneBtn);
		return this.doneBtn.isPresent();		
	};
	
};
module.exports = new summaryPge;