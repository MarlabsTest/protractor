//this represnts a modal for the SIM page

'use strict';
var ElementsUtil = require("../util/element.util");

var sim = function() {
	
	this.identifySIMPage = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[2]/subheading/div/div/div[1]/table/tbody/tr[1]/td"));
	this.simErrorField = element(by.xpath("/html/body/tf-update-lang/div[1]/div[2]/div/div/div[2]/div/div/div[1]/div/div/div"));
	this.simTextBox = element.all(by.name('sim')).get(1);//element.all(by.xpath('//*[@id="sim"]')).get(1);//*[@id="sim"]
	this.simContinueBtn = element.all(by.id('btn_continuesimcardnumber')).get(0);
	//this.byopSimTextBox = element.all(by.id('number')).get(1);
	this.byopSimContinueBtn = element.all(by.id('btn_continuealreadyhave')).get(0);
	this.byopPurchasePinBtn = element.all(by.css('[ng-click="action()"]')).get(3);
	this.byopSimTextBox = element.all(by.name('sim')).get(1);
	this.simNotSureBtn = element.all(by.id('btn_continuenotsure')).get(1);
	this.cdmabyopSimContinueBtn = element.all(by.css('[ng-click="action()"]')).get(8);
	this.cdmaByopText = element.all(by.css("span[translate='ACRE_6736']")).get(0);
	
	this.isCdmaByopSimLoaded = function(){
		ElementsUtil.waitForElement(this.cdmaByopText);
		return this.cdmaByopText.isPresent();
	}
	
	this.enterCdmaByopSim = function(sim){
		this.simTextBox.clear().sendKeys(sim);
		this.cdmabyopSimContinueBtn.click();
		
	}
	
	this.selectNoSim = function(){
		this.simNotSureBtn.click();
	}

	this.enterByopSIM = function(simNum){
		this.byopSimTextBox.clear().sendKeys(simNum);
		this.byopSimContinueBtn.click();
	};
	
	this.selectByopPurchaseAirTime = function() {
		this.byopPurchasePinBtn.click();
	};
  
	this.isByopSIMPage = function(){
        return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /byopcollectsim/.test(url);
		});
	};
	
	//** method to check whether the SIM page is loaded
	this.isSIMPage = function(){
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /collecttfsim/.test(url);
		});
	};
	
	//** method to enter the SIM
	this.enterSIM = function(simNum){
		ElementsUtil.waitForElement(this.simTextBox);//for add device scenario
		this.simTextBox.clear().sendKeys(simNum);
		this.simContinueBtn.click();
	};
  
	//** method to display error after entering invalid SIM 
	this.isSIMError = function(){
		//browser.wait(expectedConditions.visibilityOf(this.simErrorField),10000);
		//ElementsUtil.waitForElement(this.simErrorField);
		return this.simErrorField.getText();
	};
	
};
module.exports = new sim;