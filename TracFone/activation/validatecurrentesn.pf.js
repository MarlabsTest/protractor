//this is the page fragment that validate the current device esn

'use strict';
var ElementsUtil = require("../util/element.util");
var validateEsn = function() {
		
	this.esnTextBox = element.all(by.id('esnsimmin')).get(1);
	//this.esnTextBox = element.all(by.id('sim')).get(1)
	//this.esnContinueBtn =  element.all(by.id('btn_continue')).get(1);//.get(2);
	this.esnContinueBtn = element.all(by.css('[ng-click="action()"]')).get(1);
	
	
	//** method to check whether the zipcode(new number)/porting flow page is loaded or not
	this.validateEsnLastNumbersPageLoaded = function(){
		ElementsUtil.waitForElement(this.esnTextBox);
		return browser.getCurrentUrl().then(function(url) {
			//console.log('url: ', url);
			return /activation!collectlastfourinvalidacct$/.test(url);
		});
	};
  
	//** this method is to check whether the validate last four esn number of the current device
	this.enterCurrentEsnLastFourDigits = function(number) {
		this.esnTextBox.sendKeys(number);
		this.esnContinueBtn.click();
	};
};
module.exports = new validateEsn;
