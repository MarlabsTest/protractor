//**this represents a modal for account creation

'use strict';    

var ElementUtil = require("../util/element.util");

var CreateAccount = function() {

	this.fbButtonText = element(by.buttonText('Sign up with Facebook'));
	this.emailTextBox= element.all(by.name('createacfm.user_email')).get(1);
	this.passwordBox=element.all(by.name('createacfm-user_pwd')).get(1);
	this.DOBBox=element.all(by.name('createacfm-DOB')).get(1);
	this.confirmPswdBox=element.all(by.name('createacfm-confirmuser_pwd')).get(1);
	this.secPinBox=element.all(by.id('createacfm-user_pin')).get(1);
	this.accountCreatedPopUp = element(by.xpath('//*[@id="modal-body"]/div[2]/form/div/div[1]/div[2]'));
	this.accountCreatedPopupContinue = element.all(by.css('button[id="btn_continuemyaccount"]')).get(1);
	//this.actualAccoutnCreatedPopup = this.accountCreatedPopupContinue.
	this.actCreationBtn = element.all(by.css('button[id="btn_createaccount"]')).get(1);
	this.accountCreationContinueBtn = element.all(by.css('[ng-click="action()"]'));//.get(3);
	//this.actualAccountCreationContinueBtn = this.actCreationBtn.get(1);
	//this.newAccountContinueBtn = element.all(by.id('btn_createnew'));
	this.actualAccountCreated = element.all(by.css('button[id="btn_createaccount"]')).get(3);//element(by.id('btn_createnew'));//this.newAccountContinueBtn.get(0);
	this.skipAccountLink = element(by.css("[ng-click='skipLogin()']"));
	this.loginBtn = element.all(by.css('button[id="btn_login"]')).get(1);
	this.email = element(by.css('input[id="email"]'));
	this.password = element(by.css('input[id="password"]'));
	this.continueLoginBtn = element.all(by.css('button[id="btn_login"]')).get(2);
	
	this.skipAccountLogin = function() {
	return this.skipAccountLink.click();
	};
	
	//** this method checks whether the account creation page is loaded or not
	this.activationAccountPageLoaded = function(){  
		//return ElementUtil.waitForUrlToChangeTo(/activation!keepcollectphone$/);
		return this.actCreationBtn.isPresent();
	};

	//** this method checks whether the account creation page is loaded or not while porting
	this.activationAccountPageLoadedForPort = function(){  
		//return ElementUtil.waitForUrlToChangeTo(/activation!keepcollectphone$/);
		return browser.getCurrentUrl().then(function(url){
			return /activation!keepcollectphone$$/.test(url);
		});
	};
  
	//** this method is to proceed with account creation flow
	this.clickonAccountCreationContinueBtn = function(){
//		browser.executeScript("arguments[0].click();", this.actualAccountCreationContinueBtn.getWebElement());			
		this.actCreationBtn.click();
		
	};
	
	//** this method checks whether the account details fields to be enetered  are loaded or not
	this.fbButtonLoaded = function(){
		return this.fbButtonText.isPresent();
	};
	
	//** this method is to enter the required fields to create new account
	this.enterAccountDetails= function(email,password,DOB,pin){
		//ElementsUtil.waitForElement(this.emailTextBox);
		this.emailTextBox.clear().sendKeys(email);
		//ElementsUtil.waitForElement(this.passwordBox);
		this.passwordBox.clear().sendKeys(password);
		//ElementsUtil.waitForElement(this.confirmPswdBox);
		this.confirmPswdBox.clear().sendKeys(password);
		//ElementUtil.waitForElement(this.DOBBox);
		this.DOBBox.clear().sendKeys(DOB);
		//ElementUtil.waitForElement(this.secPinBox);
		this.secPinBox.clear().sendKeys(pin);
//		this.actualAccountCreated.click();
		
		browser.executeScript("arguments[0].click();", this.actualAccountCreated.getWebElement());
		
	};
	
	//** this method is to check whether the popup page is loaded after successful account creation
	this.accountCreationDone = function(){
		//ElementUtil.elementHasCome(this.accountCreatedPopupContinue, 2);
		return this.accountCreatedPopupContinue.isPresent();
		
	};
	
	//** this method is to proceed from the popup page
	this.clickOnAccountCreatedPopupBtn = function(){
	//browser.wait(Waitutil.elementHasCome(this.accountCreatedPopupContinue, 2), 20000);
		//ElementUtil.elementHasCome(this.accountCreatedPopupContinue, 2);
		this.accountCreatedPopupContinue.click();
		
	};
	
	this.proceedWithExistingUser = function(){
		this.loginBtn.click();
	};
	
	this.enterUserDetails = function(email,password){
		this.email.sendKeys(email);
		this.password.sendKeys(password);
		this.continueLoginBtn.click();
	};
	
	
};
module.exports = new CreateAccount;