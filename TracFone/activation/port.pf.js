//this represents a modal for phoneNumber page(port)
'use strict';
var expectedConditions = protractor.ExpectedConditions;
var port = function() {
	this.keepNoYesBtn = element.all(by.id('btn_keepnumber')).get(1);
	this.KeepMyNumberBox = element.all(by.id('tfvaldevice-phonenumber')).get(1);
	this.keepMyNumContinueBtn =  element.all(by.id('btn_continuekeepsimnumber')).get(0);
	
   	this.enterMobNumber = function(number){
   	this.keepNoYesBtn.click();
	//$$//browser.wait(expectedConditions.visibilityOf(this.KeepMyNumberBox),10000);
	this.KeepMyNumberBox.clear().sendKeys(number);
	this.keepMyNumContinueBtn.click();
  
  };
  
};

module.exports = new port;