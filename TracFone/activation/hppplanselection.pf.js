//this represnts a modal for the survey page

'use strict';
var ElementsUtil = require("../util/element.util");
var sessionData = require("../common/sessiondata.do");
var CommonUtil= require("../util/common.functions.util");

var surveyPge = function() {

		
	this.hppPlans = element.all(By.xpath("//span[@translate='BAT_24873']")).get(6);
	this.hppEnrol24 =  element.all(by.css("span[aria-label='ADD $31.99']"));
	this.hppEnrol13 =  element.all(by.css("span[aria-label='ADD $24.99']"));
	this.ccv =  element.all(by.id('ccv')).get(1);
	this.placeOrder= element.all(by.id('btn_placeyourorder')).get(0);
	this.orderDone = element.all(by.id('btn_done')).get(1);
	
	//** this method is to check whether the survey page is loaded or not
	this.selectyourprogramPageLoaded = function(){
		//browser.wait(expectedConditions.visibilityOf(this.thankYouBtn),40000);
		ElementsUtil.waitForElement(this.hppPlans);
		return this.hppPlans.isPresent();
	};
  
	//** this method is to proceed from the survey page using the THANK YOU option
	this.chooseHppPlan = function(hppPlan){
		if(hppPlan == "24"){
			this.hppEnrol24.click();
		}
		else if(hppPlan == "13"){
			this.hppEnrol13.click();
		}
//		this.hppEnrol.click();		
	};
	
	//** this method is to proceed from the enroll page for checkout
	this.hppcheckout = function(){
		this.ccv.sendKeys(CommonUtil.getCvv(sessionData.tracfone.cardType));	
		this.placeOrder.click();
	};
	
	//** this method is to proceed from the hpp checkout page to order summary page
	this.orderSummary = function(){
		this.orderDone.click();
	};
	

 };
module.exports = new surveyPge;