'use strict';

var homePage = require("../../common/homepage.po");
var activate = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var dataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedPin ={};
var generatedMin ={};
var generatedSim = {};

describe('CDMA BYOP activation', function() {
	it('Go to activate option', function(done) {		
		//console.log('click the activate link started waiting for activation page');
		homePage.goToActivate();	
		expect(activate.isActivateLoaded()).toBe(true);
		done();
	});
	
	it('Choose device type as BYOP',  function(done) {
		//console.log("Choosing device type BYOP");
		activate.selectByopDeviceType();
		expect(activate.isTermsAndConditionsPopUpShown()).toBe(true);
		done();
	});
	
	it('Accept terms and conditions',  function(done) {
		//console.log("Accept terms and conditions");	
		activate.acceptTermsConditions();
		expect(activate.isByopSIMPage()).toBe(true);
		done();
	});
	
	it('select no sim',  function(done) {
		
		activate.selectNoSim();
		expect(activate.carrierLoaded()).toBe(true);
		done();
	});
	
	it('select verizon as carrier', function(done) {
		activate.selectVerizon();
		expect(activate.isByopEsnLoaded()).toBe(true);
		done();		
	});
	
	it('Enter ESN',function(done){
	var esnVal = dataUtil.getcdmaEsn();
	sessionData.tracfone.esn = esnVal;
	activate.enterCdmaByopEsn(esnVal);
	})
	
	it('update ESN', function(done) {
		var isActive = sessionData.tracfone.isActive;
		var isIPhone = sessionData.tracfone.isIPhone;
		var isLTE = sessionData.tracfone.isLTE;
		var zip = sessionData.tracfone.zip;
		var carrier = sessionData.tracfone.carrier;
		var isHD = sessionData.tracfone.isHD;
		generatedEsn['esn'] = sessionData.tracfone.esn;
		//console.log("isActive::"+isActive+" isIphone::"+isIPhone+" isLTE::"+isLTE+" carrier::"+carrier);
		dataUtil.updateCdmaEsn(sessionData.tracfone.esn,isActive,zip,isLTE,carrier,isIPhone,isHD);
		activate.enterCdmaByopEsn(sessionData.tracfone.esn);
		expect(activate.isCdmaByopSimLoaded()).toBe(true);
		done();
	}).result.data = generatedEsn;
	
	it('Enter Sim',  function(done) {
		var sim = "";
		if ("yes" == sessionData.tracfone.isIPhone || "YES" == sessionData.tracfone.isIPhone) {
		if("yes" == sessionData.tracfone.isTrio || "YES" == sessionData.tracfone.isTrio)
			sim = dataUtil.getSIM("TF256PSIMV9TD");
		else
			sim = dataUtil.getSIM("TF256PSIMV9ND");
		} else {
		if("yes" == sessionData.tracfone.isTrio || "YES" == sessionData.tracfone.isTrio)
			sim = dataUtil.getSIM("TF256PSIMV9TD");
		else
			sim = dataUtil.getSIM("TF256PSIMV9DD");
		}
		activate.enterCdmaByopSim(sim);
		sessionData.tracfone.sim = sim;
		generatedEsn['sim'] = sessionData.tracfone.sim;
		expect(activate.isActiveWithCarrierLoaded()).toBe(true);
		done();
	}).result.data = generatedSim;
	
	
	it('select not active',  function(done) {
		activate.selectNotActive();
		expect(activate.zipcodeDivLoaded()).toBe(true);
		done();
	})
	
	it('enter the zipcode  navigate to airtimeserviceplan page', function(done) {
		activate.chooseDontKeepMyNumber();
		activate.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it(' enter airtime pin navigate to account creation page', function(done) {
		var pinval = dataUtil.getPIN(sessionData.tracfone.pinPartNumber);
		activate.enterAirTimePin(pinval);
		sessionData.tracfone.pin = pinval;
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activate.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	it('select create account option', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	}); 
	
	it('enter account creation details and create the account', function(done) {
		var emailval = dataUtil.getEmail();
		var password = "tracfone";  
       	activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load the final instruction page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(activate.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('continue from final instruction page', function(done) {
		activate.finalInstructionProceed();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('Load activation summary page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('skip the activation survey and load the account dashboard', function(done) {
		activate.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		sessionData.tracfone.upgradeEsn = sessionData.tracfone.esn;//specific to phone upgrade
		//Update OTA pending
		dataUtil.clearOTA(sessionData.tracfone.esn);
		dataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_Activation_with_PIN');
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);		
		done();		
	}).result.data = generatedMin;
});
	