/*
 * **********Scenario is the consolidation of Upgrade  flows in TracFone ******** 
 * 
 * 1.Read the input data for upgrade from CSV
 * 2.Based on the partnumber and AR flag,decide which flow to proceed with.
 * 3.If the partnumber starts with 'PH' ,it will be a  BYOP upgrade.In that we are initially doing a BYOP activation process
 * 4.If the AR field value is 'Y' in CSV, we will proceed with Upgrade from ESN Active and Enrolled in AR .In this we are doing a single activation with enrollment with credit card purchase. 
 * 5.The third upgrade flow is the normal TracFone phone upgrade.  We will be doing activation with PIN flow.
 * 6.In all the above cases,After activating the device, will get the MIN number.
 * 7.After getting MIN, It's executes an phone upgrade spec with new part numbers and the same MIN.Finally, the older ESN gets upgraded with the new ESN with the same MIN number.
 */

'use strict';

var sessionData = require("../../common/sessiondata.do");
var CommonUtil =  require('../../util/common.functions.util');
var FlowUtil = require('../../util/flow.util');

describe('TracFone Upgrade' , function() {
	var esnPartNumber = sessionData.tracfone.esnPartNumber;
	var arFlag = sessionData.tracfone.arFlag;
	//console.log("TracFone Upgrade is executing..");
	if(CommonUtil.isByopEsn(esnPartNumber)){
		//console.log('BYOP upgrade');
		FlowUtil.run('TF_BYOP_UPGRADE');				
	}else{
		//console.log("normal phone upgrade flow");
		FlowUtil.run('TF_PHONE_UPGRADE');		
	}
});
