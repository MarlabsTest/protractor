'use strict';

var activate = require("../activation.po");
var homePage = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedPin ={};
var generatedMin ={};

describe('Tracfone CDMA Activation with Pin', function() {
	//var activationData = activationCdmaUtil.getTestData();
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	//require("../../common/spec/tracfone.loadhomepage.spec.js");
	//drive(activationData, function(inputActivationData) {
	
	it('Select ACTIVATE option', function(done) {		
		homePage.goToActivate();
		expect(activate.isActivateLoaded()).toBe(true);		
		done();
	});
	
	it('Select phone type as Tracfone Phone',function(done){		
		activate.gotToEsnPage();
		expect(activate.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('Enter ESN ', function(done) {
		var esnval = dataUtil.getESN(sessionData.tracfone.esnPartNumber);
		//console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activate.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activate.checkBoxCheck();
		activate.continueESNClick();
			
	
		var elem = element.all(by.name('sim')).get(1);
		elem.isPresent().then(function(isVisible) {
			if (isVisible) {
				expect(activate.isSIMPage()).toBe(true);
				done();
				
				var simval = dataUtil.getSIM(sessionData.tracfone.simPartNumber);
				sessionData.tracfone.sim = simval;
				//generatedSim['sim'] = sessionData.tracfone.sim;
				activate.enterSIM(simval);
				expect(activate.keepMyPhonePageLoaded()).toBe(true);
				done();	
			}
		
			else{
				expect(activate.keepMyPhonePageLoaded()).toBe(true);
				done();
			}
			});		
	}).result.data = generatedEsn;
	
	it('enter the zipcode and load airtimeserviceplan page', function(done) {
		activate.keepCurrentPhnNum();
		activate.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('enter airtime Pin and proceed', function(done) {
		var pinval = dataUtil.getPIN(sessionData.tracfone.pinPartNumber);
		//console.log('returns', pinval);
		sessionData.tracfone.pin = pinval;
		activate.enterAirTimePin(pinval);
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activate.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	it('Select Create Account option', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('enter account creation details and create the account', function(done) {
		var emailval = dataUtil.getEmail();
		var password = "tracfone"; 
       	//console.log('returns', emailval);
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load the final instructions page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(activate.finalInstructionPageLoaded()).toBe(true);
		done();		
	}); 
	
	it('load the Summary page', function(done) {
		activate.finalInstructionProceed();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('load the survey page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it('skip the survey and load the Account dashboard', function(done) {
		activate.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.tracfone.esn);
		//console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		//console.log("MIN is  :::::" +min);
		//Update OTA pending
		dataUtil.clearOTA(sessionData.tracfone.esn);
		//check_activation		
		dataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_CDMA_Activation_with_PIN');
		generatedMin['min'] = min;
		//console.log("ITQ table updated");
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	//}); 
});