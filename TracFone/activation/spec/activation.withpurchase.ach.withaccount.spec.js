'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
// newly added for data integration
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var generatedEsn ={};
var generatedSim ={};

describe('Tracfone Activate Phone By Purchasing Plan', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);
		done();
	});  
	
	it('click on I have a family phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('enter ESN, click on continue and enter security pin in the popup', function(done) {
		var esnval = DataUtil.getESN(sessionData.tracfone.esnPartNumber);
	   	//console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activation.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();
	}).result.data = generatedEsn;
	
	it('enter sim number and navigate phone number page', function(done) {
		var simval = DataUtil.getSIM(sessionData.tracfone.simPartNumber);
	   	//console.log('returns', simval);
		activation.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();
	}).result.data = generatedSim;
	
	it(' enter zipcode navigate to airtimeserviceplan page', function(done) {
		activation.chooseDontKeepMyNumber();
		activation.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();
	});
	
	it('select purchase plan  and navigate to account creation page', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();
	});
	
	it('select smartphone plan', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.tracfone.shopPlan;
		var planName		= sessionData.tracfone.planName;
		var isAutoRefill	= sessionData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();
	});
	
	//NOTE:: uncomment if value added plans modal shown after registering auto refill
	/*
	it('should do checkout without choosing the value added plans', function(done) {
		shop.doCheckOut();
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();
	});
	*/
	
	//activation!keepcollectphone
	it('click  create account navigate to accoutn creation page', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.fbButtonLoaded()).toBe(true);
		done();
	});
	
	it('enter the account details ,click done and succes popup  appears', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
		//console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();
	});
	
	it('click on continue  in the popup ', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
	it('click on continue to payment  and should be asked to fill cc details', function(done) {
		//myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();
	});
	
	it('Enter ACH information', function(done) {
		myAccount.clickOnBankAccTab();
		myAccount.enterAchDetails("4102","121042882");
		// myAccount.enterAchBillingDetails("Test","Test","1295 Charleston Road","12345", "Miami", "33178");
		myAccount.enterBillingDetails("Test", "Test", "1295 Charleston Road", "12345", "Mountain View", "94043");
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();
	});
	
	it('click on the continue button in the final instruction page navigate to Succcess page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();
	});
	
	it('click on the done button in the summary page  navigate to survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();
	});
	
	it('click on the thank you button and navigate to the account dashboard ', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.tracfone.esn);
		var min = DataUtil.getMinOfESN(sessionData.tracfone.esn);
		//Update OTA pending
		DataUtil.clearOTA(sessionData.tracfone.esn);
		expect(myAccount.isLoaded()).toBe(true);
		done();
	});
});
