'use strict';

var myAccount = require("../../myaccount/myaccount.po");
var activation = require("../activation.po");
//newly added for data integration
var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var home = require("../../common/homepage.po");
var shop = require("../../shop/shop.po");
var CommonUtil= require("../../util/common.functions.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var generatedMin ={};

describe('Tracfone Reactivate a device with purchase', function() {
	it(' deactivate the device and navigate to account dashboard',function(done){
		var min = DataUtil.getMinOfESN(sessionData.tracfone.esn);
		DataUtil.deactivatePhone('DEACTIVATE', sessionData.tracfone.esn,min,'PAST_DUE','TRACFONE');
		generatedMin['min'] = min;
		home.homePageLoad();
		home.myAccount();
		DataUtil.changeServiceEndDate(sessionData.tracfone.esn);
		expect(myAccount.isLoaded()).toBe(true);
		done();
	}).result.data = generatedMin;
	
	it(' click on the activate and navigate to the zipcode page', function(done) {		
		myAccount.clickOnBuyServicePlan();
		expect(activation.servicePlanPageLoaded()).toBe(true);	
		done();
	});	
		
	it('select smartphone plan', function(done) {
		var planType		= sessionData.tracfone.shopPlan;
		var planName		= sessionData.tracfone.planName;
		var isAutoRefill	= sessionData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);		
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on continue to payment  and should be asked to fill cc details', function(done) {
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	
	
	it('click  continue to payment and navigate to payment page', function(done){
		myAccount.continueToPayment();
		expect(activation.paymentOptionLoaded()).toBe(true);
		done();
	});
	
	it('fill payment details and perform the payment', function(done){
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);		
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();
	});
	
	it('click on the continue button and navigated to summary page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('click on the done button navigate to survey page', function(done) {
		activation.clickOnSummaryBtn();		
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 	
});
