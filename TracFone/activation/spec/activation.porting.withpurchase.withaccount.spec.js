'use strict';


var activate = require("../activation.po");
var homePage = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var dataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var shop = require("../../shop/shop.po");
var sessionData = require("../../common/sessiondata.do");
var CommonUtil= require("../../util/common.functions.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};




describe('Tracfone4.0 Activation Porting With purchase', function() {
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	it('select ACTIVATE option from home page', function(done) {		
		homePage.goToActivate();
		expect(activate.isActivateLoaded()).toBe(true);		
		done();
	});
	
	it('select device type as Tracfone Phone',function(done){		
		activate.gotToEsnPage();
		expect(activate.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('enter ESN and continue', function(done) {
		var esnval = dataUtil.getESN(sessionData.tracfone.esnPartNumber);
		activate.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activate.checkBoxCheck();
		activate.continueESNClick();
		expect(activate.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('enter SIM number and continue', function(done) {
		var simval = dataUtil.getSIM(sessionData.tracfone.simPartNumber);
		activate.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		expect(activate.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('enter MIN for porting', function(done) {
//		var min = "9002345643";
		var min = Math.floor((Math.random() * (9999999999-9000000000)) + 9000000000);
		console.log("Random MIN ::",min);
		sessionData.tracfone.min = min;
		activate.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activate.ServiceProviderPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	it('select the phonetype and load account details page', function(done) {
		activate.selectPhoneType();
		expect(activate.accountDetailsLoaded()).toBe(true);
		done();		
	});
	
	it('enter phone account details', function(done) {
		activate.enterPhoneAccountDetails("123456789","1234","3559","Ginny","Potter","9809806751","San Juan De Trelles","Asturias","WHITES CREEK","33178");
		expect(activate.selectAddressPopUpLoaded()).toBe(true);
		done();		
	});
	
	it('select keep this address option and load address details page', function(done) {
		activate.keepThisAddress();
		expect(activate.addressDetailsPageLoaded()).toBe(true);
		done();		
	});
	
	it('enter address details and continue', function(done) {
		activate.addressDetails("40","76th","14532");
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('select purchase plan and load account selection page', function(done) {
		activate.airtimePurchase();
		expect(activate.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});

	it('select the plan and proceed to purchase', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.tracfone.shopPlan;
		var planName		= sessionData.tracfone.planName;
		var isAutoRefill	= sessionData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		expect(activate.activationAccountPageLoaded()).toBe(true);
		done();
	});
	
	//activation!keepcollectphone
	it('select create account option and  load account creation page', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('enter account creation details and create the account', function(done) {
		var emailval = dataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
       	//console.log('returns', emailval);
		activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		// Newly added by gopi
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load checkout page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
		it('continue to payment option', function(done) {
		//myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	it('enter CC details and proceed to final indtructions page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activate.finalInstructionPageLoadedPortInPurchase()).toBe(true);
		done();		
	}); 
	
	it('proceed from final instruction page and load summary page', function(done) {
		activate.finalInstructionProceedPortInPurchase();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('proceed from summary page and load survey page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it('skip the survey and load account dashboard page', function(done) {
		activate.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.tracfone.esn);
		//console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		//console.log("MIN is  :::::" +min);
		//Update OTA pending
		dataUtil.clearOTA(sessionData.tracfone.esn);
		//check_activation		
		dataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_Activation_Porting_with_PIN');
		//console.log("ITQ table updated");		
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 
	//}); 
	/*
	it('should complete payment checkout', function(done) {
		shop.completeCheckout(); 
		expect(shop.isCheckoutSuccess()).toBe(true);
		done();
	});
	
	it('Checking Order Summary and skip servey', function(done) {
		shop.completeSummaryProcess(); 
		expect(shop.isServeyCompleted()).toBe(true);
		done();
	});
	 */
	//}); 
});
