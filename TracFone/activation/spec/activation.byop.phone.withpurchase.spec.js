'use strict';

var homePage = require("../../common/homepage.po");
var activate = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
var shop = require("../../shop/shop.po");
//newly added for data integration
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var generatedEsn ={};
var generatedPin ={};
var generatedMin ={};

describe('BYOP activation with purchase', function() {
	it('select ACTIVATE option ', function(done) {		
		//console.log('click the activate link started waiting for activation page');
		console.log("sessionData.tracfone.servicePlan :: "+sessionData.tracfone.servicePlan);
		homePage.goToActivate();	
		expect(activate.isActivateLoaded()).toBe(true);
		done();
	});
	
	it('Choose BYOP device  ',  function(done) {
		//console.log("Choosing device type BYOP");
		activate.selectByopDeviceType();
		expect(activate.isTermsAndConditionsPopUpShown()).toBe(true);
		done();
	});
	
	it('Accept terms and conditions',  function(done) {
		//console.log("Accept terms and conditions");	
		activate.acceptTermsConditions();
		expect(activate.isByopSIMPage()).toBe(true);
		done();
	});
	
	it('Provide SIM number',  function(done) {
		var simval = DataUtil.getByopSim(sessionData.tracfone.esnPartNumber, sessionData.tracfone.simPartNumber);
		activate.enterByopSim(simval);
		sessionData.tracfone.sim = simval;
		var esnVal = CommonUtil.getLastDigits(simval, 15);//to get dummy esn from sim number
		sessionData.tracfone.esn = esnVal;
		generatedEsn['esn'] = sessionData.tracfone.esn;
		generatedEsn['sim'] = sessionData.tracfone.sim;
		expect(activate.keepMyPhonePageLoaded()).toBe(true);
		done();
	}).result.data = generatedEsn;
	
	it('enter  zipcode and load airtimeserviceplan page', function(done) {
		activate.chooseDontKeepMyNumber();
		activate.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('select purchase plan option and load Service Plan page', function(done) {
		activate.airtimePurchase();
		expect(activate.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});


	it('select plan and choose autorefill option if required', function(done) {
		//shop.choosePlan("PAYASGO"); 
		var planType = sessionData.tracfone.planType;
		var planName = sessionData.tracfone.planName;
		var isAutoRefill = sessionData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		expect(activate.activationAccountPageLoaded()).toBe(true);
		done();
	});
	
	it('select Create Account option', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	});

	it('enter account creation details and create the account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
	   	//console.log('returns', emailval);
	   	activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load Checkout page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
		it('continue to payment option', function(done) {
		//myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	it('enter CC details and load the final instruction page on payment', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activate.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	it('load the summary page', function(done) {
		activate.finalInstructionProceedPurchase();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('Proceed from Summary page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	it('skip the activation survey and load the Account dashboard', function(done) {
		activate.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.tracfone.esn);
		var min = DataUtil.getMinOfESN(sessionData.tracfone.esn);
		generatedMin['min'] = min;
		//Update OTA pending
		DataUtil.clearOTA(sessionData.tracfone.esn);
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin; 
});
	
