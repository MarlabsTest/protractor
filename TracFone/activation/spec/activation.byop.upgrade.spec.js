'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
// newly added for data integration

//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
//var activationUtil = require('../../util/activation.util');

// Newly added by gopi
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};

describe('Tracfone BYOP Upgrade', function() {
	
	it('Select ACTIVATE option', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it('Select device type',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('provide ESN and continue', function(done) {
		var esnval = DataUtil.getESN(sessionData.tracfone.toEsnPartNumber);
       	console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activation.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it('provide SIM and go with option Keep My Phone', function(done) {
		var simval = DataUtil.getSIM(sessionData.tracfone.toSimPartNumber);
       	console.log('returns', simval);
		activation.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('enter mobile number and continue', function(done) {
		console.log("sessionData.tracfone.upgradeEsn :: "+sessionData.tracfone.upgradeEsn);
		var minVal = DataUtil.getMinOfESN(sessionData.tracfone.upgradeEsn);
		console.log("minVal :: "+minVal);
		activation.enterMobNumber(minVal);
		generatedMin['min'] = minVal;
		expect(activation.validateEsnLastNumbersPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	
	it('Provide four digit authentication code', function(done) {
		console.log("sessionData.tracfone.upgradeEsn :: "+sessionData.tracfone.upgradeEsn);
		console.log("CommonUtil.getLastDigits(sessionData.tracfone.upgradeEsn, 4) :: "+CommonUtil.getLastDigits(sessionData.tracfone.upgradeEsn, 4));
		activation.enterCurrentEsnLastFourDigits(CommonUtil.getLastDigits(sessionData.tracfone.upgradeEsn, 4));//last four digits of the current esn
		expect(activation.isPhoneUpgradeRequestPageLoaded()).toBe(true);
		done();		
	});
	
	it('Continue to phone upgrade request page', function(done) {
		activation.continuePhoneUpgradeRequestPage();
		expect(activation.isPhoneUpgradeRequestPageLoaded()).toBe(true);
		done();		
	});
	
	it('proceed from summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('skip the activation survey and load the Account dashboard', function(done) {
		activation.clickOnThankYouBtn();			
		expect(home.isHomePageLoaded()).toBe(true);		
		done();		
	});
});
