'use strict';


var activate = require("../activation.po");
var homePage = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var dataUtil= require("../../util/datautils.util");
var commonUtil = require("../../util/common.functions.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};

describe('InternalPorting with PIN with Account', function() {
    var esnToBePort = "";
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	it('select ACTIVATE option from home page', function(done) {		
		homePage.goToActivate();
		expect(activate.isActivateLoaded()).toBe(true);		
		done();
	});
	
	it('select device type as Tracfone phone',function(done){		
		activate.gotToEsnPage();
		expect(activate.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('enter ESN and continue', function(done) {
		var esnval = dataUtil.getESN(sessionData.tracfone.esnPartNumber);
		//console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activate.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activate.checkBoxCheck();
		activate.continueESNClick();
		expect(activate.isSIMPage()).toBe(true);
		done();	
	
	}).result.data = generatedEsn;
		
	it('enter SIM number and continue', function(done) {
		var simval = dataUtil.getSIM(sessionData.tracfone.simPartNumber);
		//console.log('returns', simval);
		sessionData.tracfone.sim = simval;
		activate.enterSIM(simval);
		generatedSim['sim'] = sessionData.tracfone.sim;
		expect(activate.keepMyPhonePageLoaded()).toBe(true);
		done();		
    }).result.data = generatedSim;
    
    it('enter mobile number for porting and continue', function(done) {
        esnToBePort = dataUtil.getActiveESNFromPartNumber(sessionData.tracfone.oldPartNumber);
		console.log(esnToBePort);
		var min = dataUtil.getMinOfESN(esnToBePort);
		console.log(min);		
		activate.enterMobNumber(min);
		generatedMin['min'] = min;
		expect(activate.validateEsnLastNumbersPageLoaded()).toBe(true);
		done();		
    }).result.data = generatedMin;

    it('Provide four digit authentication code', function(done) {
		activate.enterCurrentEsnLastFourDigits(commonUtil.getLastDigits(esnToBePort, 4));//last four digits of the current esn
		expect(activate.servicePlanPageLoadedForPort()).toBe(true);
		done();		
    });
    
    it('enter airtime Pin and redirect to account selection page', function(done) {
		var pinval = dataUtil.getPIN(sessionData.tracfone.pinPartNumber);
		console.log('returns', pinval);		
		sessionData.tracfone.pin = pinval;
		activate.enterAirTimePin(pinval);
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activate.activationAccountPageLoadedForPort()).toBe(true);
		done();		
	}).result.data = generatedPin;

	it('select Create Account option', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('enter account details and create the account', function(done) {
		var emailval = dataUtil.getEmail();
		var password = "tracfone"; 
       	//console.log('returns', emailval);
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load final indtructions page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(activate.finalInstructionPageLoadedPortInPin()).toBe(true);
		done();		
	}); 
	
	it('proceed from final instruction page and load summary page', function(done) {
		activate.finalInstructionProceedPortInPin();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('proceed from summary page and load survey page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it('skip the survey and load account dashboard page', function(done) {
		activate.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.tracfone.esn);
		//console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		//console.log("MIN is  :::::" +min);
		//Update OTA pending
		dataUtil.clearOTA(sessionData.tracfone.esn);
		//check_activation		
		dataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_Activation_Porting_with_PIN');
		//console.log("ITQ table updated");		
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 
	//}); 
});

   
    