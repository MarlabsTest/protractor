'use strict';

var myAccount = require("../../myaccount/myaccount.po");
var activation = require("../activation.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var home = require("../../common/homepage.po");
var generatedPin ={};
var generatedMin ={};

describe('Tracfone Reactivate a device with PIN', function() {
	
	/*Deactivate device
	 *Expected result - Device will be listed under inactive devices section in dashboard  
	 */
	
	it('deactivate the device and navigate to account dashboard',function(done){
		var min = DataUtil.getMinOfESN(sessionData.tracfone.esn);		
		DataUtil.deactivatePhone("DEACTIVATE", sessionData.tracfone.esn, min, 'PAST_DUE', 'TRACFONE');
		home.homePageLoad();
		home.myAccount();
		DataUtil.changeServiceEndDate(sessionData.tracfone.esn);
		expect(myAccount.isLoaded()).toBe(true);
		done();
	});
	
	
	it('should navigate to page for entering zipcode', function(done) {
		myAccount.clickAddAirtime();		
		expect(myAccount.enterAirtimePinPageLoaded()).toBe(true);	
		done();
	});	
	
	
	it('should navigate to page for entering zipcode', function(done) {
		var pinval = DataUtil.getPIN(sessionData.tracfone.pinPartNumber);
       	console.log('returns', pinval);
		sessionData.tracfone.pin = pinval;
		myAccount.enterAirtimePinRenew(pinval);	
		myAccount.proceedFromAirTimePage();		
		expect(myAccount.redemptionConfirmationPageLoaded()).toBe(true);	
		done();
	});	
	
	
	it('should navigate to page for entering zipcode', function(done) {			
		myAccount.clickOnContinueFromConfirmation();
		expect(myAccount.isLoaded()).toBe(true);	
		done();
	});	
	
});
