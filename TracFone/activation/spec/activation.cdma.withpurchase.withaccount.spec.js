'use strict';

var activate = require("../activation.po");
var homePage = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var dataUtil= require("../../util/datautils.util");
//var activationCdmaUtil = require('../../util/cdmaactivation.util');
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var CommonUtil= require("../../util/common.functions.util");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var generatedEsn ={};
var generatedMin ={};

describe('Tracfone CDMA Activation With Purchase Plan ', function() {
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	it('select ACTIVATE option from home page', function(done) {		
		homePage.goToActivate();
		expect(activate.isActivateLoaded()).toBe(true);		
		done();
	});
	
	it('select device type as Tracfone phone',function(done){		
		activate.gotToEsnPage();
		expect(activate.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('Enter ESN and continue', function(done) {
		var esnval = dataUtil.getESN(sessionData.tracfone.esnPartNumber);
		//console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activate.enterEsn(esnval);
        generatedEsn['esn'] = sessionData.tracfone.esn;
		activate.checkBoxCheck();
		activate.continueESNClick();
		var elem = element.all(by.name('sim')).get(1);
		elem.isPresent().then(function(isVisible) {
			if (isVisible) {
				expect(activate.isSIMPage()).toBe(true);
				done();
				
				var simval = dataUtil.getSIM(sessionData.tracfone.simPartNumber);
				sessionData.tracfone.sim = simval;
				//generatedSim['sim'] = sessionData.tracfone.sim;
				activate.enterSIM(simval);
				expect(activate.keepMyPhonePageLoaded()).toBe(true);
				done();	
			}
		
			else{
				expect(activate.keepMyPhonePageLoaded()).toBe(true);
				done();
			}
			});
	}).result.data = generatedEsn;	
	
	it('enter the zipcode and load the airtimeserviceplan page', function(done) {
		activate.keepCurrentPhnNum();
		activate.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('select purchase plan option and load the plan selection page', function(done) {
		activate.airtimePurchase();
		expect(activate.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('Choose plan and proceed to purchase', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.tracfone.shopPlan;
		var planName		= sessionData.tracfone.planName;
		var isAutoRefill	= sessionData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		expect(activate.activationAccountPageLoaded()).toBe(true);
		done();
	});	
	
	//activation!keepcollectphone
	it('Select Create Account option', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('enter account creation details and create the account', function(done) {
		var emailval = dataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
       	//console.log('returns', emailval);
		activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		// Newly added by gopi
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load the checkout page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
		it('continue to payment option', function(done) {
		//myAccount.continueToPayment();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	it('enter CC details and load final instruction page on payment', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activate.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	it('proceed from final instruction page and load the summary page', function(done) {
		activate.finalInstructionProceedPurchase();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('proceed from summary page and load the survey page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it('skip the survey and load the account dashboard page', function(done) {
		activate.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.tracfone.esn);
		//console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		//console.log("MIN is  :::::" +min);
		//Update OTA pending
		dataUtil.clearOTA(sessionData.tracfone.esn);
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
	//}); 
});
