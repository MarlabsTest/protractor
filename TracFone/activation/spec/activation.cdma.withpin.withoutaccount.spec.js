'use strict';


var activate = require("../activation.po");
var homePage = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedPin ={};
var generatedMin ={};

describe('Tracfone CDMA Activation with Pin without account', function() {
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	it('select ACTIVATE option', function(done) {		
		homePage.goToActivate();
		expect(activate.isActivateLoaded()).toBe(true);		
		done();
	});
	
	it('select devicetype as Tracfone phone',function(done){		
		activate.gotToEsnPage();
		expect(activate.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('Enter ESN and continue', function(done) {
		var esnval = dataUtil.getESN(sessionData.tracfone.esnPartNumber);
		//console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activate.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activate.checkBoxCheck();
		activate.continueESNClick();
		var elem = element.all(by.name('sim')).get(1);
		elem.isPresent().then(function(isVisible) {
			if (isVisible) {
				expect(activate.isSIMPage()).toBe(true);
				done();
				
				var simval = dataUtil.getSIM(sessionData.tracfone.simPartNumber);
				sessionData.tracfone.sim = simval;
				//generatedSim['sim'] = sessionData.tracfone.sim;
				activate.enterSIM(simval);
				expect(activate.keepMyPhonePageLoaded()).toBe(true);
				done();	
			}
		
			else{
				expect(activate.keepMyPhonePageLoaded()).toBe(true);
				done();
			}
			});
	}).result.data = generatedEsn;	
	
	it('enter zip code to get new phone number', function(done) {
		activate.keepCurrentPhnNum();
		activate.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('enter airtime Pin and continue', function(done) {
		var pinval = dataUtil.getPIN(sessionData.tracfone.pinPartNumber);
		//console.log('returns', pinval);
		sessionData.tracfone.pin = pinval;
		activate.enterAirTimePin(pinval);
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activate.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	it('skip account creation and load the final instruction page', function(done) {
		activate.skipAccountLogin();
		expect(activate.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
		
	it('proceed from final instruction  page and load summary page', function(done) {
		activate.finalInstructionProceed();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('proceed form summary page and load the survey page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it('skip the survey and load the account dashboard page', function(done) {
		activate.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.tracfone.esn);
		//console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		//console.log("MIN is  :::::" +min);
		//Update OTA pending
		dataUtil.clearOTA(sessionData.tracfone.esn);
		//check_activation		
		dataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_CDMA_Activation_with_PIN');
		generatedMin['min'] = min;
		//console.log("ITQ table updated");
		//expect(myAccount.isLoaded()).toBe(true);
		expect(homePage.isHomePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
});