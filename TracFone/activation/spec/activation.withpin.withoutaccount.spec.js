'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};
var generatedPin = {};

describe('Tracfone Activation GSM with PIN without account', function() {
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it('click on  I have a family phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('enter ESN ,click on continue and enter security pin in the popup', function(done) {
		var esnval = DataUtil.getESN(sessionData.tracfone.esnPartNumber);
       	//console.log('returns', esnval);
		// var simval = DataUtil.getSIM(inputActivationData.SIM);
       	// console.log('returns', simval);
		// db call for EN-SIM marry
		// DataUtil.addSimToEsn(esnval,simval);
		// console.log("DB updated !!");
		sessionData.tracfone.esn = esnval;
		// activation.enterMarriedSim(simval);
		// sessionData.wfm.sim = simval;
		activation.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	/*
	 * it('should navigate to page for entering the SIM number after entering 4
	 * digit PIN', function(done) { activation.enterPin("1234");
	 * activation.clickOnPinContinue();
	 * expect(activation.isSIMPage()).toBe(true);
	 * //expect(activation.keepMyPhonePageLoaded()).toBe(true); done(); });
	 */
	it(' enter sim number and navigate phone number page', function(done) {
		var simval = DataUtil.getSIM(sessionData.tracfone.simPartNumber);
       	//console.log('returns', simval);
		activation.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('enter the zipcode  navigate to airtimeserviceplan page', function(done) {
		activation.chooseDontKeepMyNumber();
		activation.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*
	it('should enter the zipcode and will be navigated to airtimeserviceplan page', function(done) {
		activation.enterZipCode(sessionData.tracfone.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	*/
	
	it(' enter airtime pin navigate to account creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.tracfone.pinPartNumber);
       	//console.log('returns', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.tracfone.pin = pinval;
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	it(' skip account creation', function(done) {
		activation.skipAccountLogin();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it(' click on the continue  in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it(' click on the done button in the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('click on the thank you button navigate to the account dashboard ', function(done) {
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.tracfone.esn);
		//console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = DataUtil.getMinOfESN(sessionData.tracfone.esn);
		//console.log("MIN is  :::::" +min);
		//Update OTA pending
		DataUtil.clearOTA(sessionData.tracfone.esn);
		//check_activation		
		DataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_Activation_with_PIN');
		generatedMin['min'] = min;
		//console.log("ITQ table updated");			
		//expect(home.homePageLoad()).toBe(true);
		expect(home.isHomePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedMin;
});
