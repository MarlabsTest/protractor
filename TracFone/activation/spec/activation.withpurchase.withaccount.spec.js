'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var shop = require("../../shop/shop.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
var generatedEsn ={};
var generatedSim ={};

describe('Tracfone Activation with Purchase with Account', function() {
	beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });
	
	it('click on activate and navigate to activation page', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it('click on I have a family phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('enter ESN ,click on continue and enter security pin in the popup', function(done) {
		var esnval = DataUtil.getESN(sessionData.tracfone.esnPartNumber);
       	//console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activation.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	it(' enter sim number and navigate phone number page', function(done) {
		var simval = DataUtil.getSIM(sessionData.tracfone.simPartNumber);
       	//console.log('returns', simval);
		activation.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it(' enter the zipcode and navigate to airtimeserviceplan page', function(done) {
	    activation.keepCurrentPhnNum();
		activation.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});

	it(' select purchase plan ', function(done) {
		activation.airtimePurchase();
		expect(activation.selectServicePlanPageLoaded()).toBe(true);
		done();		
	});

	it('choose  PAYASGO plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.tracfone.shopPlan;
		var planName		= sessionData.tracfone.planName;
		var isAutoRefill	= sessionData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();
	});
	
	//activation!keepcollectphone
	it(' select create account ', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('enter the account details ,click done and succes popup  appears', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
       	//console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		// browser.sleep(1000);
		// Newly added by gopi
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	it(' click on the continue  in the popup page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
		it(' click on continue to payment button and should be asked to fill cc details', function(done) {
		//myAccount.continueToPayment();
		//	myAccount.goToPaymentTab();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	it(' enter cc and billing address details,click on proceed and navigate to final indtructions page', function(done) {
		var cardType =sessionData.tracfone.cardType
		var isAutoRefill	= sessionData.tracfone.autoRefill;
		if(cardType == "ach" && (isAutoRefill == "Y" || isAutoRefill == "YES" )){
			myAccount.clickOnBankAccTab();
			myAccount.enterAchDetails("4102","121042882");
			myAccount.enterAchBillingDetails("Test","Test","1295 Charleston Road","12345", "Miami", "33178");
		}
		else{
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		}
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	it(' click on the continue button  and  navigate to Succcess page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it(' click on the done button  navigate to survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it(' click on the thank you button and navigate to the account dashboard ', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 
});
