'use strict';

var homePage = require("../../common/homepage.po");
var activate = require("../activation.po");
var myAccount = require("../../myaccount/myaccount.po");
//newly added for data integration
var DataUtil= require("../../util/datautils.util");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedPin ={};
var generatedMin ={};

describe('BYOP activation', function() {
	it('Go to activate option', function(done) {		
		//console.log('click the activate link started waiting for activation page');
		homePage.goToActivate();	
		expect(activate.isActivateLoaded()).toBe(true);
		done();
	});
	
	it('Choose device type as BYOP',  function(done) {
		//console.log("Choosing device type BYOP");
		activate.selectByopDeviceType();
		expect(activate.isTermsAndConditionsPopUpShown()).toBe(true);
		done();
	});
	
	it('Accept terms and conditions',  function(done) {
		//console.log("Accept terms and conditions");	
		activate.acceptTermsConditions();
		expect(activate.isByopSIMPage()).toBe(true);
		done();
	});
	
	it('Provide SIM number',  function(done) {
		//console.log("Provide SIM number");
		//console.log("sessionData.tracfone.esnPartNumber :: "+sessionData.tracfone.esnPartNumber);
		//console.log("sessionData.tracfone.simPartNumber :: "+sessionData.tracfone.simPartNumber);
		var simval = DataUtil.getByopSim(sessionData.tracfone.esnPartNumber, sessionData.tracfone.simPartNumber);
		//console.log("simval :: "+simval);
		activate.enterByopSim(simval);
		sessionData.tracfone.sim = simval;
		var esnVal = CommonUtil.getLastDigits(simval, 15);//to get dummy esn from sim number
		sessionData.tracfone.esn = esnVal;
		generatedEsn['esn'] = sessionData.tracfone.esn;
		sessionData.tracfone.upgradeEsn = esnVal;
		generatedEsn['sim'] = sessionData.tracfone.sim;
		//activate.registerSimNumber('8901260642101931132');
		expect(activate.keepMyPhonePageLoaded()).toBe(true);
		done();
	}).result.data = generatedEsn;
	
	it('enter the zipcode and load airtimeserviceplan page', function(done) {
		activate.chooseDontKeepMyNumber();
		activate.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*
	it('Provide zipcode',  function(done) {
		console.log("Provide zipcode");
		//activate.registerZipcode('33178');
		activate.enterZipCode(sessionData.tracfone.zip);
		sessionData.tracfone.zip = sessionData.tracfone.zip;
		//expect(activate.isAirtimeServicePlanPageLoaded()).toBe(true);
		expect(activate.servicePlanPageLoaded()).toBe(true);
		done();
	});
	*/
	
	it('Provide service plan pin',  function(done) {
		//console.log("Provide service plan pin");
		var pinval = DataUtil.getPIN(sessionData.tracfone.pinPartNumber);
		activate.enterAirTimePin(pinval);
		sessionData.tracfone.pin = pinval;
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activate.activationAccountPageLoaded()).toBe(true);
		done();
	}).result.data = generatedPin;
	
	it('select create account option', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('enter account creation details and create the account', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
       	//console.log('returns', emailval);
       	activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		// Newly added by gopi
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load the final instruction page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(activate.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('continue from final instruction page', function(done) {
		activate.finalInstructionProceed();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('Load activation summary page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('skip the activation survey and load the account dashboard', function(done) {
		activate.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.tracfone.esn);
		var min = DataUtil.getMinOfESN(sessionData.tracfone.esn);
		sessionData.tracfone.upgradeEsn = sessionData.tracfone.esn;//specific to phone upgrade
		//Update OTA pending
		DataUtil.clearOTA(sessionData.tracfone.esn);
		DataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_Activation_with_PIN');
		generatedMin['min'] = min;
		expect(myAccount.isLoaded()).toBe(true);		
		done();		
	}).result.data = generatedMin;
});
	