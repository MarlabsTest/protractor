'use strict';
//Firstly provide esn and sim for the activation process
//then enter the zipcode, it will navigate to service plan page  
//here provide the invalid pin,so that it will show you the error message.

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};

describe('Tracfone activation with invalid pin', function() {
	//from the menu it goes to the activation page for selecting the device upon clicking activate link
	it('select Activate option', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	//here we have to click on the button of I Have A Family Phone and ESN page will be loaded
	it('select device type',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	//enter the ESN and should navigate to pop up for providing the security PIN number upon clicking on continue button 
	it('enter ESN', function(done) {
		var esnval = DataUtil.getESN(sessionData.tracfone.esnPartNumber);
       	//console.log('returns', esnval);
		sessionData.tracfone.esn = esnval;
		activation.enterEsn(esnval);
		activation.checkBoxCheck();
		activation.continueESNClick();
		generatedEsn['esn'] = sessionData.tracfone.esn;
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	//enter the sim and should navigate to next page for selecting keep current phone or get new number 
	it('enter the sim number', function(done) {
		var simval = DataUtil.getSIM(sessionData.tracfone.simPartNumber);
       	//console.log('returns', simval);
		activation.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		expect(activation.keepMyPhonePageLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	//here enter rthe Zipcode and will be navigated to airtimeserviceplan page
	it('enter the zipcode and load service plan page', function(done) {
		activation.chooseDontKeepMyNumber();
		activation.enterZipCodeTabView(sessionData.tracfone.zip);
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	/*
	it('should enter the zipcode ', function(done) {
		activation.enterZipCode(sessionData.tracfone.zip);
		//sessionData.tracfone.zip = inputActivationData.ZipCode;
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	*/
	
	//should enter airtime invalid pin then it will display the invalid message
	it('enter an invalid airtime pin to test', function(done) {
		var pin = "2345567484638"
		activation.enterAirTimePin(pin);
		generatedPin['pin'] = pin;
		expect(activation.errorMessageIsDisplayed()).toBe(true);
		done();		
	}).result.data = generatedPin;
});
