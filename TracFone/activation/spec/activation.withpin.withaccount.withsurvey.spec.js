'use strict';

var activation = require("../activation.po");
var home = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
//var drive = require('jasmine-data-provider');
var DataUtil= require("../../util/datautils.util");
//var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");
var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};

describe('Tracfone Activation GSM', function() {
	it('should navigate to the activation page for selecting the device upon clicking activate link', function(done) {		
		home.goToActivate();
		expect(activation.isActivateLoaded()).toBe(true);		
		done();
	});  
	
	it('should click on button of I have a family phone',function(done){		
		activation.gotToEsnPage();
		expect(activation.esnPageLoaded()).toBe(true);
		done();
	});
	
	it('should navigate to pop up for providing the SIM page after clicking on continue button', function(done) {
		var esnval = DataUtil.getESN(sessionData.tracfone.esnPartNumber);
       	//console.log('returns', esnval);
		// var simval = DataUtil.getSIM(inputActivationData.SIM);
       	// console.log('returns', simval);
		// db call for EN-SIM marry
		// DataUtil.addSimToEsn(esnval,simval);
		// console.log("DB updated !!");
		sessionData.tracfone.esn = esnval;
		sessionData.tracfone.esnsToReactivate.push(esnval);
		// activation.enterMarriedSim(simval);
		// sessionData.wfm.sim = simval;
		activation.enterEsn(esnval);
		generatedEsn['esn'] = sessionData.tracfone.esn;
		activation.checkBoxCheck();
		activation.continueESNClick();
		expect(activation.isSIMPage()).toBe(true);
		done();	
	}).result.data = generatedEsn;
	
	/*
	 * it('should navigate to page for entering the SIM number after entering 4
	 * digit PIN', function(done) { activation.enterPin("1234");
	 * activation.clickOnPinContinue();
	 * expect(activation.isSIMPage()).toBe(true);
	 * //expect(activation.keepMyPhonePageLoaded()).toBe(true); done(); });
	 */
	it('should navigate to next page for selecting keep current phone or get new number after entering valid sim number', function(done) {
		var simval = DataUtil.getSIM(sessionData.tracfone.simPartNumber);
       	//console.log('returns', simval);
		activation.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		activation.keepCurrentPhnNum();
		expect(activation.zipcodeDivLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('should enter the zipcode and will be navigated to airtimeserviceplan page', function(done) {
		activation.enterZipCodeTabView(sessionData.tracfone.zip);
		//sessionData.tracfone.zip = inputActivationData.ZipCode;
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('should enter airtime pin and will be navigated to accoutn creation page', function(done) {
		var pinval = DataUtil.getPIN(sessionData.tracfone.pinPartNumber);
       	//console.log('returns', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.tracfone.pin = pinval;
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activation.activationAccountPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	it('should select create account option and will be navigated to accoutn creation page', function(done) {
		activation.clickonAccountCreationContinueBtn();
		expect(activation.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('should enter the accoutn details and will be navigated to account creation successful popup page', function(done) {
		var emailval = DataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
		//console.log('returns', emailval);
		activation.enterAccountDetails(emailval,password,"02/02/1990","12345");
		// Newly added by gopi
		sessionData.tracfone.cardpin = "12345";
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activation.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('should click on the continue button in the popup page', function(done) {
		activation.clickOnAccountCreatedPopupBtn();
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	});
	
	it('should click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it('should click on the done button in the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('should click on the thank you button and will be redirected to the account dashboard page', function(done) { //
		activation.clickOnThankYouBtn();
		DataUtil.activateESN(sessionData.tracfone.esn);
		//console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = DataUtil.getMinOfESN(sessionData.tracfone.esn);
		//console.log("MIN is  :::::" +min);
		sessionData.tracfone.upgradeEsn = sessionData.tracfone.esn;//specific to phone upgrade
		//Update OTA pending
		DataUtil.clearOTA(sessionData.tracfone.esn);
		//check_activation		
		DataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_Activation_with_PIN');
		generatedMin['min'] = min;
		//console.log("ITQ table updated");	
		expect(myAccount.isLoaded()).toBe(true);		
		done();		
	}).result.data = generatedMin;
	// });
});
