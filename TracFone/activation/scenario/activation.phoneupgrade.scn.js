'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var phoneupgradeUtil = require('../../util/phoneupgrade.util');
var sessionData = require("../../common/sessiondata.do");

describe('TracFone Phone Upgrade', function() {
	var phoneupgradeData = phoneupgradeUtil.getTestData();
	//console.log('phoneupgradeData:', phoneupgradeData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(phoneupgradeData, function(inputPhoneUpgradeData) {
		sessionData.tracfone.esnPartNumber = inputPhoneUpgradeData.FromPartNumber;
		//console.log('sessionData.tracfone.esnPartNumber: ',sessionData.tracfone.esnPartNumber );
		sessionData.tracfone.arFlag = inputPhoneUpgradeData.AR;
		//console.log('sessionData.tracfone.arFlag::',sessionData.tracfone.arFlag);
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.simPartNumber = inputPhoneUpgradeData.FromSIM;
				sessionData.tracfone.zip = inputPhoneUpgradeData.FromZipCode;
				sessionData.tracfone.pinPartNumber = inputPhoneUpgradeData.FromPIN;
				sessionData.tracfone.phoneStatus = inputPhoneUpgradeData.PhoneStatus;
				sessionData.tracfone.toEsnPartNumber = inputPhoneUpgradeData.ToPartNumber;
				sessionData.tracfone.toSimPartNumber = inputPhoneUpgradeData.ToSIM;
				sessionData.tracfone.toPinPartNumber = inputPhoneUpgradeData.ToPIN;
				//console.log('Copying upgrade', inputPhoneUpgradeData);
				done();
			});
			FlowUtil.run('TF_UPGRADE_FLOWS');
		}).result.data = inputPhoneUpgradeData;
	});
});