'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationCdmaUtil = require('../../util/cdmaactivation.util');
var sessionData = require("../../common/sessiondata.do");

describe('TF CDMA Activation with purchase with account', function() {
	var activationData = activationCdmaUtil.getTestData();
	//console.log('CDMA ActivationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		sessionData.tracfone.shopPlan = inputActivationData.shopplan;
		sessionData.tracfone.planName = inputActivationData.PlanName;
		sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.cardType = inputActivationData.cardType;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				//sessionData.tracfone.pinPartNumber = inputActivationData.PinCard;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_CDMA_ACTIVATION_WITHPURCHASE_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});