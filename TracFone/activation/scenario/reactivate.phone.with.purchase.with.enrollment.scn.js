'use strict';

var FlowUtil 			= require('../../util/flow.util');
var drive 				= require('jasmine-data-provider');
var sessionData 		= require("../../common/sessiondata.do");
var DataUtil			= require("../../util/datautils.util");
var activationUtil 		= require('../../util/activation.util');

describe('Reactivation with purchase along with enrollment option', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			sessionData.tracfone.shopPlan = inputActivationData.shopplan;
			sessionData.tracfone.planName = inputActivationData.PlanName;
			sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
			sessionData.tracfone.cardType = inputActivationData.cardType;
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber 	= inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber 	= inputActivationData.SIM;
				sessionData.tracfone.zip 			= inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber 	= inputActivationData.PIN;				
				//console.log('Copied activation data:', inputActivationData);
				done();
			});
			FlowUtil.run('TF_REACTIVATION_WITH_PURCHASE_ENROLLMENT');
		}).result.data = inputActivationData;
	});
});