'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Internal Porting with PIN with Account', function() {

	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
                sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
                sessionData.tracfone.oldPartNumber = inputActivationData.oldPartNumber;
                
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_ACTIVATION_INTERNALPORTING_WITHPIN_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});