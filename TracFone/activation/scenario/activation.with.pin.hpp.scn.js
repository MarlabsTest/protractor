'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
//var activationUtil = require('../../util/activation.util');
var hppActivationUtil = require('../../util/hpp.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('TF Activation GSM with PIN with Account', function() {
//	var activationData = activationUtil.getTestData();
	var hppActivationData = hppActivationUtil.getTestData();
	//console.log('hppActivationData:', hppActivationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(hppActivationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_ACTIVATION_WITHPIN_HPP');
		}).result.data = inputActivationData;
	});
});