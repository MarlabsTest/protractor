'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/byop.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('CDMA BYOP activation', function() {

	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.isActive = inputActivationData.isActive;
				sessionData.tracfone.isLTE = inputActivationData.isLTE;
				sessionData.tracfone.carrier = inputActivationData.carrier;
				sessionData.tracfone.isIPhone = inputActivationData.isIphone;
				sessionData.tracfone.isHD = inputActivationData.isHD;
				sessionData.tracfone.isTrio = inputActivationData.isTrio;
				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				
				
				
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_CDMA_BYOP_PIN_ACTIVATION');
		}).result.data = inputActivationData;
	});
});