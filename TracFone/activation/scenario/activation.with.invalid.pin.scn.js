'use strict';
//Firstly provide esn and sim for the activation process
//then enter the zipcode, it will navigate to service plan page  
//here provide the invalid pin,so that it will show you the error message.

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Tracfone activation with invalid pin', function() {
	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				done();
			});
			FlowUtil.run('TF_ACTIVATION_GSM_WITH_INVALID_PIN');
		}).result.data = inputActivationData;
	});
});