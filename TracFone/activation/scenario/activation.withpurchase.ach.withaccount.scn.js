'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('TF Activation GSM with ACH purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			sessionData.tracfone.shopPlan = inputActivationData.shopplan;
			sessionData.tracfone.planName = inputActivationData.PlanName;
			sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_ACTIVATION_WITHPURCHASE_ACH_WITHACCOUNT');
		}).result.data = inputActivationData;
	});
});