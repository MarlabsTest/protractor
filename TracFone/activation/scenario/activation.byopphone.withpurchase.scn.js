'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/byop.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('BYOP activation with purchase', function() {
	
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				sessionData.tracfone.planType = inputActivationData.PlanType;
				sessionData.tracfone.planName = inputActivationData.PlanName;
				sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
				sessionData.tracfone.cardType = inputActivationData.cardType;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_BYOP_ACTIVATION_WITH_PURCHASE');
		}).result.data = inputActivationData;
	});
});