'use strict';

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/hpp.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('TF Activation GSM with purchase with Account', function() {
	var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			sessionData.tracfone.shopPlan = inputActivationData.shopplan;
			sessionData.tracfone.planName = inputActivationData.PlanName;
			sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
			sessionData.tracfone.cardType = inputActivationData.cardType;
			sessionData.tracfone.hppPlan = inputActivationData.HppPlan;

			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				//console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_ACTIVATION_WITHPURCHASE_WITHACCOUNT_HPP');
		}).result.data = inputActivationData;
	});
});