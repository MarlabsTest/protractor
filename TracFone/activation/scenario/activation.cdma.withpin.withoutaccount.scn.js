'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationCdmaUtil = require('../../util/cdmaactivation.util');
var sessionData = require("../../common/sessiondata.do");

describe('TF CDMA Activation with PIN without Account', function() {
	var activationData = activationCdmaUtil.getTestData();
	//console.log('CDMA ActivationData:', activationData);
	//console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber = inputActivationData.PinCard;
				//console.log('Copying activation', inputActivationData);
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				done();
			});
			FlowUtil.run('TF_CDMA_ACTIVATION_WITHPIN_WITHOUTACCOUNT');
		}).result.data = inputActivationData;
	});
});