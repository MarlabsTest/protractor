//this represents a modal for airtimepin page

'use strict';
var ElementsUtil = require("../util/element.util");

var AirtimePin = function() {

	this.airtimePinBox = element.all(by.id('sim')).get(1);//2
	//this.reactivationPin = element(by.css("input[id='formPlan.pin']"));
	this.reactivationPin = element.all(by.id("formPlan.pin")).get(1);
	this.airtimePinContinueBtn =  element.all(by.id('btn_continueserviceplanpin')).get(0);
	this.airtimeReactivationPinContinueBtn =  element.all(by.css("button[id='btn_addairtime']")).get(2);
	this.byopAirtimePinBox = element.all(by.name('sim')).get(1);
	this.byopAirtimePinContinueBtn =  element.all(by.css('[ng-click="action()"]')).get(0);
	this.errorMessage = element(by.css("span[translate='RDSG_5477']"));
	
	//** this method is to check whether the error message is displayed after giving the invalid pin
	this.errorMessageIsDisplayed = function(){
		//$$//ElementsUtil.waitForElement(this.errorMessage);
		return this.errorMessage.isPresent();
	};

	//** method to check whether the plan(purchase plan/already have a pin -flows) page is loaded
	this.servicePlanPageLoaded = function(){
		//$$//ElementsUtil.waitForElement(this.airtimePinContinueBtn);
		return this.airtimePinContinueBtn.isPresent();
		
		
	};
	
	this.enterReactivationAirTimePin = function(pin){	
		//$$//ElementsUtil.waitForElement(this.reactivationPin);
		this.reactivationPin.clear().sendKeys(pin);
		//ElementsUtil.waitForElement(this.airtimePinContinueBtn);
		this.airtimeReactivationPinContinueBtn.click();
	};
	
	//** method to enter airtime pin and proceed from the page
	this.enterAirTimePin = function(pin){
		//$$//ElementsUtil.waitForElement(this.airtimePinBox);
		this.airtimePinBox.clear().sendKeys(pin);
		//ElementsUtil.waitForElement(this.airtimePinContinueBtn);
		this.airtimePinContinueBtn.click();
	};

	this.enterByopAirTimePin = function(pin){
		//$$//ElementsUtil.waitForElement(this.byopAirtimePinBox);
		this.byopAirtimePinBox.clear().sendKeys(pin);
		//$$//ElementsUtil.waitForElement(this.byopAirtimePinContinueBtn);
		this.byopAirtimePinContinueBtn.click();
	};

};
module.exports = new AirtimePin;