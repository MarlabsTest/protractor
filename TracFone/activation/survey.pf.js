//this represnts a modal for the survey page

'use strict';
var ElementsUtil = require("../util/element.util");

var surveyPge = function() {

	this.thankYouBtn =  element.all(by.css('[ng-click="action()"]')).get(1);
	
	//** this method is to check whether the survey page is loaded or not
	this.surveyPageLoaded = function(){
		//browser.wait(expectedConditions.visibilityOf(this.thankYouBtn),40000);
		ElementsUtil.waitForElement(this.thankYouBtn);
		return this.thankYouBtn.isPresent();
	};
  
	//** this method is to proceed from the survey page using the THANK YOU option
	this.clickOnThankYouBtn = function(){
		//browser.wait(expectedConditions.visibilityOf(this.thankYouBtn),40000);
		//ElementsUtil.waitForElement(this.thankYouBtn);
		this.thankYouBtn.click();
	};

 };
module.exports = new surveyPge;