'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var myAccount = require("../../myaccount/myaccount.po");
var activation = require("../../activation/activation.po");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");
var CommonUtil= require("../../util/common.functions.util");
//Firstly go to the shop page 
//click on the select service plans
//enter the MIN which is activated in the tracfone
//choose the plans for your smartphone and add to the cart
//make the payment and place an order
//finally it redirects to the confirmation page

describe('Tracfone buy service plans', function() {

	//click on the shop link which is shown in the homepage
	//should check whether the shop service plan page is loaded or not
	it('should open shopping page', function(done) {
		homePage.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);
		done();
	});
	
	//click on the select service plans now and will be redirected to enter the MIN page
	it('should open shop phone page', function(done) {
		shop.goToShopServicePlan();
		//expect(shop.isShopServicePlanPageLoaded()).toBe(true);
		expect(shop.isShopServicePlansListed()).toBe(true);
		done();
	});
	
	// enter the MIN and confirm the same
	// It will be redirected to buy the phones.
	/*it('should provide Min and Confirm', function(done) {
		var minval = dataUtil.getMinOfESN(loginData.tracfone.esn);//other_min
		console.log("MINNNN :: ",minval);
		shop.providePhoneNumber(minval); 
		expect(shop.isShopServicePlansListed()).toBe(true);
		done();
	});*/
	
	//select the plans for the smart phone ans add that plan to the cart
	/*it('should choose an smartPhone plan and add to cart', function(done) {
		//shop.choosePlan(loginData.tracfone.shopPlan); //$10ILD  PAYASGO SMARTPHONE
		var planType		= loginData.tracfone.shopPlan;
		var planName		= loginData.tracfone.planName;
		var isAutoRefill	= loginData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});*/
	
	it('choose  PAYASGO plan and add to cart', function(done) {
		//shop.choosePlan("PAYASGO");
		var planType		= sessionData.tracfone.shopPlan;
		var planName		= sessionData.tracfone.planName;
		var isAutoRefill	= sessionData.tracfone.autoRefill;
		shop.choosePlanByName(planType,planName,isAutoRefill);
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();
	});
	
	/*//activation!keepcollectphone
	it('Select Create Account option', function(done) {
		activate.clickonAccountCreationContinueBtn();
		expect(activate.fbButtonLoaded()).toBe(true);
		done();		
	});
	
	it('enter account creation details and create the account', function(done) {
		var emailval = dataUtil.getEmail();
		var password = "tracfone";  // Newly added by gopi
       	//console.log('returns', emailval);
		activate.enterAccountDetails(emailval,password,"02/02/1990","12345");
		// Newly added by gopi
		sessionData.tracfone.username = emailval;
		sessionData.tracfone.password = password;
		expect(activate.accountCreationDone()).toBe(true);
		done();		
	});
	
	it('load the checkout page', function(done) {
		activate.clickOnAccountCreatedPopupBtn();
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});*/
	
	//enter the credit card details for the payment and proceed to place an order
	/*it('should complete payment checkout', function(done) {
		shop.completeCheckout(); 
		expect(shop.isCheckoutSuccess()).toBe(true);
		done();
	});
	
	it('Checking Order Summary and skip survey', function(done) {
		shop.completeSummaryProcess(); 
		expect(shop.isServeyCompleted()).toBe(true);
		done();
	});*/
	it(' click on continue to payment button and should be asked to fill cc details', function(done) {
		//myAccount.continueToPayment();
		//	myAccount.goToPaymentTab();
		expect(myAccount.paymentOptionLoaded()).toBe(true);
		done();		
	});
	
	it(' enter cc and billing address details,click on proceed and navigate to final indtructions page', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activation.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	it(' click on the continue button  and  navigate to Succcess page', function(done) {
		activation.finalInstructionProceedPurchase();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	it(' click on the done button  navigate to survey page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});
	
	//thankyou
	it(' click on the thank you button and navigate to the account dashboard ', function(done) {
		activation.clickOnThankYouBtn();
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	}); 
	
});
