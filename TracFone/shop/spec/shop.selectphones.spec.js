'use strict';

var homePage = require("../../common/homepage.po");
var shop = require("../shop.po");
var sessionData = require("../../common/sessiondata.do");

//Firstly go to the shop page 
//click on the select phones
//enter the ZIP Code of the area where you will be using your tracfone
//confirm the zip code by clicking yes button
//it redirects to the page where we can purchase the phones

describe('Tracfone buy phones', function() {

	//click on the shop link which is shown in the homepage
	//should check whether the shop phone page is loaded or not
	it('should open shopping page', function(done) {
		homePage.goToShop();
		expect(shop.isShopPageLoaded()).toBe(true);
		done();
	});
	
	//click on the select phones now and will be redirected to enter the zipcode page
	it('should open shop phone page', function(done) {
		shop.goToShopPhones();
		expect(shop.isShopPhonePageLoaded()).toBe(true);
		done();
	});
	
	// enter the ZIP Code of the area where you will be using your TRACFONE and confirm the zipcode
	// It will be redirected to buy the phones.
	it('should provide zipcode and Confirm', function(done) {
		shop.enterZipcode(sessionData.tracfone.zip); 
		expect(shop.isSelectPhonesPageLoaded()).toBe(true);
		done();
	});
});
