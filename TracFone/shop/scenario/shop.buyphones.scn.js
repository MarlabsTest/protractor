'use strict';
//Firstly go to the shop page 
//click on the select phones
//enter the ZIP Code of the area where you will be using your tracfone
//confirm the zip code by clicking yes button
//it redirects to the page where we can purchase the phones

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Tracfone buy phones', function() {

	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
//				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
//				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
//				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_SHOP_SELECT_PHONES');
		}).result.data = inputActivationData;
	});
});