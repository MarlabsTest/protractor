'use strict';
//Firstly go to the shop page 
//click on the select service plans
//enter the MIN which is activated in the tracfone
//choose the plans for your smartphone and add to the cart
//make the payment and place an order
//finally it redirects to the confirmation page

var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var shopplansUtil = require('../../util/shopplans.util');
var sessionData = require("../../common/sessiondata.do");

describe('Tracfone buy service plans', function() {

	var shopplansData = shopplansUtil.getTestData();
	console.log('activationData:', shopplansData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(shopplansData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				sessionData.tracfone.shopPlan = inputActivationData.shopplan;
				sessionData.tracfone.planName = inputActivationData.PlanName;
				sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_SHOP_BUY_PLANS');
		}).result.data = inputActivationData;
	});
});