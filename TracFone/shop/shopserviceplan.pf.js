'use strict';
var ElementUtil = require("../util/element.util");
var ConstantsUtil = require("../util/constants.util");

var ShopServicePlan = function() {

	//Newly added $$
    this.smartPhonePlansTab = element(By.id("top-panel-desk-tab2"));
    this.payAsGoPlansTab = element(By.id("top-panel-desk-tab3"));
    this.addOnPlansTab = element(By.id("top-panel-desk-tab4"));
    
    this.smartPhonePlans = element.all(By.id('btn_plancardaddtocart'));
    this.payAsGoPlans = element.all(By.id('btn_paygplancardaddtocart'));
    this.addOnPlans = element.all(By.id('btn_plancardaddonaddtocart'));

	
	this.oneTimePurchaseBtn = element.all(By.id('btn_onetimepurchase'));
	this.autoRefillBtn = element.all(By.id('btn_enrollinautorefill')).get(1);
	this.actualOneTimePurchase = element.all(By.id('btn_onetimepurchase')).get(1);
	this.phoneNumberTextbox = element.all(By.id('other_min')).get(1);
	this.phoneNumberContinueBtn = element.all(By.id('btn_viewplans')).get(1);
	this.addPopUpOne = element(By.css("[ng-click='cancel()']"));
	this.serviceAddonTab = element.all(By.css("span[translate='BAT_24212']")).get(0);
	this.smartPhonePlanTab = element.all(by.css('[ng-click="toggleOpen()"]')).get(1);
	this.smartPhonePlanBuy = element.all(By.id("btn_plancardaddtocart")).get(1);
	this.tenILDPlanAddToCart = element.all(By.css("button[id='btn_plancardaddonaddtocart']")).get(3);
	//this.oneTimePurchaseBtn = element.all(By.id('btn_onetimepurchase')).get(1);
	this.continuePaymentBtn = element.all(By.id('btn_continuetopayment')).get(1);
	this.payAsYouGoTab = element(By.xpath("//span[@translate='ACRE_6801']"));
	this.payAsYouGoPlanAddToCart = element.all(By.id("btn_paygplancardaddtocart")).get(1);
	this.continueCheckoutBtn = element.all(By.id("btn_continuecheckout")).get(1);
	
	this.valueAddedPlanModal = element(by.css('.modal-dialog'));
	this.checkOutBtn = element.all(by.id('btn_continuecheckout')).get(1);
	this.ILDRefillBtn = element.all(by.css("button[id='btn_enrollininstantlowbalancerefill']")).get(1);
	
	
	this.choosePlanByName = function(planType,planName,isAutoRefill) {
		
        if (planType == ConstantsUtil.SHOP_PLANS[0]) {
        	
        	this.smartPhonePlansTab.click();
              this.smartPhonePlans.get(ConstantsUtil.SMARTPHONE_PLANS[planName]).click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[1]){this.payAsGoPlansTab.click();
              this.payAsGoPlans.get(ConstantsUtil.SPAYASGO_PLANS[planName]).click();
        }else if(planType == ConstantsUtil.SHOP_PLANS[2]){
              this.addOnPlansTab.click();
              this.addOnPlans.get(ConstantsUtil.ADDON_PLANS[planName]).click();
        }
        
        if(isAutoRefill == 'YES' || isAutoRefill == 'Y'){
		ElementUtil.waitForElement(this.autoRefillBtn);
              if(this.autoRefillBtn.isPresent()){
              this.autoRefillBtn.click();
              }
        }else{
		ElementUtil.waitForElement(this.actualOneTimePurchase);
              if(this.actualOneTimePurchase.isPresent()){
              this.actualOneTimePurchase.click();
              }
        }
  }

	
	//this will navigate to enter the phone number
	this.isShopServicePlanPageLoaded = function() {
		return ElementUtil.waitForUrlToChangeTo(/shop\/plans$/);
	};
	
	// enter the MIN which is active
	this.providePhoneNumberAndContinue = function(phoneNumber) {
		ElementUtil.waitForElement(this.phoneNumberTextbox);
		this.phoneNumberTextbox.sendKeys(phoneNumber);
		ElementUtil.waitForElement(this.phoneNumberContinueBtn);
		this.phoneNumberContinueBtn.click();//btn_viewplans
	};
	
	//check whether the page is navigated to the service plan page
	this.isShopServicePlansListed = function() {
		ElementUtil.waitForElement(this.addPopUpOne);
		this.addPopUpOne.click();
//		return ElementUtil.waitForUrlToChangeTo(/serviceplan$/);
		return ElementUtil.waitForUrlToChangeTo(/shop\/plans$/);
	};
	
	//this method is to switch to smartphone plans
	this.switchToSmartPhonePlans = function() {
		//$$//ElementUtil.waitForElement(this.smartPhonePlanTab);
		this.smartPhonePlanTab.click();
	};
	
	//this method is to choose smartphone plan
	this.chooseSmartPhonePlan = function() {
		//$$//ElementUtil.waitForElement(this.smartPhonePlanBuy);
		this.smartPhonePlanBuy.click();
	};
	
	//this method is to select auto refill option
	this.registerAutoRefill = function() {
		//$$//ElementUtil.waitForElement(this.autoRefillBtn);
		this.autoRefillBtn.click();
	};
	
	//** this method is to whether the value added plans listed
	this.isValueAddedPlansModalShown = function(){
		return this.valueAddedPlanModal.isPresent();
	};
	
	//** this method is to do checkout on value added plans page
	this.doCheckOut = function(){
		this.checkOutBtn.click();
	};
	
	//choose the plan for your smart phone and continue to the checkout page
	this.choosePlan = function(planName) {
		if (planName == "$10ILD") {			
			this.tenILDPlanAddToCart.click();			
			ElementUtil.waitForElement(this.ILDRefillBtn);
            this.ILDRefillBtn.click();
		} else if (planName == "PAYASGO") {
			this.payAsYouGoTab.click();
			this.payAsYouGoPlanAddToCart.click();
		}else if(planName == "SMARTPHONE"){
			this.smartPhonePlanTab.click();
			this.smartPhonePlanBuy.click();
		}
	};
	
	//add the plan to the cart to make payment
	this.isPlanAddedOnCart = function() {
		return ElementUtil.waitForUrlToChangeTo(/checkout$/);
	};

};

module.exports = new ShopServicePlan;