'use strict';
var ElementUtil = require("../util/element.util");

var ShopPf = function() {

	this.shopServicePlanLink = element.all(by.id('lnk_shopplans_now')).get(0);
	
	this.goToShopServicePlan = function() {
		ElementUtil.waitForElement(this.shopServicePlanLink);
		this.shopServicePlanLink.click();
	};
	
	this.isShopPageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/shop$/);
	};
	
};

module.exports = new ShopPf;