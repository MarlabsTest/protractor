'use strict';
var ElementUtil = require("../util/element.util");

var ShopPhonePf = function() {

	this.shopPhonesLink= element(by.id('lnk_shopbestphone_now'));
	this.zipcode = element.all(by.id('zipcode')).get(1);
	this.continueBtn = element.all(by.css('[ng-click="action()"]')).get(0);
	this.yesBtn = element(by.css('[ng-click="getPhoneUrlByZip()"]'));
	
	//this method clicks on the shop link
	this.goToShopPhones = function() {
		ElementUtil.waitForElement(this.shopPhonesLink);
		this.shopPhonesLink.click();
	};
	
	//this method checks whether the url is changed or not
	this.isShopPhonePageLoaded = function(){
		return ElementUtil.waitForUrlToChangeTo(/phones$/);
	};
	
	//this method enters the zipcode and confirm the same
	this.enterZipcode = function(zipcode){
		this.zipcode.sendKeys(zipcode);
		this.continueBtn.click();
		this.yesBtn.click();
	};
	
	//this method navigates to buy the phones
	this.isSelectPhonesPageLoaded = function() {
		console.log("URL.........................." + browser.getCurrentUrl());
		return browser.getCurrentUrl().then(function(url) {
			console.log("url", url);
			return (url.includes("tracfone-orders"));
		});
	};
	
};

module.exports = new ShopPhonePf;