'use strict';

var shopPf = require("./shop.pf");
var shopServicePlanPf = require("./shopserviceplan.pf");
var paymentCheckoutPf = require("../common/paymentcheckout.pf");
var selectphonespf = require("./selectphones.pf");

var Shop = function() {
	

	this.goToShopServicePlan = function() {
		return shopPf.goToShopServicePlan();
	};

	this.isShopPageLoaded = function() {
		return shopPf.isShopPageLoaded();
	};
	//this method will navigate to enter the phone number
	this.isShopServicePlanPageLoaded = function() {
		return shopServicePlanPf.isShopServicePlanPageLoaded();
	};
	
    //Newly added by
    this.choosePlanByName = function(planType,planName,isAutoRefill) {
          shopServicePlanPf.choosePlanByName(planType,planName,isAutoRefill);
    };

	
	//** this method is to check out directly without choosing the value added plans
	this.isValueAddedPlansModalShown = function(){
		return shopServicePlanPf.isValueAddedPlansModalShown();
	};
	
	//** this method is to do checkout on value added plans page
	this.doCheckOut = function(){
		shopServicePlanPf.doCheckOut();
	};
	
	//this method is to enter the MIN
	this.providePhoneNumber = function(phoneNumber) {
		return shopServicePlanPf.providePhoneNumberAndContinue(phoneNumber);
	};
	
	//this method checks whether the service plan page is loaded 
	this.isShopServicePlansListed = function() {
		return shopServicePlanPf.isShopServicePlansListed();
	};
	
	//this method is to choose the smartphone plans
	this.choosePlan = function(planName) {
		shopServicePlanPf.choosePlan(planName);
	};
	
	//this method is to switch to smartphone plans
	this.switchToSmartPhonePlans = function() {
		shopServicePlanPf.switchToSmartPhonePlans();
	};
	
	//this method is to choose smartphone plan
	this.chooseSmartPhonePlan = function() {
		shopServicePlanPf.chooseSmartPhonePlan();
	};
	
	//this method is to select auto refill option
	this.registerAutoRefill = function() {
		shopServicePlanPf.registerAutoRefill();
	};
	
	//this method is to add the plans to the cart
	this.isPlanAddedOnCart = function() {
		return shopServicePlanPf.isPlanAddedOnCart();
	};
	
	//this method is to make a payment and proceed to checkout
	this.completeCheckout = function() {
		paymentCheckoutPf.completeCheckout();
	};
	
	this.isCheckoutSuccess = function() {
		return paymentCheckoutPf.isCheckoutSuccess();
	};
	
	this.completeSummaryProcess = function() {
		paymentCheckoutPf.completeSummaryProcess();
	};
	
	this.isServeyCompleted = function() {
		return paymentCheckoutPf.isServeyCompleted();
	};
	
	//this method clicks the shop link
	this.goToShopPhones = function() {
		return selectphonespf.goToShopPhones();
	};
	
	//this method checks whether the page is navigated to enter the zipcode 
	this.isShopPhonePageLoaded = function() {
		return selectphonespf.isShopPhonePageLoaded();
	};
	
	//this method enters the zipcode and confirm the same.
	this.enterZipcode = function(zipcode) {
		return selectphonespf.enterZipcode(zipcode);
	};
	
	//this method navigates to buy the phones 
	this.isSelectPhonesPageLoaded = function() {
		return selectphonespf.isSelectPhonesPageLoaded();
	};
	
};

module.exports = new Shop;