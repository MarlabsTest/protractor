'use strict';
var ElementsUtil = require("../util/element.util");

var forgotPassword = function(){
	//this.forgotPassword = element.all(by.id('lnk_forgot_password')).get(1);
	this.forgotPassword = element.all(by.linkText('FORGOT PASSWORD?')).get(0);
	this.email = element.all(by.id('email')).get(1);
	this.submitbtn = element.all(by.id('btn_submit')).get(0);
	this.popup = element(by.css('[ng-click="closePopup()"]'));
	
	//checks wheather the forgot password link is present in the new page.
	this.isHomePageLoaded = function(){
		return this.forgotPassword.isPresent();
	};
	
	//the forgotpassword link is clicked
	this.clickOnForgotPassword = function() {
		return this.forgotPassword.click();
	};
	
	//to retrieve the password, email has to be provided so checks wheather the new page is loaded using the url. 
	this.retrievePasswordPage = function() {
//		return browser.getCurrentUrl().then(function(url) {
//			console.log('url: ', url);
//			return /forgotpassword$/.test(url);
//		});
		return ElementsUtil.waitForUrlToChangeTo(/forgotpassword$/);
	};
	
	//checks wheather the submit button element is present in that page
	this.checktheemailpage = function(){
		return this.submitbtn.isPresent();
		
	};
	
	//enters the input 
	this.enterTheEmail = function(userId){
		this.email.sendKeys(userId);
		this.submitbtn.click();
	};
	
	//finally the pop up window is displayed with login button which takes to the homepage.
	this.popUpWindow = function(){
		ElementsUtil.waitForElement(this.popup);
		return this.popup.isPresent();
	};
	this.PopUpWindowClick = function(){
		this.popup.click();
	};
	
	this.resetThePassword = function(){
		return ElementsUtil.waitForUrlToChangeTo(/createaclogin$/);
		
	};
};
module.exports = new forgotPassword;
