'use strict';

var myAccount = require("../myaccount.po");
var home = require("../../common/homepage.po");
var shop = require("../../shop/shop.po");
var activate = require("../../activation/activation.po");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");

//===============Deenroll ILD enrollment Scenario========================

describe('Buy Service Plan inside Account', function() {
		
	it('Should navigate to buy plan page', function(done) {
		home.homePageLoad();
		home.myAccount();		
		expect(myAccount.isLoaded()).toBe(true);
		done();		
	});
	
});
	