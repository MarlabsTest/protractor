'use strict';

var myAccount = require("../myaccount.po");


describe('Tracfone Add New Device', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	it('Remove added device', function(done) {	
		myAccount.removeDevice();
		expect(myAccount.isDeviceRemoved()).toBe(true);
		done();
		});
		
	
});
	