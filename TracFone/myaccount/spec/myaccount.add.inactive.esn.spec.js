'use strict';

var homePage = require("../../common/homepage.po");
var myAccoutPage = require("../myaccount.po");
var loginPage = require("../../login/login.po");
//newly added for data integration
var DataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");

describe('Add inactive esn to an account', function() {
	it('Go to add device', function(done) {
		myAccoutPage.addDevice();
		expect(myAccoutPage.isAddDeviceModalShown()).toBe(true);
		done();
	});
	
	it('Add inactive esn to the account', function(done) {
		var esnVal = DataUtil.getESN(sessionData.tracfone.esnPartNumber);
		sessionData.tracfone.esn = esnVal;
		myAccoutPage.addInActiveEsn(esnVal);
		expect(myAccoutPage.isInActivceEsnAdded()).toBe(true);
		myAccoutPage.closeAddDeviceSuccessModal();
		done();
	});
});
	