'use strict';
//go to the manage payment page
//click on the edit payment details
//the card details which we saved during our payment will be deleted

var myAccount = require("../myaccount.po");
var login = require("../../login/login.po");
var loginData =require("../../common/sessiondata.do");
var homePage = require("../../common/homepage.po");

//================Manage Payment Scenario=====================

describe('Tracfone delete manage payment', function() {

	
	beforeEach(function () {
		// browser.ignoreSynchronization = true;		
	});
	
	//click on the edit payment 
	//click ont the delete option which is displayed
	//the saved card details will be deleted and it navigate to the homepage.
	it('Delete the saved card details', function(done) {
		console.log("Should navigate to manage payment page");
		myAccount.deleteCardDetails();
		expect(myAccount.paymentPageIsLoaded()).toBe(true);	//myAccount.isPaymentAdded()
		done();
	});	
});
	