'use strict';

var myAccount = require("../myaccount.po");

// ===========================Edit Contact Info scenario============================

describe('Edit Contact Info', function() {
	
	beforeEach(function () {
		// browser.ignoreSynchronization = true;		
	});
	
	it('Should navigate to manage profile page', function(done) {
		console.log("should navigate to Manage Profile page");
		myAccount.manageProfile();
		expect(myAccount.isManageProfileLoaded()).toBe(true);
		done();
	});
	
	it('Edit contact informations', function(done) {
		console.log("Edit contact informations");
		myAccount.editContactInfo();
		expect(myAccount.isContactEdited()).toEqual("test");
		done();
	});
	
});
	