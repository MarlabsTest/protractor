'use strict';

var myAccount = require("../myaccount.po");
var drive = require('jasmine-data-provider');
var dataUtil= require("../../util/datautils.util");
var sessionData = require("../../common/sessiondata.do");
var activation = require("../../activation/activation.po");
var sessionData = require("../../common/sessiondata.do");
var home = require("../../common/homepage.po");

var generatedEsn ={};
var generatedSim ={};
var generatedPin ={};
var generatedMin ={};

describe('Tracfone Add New Device', function() {
	
	console.log('Spec sessionData.tracfone.noOfLines======',sessionData.tracfone.noOfLines);
	
	if (sessionData.tracfone.noOfLines == '' || sessionData.tracfone.noOfLines == 'undefined'){
		sessionData.tracfone.noOfLines = 1;
	}
		
	for(var line = 0;line < sessionData.tracfone.noOfLines;line++)
	{
	it('Click on adddevice and enter esn', function(done) {
		browser.refresh();
		myAccount.addDevice();
		var esnval = dataUtil.getESN(sessionData.tracfone.esnPartNumber);
		sessionData.tracfone.esn = esnval;
       	console.log('returns', esnval);
		myAccount.addNewDevice(esnval);
		expect(myAccount.isLoaded()).toBe(true);
		done();
	});
	
	it('Should click on activate button and select get new number after entering the sim number', function(done) {
		myAccount.clickActivateAddedDevice();
		var simval = dataUtil.getSIM(sessionData.tracfone.simPartNumber);
       	console.log('returns', simval);
		activation.enterSIM(simval);
		sessionData.tracfone.sim = simval;
		generatedSim['sim'] = sessionData.tracfone.sim;
		activation.keepCurrentPhnNum();
		expect(activation.zipcodeDivLoaded()).toBe(true);
		done();		
	}).result.data = generatedSim;
	
	it('Should enter the zipcode and will be navigated to airtimeserviceplan page', function(done) {
		activation.enterZipCodeAddDevice(sessionData.tracfone.zip);
		//sessionData.tracfone.zip = inputActivationData.ZipCode;
		expect(activation.servicePlanPageLoaded()).toBe(true);
		done();		
	});
	
	it('Should enter airtime pin and will be navigated to instruction page', function(done) {
		var pinval = dataUtil.getPIN(sessionData.tracfone.pinPartNumber);
       	console.log('returns', pinval);
		activation.enterAirTimePin(pinval);
		sessionData.tracfone.pin = pinval;
		generatedPin['pin'] = sessionData.tracfone.pin;
		expect(activation.finalInstructionPageLoaded()).toBe(true);
		done();		
	}).result.data = generatedPin;
	
	xit('Should click on the continue button in the final instruction page', function(done) {
		activation.finalInstructionProceed();
		expect(activation.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	xit('Should click on the done button in the summary page', function(done) {
		activation.clickOnSummaryBtn();
		expect(activation.surveyPageLoaded()).toBe(true);
		done();		
	});

	it('Should click on thank you button and redirected to account dashboard page', function(done) {
		//activation.clickOnThankYouBtn();
		dataUtil.activateESN(sessionData.tracfone.esn);
		console.log("update table with esn :"+sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		console.log("MIN is  :::::" +min);
		//Update OTA pending
		dataUtil.clearOTA(sessionData.tracfone.esn);
		//check_activation		
		dataUtil.checkActivation(sessionData.tracfone.esn,sessionData.tracfone.pin,'1','Tracfone_Activation_with_PIN');
		generatedMin['min'] = min;
		console.log("ITQ table updated");
		myAccount.dashboard();			
		expect(myAccount.isLoaded()).toBe(true);		
		done();		
	}).result.data = generatedMin;
	
	/* it('check if device successfully added and activated', function(done) {	
		dataUtil.activateESN(sessionData.tracfone.esn);
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		console.log("MIN is  :::::" +min);
		expect(myAccount.isDeviceAdded()).toEqual(line+1+1);
		// myAccount.refresh();
		done();
	}); */
	}	
	});

	
