'use strict';

var myAccount = require("../myaccount.po");

//================Manage Payment Scenario=====================

describe('Manage Payment', function() {

	
	beforeEach(function () {
		// browser.ignoreSynchronization = true;		
	});

	it('Should navigate to payment method page', function(done) {
		console.log("should navigate to Manage Payment page");
		myAccount.paymentMethod();
		expect(myAccount.isManagePaymentLoaded()).toBe(true);
		done();
	});	
		
	it('Should navigate to add new payment page', function(done) {
		console.log("should navigate to Manage Payment page");
		myAccount.editPaymentDetails();
		expect(myAccount.isPaymentAdded()).toEqual(2);	//myAccount.isPaymentAdded()
		done();
	});	
});
	