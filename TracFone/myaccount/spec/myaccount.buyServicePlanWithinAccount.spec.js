'use strict';

var myAccount = require("../myaccount.po");
var home = require("../../common/homepage.po");

//===============Buy service plan with in account Scenario========================

describe('Buy Service Plan inside Account', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
	
	it('Should navigate to buy plan page', function(done) {
		console.log("should navigate to Buy Plan Page");
		//myAccount.clickContinue();   //-------Click on Continue--------------
		home.myAccount();
		myAccount.chooseBuyPlan();   //---------Choose a Plan----------
		done();
	});	
	
	it('Should choose a service plan and navigate to checkout page', function(done) {
		console.log("should a service Plan and navigate to checkout page");
		myAccount.selectAPlan();     //--------Continue with the plan-----------
		myAccount.checkout(); //---------Checkout-------------
		myAccount.isSuccessfulCheckout();
		done();
	});	
	
});
	