'use strict';

var login = require("../../login/login.po");

var homePage = require("../../common/homepage.po");
var sessionData = require("../../common/sessiondata.do");
var myAccount = require("../myaccount.po.js");

describe('Tracfone Login', function() {
	
	
	it('Should navigate to myaccount page', function(done) {	
		console.log("should navigate to access account page");
		homePage.myAccount();
		login.provideUserLoginCredentials(sessionData.tracfone.username,sessionData.tracfone.password);
		expect(myAccount.isLoaded()).toBe(true);
		done();
	});
});
	