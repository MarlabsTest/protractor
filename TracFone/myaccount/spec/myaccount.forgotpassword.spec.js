'use strict';

//Firstly click on the forgot password 
//then we have to enter the email through which we reset the password
//then the link will be sent to our email to reset the password 

var resetPassword = require("../myaccount.po");
var login = require("../../login/login.po");
var loginData = require("../../common/sessiondata.do");
var home = require("../../common/homepage.po");

describe('Tracfone Forgot Password', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = true;		
	});
		
	//tracfone homepage is loaded where we can select the forgot password and it will be navigated to the retrieve page
	it('Click on the forgot password',function(done){
		//resetPassword.isHomePageLoaded();
		home.myAccount();
		resetPassword.clickOnForgotPassword(); 
		expect(resetPassword.retrievePasswordPage()).toBe(true); 
		done();
	});
	
	// we have to enter the email and the popup window will appear saying that message sent successsfully
	//here the link to reset the password will be sent to our email which we provided
	it('Enter email id and then appears popup window',function(done){		
		resetPassword.enterTheEmail(loginData.tracfone.username); 
		expect(resetPassword.popUpWindow()).toBe(true); 
		done();
	});
	
	/*it('the password resetting mail has been sent when Login button is clicked ',function(done){
		resetPassword.PopUpWindowClick();
		expect(resetPassword.resetThePassword()).toBe(true);
	});*/

});
	