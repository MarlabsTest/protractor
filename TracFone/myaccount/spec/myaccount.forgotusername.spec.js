// for reset username scenario

var resetUser = require("../myaccount.po");
//var login = require("../../login/login.po");
var loginData = require("../../common/sessiondata.do");
var home = require("../../common/homepage.po");

describe('tracfone forgot username', function() {
	
	//homepage is loaded where we can select the forgot username and it will navigates to the retrieve page
	it('Click forgot username and retrieve page will be loaded',function(done){	
		
		console.log("for resetting user name");
		home.myAccount();
		//resetUser.resetUserIsLoaded();
		resetUser.continueWithForgotUsername(); 
		expect(resetUser.retrievePageLoaded()).toBe(true); 
		done();
	});
	//here we have to enter the sim number through which we reset our username upon proceeding from the page will be redirected to enter the security pin
	it('Should enter sim number and security pin page is loaded',function(done){
		
		console.log("for entering sim");			
		resetUser.gotToPage(loginData.tracfone.sim); 
		expect(resetUser.securityPinPageLoaded()).toBe(true); 
		done();
	});
	
	//here we have to enter the security pin and the message for reset username will be sent to our mobile number which we had provided before
	it('Should enter pin and popup window appears', function(done){
		console.log("enter pin for forget username",loginData.tracfone.cardpin);
		resetUser.goToPinPage(loginData.tracfone.cardpin); 
		expect(resetUser.popUpWindowForResetUser()).toBe(true); 
		done();
	});
});
	