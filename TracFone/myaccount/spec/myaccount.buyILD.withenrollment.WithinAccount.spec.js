'use strict';

var myAccount = require("../myaccount.po");
var home = require("../../common/homepage.po");
var shop = require("../../shop/shop.po");
var activate = require("../../activation/activation.po");
var CommonUtil= require("../../util/common.functions.util");
var sessionData = require("../../common/sessiondata.do");
var generator = require('creditcard-generator');
var CCUtil = require("../../util/creditCard.utils");

//===============Buy ILD with enrollment Scenario========================

describe('Buy Service Plan inside Account', function() {
		
	it('Should navigate to buy plan page', function(done) {
		home.homePageLoad();
		home.myAccount();
		myAccount.clickOnBuyServicePlan();
		shop.choosePlan("$10ILD");
		expect(myAccount.checkoutPageLoaded()).toBe(true);
		done();		
	});
	
	it('enter CC details and load the final instruction page on payment', function(done) {
		myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
		myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
		expect(activate.finalInstructionPageLoadedPurchase()).toBe(true);
		done();		
	}); 
	
	it('load the summary page', function(done) {
		activate.finalInstructionProceedPurchase();
		expect(activate.summaryPageLoaded()).toBe(true);
		done();		
	});
	
	
	it('Proceed from Summary page', function(done) {
		activate.clickOnSummaryBtn();
		expect(activate.surveyPageLoaded()).toBe(true);
		done();		
	});
	
});
	