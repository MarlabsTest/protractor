'use strict';
var ElementsUtil = require("../util/element.util");

/*
	This represents modal popup used to add a device
	Methods
		1. addDeviceWithEsn - 
		2.
		3.
*/
var AddDevice = function() {

	this.esnTextbox= element(by.css("input[id='adddevice-esnsimmin']"));	
	this.addDeviceBtn = element.all(by.css("button[id='btn_submit']")).get(1);   
	this.successPopupClose = element(by.css('[ng-click="cancel()"]'));
	//this.activateBtn = element.all(by.css("button[id='btn_activate']")).get(3);
	this.activateBtn = element.all(by.css("button[id='btn_activate']")).get(1);
	this.enterSim = element(by.css("input[id='sim']"));
	this.simContinue = element.all(by.css("button[id='btn_continuesimcardnumber']")).get(0); 
	this.enterZip = element.all(by.css("input[id='zip']")).get(0);
	this.zipContinue = element.all(by.css("button[id='btn_continuezipcode']")).get(0);
	this.enterPin = element.all(by.css("input[id='sim']")).get(0);
	this.pinContinue = element.all(by.css("button[id='btn_continueserviceplanpin']")).get(0);
	this.activationContinue = element.all(by.css("button[id='btn_continuesetupphone']")).get(1);
	this.activationDone = element.all(by.css("button[id='btn_done']")).get(1);  //100000088729420
	this.survey = element.all(by.buttonText('NO, THANKS')).get(1);
	
	//addDeviceWithMin
	
	this.addDevice = function(esn) {

		ElementsUtil.waitForElement(this.esnTextbox);
		this.esnTextbox.sendKeys(esn);
		ElementsUtil.waitForElement(this.addDeviceBtn);
		this.addDeviceBtn.click();
		ElementsUtil.waitForElement(this.successPopupClose);
		this.successPopupClose.click();
	};

	this.clickActivateAddedDevice = function() {
		ElementsUtil.waitForElement(this.activateBtn);
		this.activateBtn.click();		
	};
	
	this.newDeviceActivation = function(simval,pinval,zip) {
		//$$//ElementsUtil.waitForElement(this.enterSim);
		this.enterSim.clear().sendKeys(simval);
		this.simContinue.click();
		//$$//ElementsUtil.waitForElement(this.enterZip);
		this.enterZip.clear().sendKeys(zip);
		this.zipContinue.click();
		//$$//ElementsUtil.waitForElement(this.enterPin);
		this.enterPin.clear().sendKeys(pinval);
		this.pinContinue.click();
		//$$//ElementsUtil.waitForElement(this.activationContinue);
		this.activationContinue.click();
		//$$//ElementsUtil.waitForElement(this.activationDone);
		this.activationDone.click();
		//$$//ElementsUtil.waitForElement(this.survey);
		this.survey.click();
	};
	
	this.isDeviceAdded = function(esn) {
			browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			browser.get(url);
			});
			var devicePannel = element.all(by.css("div[class='headerbar tf-headerbar-primary tf-headerbar-xs']"));
			return devicePannel.count().then(function (num) {
			console.log("count======================",num);
			return num;
    });
	};
};

module.exports = new AddDevice;