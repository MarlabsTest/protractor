'use strict';
var ElementsUtil = require("../util/element.util");

var InActiveDevices = function() {
	//this.editBtn = element.all(by.css('.headerbar.tf-headerbar-primary.tf-headerbar-xs .text-xs-center.text-lg-left')).get(0);
	//this.editBtn = element.all(by.css('[ng-transclude="edit"]')).get(0);
	//this.editBtn = element(by.xpath('/html/body/tf-update-lang/div[1]/div[2]/div[2]/div/div/div[2]/div/div[1]/tf-page/div/div/div/div[2]/div/tf-page-body/div/div[3]/ng-include/div[2]/div[1]/table/tbody/tr/td[2]'));
	this.editBtn = element.all(by.css('[ng-click="editAction()"]')).get(0);
	//this.removeDeviceBtn = element(by.css('a[title=REMOVE DEVICE]'));
	//this.removeDeviceBtn = element.all(by.css('[ng-click="editAction()"]')).get(0);
	//this.removeDeviceBtn = element(by.xpath('/html/body/tf-update-lang/div[1]/div[2]/div[2]/div/div/div[2]/div/div[1]/tf-page/div/div/div/div[2]/div/tf-page-body/div/div[3]/ng-include/div[2]/div[1]/div/tf-nickname-device/div/tf-nickname-device-class/div/div[2]/h3/a'));
	this.removeDeviceBtn = element.all(by.css('[ng-click="removeDevice()"]')).get(0);
	this.removeDeviceBtnArr = element.all(by.css('[ng-click="removeDevice()"]'));
	this.removeDeviceConfirmPopup = element(by.css('.removedevice.ng-scope'));
	this.removeDeviceConfirmYesBtn = element.all(by.id('btn_yes')).get(1);
	this.removeDeviceSuccessMsg = element(by.css('.removedevice.ng-scope'));
	this.removeDeviceSuccessMsgCloseBtn = element(by.css('.btn-modal.modal-close'));

	
	this.removeEsn = function() {
		//$$//ElementsUtil.waitForElement(this.editBtn);
		this.editBtn.click();
		this.removeDeviceBtn.click();
		//$$//ElementsUtil.waitForElement(this.removeDeviceConfirmPopup);
		this.removeDeviceConfirmYesBtn.click();
	}
	
	this.isEsnRemoved = function() {
		//$$//ElementsUtil.waitForElement(this.removeDeviceSuccessMsg);
		return this.removeDeviceSuccessMsg.isPresent();
	}
	
	this.closeEsnRemovalSuccessPopup = function() {
		this.removeDeviceSuccessMsgCloseBtn.click();
	}
};

module.exports = new InActiveDevices;