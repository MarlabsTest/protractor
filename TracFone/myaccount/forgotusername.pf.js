'use strict';
var ElementsUtil = require("../util/element.util");

var resetUserName = function(){

	//this.forgotUserName = element.all(by.id('lnk_forgot_username')).get(1);
	this.forgotUserName = element.all(by.linkText('FORGOT USERNAME?')).get(0);	
	this.number = element.all(by.id('entermin.min')).get(1);
	this.continuebtn = element.all(by.css("button[id='btn_continue']")).get(0);
	this.pinNumber = element.all(by.id('secpin-security_pin')).get(1);
	this.submit = element(by.id('btn_submit'));
	this.popUp =  element(by.css('[ng-click="closePopup()"]'));

	//for loading home page
	this.isHomePageLoaded = function(){
		return this.forgotUserName.isPresent();
	};

	//forgot username link is clicked,so it navigates to the new page
	this.continueWithForgotUsername = function(){
		 this.forgotUserName.click();
	};
	
	//enter the sim number and click on the continue button to proceed 
	this.gotToRetrievePage= function(sim) {
		console.log('gotoretrievepage');
		this.number.sendKeys(sim);
		this.continuebtn.click();
	};

	//After the forgot username link is clicked, the new page is loaded
	this.retrieveUsernamePageLoaded = function(){
//		return browser.getCurrentUrl().then(function(url) {
//			console.log('url: ', url);
//			return /forgotusername$/.test(url);
//		});
		return ElementsUtil.waitForUrlToChangeTo(/forgotusername$/);
	};
	
	//enter the pin number and proceed by clicking the submit button
	this.goToPinPage = function(pin) {
		//console.log('go to pin page');
		this.pinNumber.sendKeys(pin);
		this.submit.click();
	};
	
	//checks wheather the submit button is present in the new page
	this.securityPinPageLoaded = function() {
		//console.log('security pin page');
		return this.submit.isPresent();
	};
	
	//after giving the details pop up window will be shown
	this.popUpWindow = function(){
		//console.log('pop up window');
	    ElementsUtil.waitForElement(this.popUp);
		return this.popUp.isPresent();
	};

};
module.exports = new resetUserName;
