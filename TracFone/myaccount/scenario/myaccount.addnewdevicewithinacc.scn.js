'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Add new device within account', function() {

	var activationData = activationUtil.getTestData();
	drive(activationData, function(inputActivationData) {
		sessionData.tracfone.noOfLines = inputActivationData.noOfLines;//set outside "it" to get outside "it"
		console.log('Inside scn sessionData.tracfone.noOfLines======',sessionData.tracfone.noOfLines);
		
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_ADD_NEW_DEVICE');
		}).result.data = inputActivationData;
	});
});