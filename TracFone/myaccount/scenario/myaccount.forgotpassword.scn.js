'use strict';
//forgot password scenario, the link will be sent to the given mail for resetting the password
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Tracfone Forgot Password', function() {
	
	var activationData = activationUtil.getTestData();
	console.log('activationData:', activationData);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_FORGOT_PASSWORD');
		}).result.data = inputActivationData;
	});
		
});
	
