'use strict';
var expectedConditions = protractor.ExpectedConditions; 
var ElementsUtil = require("../util/element.util");
var generator = require('creditcard-generator');
var CCUtil = require("../util/creditCard.utils");
var CommonUtil= require("../util/common.functions.util");
var sessionData = require("../common/sessiondata.do");

//===================Enter Card Details For Manage Payment in MyAccount=====================

var managePayment = function() {
		
	//this.editBtn = element(by.css("a[title='MYAC_99744']")); 
	// this.PymntBtn = element(by.linkText('NEW PAYMENT METHOD'));
	this.PymntBtn = element(by.css("span[translate='MYAC_99833']")); 
	this.enterCardInfo = element(by.css("input[id='formName.number_mask']"));
	//this.enterNickName = element(by.css("input[id='nickname']"));
	this.month = element.all(by.model('$select.search')).get(0); 
	this.year =  element.all(by.model('$select.search')).get(1);
	this.enterMonth = element(by.xpath("//div[@id='exp_month']/div[2]/div/div/div[8]/div/div/p"));
	this.enterYear =  element(by.xpath("//div[@id='exp_year']/div[2]/div/div/div[4]/div/div/p")); 
	this.enterFirstName = element(by.css("input[id='fname']"));
	this.enterLastName = element(by.css("input[id='lname']"));
	this.enterAddress = element(by.css("input[id='address1']"));
	this.enterCity = element(by.css("input[id='city']"));
	this.state = element.all(by.model('$select.search')).get(2);
	this.enterState = element(by.xpath("//div[@id='state']/div[2]/div/div/div[6]/div/div/p"));
	this.enterCountry = element(by.model('billingInfo.country')).$('[label="USA"]');
	this.enterZip = element(by.css("input[id='zipcode']"));
	this.addCreditCardBtn = element.all(by.css("button[id='btn_addcreaditcard']")).get(0);
	// this.isPaymentAddedPanel  = element.all(by.css("div[class='panel-default ng-scope ng-isolate-scope panel']"));
		
	this.deleteCard = element.all(by.css('[ng-click="action()"]')).get(2);
	this.editPymntBtn = element.all(by.css('[ng-click="editAction()"]')).get(0);
	
	this.goToEditPaymentInfo = function() {
		//$$//browser.wait(expectedConditions.visibilityOf(this.PymntBtn),40000);
		this.PymntBtn.click();
		this.enterCardInfo.clear().sendKeys(""+generator.GenCC(sessionData.tracfone.cardType));
		//this.enterNickName.clear().sendKeys('test');
		this.month.click();
		this.enterMonth.click();
		this.year.click();
		this.enterYear.click();
		this.enterFirstName.clear().sendKeys(CCUtil.firstName);
		this.enterLastName.clear().sendKeys(CCUtil.lastName);
		this.enterAddress.clear().sendKeys(CCUtil.addOne);	
		this.enterCity.clear().sendKeys(CCUtil.city);		
		this.state.click();
		this.enterState.click();
		// this.enterCountry.click();
		this.enterZip.clear().sendKeys(CCUtil.pin);	
		this.addCreditCardBtn.click();

	};
	
	//---------------Check if Payment Added--------------
	this.isPaymentAdded = function() {
		expect(this.isLoaded()).toBe(true);
		var paymentAddedPanel = element.all(by.css("div[class='panel-default ng-scope ng-isolate-scope panel']"));
		return paymentAddedPanel.count().then(function (num) {
        console.log("count======================",num);
		return num;
    });
	};
	
	//--------------Check if manage Payment page loaded----------------------
	this.isLoaded = function() {	
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /paymentmethod$/.test(url); 
		});
	};
	
	// here it deletes the credit card details which is already saved 
	this.deleteCardDetails = function(){
		//$$//ElementsUtil.waitForElement(this.editPymntBtn);
		this.editPymntBtn.click();
		//ElementsUtil.waitForElement(this.deleteCard);
		this.deleteCard.click();
	};
	this.paymentPageIsLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/paymentmethod$/);		
	};
	
};

module.exports = new managePayment;
