'use strict';

var myAccount = require("./myaccount.pf");

var manageProfile = require("./manageprofile.pf");
var managePayment = require("./managepayment.pf");
var addDevice = require("./adddevice.pf");
var optionsQuickLinks = require("./optionquicklink.pf");
var addservicepin = require("./addservicepin.pf");
var serviceRenewal = require("./checkout.pf");
var hppcheckout = require("./hppcheckout.pf");
var cardPayment = require("../common/creditcardpayment.pf");
// var cardPayment = require("../common/paymentcheckout.pf");
var activeDevices = require("./activedevices.pf");
var addDeviceModal = require("./adddevicemodal.pf");
var resetUser = require("./forgotusername.pf");
var resetPassword = require("./forgotpassword.pf");

var MyAccount = function() {
	
	this.AutopayCheckout = function() {
		return myAccount.goToMultilineAutopay();
	};
	
	this.dashboard = function() {
		return myAccount.dashboard();
	};
	
	this.clickAddAirtime = function() {
		return myAccount.clickAddAirtime();
	};
		
	this.payService = function(){
		myAccount.payService();
	};
	
	this.enroll= function(){
		myAccount.enroll();
	};
	
	//Method to click on Bank Account tab in Payment - for reactivation with ACH paurchase scenario
	this.clickOnBankAccTab = function(){
		serviceRenewal.clickOnBankAccTab();
	};
	
	//method to enter ACH payment details: for reactivation with ACH purchase scenario
	this.enterAchDetails = function(accno,routingno){
		serviceRenewal.enterAchDetails(accno,routingno);
	};	
		
	this.checkout = function() {
		return cardPayment.goToPayCheckout();
	};
	
	this.isSuccessfulCheckout = function() {
		return cardPayment.goToSuccessfulCheckout();
	};
	
	this.clickContinue = function() {
		return myAccount.clickContinue();
	};
	
	this.allOptions = function() {
		return myAccount.goToAllOptions();
	};
	
	this.chooseBuyPlan = function() {
		return myAccount.chooseBuyPlan();
	};
	
	this.isbuyPlanPageLoaded = function() {
		return myAccount.isBuyPlanLoaded();
	};
	
	this.selectAPlan = function() {
		return myAccount.selectAPlan();
	};
	
	this.myDevices = function() {
		return myAccount.goToMyDevices();
	};
	
	this.manageProfile = function() {
		return myAccount.goToManageProfile();
	};
	
	this.paymentMethod = function() {
		return myAccount.goToPaymentMethod();
	};
	
	this.paymentHistory = function() {
		return myAccount.goToPaymentHistory();
	};
	
	this.addDevice = function() {
		return myAccount.goToAddDevice();
	};
	
	this.isAddDeviceModalShown = function() {
		return addDeviceModal.isAddDeviceModalShown();
	}
	
	this.addInActiveEsn = function(esn) {
		addDeviceModal.addInActiveEsn(esn);
	};
	
	this.isInActivceEsnAdded = function() {
		return addDeviceModal.isAddDeviceSuccessModalShown();
	}
	
	this.closeAddDeviceSuccessModal = function() {
		addDeviceModal.closeAddDeviceSuccessModal();
	}
	
	this.signOut = function() {
		return myAccount.goToSignOut();
	};
  	
	this.isManageProfileLoaded = function() {
	return manageProfile.isLoaded();
	};
	
	this.isManagePaymentLoaded = function() {
	return managePayment.isLoaded();
	};
	
	this.editContactInfo = function() {
		manageProfile.goToEditContactInfo();
	};
	
	this.isPaymentAdded = function() {
		return managePayment.isPaymentAdded();
	};
	
	this.isContactEdited = function() {
	return manageProfile.isContactEdited();
	};
	
	this.editPaymentDetails = function() {
		return managePayment.goToEditPaymentInfo();
	};
	
	this.addNewDevice = function(esn) {
		addDevice.addDevice(esn);
	};
	
	this.clickActivateAddedDevice = function() {
		addDevice.clickActivateAddedDevice();
	};
	
	this.newDeviceActivation = function(simval,pinval,zip) {
		addDevice.newDeviceActivation(simval,pinval,zip);
	};
	
	this.removeDevice = function() {
		myAccount.removeDevice();
	};
	
	this.isDeviceRemoved = function() {
		return myAccount.isDeviceRemoved();
	};
	
	this.refresh = function() {
		myAccount.refresh();
	};
	this.isNewDeviceAdded = function(esn) {
		addDevice.isDeviceAdded(esn);
	};
	
	//** method checks whether the account dashboard is loaded or not
	this.isLoaded = function(){
		return myAccount.isLoaded();
	};
	this.isDeviceAdded = function(){
		return addDevice.isDeviceAdded();
	};
	
	//** method is to click on the All Options link
	this.clickOnAllOptions = function(){
		optionsQuickLinks.clickOnAllOptions();
	};
	
	//** method is to proceed with the add service plan flow
	this.clickOnservicePlanLink = function(){
		addservicepin.clickOnservicePlanLink();
	};
	
	//** method checks whether the renew service plan page is loaded or not
	this.renewServicePageLoaded = function(){
		return addservicepin.renewServicePageLoaded();
	}
  
	//** method to enter new pin 
	this.enterAirtimePinRenew = function(pin){
		return addservicepin.enterAirtimePin(pin);
	};
  
	//** method to check the redemption confirmation page is loaded after entering the new pin
	this.redemptionSuccessPage = function(){	
		return addservicepin.redemptionSuccessPage(); 
	};
  
	//** method to proceed from the confirmation page
	this.clickOnContinueBtnFromConfirmation = function(){
		addservicepin.clickOnContinueBtnFromConfirmation();
	};
	
	//** method to proceed with the pay service flow
	this.payService = function(){
		myAccount.payService();
	}
	
	//** method to check whether the checkout page is loaded or not
	this.checkoutPageLoaded = function(){
		return myAccount.checkoutPageLoaded();
	};
	
	//** method to click on the continue to payment button in the checkout page
	this.continueToPayment = function(){
		serviceRenewal.continueToPayment();
	};
	
	//** method to check whether the payment form for entering the cc and billing details are displayed or not
	this.paymentOptionLoaded = function(){
		return hppcheckout.paymentOptionLoaded();
	};
	
	
	this.clickNewPayment = function(){
		serviceRenewal.clickNewPayment();
	};

	this.enterContactDetails = function (fname, lname, address1, houseNum, city, zipcode, email) {
		hppcheckout.enterContactDetails(fname, lname, address1, houseNum, city, zipcode, email);
	};

	//** method to enter cc details
	this.enterCcDetails = function (ccNum, cvv) {
		serviceRenewal.enterCcDetails(ccNum, cvv);
	};

	//** method to enter billing details
	this.enterBillingDetails = function (fname, lname, address1, houseNum, city, zipcode) {
		serviceRenewal.enterBillingDetails(fname, lname, address1, houseNum, city, zipcode);
	};
	
	this.goToPaymentTab = function () {
		serviceRenewal.goToPaymentTab();
	};

	//** method to enter ach billing details
	this.enterAchBillingDetails = function (fname, lname, address1, houseNum, city, zipcode) {
		serviceRenewal.enterAchBillingDetails(fname, lname, address1, houseNum, city, zipcode);
	};
	
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function () {
		return serviceRenewal.confirmationPageLoaded();
	};

	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.autoRefillConfirmationPageLoaded = function () {
		return serviceRenewal.autoRefillConfirmationPageLoaded();
	};

	//** method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function () {
		serviceRenewal.proceedFromConfirmationPage();
	};
	//** method to proceed from check out in Auto Refill option
	this.autoRefilProceedFromConfirmation = function () {
		serviceRenewal.autoRefilProceedFromConfirmation();
	}

	//** method to click on the activate button, after adding a device through the dashboard
	this.clickOnActivate = function () {
		myAccount.clickOnActivate();
	};
	
	//** method to proceed from the survey page
	this.clickOnSurvey = function(){
		serviceRenewal.clickOnSurvey();
	};

	//** method to check whether  a popup is dispalyed after clicking the pay service button in the account dashboard
	this.newPopupLoaded = function(){		
		return myAccount.newPopupLoaded();
	};
	
	//** method to proceed from the popup page
	this.continueFromPopUp = function(){
		return myAccount.continueFromPopUp();
	};
	
	//** method to click on the Add Airtime Pin button in dashboard
	this.clickOnAddAirtimePin = function(){
		myAccount.clickOnAddAirtimePin();
	};
	//** method to click check whether the page to enter the airtime pin is loaded or not
	this.enterAirtimePinPageLoaded = function(){
		return addservicepin.enterAirtimePinPageLoaded();
	};
	//** method to enter Airtime Pin 
	this.enterAirtimePin = function(pin){
		addservicepin.enterAirtimePin(pin);
	};
	//** method to proceed from Airtime pin page
	this.proceedFromAirTimePage = function(){
		addservicepin.proceedFromAirTimePage();
	};
	//** method to check whether the redemption confirmation page is loaded or not
	this.redemptionConfirmationPageLoaded = function(){
		return addservicepin.redemptionConfirmationPageLoaded();
	};
	//** method to continue from the confirmation page
	this.clickOnContinueFromConfirmation = function(){
		addservicepin.clickOnContinueFromConfirmation();
	};
	
	//** method to click on the Activate button in dashboard under inactive devices -- Reactivation scenario
	this.clickOnActivateBtn = function(){
		myAccount.clickOnActivateBtn();
	};
	
	this.removeEsn = function() {
		activeDevices.removeEsn();
	}
	
	this.isEsnRemoved = function() {
		return activeDevices.isEsnRemoved();
	}
	
	this.closeEsnRemovalSuccessPopup = function() {
		return activeDevices.closeEsnRemovalSuccessPopup();
	}
	
	//***********checks wheather the forgot username homepage loaded***********
	this.resetUserIsLoaded = function() {
		return resetUser.isHomePageLoaded();
	};
	
	this.continueWithForgotUsername = function(){
		return resetUser.continueWithForgotUsername();
	};
	
	// asks us to enter the sim number
	this.gotToPage= function(sim) {
		return resetUser.gotToRetrievePage(sim);
	};
	
	
	this.retrievePageLoaded=function(){
		return resetUser.retrieveUsernamePageLoaded();
	};
	
	//asks us to enter the pin number to reset the username.
	this.goToPinPage = function(pin) {
		return resetUser.goToPinPage(pin);
	}
	
	this.securityPinPageLoaded = function() {		
		return resetUser.securityPinPageLoaded();
	};
	
	//pop up window with log in button comes for navigating to the homepage
	this.popUpWindowForResetUser = function(){
		return resetUser.popUpWindow();
	};
	
	//checks for the forgot password homepage load.
	this.isHomePageLoaded = function() {
		return resetPassword.isHomePageLoaded();	
	};
	
	this.clickOnForgotPassword = function(){
		return resetPassword.clickOnForgotPassword();
	};
	
	//navigates to the new page where asks us to enter the email.
	this.retrievePasswordPage = function(){
		return resetPassword.retrievePasswordPage();
	};
	
	// asks us to enter the email to retrieve the password
	this.enterTheEmail = function(userId){
		return resetPassword.enterTheEmail(userId);
	};
	
	//the message to reset password is sent to your provided email is shown in the popup window.
	this.popUpWindow = function(){
		return resetPassword.popUpWindow();
	};
	
	this.PopUpWindowClick = function(){
		return resetPassword.PopUpWindowClick();
	}
	this.resetThePassword = function(){
		return resetPassword.resetThePassword();
	};
	
	//method to delete the saved card details
	this.deleteCardDetails = function(){
		return managePayment.deleteCardDetails();
	};
	//checks whether the payment page is loaded or not
	this.paymentPageIsLoaded = function(){
		return managePayment.paymentPageIsLoaded();
	};
	
	this.clickOnBuyServicePlan = function(){
		myAccount.clickOnBuyServicePlan();
	};
	
};


module.exports = new MyAccount;
