//this represents a modal for add service plan option

'use strict';
var ElementsUtil = require("../util/element.util");

var AddServicePin = function() {

	this.proceedFromCOnfirmationPage = element.all(by.id('btn_done')).get(1);	
	this.airtimePinContnueBtn = element.all(by.css('button[id="btn_addairtime"]')).get(2);
	this.airtimePinTextBox = element(by.css("input[id='formPlan.pin']"));
	
	//** method to click check whether the page to enter the airtime pin is loaded or not
	this.enterAirtimePinPageLoaded = function(){
		return this.airtimePinContnueBtn.isPresent();
	};
	//** method to enter Airtime Pin 
	this.enterAirtimePin = function(pin){
		this.airtimePinTextBox.sendKeys(pin);
	};
	//** method to proceed from Airtime pin page
	this.proceedFromAirTimePage = function(){
		this.airtimePinContnueBtn.click();
	};
	//** method to check whether the redemption confirmation page is loaded or not
	this.redemptionConfirmationPageLoaded = function(){
		return ElementsUtil.waitForUrlToChangeTo(/redemptionconfirmation$/);
	};
	
	//** method to proceed from the confirmation page
	this.clickOnContinueFromConfirmation = function(){
		//$$//ElementsUtil.waitForElement(this.proceedFromCOnfirmationPage);
		this.proceedFromCOnfirmationPage.click();
	};

};

module.exports = new AddServicePin;