//this represents a modal for the checkout page

'use strict';
var ElementsUtil = require("../util/element.util");

var ServiceRenewChkout = function() {
	this.paymentTab = element(by.xpath("//span[@translate='BAT_24249']"));
	//this.termsCheckbox = element(by.css("span[ng-bind-html='termscontent']"));
	this.termsCheckbox = element(by.id("terms"));
	this.contactCheckbox = element(by.css("label[for='addinfo_profileadd']"));
	
	this.proceedFromConfirmationAutoRefil = element.all(by.id('btn_done')).get(1);
	this.proceedFromConfirmation = element.all(by.id('done')).get(1);
	this.surveyContyinue = element.all(by.css('[ng-click="action()"]')).get(1);
	this.chkoutContinue = element.all(by.id('btn_continuetopayment')).get(1);
	//this.btnContinueSetupPhone	= element.all(by.id('btn_continuesetupphone')).get(3);
	//this.btnPurchaseDone	= element.all(by.id('btn_done')).get(1);
	this.cvvText = element.all(by.name('cvv')).get(1);
	//remya
	//this.creditTabOption	= element.all(by.css('[ng-click="selectTab(1)"]')).get(0);
	//remya
	this.newPaymentForm = element(by.css('[ng-click="selectTab(2)"]'));
	this.ccnumber = element.all(by.name('formName.number_mask')).get(1);
	this.nickName = element.all(by.id('nickname')).get(1);
	//button for 'continue to Payment' in reactivation with purchase scenario
	//this.btncontinuetopayment = element.all(by.id('btn_continuetopayment')).get(2);
	
	//this.monthDropDown = element.all(by.className('selectize-input focus')).get(0); //ui-select-match ng-scope
	this.monthDropDown = element.all(by.className('selectize-input')).get(2); 
	this.monthDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(6);
	
	this.yearDropDown = element.all(by.className('selectize-input')).get(3);	
	this.yearDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(2);
	
	this.fname = element.all(by.name('fname')).get(1);
	this.lname = element.all(by.name('lname')).get(1);
	this.address = element.all(by.name('address1')).get(1);
	this.houseNo = element.all(by.name('address2')).get(1);
	this.city = element.all(by.id('city')).get(1);
	this.zipcode = element.all(by.id('zipcode')).get(1);
	
	this.cfname = element.all(by.name('addinfo_fname')).get(1);
	this.clname = element.all(by.name('addinfo_lname')).get(1);
	this.caddress = element.all(by.name('addinfo_address1')).get(1);
	this.chouseNo = element.all(by.name('addinfo_address2')).get(1);
	this.ccity = element.all(by.id('addinfo_city')).get(1);
	this.czipcode = element.all(by.id('addinfo_zipcode')).get(1);
	this.cstate = element.all(by.className('selectize-input')).get(0);	
	this.cstateDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	this.state = element.all(by.className('selectize-input')).get(4);	
	this.stateDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	this.stateAch = element.all(by.className('selectize-input')).get(1);	
	this.stateAchDropDownSelect = element.all(by.className('ui-select-choices-row ng-scope')).get(4);
	
	this.country = element.all(by.model('$select.search'));
	
	//this.placeOrderBtn  = element.all(by.id('btn_placeyourorder')).get(0); //PlaceMyorder
//	this.placeOrderBtn  = element.all(by.css('[ng-click="action()"]')).get(2);
	this.placeOrderBtn =  element.all(by.css("span[translate='BAT_24015']")).get(0); 
	
	this.tabBankAccount	= element(by.linkText('Bank Account'));
	
	this.selectAccType		= element.all(by.className('selectize-input')).get(0);
	this.selectAccTypeOpt	= element.all(by.className('dropdown-content-item ng-scope')).get(1);
	this.txtAccNo			= element.all(by.name('formName.number_mask')).get(1); 
	this.txtRoutingNo		= element.all(by.name('routingNumber')).get(1);
	this.previousEmail      = element.all(by.id('email')).get(1);
	
	//** method to click on the continue to payment button in the checkout page
	this.continueToPayment = function() {
		//$$//ElementsUtil.waitForElement(this.chkoutContinue);
		this.chkoutContinue.click();
	};
	
	//Method to click on Bank Account tab in Payment - for reactivation with ACH paurchase scenario
	this.clickOnBankAccTab = function(){
		//$$//ElementsUtil.waitForElement(this.tabBankAccount);
		this.tabBankAccount.click();
	};
	
	this.goToPaymentTab = function(){
		this.paymentTab.click();
	};	
	
	// method to enter ACH details for reactivation with purchase scenario
	this.enterAchDetails = function(accno,routingno){
		this.selectAccType.click();
		this.selectAccTypeOpt.click();
		this.txtAccNo.clear().sendKeys(accno);
		this.txtRoutingNo.clear().sendKeys(routingno);
    };
	
	//** method to check whether the payment form for entering the cc and billing details are displayed or not
	this.paymentOptionLoaded = function() {
		//$$//ElementsUtil.waitForElement(this.ccnumber);
		return this.ccnumber.isPresent();
	};
	
	
	this.clickNewPayment = function(){
		//$$//ElementsUtil.waitForElement(this.newPaymentForm);
		this.newPaymentForm.click();
	};
	
	//** method to enter cc details
	this.enterCcDetails = function(ccNum,cvv){
		this.ccnumber.clear().sendKeys(ccNum);
		//this.nickName.clear().sendKeys('Test');
		this.cvvText.clear().sendKeys(cvv);
		this.monthDropDown.click();
		this.monthDropDownSelect.click();
		this.yearDropDown.click();
		this.yearDropDownSelect.click();
		
		
		
    };
   /* //Method to enter bill details for reactivation
    this.enterReactivateBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		console.log('billing info');
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		
		this.state.click();//clear().sendKeys("CA");//click();
		this.stateDropDownSelect.click();
			
		this.country.click();
		this.country.clear().sendKeys("USA");
        this.city.clear().sendKeys(city);
        this.zipcode.clear().sendKeys(zipcode);	
		//ElementsUtil.elementHasCome(this.placeOrderBtn, 1);
		this.placeOrderBtn.click();
	};*/

    //**method to enter contact details hpp
    this.enterContactDetails = function(fname,lname,address1,houseNum,city,zipcode,email){
		console.log('contact info');
//		this.contactCheckbox.click();
		this.cfname.clear().sendKeys(fname);
		this.clname.clear().sendKeys(lname);
		this.caddress.clear().sendKeys(address1);
		this.ccity.clear().sendKeys(city);
		this.cstate.click();//clear().sendKeys("CA");//click();
		this.cstateDropDownSelect.click();
		this.czipcode.clear().sendKeys(zipcode);	
		this.previousEmail.clear().sendKeys(email);
	};
    
	//** method to enter billing details
	this.enterBillingDetails = function(fname,lname,address1,houseNum,city,zipcode){
		console.log('billing info');
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		this.zipcode.sendKeys(zipcode);	
		
		this.city.sendKeys(city);
		this.state.click();//clear().sendKeys("CA");//click();
		this.stateDropDownSelect.click();
		
		browser.executeScript("arguments[0].click();", this.termsCheckbox.getWebElement());
		browser.executeScript("arguments[0].click();", this.placeOrderBtn.getWebElement());
		
	};
	
	
	//** method to enter ach billing details
	this.enterAchBillingDetails = function(fname, lname, address1, houseNum, city, zipcode){
		this.fname.clear().sendKeys(fname);
		this.lname.clear().sendKeys(lname);
		this.address.clear().sendKeys(address1);
		this.stateAch.click();
		this.stateAchDropDownSelect.click();
		this.city.clear().sendKeys(city);
		this.zipcode.clear().sendKeys(zipcode);	
		this.termsCheckbox.click();
		this.placeOrderBtn.click();
	};
	
	//** method to proceed from the survey page
	this.clickOnSurvey = function(){
		//$$//ElementsUtil.waitForElement(this.surveyContyinue);
        this.surveyContyinue.click();		
	};
	
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function(){	
		return ElementsUtil.waitForUrlToChangeTo(/confirmation_activation$/);		
	};		
	//** method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function(){
		//$$//ElementsUtil.waitForElement(this.proceedFromConfirmation);
		this.proceedFromConfirmation.click();
	}; 
	
	//** For auto refill - method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.autoRefillConfirmationPageLoaded = function(){	
		return ElementsUtil.waitForUrlToChangeTo(/purchasesummary$/);		
	};	
	
	this.autoRefilProceedFromConfirmation = function(){
		this.proceedFromConfirmationAutoRefil.click();
	}
	/*this.clickOnDoneBtn = function(){
		ElementsUtil.waitForElement(this.btnPurchaseDone);
		this.btnPurchaseDone.click();
	};*/
	/*//remya
	this.isChkoutPageLoaded = function(){
		ElementsUtil.waitForElement(this.btncontinuetopayment);
		return this.btncontinuetopayment.isPresent();
	
	};*/
	/*this.clickCreditTab = function(){
		ElementsUtil.waitForElement(this.creditTabOption);
		this.creditTabOption.click();
	}*/
	//remya
};

module.exports = new ServiceRenewChkout;