'use strict';
var ElementsUtil = require("../util/element.util");

var AddDeviceModal = function() {
	this.addDeviceModal = element(by.css('.modal-dialog.modal-md'));//need alternate selection method
	this.modalEsnInput = element.all(by.id('adddevice-esnsimmin')).get(1);
	this.addDeviceModalBtn = element.all(by.id('btn_submit')).get(1);
	this.addDeviceSuccessModal = element(by.css('.modal-dialog.modal-md'));//need alternate selection method
	//this.addDeviceSuccessModalCloseBtn = element(by.buttonText('X'));
	this.addDeviceSuccessModalCloseBtn = element(by.css('[ng-click="cancel()"]'));
	
	//add other menus here as we proceed
    
	this.isAddDeviceModalShown = function() {
		return this.addDeviceModal.isPresent();
	};
	
	this.addInActiveEsn = function(esn) {
		this.modalEsnInput.clear().sendKeys(esn);
		this.addDeviceModalBtn.click();
	};
	
	this.isAddDeviceSuccessModalShown = function() {
		return this.addDeviceSuccessModal.isPresent();
	}
	
	this.closeAddDeviceSuccessModal = function() {
		//$$//ElementsUtil.waitForElement(this.addDeviceSuccessModalCloseBtn);
		this.addDeviceSuccessModalCloseBtn.click();
	}
};

module.exports = new AddDeviceModal;