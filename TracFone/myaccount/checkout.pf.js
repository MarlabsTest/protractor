//this represents a modal for the checkout page

'use strict';
var ElementsUtil = require("../util/element.util");

var ServiceRenewChkout = function() {
	this.paymentTab = element(by.xpath("//span[@translate='BAT_24249']"));
	//this.termsCheckbox = element(by.xpath("//div[@class='checkbox']"));
	this.termsCheckbox = element(by.id("terms"));
	this.proceedFromConfirmationAutoRefil = element.all(by.id('btn_done')).get(1);
	this.proceedFromConfirmation = element.all(by.id('done')).get(1);
	this.surveyContyinue = element.all(by.css('[ng-click="action()"]')).get(1);
	this.chkoutContinue = element.all(by.id('btn_continuetopayment')).get(1);
	//this.btnContinueSetupPhone	= element.all(by.id('btn_continuesetupphone')).get(3);
	//this.btnPurchaseDone	= element.all(by.id('btn_done')).get(1);
	this.cvvText = element(by.css('input[id="cvv"]'));
	//remya
	//this.creditTabOption	= element.all(by.css('[ng-click="selectTab(1)"]')).get(0);
	//remya
	this.newPaymentForm = element(by.css('[ng-click="selectTab(2)"]'));
	this.ccnumber = element(by.css('input[id="formName.number_mask"]'));
	this.nickName = element.all(by.id('nickname')).get(1);
	//button for 'continue to Payment' in reactivation with purchase scenario
	//this.btncontinuetopayment = element.all(by.id('btn_continuetopayment')).get(2);
	
	//this.monthDropDown = element.all(by.className('selectize-input focus')).get(0); //ui-select-match ng-scope
	this.monthDropDown = element.all(by.className('selectize-input')).get(0); 
	this.monthDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(6);
	
	this.yearDropDown = element.all(by.className('selectize-input')).get(1);	
	this.yearDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(2);
	             
	this.fname = element(by.css('input[id="fname"]'));
	this.lname = element(by.css('input[id="lname"]'));
	this.address = element(by.css('input[id="address1"]'));
	this.houseNo = element(by.css('input[id="address2"]'));
	this.city = element(by.css('input[id="city"]'));
	this.zipcode = element(by.css('input[id="zipcode"]'));
	
	this.state = element.all(by.className('selectize-input')).get(2);	
	this.stateDropDownSelect = element.all(by.className('dropdown-content-item ng-scope')).get(4);
	
	this.stateAch = element.all(by.className('selectize-input')).get(1);	
	this.stateAchDropDownSelect = element.all(by.className('ui-select-choices-row ng-scope')).get(4);
	
	this.country = element.all(by.model('$select.search')).get(1);
	
	//this.placeOrderBtn  = element.all(by.id('btn_placeyourorder')).get(0); //PlaceMyorder
	this.placeOrderBtn =  element.all(by.css("span[translate='BAT_24015']")).get(0); 
	
	this.tabBankAccount	= element(by.linkText('Bank Account'));
	
	this.selectAccType		= element.all(by.className('selectize-input')).get(0);
	this.selectAccTypeOpt	= element.all(by.className('dropdown-content-item ng-scope')).get(1);
	this.txtAccNo			= element.all(by.name('formName.number_mask')).get(1); 
	this.txtRoutingNo		= element.all(by.name('routingNumber')).get(1);
	
	//** method to click on the continue to payment button in the checkout page
	this.continueToPayment = function() {
		//$$//ElementsUtil.waitForElement(this.chkoutContinue);
		this.chkoutContinue.click();
	};
	
	//Method to click on Bank Account tab in Payment - for reactivation with ACH paurchase scenario
	this.clickOnBankAccTab = function() {
		//$$//ElementsUtil.waitForElement(this.tabBankAccount);
		this.tabBankAccount.click();
	};
	
	this.goToPaymentTab = function() {
		this.paymentTab.click();
	};	
	
	// method to enter ACH details for reactivation with purchase scenario
	this.enterAchDetails = function(accno,routingno) {
		this.selectAccType.click();
		this.selectAccTypeOpt.click();
		this.txtAccNo.clear().sendKeys(accno);
		this.txtRoutingNo.clear().sendKeys(routingno);
    };
	
	//** method to check whether the payment form for entering the cc and billing details are displayed or not
	this.paymentOptionLoaded = function() {
		//$$//ElementsUtil.waitForElement(this.ccnumber);
		return this.ccnumber.isPresent();
	};
		
	this.clickNewPayment = function() {
		//$$//ElementsUtil.waitForElement(this.newPaymentForm);
		this.newPaymentForm.click();
	};
	
	//** method to enter cc details
	this.enterCcDetails = function(ccNum,cvv) {
		this.ccnumber.clear().sendKeys(ccNum);
		//this.nickName.clear().sendKeys('Test');
		this.cvvText.clear().sendKeys(cvv);
		this.monthDropDown.click();
		this.monthDropDownSelect.click();
		this.yearDropDown.click();
		this.yearDropDownSelect.click();		
    };
   
	//** method to enter billing details
	this.enterBillingDetails = function(fname1,lname1,address1,houseNum1,city1,zipcode1) {
		console.log('billing info');
		this.fname.sendKeys(fname1);
		this.lname.sendKeys(lname1);
		this.address.sendKeys(address1);
		console.log('city',city1);
		
		this.city.sendKeys(city1);
		this.state.click();
		this.stateDropDownSelect.click();
		this.zipcode.sendKeys(zipcode1);	
		browser.executeScript("arguments[0].click();", this.termsCheckbox.getWebElement());
		browser.executeScript("arguments[0].click();", this.placeOrderBtn.getWebElement());		
	};	
	
	//** method to enter ach billing details
	this.enterAchBillingDetails = function(fname, lname, address1, houseNum, city, zipcode) {
		this.fname.sendKeys(fname);
		this.lname.sendKeys(lname);
		this.stateAch.click();
		this.stateAchDropDownSelect.click();
		this.zipcode.sendKeys(zipcode);
		this.address.sendKeys(address1);
		
		this.city.sendKeys(city);		
		browser.executeScript("arguments[0].click();", this.termsCheckbox.getWebElement());
		browser.executeScript("arguments[0].click();", this.placeOrderBtn.getWebElement());		
	};
	
	//** method to proceed from the survey page
	this.clickOnSurvey = function() {
		//$$//ElementsUtil.waitForElement(this.surveyContyinue);
        this.surveyContyinue.click();		
	};
	
	//** method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.confirmationPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/confirmation_activation$/);		
	};

	//** method to proceed from the confirmation page
	this.proceedFromConfirmationPage = function() {
		//$$//ElementsUtil.waitForElement(this.proceedFromConfirmation);
		this.proceedFromConfirmation.click();
	};
	
	//** For auto refill - method to check whether the confirmation page is loaded after clicking the place order button in the checkout page
	this.autoRefillConfirmationPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/purchasesummary$/);		
	};	
	
	this.autoRefilProceedFromConfirmation = function() {
		this.proceedFromConfirmationAutoRefil.click();
	}
	/*this.clickOnDoneBtn = function(){
		ElementsUtil.waitForElement(this.btnPurchaseDone);
		this.btnPurchaseDone.click();
	};*/
	/*//remya
	this.isChkoutPageLoaded = function(){
		ElementsUtil.waitForElement(this.btncontinuetopayment);
		return this.btncontinuetopayment.isPresent();
	
	};*/
	/*this.clickCreditTab = function(){
		ElementsUtil.waitForElement(this.creditTabOption);
		this.creditTabOption.click();
	}*/
	//remya
};

module.exports = new ServiceRenewChkout;