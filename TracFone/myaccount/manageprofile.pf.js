'use strict';
//var waitutil = require("../util/waitutil.pf");
var ElementsUtil = require("../util/element.util");

//=============To Edit your Account Profile============

var manageProfile = function() {

//----------------Enter Details--------------------------
	
	this.editContactInfo = element.all(by.css("a[title='EDIT']")).get(0);
	this.enterFirstName = element.all(by.id('contact_form-first_name')).get(1);
	this.enterLastName = element.all(by.id('contact_form-last_name')).get(1);
	// this.enterDOB = element.all(by.id('contact_form-DOB'))get(1);
	this.addressLine1 = element.all(by.id('contact_form-address1')).get(1);
	this.addressLine2 = element.all(by.id('contact_form-address2')).get(1);
	this.enterPhoneNum = element.all(by.id('contact_form-phone')).get(1);
	this.saveButton = element.all(by.css('[ng-click="action()"]')).get(0);
	this.confirmPassword = element.all(by.id('pswpopup-password')).get(1);
//	this.confirmSave = element.all(by.css('[ng-click="action()"]')).get(1);
	//this.confirmSave = element.all(by.id('btn_save')).get(1);
	this.confirmSave = element.all(by.id('btn_save')).get(1);
	
	this.editedFirstName = element.all(by.xpath("//div[@class='col-xs-12 col-sm-6 col-md-6 col-lg-6']/p")).get(1);
	this.goToEditContactInfo = function() {
		this.editContactInfo.click();
		this.enterFirstName.clear().sendKeys('test');
		this.enterLastName.clear().sendKeys('lastname');  // 1
		this.addressLine1.clear().sendKeys('Address line one'); 
		this.addressLine2.clear().sendKeys('Address line two');
		this.enterPhoneNum.clear().sendKeys(9098765432);		// 1
		this.saveButton.click();   //contact_form-phone 1
		this.confirmPassword.click();
		this.confirmPassword.sendKeys('tracfone');
		//this.confirmSave.click();
		browser.executeScript("arguments[0].click();", this.confirmSave.getWebElement());
	};
	
	//---------------Check if profile edited--------------
	this.isContactEdited = function() {		
		var editDone = element.all(by.css("div[class='col-xs-12 col-sm-6 col-md-6 col-lg-6']")).get(1).element(by.tagName('p'));
		ElementsUtil.waitForElement(editDone);
		return editDone.getText().then(function (text) { 
			//console.log("EditedName========================",text);
			return text;
			}); 
	};
	
	//----------------Check if MyProfile page loaded----------
	this.isLoaded = function() {	
		return browser.getCurrentUrl().then(function(url) {
			console.log('url: ', url);
			return /myprofile$/.test(url);
		});
	};
	
};

module.exports = new manageProfile;