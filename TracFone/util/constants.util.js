'use strict';

var Constants = {
	ENV : {sit1:'https://sit1.tracfone.com/',test:'https://test.tracfone.com/',sitb:'https://sitbng.tracfone.com/',sitc:'https://sitci.tracfone.com/',sitz:'https://sitz.tracfone.com/',sitg:'https://sitgng.tracfone.com/',sitf:'https://sitfng.tracfone.com/',site:'https://siteng.tracfone.com/'},
	LOGIN_CSV_FILE: './testdata/login.td.csv',
	LOGIN_JSON_FILE: './testdata/login.td.json',
	SHOP_PLANS_CSV_FILE: './testdata/shopplans.td.csv',
	SHOP_PLANS_JSON_FILE: './testdata/shopplans.td.json',
	ACTVE_PIN_CSV_FILE: './testdata/activatepin.td.csv',
	ACTVE_PIN_JSON_FILE: './testdata/activatepin.td.json',
	ACTVE_PIN_HPP_CSV_FILE: './testdata/activatepin.hpp.td.csv',
	ACTVE_PIN_HPP_JSON_FILE: './testdata/activatepin.hpp.td.json',
	BYOP_ACTVE_PIN_CSV_FILE: './testdata/activatebyoppin.td.csv',
	BYOP_ACTVE_PIN_JSON_FILE: './testdata/activatebyoppin.td.json',
	REFILL_CSV_FILE: './testdata/refill.td.csv',
	REFILL_JSON_FILE: './testdata/refill.td.json',	
	UTIL_JAR_PATH: './javautils/DataGen.jar',
	ALL: 'All',
	PHONE_UPGRADE_CSV_FILE: './testdata/phoneupgrade.td.csv',
	PHONE_UPGRADE_JSON_FILE: './testdata/phoneupgrade.td.json',
	ACTVE_CDMA_PIN_CSV_FILE: './testdata/activatecdmapin.td.csv',
	ACTVE_CDMA_PIN_JSON_FILE: './testdata/activatecdmapin.td.json',
	SHOP_PLANS:['smartphone','payasyougo','addon'],
    SMARTPHONE_PLANS : {125:'1',50:'5',45:'9',35:'13',25:'17',20:'21',15:'25'},
    SPAYASGO_PLANS : {199:'1',159:'3',99:'5',79:'7',39:'9',29:'11',19:'13',9:'15'},
    ADDON_PLANS : {10:'1','10ILD':'3',5:'5'}

};

module.exports = Constants;