module.exports = {

	requireSuite : function(suite) {
		delete require.cache[require.resolve(suite)]
		return require(suite);
	}
};