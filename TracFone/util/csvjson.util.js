'use strict';

var CsvJsonUtil = {
	
	convertCsvToJson: function(source, target) {	
		
		var Conv = require('csvtojson').Converter;
		var converter = new Conv({constructResult:false, toArrayString:true});
		require("fs").createReadStream(source)
			.pipe(converter)
			.pipe(require("fs").createWriteStream(target));
	},
	
	getJsonData: function(jsonFile) {
		return JSON.parse(require('fs').readFileSync(jsonFile, 'utf8'));
	}	
};

module.exports = CsvJsonUtil;