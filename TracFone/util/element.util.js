'use strict';
var expectedConditions = protractor.ExpectedConditions;

var ElementUtil = {
	
	hasCome : function(finder, cnt) {
		var hasCome = function() {
			return finder.count().then(function(count) {
				console.log('Count:', count);
				return count >= cnt;
			});
		};
		return expectedConditions.and(hasCome);
	},
	
	elementHasCome : function(finder, cnt){
		browser.wait(this.hasCome(finder, cnt), 20000);
	},
	
	waitForElement : function(element,timeout){
		browser.wait(expectedConditions.visibilityOf(element),timeout);
	},
	
	waitForElement : function(element) {
		browser.wait(expectedConditions.visibilityOf(element),40000);
	},
	
	waitForUrlToChangeTo : function(urlRegex) {
    var currentUrl;
    console.log('waitForUrlToChangeTo');
    return browser.getCurrentUrl().then(function storeCurrentUrl(url) {
            currentUrl = url;
			
			console.log('currentUrl******* '+currentUrl);
        }
    ).then(function waitForUrlToChangeTo() {
            return browser.wait(function waitForUrlToChangeTo() {
                return browser.getCurrentUrl().then(function compareCurrentUrl(url) {
				console.log('urlRegex******* '+urlRegex+'  ********   '+url);
                    return urlRegex.test(url);
                });
            });
        }
    );
}
	
};
module.exports = ElementUtil;