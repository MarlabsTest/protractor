'use strict';

var loginUtil = require('./login.util');
var activationUtil = require('./activation.util');
var activationCdmaUtil = require('./cdmaactivation.util');
var phoneupgradeUtil = require('./phoneupgrade.util');
var byopActivationUtil = require('./byop.activation.util');
var shopplansUtil = require('./shopplans.util');
var hppActivationUtil = require('./hpp.activation.util');
var refillUtil = require('./refill.util');

var TestDataGenerator = {
	
	generateTestData: function() {
		hppActivationUtil.prepareActivationData();
		loginUtil.prepareLoginData();
		activationUtil.prepareActivationData();
		phoneupgradeUtil.prepareActivationData();
		byopActivationUtil.prepareActivationData();
		activationCdmaUtil.prepareCdmaActivationData();
		shopplansUtil.prepareShopplansData();
		refillUtil.prepareRefillData();
	}
};

module.exports = TestDataGenerator;