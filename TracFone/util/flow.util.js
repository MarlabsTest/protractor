'use strict';

var runUtil = require('./run.util');
const path = require('path');

var FLOWS = {
		//TF_ACTIVATION_WITHPIN_HPP:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.with.pin.hpp.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_REFILL_WITHPIN_HPP:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.with.pin.hpp.spec.js','common/spec/myaccount.logout.spec.js','refill/spec/refill.HPP.plan.purchase.spec.js'],
		TF_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.withsurvey.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_CDMA_ACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.cdma.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_CDMA_ACTIVATION_WITHPIN_WITHOUTACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.cdma.withpin.withoutaccount.spec.js'],
		TF_BYOP_ACTIVATION:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.byop.phone.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_BYOP_ACTIVATION_WITH_PURCHASE:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.byop.phone.withpurchase.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_ADD_INACTIVE_ESN:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.add.inactive.esn.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_ACTIVATION_GSM_WITH_INVALID_PIN:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activate.with.invalid.pin.spec.js'],
		TF_GSM_ACTIVATION_WITH_AUTOREFILL: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js', 'autorefill/spec/activation.with.autorefill.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_MANAGE_PAYMENT:['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_EDIT_CONTACT_INFO:['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.editcontactinfo.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_BUY_SERVICE_PLAN_WITHIN_ACC:['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.buyserviceplanwithinaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_ADD_NEW_DEVICE:['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.addnewdevicewithinacc.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_REMOVE_DEVICE:['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.addnewdevicewithinacc.spec.js','myaccount/spec/myaccount.removedevicewithinacc.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_GSM_ACTIVATION_WITH_AUTOREFILL: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js', 'autorefill/spec/activation.with.autorefill.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_REFILL_WITHPIN_WITHOUTACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','refill/spec/refill.payservice.outside.account.spec.js'],
		TF_REFILL_WITHPLAN_WITHOUTACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','refill/spec/refill.refillnewservice.spec.js'],
	    TF_CDMA_ACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.cdma.withpurchase.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_ACTIVATION_PORTING_WITHPIN_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.porting.withpin.withaccount.js','common/spec/myaccount.logout.spec.js'],
		TF_ACTIVATION_PORTING_WITHPURCHASE_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.porting.withpurchase.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_ACTIVATION_WITHPIN_WITHOUTACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withoutaccount.spec.js'],
		TF_ACTIVATION_WITHPURCHASE_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpurchase.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_ACTIVATION_WITHPURCHASE_WITHACCOUNT_HPP: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpurchase.withaccount.hpp.spec.js'],
		TF_ACTIVATION_WITHPURCHASE_ACH_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpurchase.ach.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_REACTIVATION_WITHPIN_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','activation/spec/reactivate.phone.with.pin.spec','common/spec/myaccount.logout.spec.js'],
		TF_FORGOT_PASSWORD : ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.forgotpassword.spec.js'],
		TF_FORGOT_USERNAME :['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.forgotusername.spec.js'],
		TF_DELETE_MANAGE_PAYMENT : ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.managepayment.spec.js','myaccount/spec/myaccount.delete.managepayment.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_SHOP_SELECT_PHONES : ['common/spec/tracfone.loadhomepage.spec.js','shop/spec/shop.selectphones.spec.js'],
		TF_LOGIN: ['common/spec/tracfone.loadhomepage.spec.js', 'activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','myaccount/spec/myaccount.login.spec.js'],
		TF_UPGRADE_FLOWS: ['activation/spec/activation.upgrade.flows.spec.js'],
		TF_PHONE_UPGRADE: ['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.phoneupgrade.spec.js'],
		TF_BYOP_UPGRADE: ['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js','activation/spec/activation.byop.upgrade.spec.js'],
		TF_SHOP_BUY_PLANS:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','shop/spec/shop.buypalns.fortracfonecustomer.spec.js'],
		TF_ACTIVATION_INTERNALPORTING_WITHPIN_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.internalportin.withpin.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_ACTIVATION_INTERNALPORTING_WITHPURCHASE_WITHACCOUNT: ['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.internalportin.withpurchase.withaccount.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_CDMA_BYOP_PIN_ACTIVATION:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.byop.cdma.pin.phone.spec.js','common/spec/myaccount.logout.spec.js'],
		TF_REACTIVATION_WITH_PURCHASE_ENROLLMENT:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','activation/spec/reactivate.phone.with.purchase.with.enrollment.spec.js'],		
		TF_BUY_ILD_ENROLLMENT_INSIDE_ACCOUNT:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js','myaccount/spec/myaccount.buyILD.withenrollment.WithinAccount.spec.js'],
		TF_ILD_DE_ENROLLMENT_INSIDE_ACCOUNT:['common/spec/tracfone.loadhomepage.spec.js','activation/spec/activation.withpin.withaccount.spec.js',
		'myaccount/spec/myaccount.buyILD.withenrollment.WithinAccount.spec.js','myaccount/spec/myaccount.ILD.deenrollment.WithinAccount.spec.js']
		
};
	
var FlowUtil = {
	
	run: function(flow) {		
		FLOWS[flow].forEach(function (spec) {
			//console.log("running spec..",path.join(protractor.basePath, spec));
			runUtil.requireSuite(path.join(protractor.basePath, spec));
		});
	}
};

module.exports = FlowUtil;
