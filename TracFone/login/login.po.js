'use strict';

var login = require("./login.pf");

var Login = function() {

	this.resetPassword = function() {
		login.resetPassword();
	};
	
	this.isPasswordReset = function() {
		return login.isPasswordReset();
	};

	this.validateLogin = function(mailOrNumber) {
		login.validateLogin(mailOrNumber);
	};
	
	this.validatePassword = function(password) {
		login.validatePasssword(password);
	};
	
	this.validatePassswordForMin = function(password) {
		login.validatePassswordForMin(password);
	};
	
	this.isValidLogin = function() {
		return login.isValidLogin();
	};  
	
	this.isSignOut = function() {
		return login.isSignOut();
	}; 
	
	this.provideCredentials = function(userName, password) {
		login.provideUserLoginInfo(userName, password);
	};
	//*********** if we need to be login again, after logging out*****************
	//method to enter the username and password 
	this.provideUserLoginCredentials = function(userId,password){
		login.provideUserLoginInfo(userId,password)
	};
	//method to navigate to the dashboard
	this.isValidPassword = function() {
		return login.isValidPassword();
	};
	
};

module.exports = new Login;