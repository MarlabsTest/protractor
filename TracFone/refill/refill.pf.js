'use strict';
var ElementsUtil = require("../util/element.util");

var Refill = function() {
	
	//this.refillButton = element.all(by.id('AddAirtime')).get(0);
	this.refillButton = element.all(by.css('[ng-click="action()"]')).get(4);
    this.refillPinBox = element.all(by.id('form_redeem.pin')).get(1);
	this.refillMinBox = element.all(by.name('devicemin')).get(1);
	this.doneButton = element.all(by.id('btn_done')).get(1);
	this.payService = element(by.id('lnk_PayService'));
	this.phoneNo = element.all(by.id('formphone-min')).get(1);
	this.renewServiceBtn = element.all(by.id('btn_viewplans')).get(2);
	this.selectPlanBtn = element(by.xpath("/html/body/tf-update-lang/div[1]/div/div[2]/div/div/div[2]/div/div/div/div[4]/div[1]/tf-wfm-service-plan-card/div/div[2]/div[2]/tf-button-primary/span[2]/button/span"));	
	this.notThisTimeBtn = element(by.xpath('//*[@id="modal-body"]/div[2]/div/div[2]/wfm-button-primary[1]/span[2]/button/span'));	
	this.paymentBtn = element.all(by.className('tf-inner-icon icon-plus tf-icon-lg-1-4 tf-icon-sm-1-4 tf-icon-xs-1-4 tf-icon-top')).get(1);	
	this.ctnToCheckOutBtn = element(by.xpath('//*[@id="modal-body"]/div[2]/div/div/div[2]/tf-button-primary/span[2]/button/span'));	
	this.creditCardTextBox = element.all(by.id('formName.number_mask')).get(1);
	this.ccnumber = element.all(by.name('formName.number_mask')).get(1);
	this.ccvTextBox = element.all(by.id('cvv')).get(1);
	this.expMonthDropDown =element(by.xpath('//*[@id="exp_month"]'));
	this.expMonthOption = element(by.xpath('//*[@id="exp_month"]/option[8]'));
	this.expYearDropDown = element(by.id('exp_year'));
	this.expYearOption = element(by.xpath('//*[@id="exp_year"]/option[4]'));
	this.fNameTextBox = element.all(by.id('fname')).get(1);
	this.lNameTextBox = element.all(by.id('lname')).get(1);
	this.addr1TextBox = element.all(by.id('address1')).get(1);
	this.addr2TextBox = element.all(by.id('address2')).get(1);
	this.cityTextBox = element.all(by.id('city')).get(1);
	this.stateDropDown = element(by.xpath('//*[@id="state"]'));
	this.stateOption = element(by.xpath('//*[@id="state"]/option[11]'));
	this.zipCodeTextBox = element.all(by.id('zipcode')).get(1);
	this.countryDropDown = element(by.xpath('//*[@id="country"]'));
	this.countryOption = element(by.xpath('//*[@id="country"]/option[2]'));
	this.placeMyOrderBtn = element(by.xpath('/html/body/tf-update-lang/div[1]/div/div[2]/div/div/div[2]/div/div/div/div[1]/div[2]/div[2]/ng-include/div[2]/div/div/div/div[10]/div/div[2]/tf-button-primary/span[2]/button/span'));
	this.doneBtn = element(by.xpath('/html/body/tf-update-lang/div[1]/div/div[2]/div/div/div[2]/div/div/div[2]/div/div/div[3]/div[2]/tf-button-primary/span[2]/button/span'));
	this.isSplDayPopup = element(by.css('[ng-click="cancel()"]'));

	
	this.goToRefillPage = function() {
		return this.payService.click();
	};
	
	this.fillPaymentDetails = function(cardNo,cvv,fName,lName,addr1,addr2,city,zipcode) {
		this.creditCardTextBox.clear().sendKeys(cardNo);
		this.ccvTextBox.clear().sendKeys(cvv);
		this.expMonthDropDown.click();
		this.expMonthOption.click();
		this.expYearDropDown.click();
		this.expYearOption.click();
		this.fNameTextBox.clear().sendKeys(fName);
		this.lNameTextBox.clear().sendKeys(lName);
		this.addr1TextBox.clear().sendKeys(addr1);
		this.addr2TextBox.clear().sendKeys(addr2);
		this.cityTextBox.clear().sendKeys(city);
		this.stateOption.click();
		this.zipCodeTextBox.clear().sendKeys(zipcode);
		this.countryDropDown.click();
		this.countryOption.click();
		this.placeMyOrderBtn.click();
		
	};
	
	
	this.goToRefillPageSendDatas = function(phno) {
		
		console.log('phno: ', phno);
		//$$//ElementsUtil.waitForElement(this.phoneNo);
		this.phoneNo.sendKeys(phno);
		//$$//ElementsUtil.waitForElement(this.renewServiceBtn);
		return this.renewServiceBtn.click();
	};
	
	this.selectPlan = function() {
		this.selectPlanBtn.click();
	};
	
	this.selectPlanPopUp = function() {
		//$$//ElementsUtil.waitForElement(this.notThisTimeBtn);
		this.notThisTimeBtn.click();
	}
	
	this.selectPlanPopUpAddon = function() {
		this.ctnToCheckOutBtn.click();
	}
	
	this.paymentSection = function() {
		this.paymentBtn.click();
	}
	
	this.done = function() {
		this.doneBtn.click();
	}
	
	this.isSuccess = function() {
		return this.payService.isPresent();
	}
	
	//*** method to check whether the refill page loaded or not
	this.isRefillPageLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/collectminpinpromo$/);
	};
  
	this.isServiceplanPageLoaded = function() {	
				
		
		//$$//ElementsUtil.waitForElement(this.isSplDayPopup);
		/*this.isSplDayPopup.isPresent();
		 
		 if(this.isSplDayPopup){
			 this.isSplDayPopup.click();
		 }*/
		return ElementsUtil.waitForUrlToChangeTo(/serviceplan$/);
	};
	

	this.isPaymentSection = function() {	
		return this.creditCardTextBox.isPresent();
	}; 
	
  
	this.isCheckOutPageLoaded = function() {
		return ElementsUtil.waitForUrlToChangeTo(/checkout$/);	
	};
	
	this.isPaymentDone = function() {	
		//$$//ElementsUtil.waitForElement(this.doneBtn);
		return this.doneBtn.isPresent();
	};
	
	//*** method to  refill the service plan by entering min, pin and add
	this.refillServicePlan = function(min,pin) {
	    console.log('min: ', min);
		console.log('pin: ', pin);
		//$$//ElementsUtil.waitForElement(this.refillMinBox);
		this.refillMinBox.sendKeys(min);
		//$$//ElementsUtil.waitForElement(this.refillPinBox);
		this.refillPinBox.clear().sendKeys(pin);
		//$$//ElementsUtil.waitForElement(this.refillButton);
		return this.refillButton.click();
	};
	
	  //*** method to  check the refill with pin is successful or not
	this.isConfirmationPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/redemptionconfirmation$/);	
	};
	
	 //*** method to  check the refill of service plan is successful or not
	this.isServiceplanConfirmationPageLoaded = function() {	
		return ElementsUtil.waitForUrlToChangeTo(/isServiceplanConfirmationPageLoaded$/);	
	};
  
  //*** method to  click the done button once the refill is successful
	this.clickDoneButton = function() {
		//$$//ElementsUtil.waitForElement(this.doneButton);
		return this.doneButton.click();
	};
};

module.exports = new Refill;