'use strict';

/*var homePage = require("../../common/homepage.po");*/
var FlowUtil = require('../../util/flow.util');
var drive = require('jasmine-data-provider');
var hppActivationUtil = require('../../util/hpp.activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('Tracfone Refill Service with PIN outside Account', function() {

	var hppActivationData = hppActivationUtil.getTestData();
	console.log('activationData:', hppActivationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(hppActivationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			sessionData.tracfone.shopPlan = inputActivationData.shopplan;
			sessionData.tracfone.planName = inputActivationData.PlanName;
			sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
			sessionData.tracfone.cardType = inputActivationData.cardType;
			sessionData.tracfone.hppPlan = inputActivationData.HppPlan;
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber = inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber = inputActivationData.SIM;
				sessionData.tracfone.zip = inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber = inputActivationData.PIN;
				console.log('Copying activation', inputActivationData);
				done();
			});
			FlowUtil.run('TF_REFILL_WITHPIN_HPP');
		}).result.data = inputActivationData;
	});
});