'use strict';

var homePage = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var activation = require("../../activation/activation.po");
var refillPage = require("../refill.po");
var shop = require("../../shop/shop.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");



describe('Tracfone Refill with Serive plan outside Account', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = false;		
	});
	
	
	
	it('should navigate to Refill page', function(done) {
		console.log("should navigate to My Refill page");
		homePage.goToRefill();
		expect(refillPage.isRefillPageLoaded()).toBe(true);		
		done();
	});
	
	
	   it('Enter min and pin then click on add service plan button', function(done) {
		
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		console.log("minVal::: "+min);
		refillPage.refillPageSendDatas(min);
		expect(refillPage.isServiceplanPageLoaded()).toBe(true);
		done();
	   });
	 
		
	 it('should select PayAsYouGo plan and listed with available plans', function(done){
			//activation.selectPayAsYouGoPlan();
			 var planType		= sessionData.tracfone.shopPlan;
			 var planName		= sessionData.tracfone.planName;
			 var isAutoRefill	= sessionData.tracfone.autoRefill;
			 console.log("pType"+planType);
			 console.log("pName"+planName);
			 console.log("isAutorefill"+isAutoRefill);
		 	shop.choosePlanByName(planType,planName,isAutoRefill);
			expect(activation.activationAccountPageLoaded).toBe(true);
			done();		
		});
		
		it('should enter details of customer', function(done){
			activation.proceedWithExistingUser();	
			activation.enterUserDetails(sessionData.tracfone.username,sessionData.tracfone.password);
			expect(myAccount.checkoutPageLoaded()).toBe(true);
			done();
		});
		it('should navigate to payment page on click of continue to payment', function(done){
			myAccount.continueToPayment();
			expect(activation.paymentOptionLoaded()).toBe(true);
			done();
		});
		it('should fill the payment details and perform the payment', function(done){
			
			//activation.clickNewPayment();
			activation.enterCcDetails("372523281441007","1234");
			activation.enterBillingDetails("Test","Test","1295 Charleston Road"," ","Mountain View","94043");
			expect(refillPage.isConfirmationPageLoaded()).toBe(true);		
		done();
	});
	 
	
	it('click on Done button for Confirmation', function(done) {
		refillPage.clickDoneButton();        
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(loginData.tracfone.esn,'0','6','Tracfone_Purchase','true','false');
        console.log('ITQ_DQ_CHECK table updated');
		expect(homePage.isHomePageLoaded()).toBe(true);       
		done();
	});

});
	