'use strict';

var homePage = require("../../common/homepage.po");
var activation = require("../../activation/activation.po");
var refillPage = require("../refill.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");

describe('Tracfone Refill Service with PIN outside Account', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = false;		
	});
	
	
	it('should navigate to Refill page', function(done) {
		console.log("should navigate to My Refill page");
		homePage.goToRefill();
		expect(refillPage.isRefillPageLoaded()).toBe(true);		
		done();
	});
	
	
	 it('Enter min and pin then click on add service plan button', function(done) {
		
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
        console.log('minVal ,    ::'+min);
		var pinval = dataUtil.getPIN(sessionData.tracfone.redemptionPin);
       	console.log('pinVal', pinval);
		//refillPage.refillServicePlan(sessionData.tracfone.min,pinval); 
        refillPage.refillServicePlan(min,pinval);		
		expect(refillPage.isConfirmationPageLoaded()).toBe(true);		
		done();
	});
	
	 
	
	it('click on Done button for Confirmation', function(done) {
		refillPage.clickDoneButton();        
		//db call for inserting redemption record into itq_dq_check table
		dataUtil.checkRedemption(sessionData.tracfone.esn,'0','6','Tracfone_Purchase','true','false');
        console.log('ITQ_DQ_CHECK table updated');
		expect(homePage.isHomePageLoaded()).toBe(true);       
		done();
	});
	
	

});
	