'use strict';

var homePage = require("../../common/homepage.po");
var myAccount = require("../../myaccount/myaccount.po");
var activation = require("../../activation/activation.po");
var refillPage = require("../refill.po");
var shop = require("../../shop/shop.po");
var sessionData = require("../../common/sessiondata.do");
var dataUtil= require("../../util/datautils.util");
var CCUtil = require("../../util/creditCard.utils");
var generator = require('creditcard-generator');
var CommonUtil= require("../../util/common.functions.util");
var generatedEsn ={};
var generatedSim ={};
var generatedMin ={};


describe('Tracfone Refill with Serive plan outside Account', function() {
	
	beforeEach(function () {
		//browser.ignoreSynchronization = false;		
	});
	
	
	
	it('should navigate to Refill page', function(done) {
		console.log("should navigate to My Refill page");
		homePage.goToRefill();
		expect(refillPage.isRefillPageLoaded()).toBe(true);		
		done();
	});
	
	
	   it('Enter min and pin then click on add service plan button', function(done) {
		
		var min = dataUtil.getMinOfESN(sessionData.tracfone.esn);
		console.log("minVal::: "+min);
		refillPage.refillPageSendDatas(min);
		expect(refillPage.isServiceplanPageLoaded()).toBe(true);
		done();
	   });
	 
		
	 it('should select PayAsYouGo plan and listed with available plans', function(done){
			//activation.selectPayAsYouGoPlan();
			 var planType		= sessionData.tracfone.shopPlan;
			 var planName		= sessionData.tracfone.planName;
			 var isAutoRefill	= sessionData.tracfone.autoRefill;
			 console.log("planType::: "+planType);
			 console.log("planName::: "+planName);
			 console.log("isAutoRefill::: "+isAutoRefill);
		 	shop.choosePlanByName(planType,planName,isAutoRefill);
		 	expect(activation.selectyourprogramPageLoaded()).toBe(true);
			done();		
		});

	 it(' enter cc and billing address details,click on proceed and navigate to final indtructions page', function(done) {
			var hppPlan= sessionData.tracfone.hppPlan;
			var cardType =sessionData.tracfone.cardType;
			var isAutoRefill	= sessionData.tracfone.autoRefill;
			var email		= sessionData.tracfone.username;
			console.log('email',email);
			console.log('hpp plan',hppPlan);
			console.log('Add city',CCUtil.city);
			activation.chooseHppPlan(hppPlan);
			
			if(cardType == "ach" && (isAutoRefill == "Y" || isAutoRefill == "YES" )){
				myAccount.clickOnBankAccTab();
				myAccount.enterAchDetails("4102","121042882");
				myAccount.enterAchBillingDetails("Test","Test","1295 Charleston Road","12345", "Miami", "33178");
			}
			else{
			myAccount.enterContactDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin,email);	
			myAccount.enterCcDetails(""+generator.GenCC(sessionData.tracfone.cardType),CommonUtil.getCvv(sessionData.tracfone.cardType));
			myAccount.enterBillingDetails(CCUtil.firstName,CCUtil.lastName,CCUtil.addOne," ",CCUtil.city,CCUtil.pin);
			}
			expect(activation.hppFinalInstructionPageLoaded()).toBe(true);
			done();		
		});
	 
	 it(' click on the continue button  and  navigate to Succcess page', function(done) {
			activation.hppFinalInstructionProceedPurchase();
			expect(activation.surveyPageLoaded()).toBe(true);
			done();		
		});
	
		it(' click on the thank you button and navigate to the account dashboard ', function(done) {
			activation.clickOnThankYouBtn();
			expect(myAccount.isLoaded()).toBe(true);
			done();		
		}); 

});
	