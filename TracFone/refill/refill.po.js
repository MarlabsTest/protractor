'use strict';
var refillPf = require("./refill.pf");

var Refill = function() {

	
	this.refillPage = function() {
		return refillPf.goToRefillPage();
	};
	
	this.refillPageSendDatas = function(phno) {
		return refillPf.goToRefillPageSendDatas(phno);
	};
	
		
	//*** method to check the refill page loaded
	this.isRefillPageLoaded = function() {	
		return refillPf.isRefillPageLoaded();
	};
	
	 
	this.done = function() {	
		return refillPf.done();
	};
  
	this.isServiceplanPageLoaded = function() {	
		return refillPf.isServiceplanPageLoaded();
	};	
	
	this.clickSplDayPopUpoaded = function() {	
		return refillPf.clickSplDayPopUpoaded();
	};
	
	this.isSplDayPopUpLoaded = function() {	
		return refillPf.isSplDayPopUpLoaded();
	};
	
  
	this.isSuccess = function() {	
		return refillPf.isSuccess();
	};
  
    //*** method to refill the service plan
	this.refillServicePlan = function(min,pin) {
		refillPf.refillServicePlan(min,pin);
	};
	
    //*** method to check the refill of service plan is successfully done
	this.isConfirmationPageLoaded = function() {
		return refillPf.isConfirmationPageLoaded();
	};
	
	//*** method to check the refill of service plan is successfully done
	this.isServiceplanConfirmationPageLoaded = function() {
		return refillPf.isServiceplanConfirmationPageLoaded();
	};
	
     //*** method to  click the done button once the refill is successful
	this.clickDoneButton = function() {
		return refillPf.clickDoneButton();
	};
	
};

module.exports = new Refill;