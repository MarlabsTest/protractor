'use strict';

//var drive 				= require('jasmine-data-provider');
//var dataUtil			= require("../../util/datautils.util");
//var activationUtil 		= require('../../util/activation.util');
var sessionData 		= require("../../common/sessiondata.do");
var autoPayPo 			= require("../autopay.po");
var activationPo 		= require("../../activation/activation.po");
var myAccountPo			= require("../../myaccount/myaccount.po");

describe('Tracfone activate a device with autorefill plan', function() {

	//var activationData = activationUtil.getTestData();
	//console.log('activationData:', activationData);	
	
	/*beforeEach(function () {
		//browser.ignoreSynchronization = true;	
    });	
	*/
	//require("../../activation/spec/activation.withpin.withaccount.spec.js");
	
	//drive(activationData, function(inputActivationData) {
	
		it('should navigate to AirTime service plan page on clicking AutoRefill option', function(done) {	
//			autoPayPo.checkForContinueBtn();
//			console.log("Click on auto refill btn");
			autoPayPo.clickOnAutoRefillBtn();
			console.log("Click of auto refill btn done");
			autoPayPo.isPopUpPresent();
			expect(autoPayPo.isLoadedServicePlanPage()).toBe(true);
			done();
		});
		it('should click on Smart Phone Only Plans in service plan page and load the service plans under that', function(done) {		
			autoPayPo.selectSmartPhoneOnlyPlan();
			expect(autoPayPo.isLoadedSmartPhonePlanPage()).toBe(true);
			done();
		});
		it('should click on Enroll Now button and navigate to pop-up page', function(done) {		
			autoPayPo.clickOnEnrollNow();
			expect(activationPo.isChkoutPopUpLoaded()).toBe(true);
			done();
		});
		it('should navigate to checkout page after clicking checkout button', function(done){
			activationPo.clickOnCheckout();
			expect(myAccountPo.checkoutPageLoaded()).toBe(true);
			done();
		});
		it('should navigate to payment page on click of continue to payment', function(done){
			myAccountPo.continueToPayment();
			//activationPo.clickCreditTab();
			expect(activationPo.paymentOptionLoaded()).toBe(true);
			done();
		});
		it('should fill the payment details and perform the payment', function(done){
			
			//activation.clickNewPayment();
			myAccountPo.enterCcDetails("4929858612795311","123");
			myAccountPo.enterBillingDetails("Test","Test","1295 Charleston Road"," ","Mountain View","94043");
			expect(myAccountPo.autoRefillConfirmationPageLoaded()).toBe(true);
			done();
		});
		it('should click on the button in the confirmation page and will be navigated to the account dashboard page', function(done) {
			myAccountPo.autoRefilProceedFromConfirmation();
			myAccountPo.clickOnSurvey();	
			expect(myAccountPo.isLoaded()).toBe(true);
			done();		
		});
	
	
	//}); 
	
	
});