'use strict';

var FlowUtil 			= require('../../util/flow.util');
var drive 				= require('jasmine-data-provider');
//var activationCdmaUtil = require('../../util/cdmaactivation.util');
var activationUtil = require('../../util/activation.util');
var sessionData = require("../../common/sessiondata.do");

describe('TF GSM Activation with Autorefill option', function() {

	var activationData = activationUtil.getTestData();
	console.log('GSM ActivationData:', activationData);
	console.log('protractor.basePath ', protractor.basePath);
	drive(activationData, function(inputActivationData) {
		describe('Drive Spec', function() {
			it('Copying activation test data to session', function(done) {
				sessionData.tracfone.esnPartNumber 	= inputActivationData.PartNumber;
				sessionData.tracfone.simPartNumber 	= inputActivationData.SIM;
				sessionData.tracfone.zip 			= inputActivationData.ZipCode;
				sessionData.tracfone.pinPartNumber 	= inputActivationData.PIN;
				sessionData.tracfone.autoRefill = inputActivationData.AutoRefill;
				console.log('Copied activation data:', inputActivationData);
				done();
			});
			FlowUtil.run('TF_GSM_ACTIVATION_WITH_AUTOREFILL');
		}).result.data = inputActivationData;
	});
});