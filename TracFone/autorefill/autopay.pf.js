'use strict';
var ElementsUtil = require("../util/element.util");
	/*
	This PF will Do Enrolment of user for Auto Pay
	*/

var AutoPay = function() {
	
	this.btnAutoRefill 						= element.all(by.id('btn_autorefill')).get(1);
	this.btnPopupCancel						= element.all(By.css("[ng-click='cancel()']")).get(0);
//	this.selectSmartPhonePlanOpt			= element.all(By.css("[ng-click='iconClick()']")).get(2);
	this.selectSmartPhonePlanOpt  			= element(by.id('accordiongroup-793-831-tab'));
	this.btnplancardenrollnow				= element.all(by.id('btn_paygplancardenrollnow')).get(1);
	this.btnContinue						= element.all(by.id('btn_')).get(1);
	
	
	this.checkForContinueBtn = function(){
		//$$//ElementsUtil.waitForElement(this.btnContinue);
		if(this.btnContinue.isPresent()){
			this.btnContinue.click();			
		}			
	};
	this.clickOnAutoRefillBtn = function(){
		ElementsUtil.waitForElement(this.btnAutoRefill);
		console.log('click on the auto refill');
		this.btnAutoRefill.click();
	};
	this.isPopUpPresent = function(){
		//$$//ElementsUtil.waitForElement(this.btnPopupCancel);
		if(this.btnPopupCancel.isPresent()){
			this.btnPopupCancel.click();			
		}		
	};
	/*this.isLoadedServicePlanPage = function(){
		return ElementsUtil.waitForUrlToChangeTo(/enrolled$/);	
	};*/
	this.isLoadedServicePlanPage = function(){
		return browser.getCurrentUrl().then(function(url){
			console.log('isLoadedServicePlanPage url:-',url);
			return /enrolled$/.test(url);
		});
	};
	this.selectSmartPhoneOnlyPlan = function(){
		ElementsUtil.waitForElement(this.selectSmartPhonePlanOpt);
		this.selectSmartPhonePlanOpt.click();
	};
	this.isLoadedSmartPhonePlanPage = function(){
		//$$//ElementsUtil.waitForElement(this.btnplancardenrollnow);
		return this.btnplancardenrollnow.isPresent();	
	};
	this.clickOnEnrollNow = function(){
		//$$//ElementsUtil.waitForElement(this.btnplancardenrollnow);
		this.btnplancardenrollnow.click();
	};
	
};

module.exports = new AutoPay;