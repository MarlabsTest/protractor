'use strict';

/*
This PO will Do Enrolment of user for Auto Pay
*/
var autoPayPf 			= require("./autopay.pf");
var AutoPayPO = function() {
	
	//this method checks whether a continue button present in account dashboard.
	this.checkForContinueBtn = function() {
		autoPayPf.checkForContinueBtn();
	};	
	 //This function will click on AutoRefill button in dashboard
	this.clickOnAutoRefillBtn = function() {
		autoPayPf.clickOnAutoRefillBtn();
	};
	//This method checks whether any pop-up displayed on clicking the AutoRefill button(For special occasions,the brand offers special plans.Eg.Mother's day plan)
	this.isPopUpPresent = function() {
		autoPayPf.isPopUpPresent();
	}; 
	//Method checks whether the serviceplan page loaded or not
	this.isLoadedServicePlanPage = function() {
		return autoPayPf.isLoadedServicePlanPage();
	}; 
	//method select SmartPhone-only plan from the page 
	this.selectSmartPhoneOnlyPlan = function() {
		autoPayPf.selectSmartPhoneOnlyPlan();
	};
	//Method checks whether Smart Phone plans are loaded or not
	this.isLoadedSmartPhonePlanPage = function() {
		return autoPayPf.isLoadedSmartPhonePlanPage();
	};
	//Method clicks the 'Enroll Now' Button
	this.clickOnEnrollNow = function() {
		autoPayPf.clickOnEnrollNow();
	};
	  
};

module.exports = new AutoPayPO;